﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FMS.BL.Utilities;

namespace FMS.BL.Buisness
{
   public class SaleBuisness : Common
    {
        public Status AddUpdateProductionStockEntryDetails(ProductionStockEntry productionStockEntry)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    para.Add("@PRODUCTIONSTOCKID", productionStockEntry.ProductionStockId);
                    para.Add("@ITEMTYPE ", productionStockEntry.ItemId);
                   
                    para.Add("@WEIGHT", productionStockEntry.Weight);
                    para.Add("@FLOUR", productionStockEntry.Flour);  
                    para.Add("@BRAN", productionStockEntry.Bran);
                    para.Add("@REF", productionStockEntry.Ref);
                    para.Add("@VITAMIN", productionStockEntry.Vitamin);

                    const string procName = "[dbo].[insertupdateProductionStockEntry]";
                    connection.Open();
                    return connection.Query<Status>(procName, para, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to insert or update Purchase Details", ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                return null;
            }

        }

        public List<ProductionStockEntry> GetSaleStockDetails()
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();

                    const string procName = "[dbo].[spGetSaleStockEntryDetails]";
                    connection.Open();
                    return connection.Query<ProductionStockEntry>(procName, para, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch purchase stock", ex, "GetPurchaseStockCount");
                return null;
            }

        }

        public List<FlourBranRefStock> GetSaleStockCount()
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();

                    const string procName = "[dbo].[spSaleStockCount]";
                    connection.Open();
                    return connection.Query<FlourBranRefStock>(procName, para, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch purchase stock", ex, "GetPurchaseStockCount");
                return null;
            }

        }

        public Status AddUpdateSaleDetails(Sales sales)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();


                    para.Add("@SalesID",sales.SalesID);
                    para.Add("@InvoiceNumber", sales.InvoiceNumber);
                    para.Add("@MemoNumber", sales.MemoNumber);
                    para.Add("@DispathDocNumber", sales.DispathDocNumber);
                    para.Add("@CustomerId", sales.CustomerId);
                    para.Add("@TruckNumber", sales.TruckNumber);
                    para.Add("@Destination", sales.Destination);
                    para.Add("@BatchNumber", sales.BatchNumber);
                    para.Add("@ItemId", sales.ItemId);
                    para.Add("@PacketId", sales.PacketId);
                    para.Add("@NoOfPacket", sales.NoOfPacket);
                    para.Add("@NoOfBag", sales.NoOfBag);
                    para.Add("@TotalWeight", sales.TotalWeight);
                    para.Add("@CreatedDate", sales.CreatedDate);
                    //para.Add("@TRANSACTIONMODE", sales.tra);
                    para.Add("@PAYMENTMODE", sales.PaymentMode);
                    para.Add("@BANKNAME", sales.BankName);
                    para.Add("@CHEQUENUMBER", sales.ChequeNumber);
                    para.Add("@DRAFTNUMBER", sales.DraftNumber);
                    para.Add("@TOTAL", sales.Total);
                    const string procName = "[dbo].[insertupdateSales]";
                    connection.Open();
                    return connection.Query<Status>(procName, para, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to insert or update sale Details", ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                return null;
            }



        }

        public List<Sales> GetSaleDetails()
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();

                    const string procName = "[dbo].[spSaleDetails]";
                    connection.Open();
                    return connection.Query<Sales>(procName, para, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch purchase stock", ex, "GetPurchaseStockCount");
                return null;
            }

        }
    }
}

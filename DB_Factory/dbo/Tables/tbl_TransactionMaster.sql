﻿CREATE TABLE [dbo].[tbl_TransactionMaster] (
    [TransactionID]   BIGINT          IDENTITY (1, 1) NOT NULL,
    [PaymentMode]     NVARCHAR (50)   NULL,
    [IFSC]            NCHAR (10)      NULL,
    [ChequeNo]        NCHAR (10)      NULL,
    [DraftNo]         NCHAR (10)      NULL,
    [BankName]        NCHAR (10)      NULL,
    [LedgerId]        NCHAR (10)      NULL,
    [Partyname]       NVARCHAR (100)  NULL,
    [Amount]          DECIMAL (18, 2) NULL,
    [TransactionDate] DATETIME        NULL,
    [CreatedBy]       BIGINT          NULL,
    [CreatedOn]       DATETIME        NULL,
    [UpdatedBy]       BIGINT          NULL,
    [UpdatedOn]       DATETIME        NULL,
    CONSTRAINT [PK_tbl_TransactionMaster] PRIMARY KEY CLUSTERED ([TransactionID] ASC)
);




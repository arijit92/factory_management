﻿CREATE TABLE [dbo].[tbl_Company] (
    [Id]                   BIGINT         IDENTITY (1, 1) NOT NULL,
    [CompanyName]          NVARCHAR (MAX) NOT NULL,
    [Address]              NVARCHAR (MAX) NULL,
    [LandMark]             NVARCHAR (MAX) NULL,
    [AboutUs]              NVARCHAR (MAX) NULL,
    [ContactUs]            NVARCHAR (MAX) NULL,
    [Mobile]               NVARCHAR (50)  NULL,
    [Email]                NVARCHAR (200) NULL,
    [GST]                  NVARCHAR (50)  NULL,
    [Registration Details] NVARCHAR (MAX) NULL,
    [VATNo]                NVARCHAR (50)  NULL,
    [CSTNo]                NVARCHAR (50)  NULL,
    [PAN]                  NVARCHAR (50)  NULL,
    [CreatedBy]            BIGINT         NULL,
    [CreatedOn]            DATETIME       NULL,
    [UpdatedBy]            BIGINT         NULL,
    [UpdatedOn]            DATETIME       NULL,
    [CompanyId]            BIGINT         NULL
);




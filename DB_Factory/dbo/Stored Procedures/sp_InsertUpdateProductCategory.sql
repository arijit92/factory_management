﻿CREATE PROCEDURE  [dbo].[sp_InsertUpdateProductCategory]
-- Add the parameters for the stored procedure here
@CategoryId [bigint]
,@CategoryName [nvarchar](100)
,@Description [nvarchar](max) NULL
,@IsActive [int] NULL
,@CreatedBy [bigint] NULL
,@CompanyId [bigint] NULL
AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
--update or insert checking
IF(@CategoryId>0)
BEGIN
--Duplicate value check
IF EXISTS(SELECT * FROM tbl_ProductCategory WITH(NOLOCK) WHERE CategoryName = @CategoryName AND CategoryId != @CategoryId)
BEGIN
SELECT 'Already exists.' as Msg,0 as SuccessFlag
END
ELSE
BEGIN
--update
UPDATE tbl_ProductCategory
SET
CategoryName =UPPER(@CategoryName)
,Description =@Description
,IsActive =@IsActive
,UpdatedBy =@CreatedBy
,UpdatedOn      =GETDATE()
,CompanyId =@CompanyId
WHERE CategoryId =@CategoryId
SELECT 'Category updated successfully.' as Msg,1 as SuccessFlag
END
END
ELSE
BEGIN
--Duplicate value check
IF EXISTS (SELECT * FROM tbl_ProductCategory WITH(NOLOCK) WHERE CategoryName = @CategoryName)
BEGIN
SELECT 'Already exists.' as Msg,0 as SuccessFlag
END
ELSE
BEGIN
--insert
INSERT INTO tbl_ProductCategory
(
CategoryName
,Description
,IsActive
,IsDeleted
,CreatedBy
,CreatedOn
,CompanyId
)
VALUES
(
UPPER(@CategoryName)
,@Description
,@IsActive
,0
,@CreatedBy
,GETDATE()
,@CompanyId
)
SELECT 'Category created successfully.' as Msg,1 as SuccessFlag
END
END
END TRY
BEGIN CATCH
SELECT 'Sorry something went wrong, unable to process,' as Msg,0 as SuccessFlag
RETURN
END CATCH
END

﻿CREATE TABLE [dbo].[tbl_User_Role_Module] (
    [Id]        BIGINT   IDENTITY (1, 1) NOT NULL,
    [ModuleId]  BIGINT   NOT NULL,
    [RoleId]    BIGINT   NOT NULL,
    [CreatedBy] BIGINT   NULL,
    [CreatedOn] DATETIME NULL,
    [UpdatedBy] BIGINT   NULL,
    [UpdatedOn] DATETIME NULL,
    [CompanyId] BIGINT   NULL
);


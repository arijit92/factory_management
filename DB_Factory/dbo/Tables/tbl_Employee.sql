﻿CREATE TABLE [dbo].[tbl_Employee] (
    [EmployeeId]                 BIGINT          IDENTITY (1, 1) NOT NULL,
    [FirstName]                  NVARCHAR (50)   NOT NULL,
    [LastName]                   NVARCHAR (50)   NOT NULL,
    [BranchId]                   BIGINT          NULL,
    [Mobile]                     NVARCHAR (50)   NULL,
    [Email]                      NVARCHAR (100)  NULL,
    [PAddress]                   NVARCHAR (MAX)  NULL,
    [IsCurrentSameWithPermanent] BIT             NULL,
    [CAddress]                   NVARCHAR (MAX)  NOT NULL,
    [EmployeeTypeId]             BIGINT          NOT NULL,
    [PAKA_SALARY]                NUMERIC (18, 2) NULL,
    [KACHA_SALARY]               NUMERIC (18, 2) NULL,
    [PF]                         NUMERIC (18, 2) NULL,
    [IsActive]                   INT             NULL,
    [IsDeleted]                  INT             NULL,
    [CreatedBy]                  BIGINT          NULL,
    [CreatedOn]                  DATETIME        NULL,
    [UpdatedBy]                  BIGINT          NULL,
    [UpdatedOn]                  DATETIME        NULL,
    [CompanyId]                  BIGINT          NULL
);


﻿CREATE PROCEDURE  [dbo].[sp_LoanStatus]
-- Add the parameters for the stored procedure here
 @EmployeeId [bigint] NULL
AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
Declare @TotalLoan decimal(18,2)
Declare @TotalPaid decimal(18,2)
Declare @DueAmount decimal(18,2)
SET @TotalLoan = ISNULL((SELECT SUM(Amount) from tbl_LoanManagement where EmployeeId = @EmployeeId),0)
SET @TotalPaid = ISNULL((SELECT SUM(Amount) from tbl_LoanRepayManagement where EmployeeId = @EmployeeId),0)
SET @DueAmount = @TotalLoan - @TotalPaid
IF(@DueAmount<0)
SET @DueAmount = 0
SELECT @TotalLoan as LaonAmount,@DueAmount as DueAmount

END TRY
BEGIN CATCH
SELECT 'Sorry something went wrong, unable to process,' as Msg,0 as SuccessFlag
RETURN
END CATCH
END
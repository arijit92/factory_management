﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FMS.BL.Utilities;
namespace FMS.BL.Buisness
{
   public  class GSTBuisness : Common
    {
        #region get all GSTs
        public Status AddUpdateGST(GST pmodel, long UserID)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    para.Add("@GSTId", pmodel.GSTId );
                    para.Add("@HSN", pmodel.HSN);
                    para.Add("@CGST", pmodel.CGST);
                    para.Add("@IGST", pmodel.IGST);
                    para.Add("@Percentage", pmodel.Percentage);
                    para.Add("@GSTDescription", pmodel.GSTDescription);
                    para.Add("@IsActive", Convert.ToInt32(pmodel.IsActive == false ? 0 : 1));
                    para.Add("@CreatedBy", UserID);
                    para.Add("@CompanyId", pmodel.CompanyId);
                    const string procName = "[dbo].[sp_InsertUpdateGST]";
                    connection.Open();
                    return connection.Query<Status>(procName, para, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to insert or update GST", ex, "AddUpdateGST");
                return null;
            }

        }
        public List<GST> GetGST(long? GSTId, long UserID)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    if (GSTId == 0)
                    {
                        GSTId = null;
                    }
                    para.Add("@GSTId", GSTId);
                    para.Add("@OperationId", 2);
                    para.Add("@UserId", UserID);
                    const string procName = "[dbo].[sp_DeleteSelectGST]";
                    connection.Open();
                    return connection.Query<GST>(procName, para, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch GST", ex, "GetGST");
                return null;
            }

        }
        public Status DeleteGST(long? GSTId, long UserID)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    para.Add("@GSTId", GSTId);
                    para.Add("@OperationId", 1);
                    para.Add("@UserId", UserID);
                    const string procName = "[dbo].[sp_DeleteSelectGST]";
                    connection.Open();
                    return connection.Query<Status>(procName, para, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to delete GST", ex, "DeleteGST");
                return null;
            }

        }
        #endregion
    }
}

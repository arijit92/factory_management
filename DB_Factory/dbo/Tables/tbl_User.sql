﻿CREATE TABLE [dbo].[tbl_User] (
    [Id]        BIGINT         IDENTITY (1, 1) NOT NULL,
    [FullName]  NVARCHAR (200) NOT NULL,
    [UserId]    NVARCHAR (50)  NOT NULL,
    [Phone]     NVARCHAR (15)  NOT NULL,
    [Email]     NVARCHAR (50)  NOT NULL,
    [Address]   NVARCHAR (MAX) NULL,
    [Password]  NVARCHAR (50)  NOT NULL,
    [Role]      BIGINT         NULL,
    [IsActive]  INT            NULL,
    [IsDeleted] INT            NULL,
    [CreatedBy] BIGINT         NULL,
    [CreatedOn] DATETIME       NULL,
    [UpdatedBy] BIGINT         NULL,
    [UpdatedOn] DATETIME       NULL,
    [CompanyId] BIGINT         NULL
);




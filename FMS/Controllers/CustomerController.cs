﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FMS.BL;
using FMS.BL.Buisness;
using FMS.BL.Utilities;

namespace FMS.Controllers
{
    public class CustomerController : Controller
    {
        // GET: Role  
        public ActionResult List()
        {
            ViewBag.ParentMenu = "master";
            ViewBag.ChildMenu = "Customer";
            return View();
        }

        [HttpGet]
        public ActionResult Add(int? Customerid)
        {
            ViewBag.ParentMenu = "Master";
            ViewBag.ChildMenu = "Customer";

            Customer Customer = null;
            CustomerBuisness CustomerBuisness = null;
            
            long userid = 11;
            try
            {
               
                Customer = new Customer();
                CustomerBuisness = new CustomerBuisness();
                
                if (Customerid > 0)
                {
                    Customer = CustomerBuisness.GetCustomer(Customerid, userid).SingleOrDefault();
                }
               // CustomersType = CustomerTypeBuisness.GetCustomerType(0, 11).Where(i => i.IsActive == true && i.IsDeleted == false).ToList();
               // ViewBag.CustomerType = new SelectList(CustomersType, "CustomerTypeId", "CustomerTypeName", 0);
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Customer list", ex, "Customer");
            }
            return View(Customer);
        }
        [HttpPost]
        public ActionResult Add(Customer Customer)
        {
            ViewBag.ParentMenu = "Master";
            ViewBag.ChildMenu = "Customer";
            CustomerBuisness buisness = null;
            //List<CustomerType> CustomersType = null;
           // CustomerTypeBuisness CustomerTypeBuisness = null;
           // CustomerTypeBuisness = new CustomerTypeBuisness();
           // CustomersType = CustomerTypeBuisness.GetCustomerType(0, 11).Where(i => i.IsActive == true && i.IsDeleted == false).ToList();
           // ViewBag.CustomerType = new SelectList(CustomersType, "CustomerTypeId", "CustomerTypeName", 0);
            if (ModelState.IsValid)
            {
                buisness = new CustomerBuisness();

                Status status = new Status();
                status = buisness.AddUpdateCustomer(Customer, 1);
                TempData["SuccessFlag"] = status.SuccessFlag;
                TempData["Msg"] = status.Msg;
                if (status.SuccessFlag == "1")
                {
                    return RedirectToAction("List");
                }
                else
                {
                    return View(Customer);
                }
            }
            return View(Customer);
        }


        [HttpPost]
        public JsonResult LoadCustomer(int CustomerId)
        {
            ViewBag.ParentMenu = "Master";
            ViewBag.ChildMenu = "Customer";
            long userid = 11;
            List<Customer> Customer = null;
            CustomerBuisness CustomerBuisness = null;
            try
            {
                Customer = new List<Customer>();
                CustomerBuisness = new CustomerBuisness();
                Customer = CustomerBuisness.GetCustomer(CustomerId, userid).Where(i => i.IsDeleted == false).ToList();
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Customer list", ex, "Customer");
            }

            return Json(Customer, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(int Customerid)
        {
            ViewBag.ParentMenu = "Master";
            ViewBag.ChildMenu = "Customer";
            CustomerBuisness CustomerBuisness = null;
            Status status = null;
            long userid = 11;
            try
            {
                CustomerBuisness = new CustomerBuisness();
                status = new Status();
                status = CustomerBuisness.DeleteCustomer(Customerid, userid);
                TempData["SuccessFlag"] = status.SuccessFlag;
                TempData["Msg"] = status.Msg;
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to delete Customer ", ex, "Customer");
            }
            return RedirectToAction("List");
        }
    }
}
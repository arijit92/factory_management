﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;

namespace FMS.BL
{
    public class Common
    {
        protected IDbConnection GetConnection()
        {
            return new SqlConnection(ConfigurationManager.ConnectionStrings["dbconnection"].ToString());
        }  
    }
}

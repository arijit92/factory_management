﻿CREATE PROCEDURE  [dbo].[sp_InsertUpdateAlert]
-- Add the parameters for the stored procedure here
 @AlertId [bigint]
,@AlertName [NVARCHAR](MAX)
,@AlertDescription [NVARCHAR](MAX) NULL
,@IssueDate datetime
,@NextRenewalDate datetime
,@IsActive [int] NULL
,@CreatedBy [bigint] NULL
,@CompanyId [bigint] NULL
AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
--update or insert checking
IF(@AlertId>0)
BEGIN
--Duplicate value check
IF EXISTS(SELECT * FROM tbl_Alert WITH(NOLOCK) WHERE AlertName = @AlertName AND AlertId != @AlertId)
BEGIN
SELECT 'Already exists.' as Msg,0 as SuccessFlag
END
ELSE
BEGIN
--update
UPDATE tbl_Alert
SET
AlertName =UPPER(@AlertName)
,AlertDescription=@AlertDescription
,IssueDate =@IssueDate
,NextRenewalDate =@NextRenewalDate
,IsActive =@IsActive
,UpdatedBy =@CreatedBy
,UpdatedOn      =GETDATE()
,CompanyId =@CompanyId
WHERE AlertId =@AlertId
SELECT 'Alert updated successfully.' as Msg,1 as SuccessFlag
END
END
ELSE
BEGIN
--Duplicate value check
IF EXISTS (SELECT * FROM tbl_Alert WITH(NOLOCK) WHERE AlertName = @AlertName)
BEGIN
SELECT 'Already exists.' as Msg,0 as SuccessFlag
END
ELSE
BEGIN
--insert
INSERT INTO tbl_Alert
(
 AlertName
,AlertDescription
,IssueDate 
,NextRenewalDate 
,IsActive
,IsDeleted
,CreatedBy
,CreatedOn
,CompanyId
)
VALUES
(
UPPER(@AlertName)
,@AlertDescription
,@IssueDate
,@NextRenewalDate
,@IsActive
,0
,@CreatedBy
,GETDATE()
,@CompanyId
)
SELECT 'Alert created successfully.' as Msg,1 as SuccessFlag
END
END
END TRY
BEGIN CATCH
SELECT 'Sorry something went wrong, unable to process,' as Msg,0 as SuccessFlag
RETURN
END CATCH
END
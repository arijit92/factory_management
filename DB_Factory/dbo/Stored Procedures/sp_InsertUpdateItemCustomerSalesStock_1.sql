﻿CREATE PROCEDURE  [dbo].[sp_InsertUpdateItemCustomerSalesStock]
-- Add the parameters for the stored procedure here
 @Id int = null
 ,@CustomerId     [bigint]
,@PacketId       int
,@BostaQuantity  decimal(18,2)
,@Weight         decimal(18,2)
,@Price				decimal(18,2)
,@TransactionId			int
AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
Declare @StockBostaQuantity decimal
SET @StockBostaQuantity = (SELECT BostaQuantity from tbl_ItemSalesStock where PacketId = @packetId)
IF(@StockBostaQuantity>=@BostaQuantity)
BEGIN
--check existing or not
if(@Id>0)
--if exixting update the record
BEGIN
UPDATE  tbl_ItemCustomerSalesStockTransaction
SET 
CustomerId     	  = 	@CustomerId     	
,PacketId      	  = 	@PacketId      	
,BostaQuantity 	  = 	@BostaQuantity 	
,Weight        	  = 	@Weight        	
,Price			  = 	@Price			
,TransactionId	  = 	@TransactionId	
,Date			  = 	GETDATE()		
where Id = @Id

END
ELSE
BEGIN 
--if not exixts insert the record
INSERT INTO tbl_ItemCustomerSalesStockTransaction
(
 CustomerId    
,PacketId      
,BostaQuantity 
,Weight        
,Price			
,TransactionId
,Date
)
values
(
@CustomerId    
,@PacketId      
,@BostaQuantity 
,@Weight        
,@Price			
,@TransactionId	
,GETDATE()
)
END
Declare @TotalBosta decimal(18,2),@TotalWeight decimal(18,2)
SET @TotalBosta = (SELECT SUM(BostaQuantity) from tbl_ItemSalesStockTransaction where PacketId = @PacketId)
SET @TotalWeight= (SELECT SUM(Weight) from tbl_ItemSalesStockTransaction where PacketId = @PacketId)
--check the main stock table
IF EXISTS(select SalesStockId from tbl_ItemSalesStock where PacketId = @PacketId)
UPDATE tbl_ItemSalesStock
SET
BostaQuantity = BostaQuantity-@BostaQuantity
,TotalWeight = TotalWeight - @Weight
where PacketId = @packetId
SELECT 'Stock updated successfully,' as Msg,1 as SuccessFlag
END
ELSE
BEGIN
SELECT 'Stock not available for sale,' as Msg,0 as SuccessFlag
END
END TRY
BEGIN CATCH
SELECT 'Sorry something went wrong, unable to process,' as Msg,0 as SuccessFlag
RETURN
END CATCH
END
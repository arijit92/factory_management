﻿CREATE PROCEDURE  [dbo].[sp_IncomeExpenseList]
-- Add the parameters for the stored procedure here
 @PaymentMode int NULL
,@Type int null  --1=delete,2=Select
,@UserId [bigint] NULL

AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
select 
 TM.CreatedOn
,TM.PaymentMode
,TM.Amount
,TD.LedgerID
,L.LedgerName
,LT.LedgerTypeName
,TD.OperationID 
from tbl_TransactionMaster TM 
right join tbl_TransactionDetails TD ON TM.TransactionID = TD.TransactionID 
INNER JOIN tbl_Ledger L ON L.LedgerId = TD.LedgerID 
inner join tbl_LedgerType LT on LT.LedgerTypeId = L.LedgerTypeId

END TRY
BEGIN CATCH
SELECT 'Sorry something went wrong, unable to process,' as Msg,0 as SuccessFlag
RETURN
END CATCH
END
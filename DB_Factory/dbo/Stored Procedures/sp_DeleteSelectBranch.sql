﻿CREATE PROCEDURE  [dbo].[sp_DeleteSelectBranch]
-- Add the parameters for the stored procedure here
 @BranchId [bigint] NULL
,@OperationId [bigint]  --1=delete,2=Select
,@UserId [bigint] NULL

AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
IF(@OperationId  = 1 AND @BranchId >0)
BEGIN
UPDATE tbl_Branch
SET
IsDeleted = 1
   ,UpdatedBy = @UserId
   ,UpdatedOn = GETDATE()
    WHERE BranchId = @BranchId
SELECT 'Branch deleted successfully.' as Msg,1 as SuccessFlag
END
ELSE
BEGIN
SELECT * FROM tbl_Branch WITH(NOLOCK)
WHERE BranchId = ISNULL(@BranchId,BranchId)
END

END TRY
BEGIN CATCH
SELECT 'Sorry something went wrong, unable to process,' as Msg,0 as SuccessFlag
RETURN
END CATCH
END
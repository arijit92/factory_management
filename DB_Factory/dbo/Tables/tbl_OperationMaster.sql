﻿CREATE TABLE [dbo].[tbl_OperationMaster] (
    [OperationID]   INT            IDENTITY (1, 1) NOT NULL,
    [OperationName] NVARCHAR (500) NULL,
    [OperationDesc] NVARCHAR (MAX) NULL,
    [LedgerID]      INT            NULL,
    [IsActive]      INT            NULL,
    [IsDeleted]     INT            NULL,
    [CreatedBy]     BIGINT         NULL,
    [CreatedOn]     DATETIME       NULL,
    [UpdatedBy]     BIGINT         NULL,
    [UpdatedOn]     DATETIME       NULL,
    CONSTRAINT [PK_tbl_OperationMaster] PRIMARY KEY CLUSTERED ([OperationID] ASC)
);


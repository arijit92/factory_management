﻿CREATE PROCEDURE  [dbo].[sp_InsertUpdateUnit]
-- Add the parameters for the stored procedure here
 @UnitId [bigint]
,@UnitName [nvarchar](100)
,@Description [nvarchar](max) NULL
,@IsActive [int] NULL
,@CreatedBy [bigint] NULL
,@CompanyId [bigint] NULL
AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
--update or insert checking
IF(@UnitId>0)
BEGIN
--Duplicate value check
IF EXISTS(SELECT * FROM tbl_Unit WITH(NOLOCK) WHERE UnitName = @UnitName AND UnitId != @UnitId)
BEGIN
SELECT 'Already exists.' as Msg,0 as SuccessFlag
END
ELSE
BEGIN
--update
UPDATE tbl_Unit
SET
UnitName =UPPER(@UnitName)
,UnitDescription =@Description
,IsActive =@IsActive
,UpdatedBy =@CreatedBy
,UpdatedOn      =GETDATE()
,CompanyId =@CompanyId
WHERE UnitId =@UnitId
SELECT 'Unit updated successfully.' as Msg,1 as SuccessFlag
END
END
ELSE
BEGIN
--Duplicate value check
IF EXISTS (SELECT * FROM tbl_Unit WITH(NOLOCK) WHERE UnitName = @UnitName)
BEGIN
SELECT 'Already exists.' as Msg,0 as SuccessFlag
END
ELSE
BEGIN
--insert
INSERT INTO tbl_Unit
(
UnitName
,UnitDescription
,IsActive
,IsDeleted
,CreatedBy
,CreatedOn
,CompanyId
)
VALUES
(
UPPER(@UnitName)
,@Description
,@IsActive
,0
,@CreatedBy
,GETDATE()
,@CompanyId
)
SELECT 'Unit created successfully.' as Msg,1 as SuccessFlag
END
END
END TRY
BEGIN CATCH
SELECT 'Sorry something went wrong, unable to process,' as Msg,0 as SuccessFlag
RETURN
END CATCH
END
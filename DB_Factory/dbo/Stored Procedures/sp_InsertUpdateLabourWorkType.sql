﻿CREATE PROCEDURE  [dbo].[sp_InsertUpdateLabourWorkType]
-- Add the parameters for the stored procedure here
 @LabourWorkTypeId [bigint]
,@LabourWorkTypeName [NVARCHAR](MAX)
,@LabourWorkTypeDescription [NVARCHAR](MAX) NULL
,@PerBagrRate decimal(18,2)
,@IsActive [int] NULL
,@CreatedBy [bigint] NULL
,@CompanyId [bigint] NULL
AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
--update or insert checking
IF(@LabourWorkTypeId>0)
BEGIN
--Duplicate value check
IF EXISTS(SELECT * FROM tbl_LabourWorkType WITH(NOLOCK) WHERE LabourWorkTypeName = @LabourWorkTypeName AND LabourWorkTypeId != @LabourWorkTypeId AND IsDeleted = 0)
BEGIN
SELECT 'Already exists.' as Msg,0 as SuccessFlag
END
ELSE
BEGIN
--update
UPDATE tbl_LabourWorkType
SET
LabourWorkTypeName =UPPER(@LabourWorkTypeName)
,LabourWorkTypeDescription=@LabourWorkTypeDescription
,PerBagrRate =@PerBagrRate
,IsActive =@IsActive
,UpdatedBy =@CreatedBy
,UpdatedOn      =GETDATE()
,CompanyId =@CompanyId
WHERE LabourWorkTypeId =@LabourWorkTypeId
SELECT 'Labour Work Type updated successfully.' as Msg,1 as SuccessFlag
END
END
ELSE
BEGIN
--Duplicate value check
IF EXISTS (SELECT * FROM tbl_LabourWorkType WITH(NOLOCK) WHERE LabourWorkTypeName = @LabourWorkTypeName AND IsDeleted =0)
BEGIN
SELECT 'Already exists.' as Msg,0 as SuccessFlag
END
ELSE
BEGIN
--insert
INSERT INTO tbl_LabourWorkType
(
 LabourWorkTypeName
,LabourWorkTypeDescription
,PerBagrRate 
,IsActive
,IsDeleted
,CreatedBy
,CreatedOn
,CompanyId
)
VALUES
(
UPPER(@LabourWorkTypeName)
,@LabourWorkTypeDescription
,@PerBagrRate
,@IsActive
,0
,@CreatedBy
,GETDATE()
,@CompanyId
)
SELECT 'Labour Work Type created successfully.' as Msg,1 as SuccessFlag
END
END
END TRY
BEGIN CATCH
SELECT 'Sorry something went wrong, unable to process,' as Msg,0 as SuccessFlag
RETURN
END CATCH
END
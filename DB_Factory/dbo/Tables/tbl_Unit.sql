﻿CREATE TABLE [dbo].[tbl_Unit] (
    [UnitId]          BIGINT         IDENTITY (1, 1) NOT NULL,
    [UnitName]        NVARCHAR (50)  NOT NULL,
    [UnitDescription] NVARCHAR (200) NULL,
    [IsActive]        INT            NULL,
    [IsDeleted]       INT            NULL,
    [CreatedBy]       BIGINT         NULL,
    [CreatedOn]       DATETIME       NULL,
    [UpdatedBy]       BIGINT         NULL,
    [UpdatedOn]       DATETIME       NULL,
    [CompanyId]       BIGINT         NULL
);




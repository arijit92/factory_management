﻿CREATE PROCEDURE  [dbo].[sp_DeleteSelectUnit]
-- Add the parameters for the stored procedure here
 @UnitId [bigint] NULL
,@OperationId [bigint]  --1=delete,2=Select
,@UserId [bigint] NULL

AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
IF(@OperationId  = 1 AND @UnitId >0)
BEGIN
UPDATE tbl_Unit
SET
   IsDeleted = 1
   ,UpdatedBy = @UserId
   ,UpdatedOn = GETDATE()
    WHERE UnitId = @UnitId
SELECT 'Unit deleted successfully.' as Msg,1 as SuccessFlag
END
ELSE
BEGIN
SELECT * FROM tbl_Unit WITH(NOLOCK)
WHERE UnitId = ISNULL(@UnitId,UnitId)
END

END TRY
BEGIN CATCH
SELECT 'Sorry something went wrong, unable to process,' as Msg,0 as SuccessFlag
RETURN
END CATCH
END
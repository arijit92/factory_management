﻿CREATE PROCEDURE  [dbo].[sp_InsertUpdateItemPurchaseStock]
-- Add the parameters for the stored procedure here
 @Id int = null
 ,@CustomerId     [bigint]
,@PacketId       int
,@BostaQuantity  decimal(18,2)
,@Weight         decimal(18,2)
,@Price				decimal(18,2)
,@TransactionId			int
AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
--check existing or not
if(@Id>0)
--if exixting update the record
BEGIN
UPDATE  tbl_ItemPurchaseStockTransaction
SET 
CustomerId     	  = 	@CustomerId     	
,PacketId      	  = 	@PacketId      	
,BostaQuantity 	  = 	@BostaQuantity 	
,Weight        	  = 	@Weight        	
,Price			  = 	@Price			
,TransactionId	  = 	@TransactionId	
,Date			  = 	GETDATE()		
where Id = @Id

END
ELSE
BEGIN 
--if not exixts insert the record
INSERT INTO tbl_ItemPurchaseStockTransaction
(
 CustomerId    
,PacketId      
,BostaQuantity 
,Weight        
,Price			
,TransactionId
,Date
)
values
(
@CustomerId    
,@PacketId      
,@BostaQuantity 
,@Weight        
,@Price			
,@TransactionId	
,GETDATE()
)
END
Declare @TotalBosta decimal(18,2),@TotalWeight decimal(18,2)
SET @TotalBosta = (SELECT SUM(BostaQuantity) from tbl_ItemPurchaseStockTransaction where PacketId = @PacketId)
SET @TotalWeight = (SELECT SUM(Weight) from tbl_ItemPurchaseStockTransaction where PacketId = @PacketId)
--check the main stock table
IF EXISTS(select PurchaseStockId from tbl_ItemPurchaseStock where PacketId = @PacketId)
BEGIN
UPDATE tbl_ItemPurchaseStock
SET 
BostaQuantity = @TotalBosta,
TotalWeight = @TotalWeight,
UpdatedOn = GETDATE()
WHERE PacketId = @PacketId
END
ELSE
BEGIN
INSERT INTO tbl_ItemPurchaseStock
(
PacketId
,TotalWeight
,BostaQuantity
,UpdatedOn
)
VALUES
(
@PacketId
,@TotalWeight
,@TotalBosta
,GETDATE()
)
END
SELECT 'Stock updated successfully,' as Msg,1 as SuccessFlag
END TRY
BEGIN CATCH
SELECT 'Sorry something went wrong, unable to process,' as Msg,0 as SuccessFlag
RETURN
END CATCH
END
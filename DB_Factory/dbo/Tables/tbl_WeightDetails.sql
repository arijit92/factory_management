﻿CREATE TABLE [dbo].[tbl_WeightDetails] (
    [TransactionId]      BIGINT          IDENTITY (1, 1) NOT NULL,
    [CustomerName]       NVARCHAR (200)  NOT NULL,
    [Product]            NCHAR (10)      NULL,
    [Misc_1st_weight]    NVARCHAR (50)   NULL,
    [Misc_2nd_weight]    NVARCHAR (50)   NULL,
    [Date_First_Weight]  DATETIME        NULL,
    [Date_Second_Weight] DATETIME        NULL,
    [First_weight]       DECIMAL (18, 2) NULL,
    [Second_weight]      DECIMAL (18, 2) NULL,
    [UnitId]             BIGINT          NULL,
    [NetWeight]          DECIMAL (18, 2) NULL,
    [WeightCharges]      DECIMAL (18, 2) NULL,
    [IsActive]           INT             NULL,
    [IsDeleted]          INT             NULL,
    [CreatedBy]          BIGINT          NULL,
    [CreatedOn]          DATETIME        NULL,
    [UpdatedBy]          BIGINT          NULL,
    [UpdatedOn]          DATETIME        NULL,
    [CompanyId]          BIGINT          NULL
);


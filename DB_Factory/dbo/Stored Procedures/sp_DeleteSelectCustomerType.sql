﻿CREATE PROCEDURE  [dbo].[sp_DeleteSelectCustomerType]
-- Add the parameters for the stored procedure here
 @CustomerTypeId [bigint] NULL
,@OperationId [bigint]  --1=delete,2=Select
,@UserId [bigint] NULL

AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
IF(@CustomerTypeId  = 1 AND @CustomerTypeId >0)
BEGIN
UPDATE tbl_CustomerType
SET
IsDeleted = 0
   ,UpdatedBy = @UserId
   ,UpdatedOn = GETDATE()
    WHERE CustomerTypeId = @CustomerTypeId
SELECT 'Customer Type deleted successfully.' as Msg,1 as SuccessFlag
END
ELSE
BEGIN
SELECT * FROM tbl_CustomerType WITH(NOLOCK)
WHERE CustomerTypeId = ISNULL(@CustomerTypeId,CustomerTypeId)
END

END TRY
BEGIN CATCH
SELECT 'Sorry something went wrong, unable to process,' as Msg,0 as SuccessFlag
RETURN
END CATCH
END
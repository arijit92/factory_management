﻿CREATE TABLE [dbo].[tbl_Bank] (
    [Id]       INT            IDENTITY (1, 1) NOT NULL,
    [BankName] NVARCHAR (200) NULL,
    [IFSC]     NVARCHAR (50)  NULL,
    [Display]  NVARCHAR (500) NULL
);


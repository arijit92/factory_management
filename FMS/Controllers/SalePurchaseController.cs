﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FMS.BL;
using FMS.BL.Buisness;
using FMS.BL.Utilities;

namespace FMS.Controllers
{
    public class SalePurchaseController : Controller
    {
        // GET: SalePurchase
        [HttpGet]
        public ActionResult Sale()
        {
            List<Packet> packets = null;
            SalePurchaseBuisness saleBuisness = null;
            ItemPurchaseStockTransaction purchaseDetails = null;
            try
            {
                packets = new List<Packet>();
                saleBuisness = new SalePurchaseBuisness();
                packets = saleBuisness.GetPacketDetails().ToList();
                packets.ForEach(e => e.Description = e.Description + " (" + e.Weight + " " + e.Unit + ")");
                TempData["Packet"] = new SelectList(packets, "PacketId", "Description", 0);
                ViewBag.ParentMenu = "salepurchase";
                ViewBag.ChildMenu = "sale";
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Weight Details list", ex, "WeightDetails");
            }
            return View(purchaseDetails);
        }
        [HttpPost]
        public ActionResult Sale(ItemPurchaseStockTransaction transaction)
        {
            List<Packet> packets = null;
            packets = new List<Packet>();
            SalePurchaseBuisness buisness = new SalePurchaseBuisness();
            Status status = new Status();
            status = buisness.AddUpdateSaleDetails(transaction);
            packets = buisness.GetPacketDetails().ToList();
            packets.ForEach(e => e.Description = e.Description + " (" + e.Weight + " " + e.Unit + ")");
            TempData["Packet"] = new SelectList(packets, "PacketId", "Description", 0);
            TempData["SuccessFlag"] = status.SuccessFlag;
            TempData["Msg"] = status.Msg;
            if (status.SuccessFlag == "1")
            {
                return RedirectToAction("List");
            }
            else
            {
                return View(transaction);
            }
        }
        public ActionResult Purchase()
        {

            ViewBag.ParentMenu = "salepurchase";
            ViewBag.ChildMenu = "purchase";
            return View();
        }

        public ActionResult ItemPurchase()
        {
            return View();
        }
        [HttpPost]
        public JsonResult GetPurchaseStockCount()
        {
            long userid = 11;
            List<ItemPurchaseStock> stocks = null;
            SalePurchaseBuisness buisness = null;
            try
            {
                stocks = new List<ItemPurchaseStock>();
                buisness = new SalePurchaseBuisness();
                stocks = buisness.GetPurchaseStockCount().ToList();
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Role list", ex, "Role");
            }

            return Json(stocks, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetProductionStockCount()
        {
            long userid = 11;
            List<ItemProductionStock> stocks = null;
            SalePurchaseBuisness buisness = null;
            try
            {
                stocks = new List<ItemProductionStock>();
                buisness = new SalePurchaseBuisness();
                stocks = buisness.GetProductionStockCount().ToList();
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Role list", ex, "Role");
            }

            return Json(stocks, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetProductionStockDetails()
        {
            long userid = 11;
            List<ItemProductionStockTransaction> stocks = null;
            SalePurchaseBuisness buisness = null;
            try
            {
                stocks = new List<ItemProductionStockTransaction>();
                buisness = new SalePurchaseBuisness();
                stocks = buisness.GetProductionStockDetails().ToList();
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Role list", ex, "Role");
            }

            return Json(stocks, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult AddPurchase(int? Id, int? PacketId)
        {
            List<Packet> packets = null;
            SalePurchaseBuisness saleBuisness = null;

            ItemPurchaseStockTransaction purchaseDetails = null;
            //WeightDetailsBuisness weightDetailsBuisness = null;
            //long userid = 11;
            try
            {
                packets = new List<Packet>();
                saleBuisness = new SalePurchaseBuisness();
                purchaseDetails = new ItemPurchaseStockTransaction();
                if (Id.HasValue)
                {
                    purchaseDetails = saleBuisness.GetAllPurchaseList(Id).SingleOrDefault();
                    //weightDetails.First_Weight_Time = (weightDetails.First_Weight_Time).ToString("HH:mm");
                    //weightDetails.Time_Second_Weight = (weightDetails.Time_Second_Weight).ToString("HH:mm");
                    //dt.ToString("HH:mm");
                }

                packets = saleBuisness.GetPacketDetails().ToList();
                packets.ForEach(e => e.Description = e.Description + " (" + e.Weight + " " + e.Unit + ")");
                TempData["Packet"] = new SelectList(packets, "PacketId", "Description", 0);

            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Weight Details list", ex, "WeightDetails");
            }
            return View(purchaseDetails);
        }
        [HttpPost]
        public ActionResult AddPurchase(ItemPurchaseStockTransaction transaction)
        {
            if (ModelState.IsValid)
            {
                SalePurchaseBuisness buisness = new SalePurchaseBuisness();
                Status status = new Status();
                status = buisness.AddUpdatePurchaseDetails(transaction);
                TempData["SuccessFlag"] = status.SuccessFlag;
                TempData["Msg"] = status.Msg;
                if (status.SuccessFlag == "1")
                {
                    return RedirectToAction("List");
                }
                else
                {
                    return View(transaction);
                }
            }
            return View(transaction);
        }
        [HttpGet]
        public ActionResult List()
        {
            return View();
        }
        [HttpPost]
        public JsonResult GetAllPurchase()
        {
            List<ItemPurchaseStockTransaction> lst = new List<ItemPurchaseStockTransaction>();
            SalePurchaseBuisness salesBuisness = new SalePurchaseBuisness();
            try
            {
                lst = salesBuisness.GetAllPurchaseList().ToList();
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Accessories list", ex, "Accessories");
            }

            return Json(lst, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Production()
        {
            return View();
        }

        [HttpGet]
        public ActionResult MoveToProduction()
        {
            List<Packet> packets = null;
            SalePurchaseBuisness saleBuisness = null;

            ItemProductionStockTransaction productioDetails = null;
            //WeightDetailsBuisness weightDetailsBuisness = null;
            //long userid = 11;
            try
            {
                packets = new List<Packet>();
                saleBuisness = new SalePurchaseBuisness();
                productioDetails = new ItemProductionStockTransaction();
                //if (Id.HasValue)
                //{
                //    purchaseDetails = saleBuisness.GetAllPurchaseList(Id).SingleOrDefault();
                //    //weightDetails.First_Weight_Time = (weightDetails.First_Weight_Time).ToString("HH:mm");
                //    //weightDetails.Time_Second_Weight = (weightDetails.Time_Second_Weight).ToString("HH:mm");
                //    //dt.ToString("HH:mm");
                //}

                packets = saleBuisness.GetPacketDetails().ToList();
                packets.ForEach(e => e.Description = e.Description + " (" + e.Weight + " " + e.Unit + ")");
                TempData["Packet"] = new SelectList(packets, "PacketId", "Description", 0);

            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Weight Details list", ex, "WeightDetails");
            }
            return View(productioDetails);
        }
        [HttpPost]
        public ActionResult MoveToProduction(ItemProductionStockTransaction transaction)
        {
            if (ModelState.IsValid)
            {
                SalePurchaseBuisness buisness = new SalePurchaseBuisness();
                Status status = new Status();
                status = buisness.AddUpdateProductionDetails(transaction);
                TempData["SuccessFlag"] = status.SuccessFlag;
                TempData["Msg"] = status.Msg;
                if (status.SuccessFlag == "1")
                {
                    return RedirectToAction("List");
                }
                else
                {
                    return View(transaction);
                }
            }
            return View(transaction);
        }

        public ActionResult MoveToSale()
        {
            List<Packet> packets = null;
            SalePurchaseBuisness saleBuisness = null;

            ItemSalesStockTransaction productioDetails = null;
            //WeightDetailsBuisness weightDetailsBuisness = null;
            //long userid = 11;
            try
            {
                packets = new List<Packet>();
                saleBuisness = new SalePurchaseBuisness();
                productioDetails = new ItemSalesStockTransaction();
                //if (Id.HasValue)
                //{
                //    purchaseDetails = saleBuisness.GetAllPurchaseList(Id).SingleOrDefault();
                //    //weightDetails.First_Weight_Time = (weightDetails.First_Weight_Time).ToString("HH:mm");
                //    //weightDetails.Time_Second_Weight = (weightDetails.Time_Second_Weight).ToString("HH:mm");
                //    //dt.ToString("HH:mm");
                //}

                packets = saleBuisness.GetPacketDetails().ToList();
                packets.ForEach(e => e.Description = e.Description + " (" + e.Weight + " " + e.Unit + ")");
                TempData["Packet"] = new SelectList(packets, "PacketId", "Description", 0);

            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Weight Details list", ex, "WeightDetails");
            }
            return View(productioDetails);
        }
        [HttpPost]
        public ActionResult MoveToSale(ItemSalesStockTransaction transaction)
        {
            if (ModelState.IsValid)
            {
                SalePurchaseBuisness buisness = new SalePurchaseBuisness();
                Status status = new Status();
                status = buisness.AddUpdateSaleDetails(transaction);
                TempData["SuccessFlag"] = status.SuccessFlag;
                TempData["Msg"] = status.Msg;
                if (status.SuccessFlag == "1")
                {
                    return RedirectToAction("Production");
                }
                else
                {
                    return View("Production");
                }
            }
            return View(transaction);
        }

        public ActionResult SalesStock()
        {
            return View();
        }

        [HttpPost]
        public JsonResult GetSaleStockCount()
        {
            long userid = 11;
            List<ItemSalesStock> stocks = null;
            SalePurchaseBuisness buisness = null;
            try
            {
                stocks = new List<ItemSalesStock>();
                buisness = new SalePurchaseBuisness();
                stocks = buisness.GetSaleStockCount().ToList();
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Role list", ex, "Role");
            }

            return Json(stocks, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetSaleStockDetails()
        {
            long userid = 11;
            List<ItemSalesStockTransaction> stocks = null;
            SalePurchaseBuisness buisness = null;
            try
            {
                stocks = new List<ItemSalesStockTransaction>();
                buisness = new SalePurchaseBuisness();
                stocks = buisness.GetSaleStockDetails().ToList();
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Role list", ex, "Role");
            }

            return Json(stocks, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult SaleList()
        {
            return View();
        }
        [HttpPost]
        public JsonResult GetSaleDetails()
        {
            List<ItemCustomerSalesStockTransaction> lst = new List<ItemCustomerSalesStockTransaction>();
            SalePurchaseBuisness salesBuisness = new SalePurchaseBuisness();
            try
            {
                lst = salesBuisness.GetSaleDetails().ToList();
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Accessories list", ex, "Accessories");
            }

            return Json(lst, JsonRequestBehavior.AllowGet);
        }
    }
}
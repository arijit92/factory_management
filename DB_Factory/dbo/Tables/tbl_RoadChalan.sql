﻿CREATE TABLE [dbo].[tbl_RoadChalan] (
    [Id]                   INT            IDENTITY (1, 1) NOT NULL,
    [NameOfDistributor]    NVARCHAR (100) NULL,
    [AddressOfDistributor] NVARCHAR (MAX) NULL,
    [Destination]          NVARCHAR (50)  NULL,
    [Quantity]             NVARCHAR (100) NULL,
    [TruckNo]              NVARCHAR (50)  NULL,
    [Date]                 DATE           NULL,
    [IsDeleted]            BIT            NULL
);


﻿CREATE PROCEDURE  [dbo].[sp_InsertUpdateDocument]
-- Add the parameters for the stored procedure here
 @DocumentId [bigint]
,@DocumentName [NVARCHAR](MAX)
,@DocumentPath [NVARCHAR] (MAX)
,@IsActive [int] NULL
,@CreatedBy [bigint] NULL
,@CompanyId [bigint] NULL
AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
--update or insert checking
IF(@DocumentId>0)
BEGIN
--Duplicate value check
IF EXISTS(SELECT * FROM tbl_Documents WITH(NOLOCK) WHERE DocumentName = @DocumentName AND DocumentId != @DocumentId)
BEGIN
SELECT 'Already exists.' as Msg,0 as SuccessFlag
END
ELSE
BEGIN
--update
UPDATE tbl_Document
SET
DocumentName =UPPER(@DocumentName)
,DocumentPath = @DocumentPath
,IsActive =@IsActive
,UpdatedBy =@CreatedBy
,UpdatedOn      =GETDATE()
,CompanyId =@CompanyId
WHERE DocumentId =@DocumentId
SELECT 'Document updated successfully.' as Msg,1 as SuccessFlag
END
END
ELSE
BEGIN
--Duplicate value check
IF EXISTS (SELECT * FROM tbl_Documents WITH(NOLOCK) WHERE DocumentName = @DocumentName)
BEGIN
SELECT 'Already exists.' as Msg,0 as SuccessFlag
END
ELSE
BEGIN
--insert
INSERT INTO tbl_Documents
(
 DocumentName
 ,DocumentPath
,IsActive
,IsDeleted
,CreatedBy
,CreatedOn
,CompanyId
)
VALUES
(
UPPER(@DocumentName)
,@DocumentPath
,@IsActive
,0
,@CreatedBy
,GETDATE()
,@CompanyId
)
SELECT 'Document created successfully.' as Msg,1 as SuccessFlag
END
END
END TRY
BEGIN CATCH
SELECT ERROR_MESSAGE() as Msg,0 as SuccessFlag
RETURN
END CATCH
END
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FMS.BL;
using FMS.BL.Buisness;
using FMS.BL.Utilities;

namespace FMS.Controllers
{
    public class UnitsController : Controller
    {
        // GET: Units
        public ActionResult Index()
        {
            ViewBag.ParentMenu = "master";
            ViewBag.ChildMenu = "units";
            return View();
        }

        [HttpGet]
        public ActionResult Create()
        {
            ViewBag.ParentMenu = "master";
            ViewBag.ChildMenu = "units";
            Unit unit = new Unit();
            return View(unit);
        }
        [HttpPost]
        public ActionResult Create(Unit unit)
        {
            ViewBag.ParentMenu = "master";
            ViewBag.ChildMenu = "units";
            if (ModelState.IsValid)
            {
                UnitBuisness buisness = new UnitBuisness();
                Status status = new Status();
                status = buisness.AddUpdateUnit(unit, 1);
                TempData["SuccessFlag"] = status.SuccessFlag;
                TempData["Msg"] = status.Msg;
                if (status.SuccessFlag == "1")
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return View(unit);
                }
            }
            return View(unit);
        }


        [HttpPost]
        public JsonResult LoadUnits(int UnitId)
        {
            ViewBag.ParentMenu = "master";
            ViewBag.ChildMenu = "units";
            long userid = 11;
            List<Unit> unit = null;
            UnitBuisness unitBuisness = null;
            try
            {
                unit = new List<Unit>();
                unitBuisness = new UnitBuisness();
                unit = unitBuisness.GetUnit(UnitId, userid).Where(i => i.IsDeleted == false).ToList();
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Unit list", ex, "Units");
            }

            return Json(unit, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(int UnitId)
        {
            ViewBag.ParentMenu = "master";
            ViewBag.ChildMenu = "units";
            UnitBuisness unitBuisness = null;
            Status status = null;
            long userid = 11;
            try
            {
                unitBuisness = new UnitBuisness();
                status = new Status();
                status = unitBuisness.DeleteUnit(UnitId, userid);
                TempData["SuccessFlag"] = status.SuccessFlag;
                TempData["Msg"] = status.Msg;
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to delete Unit ", ex, "Delete");
            }
            return RedirectToAction("Index");
        }
        public ActionResult Edit(int UnitId)
        {
            ViewBag.ParentMenu = "master";
            ViewBag.ChildMenu = "units";
            Unit unit = new Unit();
            UnitBuisness unitBuisness = null;
            try
            {               
                unitBuisness = new UnitBuisness();
                unit = unitBuisness.GetUnit(UnitId, 1).FirstOrDefault();
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Unit list", ex, "Units");
            }

            return View(unit);

        }
        [HttpPost]
        public ActionResult Edit(Unit unit)
        {
            ViewBag.ParentMenu = "master";
            ViewBag.ChildMenu = "units";
            if (ModelState.IsValid)
            {
                UnitBuisness buisness = new UnitBuisness();
                Status status = new Status();
                status = buisness.AddUpdateUnit(unit, 1);
                TempData["SuccessFlag"] = status.SuccessFlag;
                TempData["Msg"] = status.Msg;
                if (status.SuccessFlag == "1")
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return View(unit);
                }
            }
           
            return View(unit);

        }
    }
}
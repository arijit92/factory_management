﻿CREATE TABLE [dbo].[tbl_Transaction] (
    [TransactionId] INT             NULL,
    [Amount]        DECIMAL (18, 2) NULL,
    [BankName]      NVARCHAR (50)   NULL,
    [IFSC]          NVARCHAR (50)   NULL,
    [ChequeNo]      NVARCHAR (50)   NULL,
    [DraftNo]       NVARCHAR (50)   NULL,
    [PaymentMode]   NVARCHAR (50)   NULL
);


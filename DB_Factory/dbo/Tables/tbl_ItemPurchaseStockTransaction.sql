﻿CREATE TABLE [dbo].[tbl_ItemPurchaseStockTransaction] (
    [Id]            INT             IDENTITY (1, 1) NOT NULL,
    [CustomerId]    INT             NOT NULL,
    [PacketId]      INT             NOT NULL,
    [BostaQuantity] DECIMAL (18, 2) NOT NULL,
    [Weight]        DECIMAL (18, 2) NOT NULL,
    [Price]         DECIMAL (18, 2) NOT NULL,
    [TransactionId] INT             NULL,
    [Date]          DATETIME        NULL
);


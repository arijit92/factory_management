﻿CREATE PROCEDURE  [dbo].[sp_DeleteSelectGST]
-- Add the parameters for the stored procedure here
 @GSTId [bigint] NULL
,@OperationId [bigint]  --1=delete,2=Select
,@UserId [bigint] NULL

AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
IF(@OperationId  = 1 AND @GSTId >0)
BEGIN
UPDATE tbl_GST
SET
IsDeleted = 1
   ,UpdatedBy = @UserId
   ,UpdatedOn = GETDATE()
    WHERE GSTId = @GSTId
SELECT 'GST deleted successfully.' as Msg,1 as SuccessFlag
END
ELSE
BEGIN
SELECT * FROM tbl_GST WITH(NOLOCK)
WHERE GSTId = ISNULL(@GSTId,GSTId)
END

END TRY
BEGIN CATCH
SELECT 'Sorry something went wrong, unable to process,' as Msg,0 as SuccessFlag
RETURN
END CATCH
END
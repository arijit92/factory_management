﻿CREATE PROCEDURE  [dbo].[sp_selectLedgerTransactionMaster]
-- Add the parameters for the stored procedure here
 @TransactionDate datetime NULL,
 @SearchType nvarchar(20) null,
 @TransactionId int null,
 @LedgerId int null
AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
IF(@SearchType='Date')
BEGIN
select TM.*,TM.TransactionID,L.LedgerID,L.LedgerName 
from tbl_TransactionMaster TM
inner join tbl_Ledger L on L.LedgerId = TM.LedgerId
where CAST(TransactionDate as DATE) = CAST(ISNULL(@TransactionDate,TransactionDate) as DATE)
END
IF(@SearchType='Id')
BEGIN
select TM.*,TM.TransactionID,L.LedgerID,L.LedgerName 
from tbl_TransactionMaster TM
inner join tbl_Ledger L on L.LedgerId = TM.LedgerId
where TransactionID = ISNULL(@TransactionId,TransactionID)
END
IF(@SearchType='LedgerId')
BEGIN
select TM.*,TM.TransactionID,L.LedgerID,L.LedgerName 
from tbl_TransactionMaster TM
inner join tbl_Ledger L on L.LedgerId = TM.LedgerId
where TM.LedgerId = @LedgerId
END
END TRY
BEGIN CATCH
SELECT 'Sorry something went wrong, unable to process,' as Msg,0 as SuccessFlag
RETURN
END CATCH
END
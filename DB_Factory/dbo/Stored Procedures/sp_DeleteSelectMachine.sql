﻿CREATE PROCEDURE  [dbo].[sp_DeleteSelectMachine]
-- Add the parameters for the stored procedure here
 @MachineId [bigint] NULL
,@OperationId [bigint]  --1=delete,2=Select
,@UserId [bigint] NULL

AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
IF(@OperationId  = 1 AND @MachineId >0)
BEGIN
UPDATE tbl_Machine
SET
IsDeleted = 1
   ,UpdatedBy = @UserId
   ,UpdatedOn = GETDATE()
    WHERE MachineId = @MachineId
SELECT 'Machine deleted successfully.' as Msg,1 as SuccessFlag
END
ELSE
BEGIN
SELECT TM.*,TU.UnitName FROM tbl_Machine TM WITH(NOLOCK) INNER JOIN tbl_Unit TU WITH(NOLOCK) ON TM.UnitId = TU.UnitId
WHERE MachineId = ISNULL(@MachineId,MachineId)
END

END TRY
BEGIN CATCH
SELECT 'Sorry something went wrong, unable to process,' as Msg,0 as SuccessFlag
RETURN
END CATCH
END
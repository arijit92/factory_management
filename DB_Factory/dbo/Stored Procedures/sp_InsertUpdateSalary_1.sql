﻿CREATE PROCEDURE  [dbo].[sp_InsertUpdateSalary]
 @Year nvarchar(4),
 @Month int,
 @Salary_Details [SalaryDetails] ReadOnly
AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
IF EXISTS(SELECT EmployeeId from tbl_EmployeeSalary where Year=@Year AND Month = @Month)
BEGIN


Delete from [dbo].[tbl_EmployeeSalary] where Year=@Year AND Month = @Month
END
INSERT INTO [dbo].[tbl_EmployeeSalary]
(Year
,Month
,EmployeeId
,IsPaid
,Paka_Salary
,Kacha_Salary
,LoanAmount
,LoanDeduction
,PF
,Date)
SELECT @Year,@Month,E.EmployeeId,E.IsPaid,E.Paka_Salary,E.Kacha_Salary,E.LoanAmount,E.LoanDeduction,E.PF,GETDATE() FROM @Salary_Details AS E


	IF NOT EXISTS(select * from tbl_EmployeeSalaryTrack where Year = @Year AND Month = @Month)
	Insert into tbl_EmployeeSalaryTrack (Year,Month,IsDisbursed,IsLocked) values (@Year,@Month,1,0)
SELECT 'Salary disbursed successfully.' as Msg,1 as SuccessFlag
END TRY
BEGIN CATCH
SELECT 'Sorry something went wrong, unable to process,' as Msg,0 as SuccessFlag
RETURN
END CATCH
END
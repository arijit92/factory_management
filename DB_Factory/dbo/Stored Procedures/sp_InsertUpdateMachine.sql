﻿CREATE PROCEDURE  [dbo].[sp_InsertUpdateMachine]
-- Add the parameters for the stored procedure here
 @MachineId [bigint]
,@MachineName [NVARCHAR](MAX)
,@Quantity decimal(18,2)
,@UnitId int NULL
,@CostOfMachineries decimal(18,2)
,@IsActive [int] NULL
,@CreatedBy [bigint] NULL
,@CompanyId [bigint] NULL
AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
--update or insert checking
IF(@MachineId>0)
BEGIN
--Duplicate value check
IF EXISTS(SELECT * FROM tbl_Machine WITH(NOLOCK) WHERE MachineName = @MachineName AND MachineId != @MachineId AND IsDeleted = 0)
BEGIN
SELECT 'Already exists.' as Msg,0 as SuccessFlag
END
ELSE
BEGIN
--update
UPDATE tbl_Machine
SET
MachineName =UPPER(@MachineName)
,Quantity=@Quantity
,UnitId =@UnitId
,CostOfMachineries =@CostOfMachineries
,IsActive =@IsActive
,UpdatedBy =@CreatedBy
,UpdatedOn      =GETDATE()
,CompanyId =@CompanyId
WHERE MachineId =@MachineId
SELECT 'Machine updated successfully.' as Msg,1 as SuccessFlag
END
END
ELSE
BEGIN
--Duplicate value check
IF EXISTS (SELECT * FROM tbl_Machine WITH(NOLOCK) WHERE MachineName = @MachineName AND IsDeleted = 0)
BEGIN
SELECT 'Already exists.' as Msg,0 as SuccessFlag
END
ELSE
BEGIN
--insert
INSERT INTO tbl_Machine
(
 MachineName
,Quantity
,UnitId 
,CostOfMachineries 
,IsActive
,IsDeleted
,CreatedBy
,CreatedOn
,CompanyId
)
VALUES
(
UPPER(@MachineName)
,@Quantity
,@UnitId
,@CostOfMachineries
,@IsActive
,0
,@CreatedBy
,GETDATE()
,@CompanyId
)
SELECT 'Machine created successfully.' as Msg,1 as SuccessFlag
END
END
END TRY
BEGIN CATCH
SELECT ERROR_MESSAGE() as Msg,0 as SuccessFlag
RETURN
END CATCH
END
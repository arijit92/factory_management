﻿$(function () {
    $("#txt_categoryId").val(categoryid);
        if (categoryid > 0) {
            LoadCategory(categoryid);
    }
    $("#example1").DataTable();
   
    $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
    });
});

//--------------Category insert or update ------------//
function CategorySubmit() {
    var requestData = {
        CategoryId: $("#txt_categoryId").val(),
        CategoryName: $("#txt_categoryname").val().trim().toString(),
        Description: $("#txt_description").val().trim().toString(),
        IsActive: 0
    };
    if ($('#cbx_active').prop("checked") == true) {
        requestData.IsActive = 1;
    }
    if (requestData.CategoryId == "") {
        requestData.CategoryId = 0;
    }
    if (requestData.CategoryName == "") {
        toastr.error('Enter category name.')
        return false;
    }

    $.ajax({
        url: '/Category/InsertUpdateCategory',
        type: 'POST',
        data: JSON.stringify(requestData),
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        async: true,
        processData: true,
        cache: false,
        error: function (xhr) {
            alert('Error: ' + xhr.statusText);
        },
        success: function (result) {
            if (result.SuccessFlag == 1) {
                bootbox.alert({
                    size: "small",
                    message: result.Msg,
                    callback: function () { window.location.href = '/Category/CategoryList';}
                })
               
                //toastr.success(result.Msg);
               // Reset();
                //LoadCategory(0);
            }
            else {
                toastr.error(result.Msg);
            }
        }
    });
}


//-- get the category for edit----//
function LoadCategory(categoryid) {

    var requestData = {
        categoryId: categoryid
    };
    $.ajax({
        url: '/Category/LoadCategory',
        type: 'POST',
        data: JSON.stringify(requestData),
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        async: true,
        processData: true,
        cache: false,
        error: function (xhr) {
            alert('Error: ' + xhr.statusText);
        },
        success: function (result) {
                for (var i = 0; i < result.length; i++) {
                    $("#txt_categoryId").val(result[i].CategoryId);
                    if (result[i].Description == null) {
                        result[i].Description == "";
                    }
                    $("#txt_categoryname").val(result[i].CategoryName);
                    $("#txt_description").val(result[i].Description);
                    $("#cbx_active").prop("checked", result[i].IsActive);
                    //$("#btnCreateRole").text("Update Role");
                    //var selectedvalue = (result[i].menuIDs).slice(0, -1).split(",");
                    //selectedvalue = selectedvalue.toString().toLowerCase();
                    //for (var i = 0; i < dropdownvalue.length; i++) {
                    //    var opt = "";
                    //    if (selectedvalue.indexOf(dropdownvalue[i].menuID) != -1) {
                    //        opt = '<option value=' + dropdownvalue[i].menuID + ' selected="selected">' + dropdownvalue[i].menuTitle + '</option>'
                    //    }
                    //    else {
                    //        opt = '<option value=' + dropdownvalue[i].menuID + '>' + dropdownvalue[i].menuTitle + '</option>'
                    //    }
                    //    $('#ddlMulti').append(opt);
                    //}
                }
                //$('#ddlMulti').multiselect('unload');
                //$('#ddlMulti').css('visibility', 'hidden');
                //setTimeout(function () {
                //    $('#ddlMulti').multiselect({
                //        columns: 3,
                //        placeholder: 'Select Module',
                //        search: true,
                //        searchOptions: {
                //            'default': 'Search Module'
                //        },
                //        selectAll: true
                //    });
                //}, 700);
                //$('#ddlMulti').css('visibility', '');
            
        },
    });
}

function Reset() {

    $("#txt_categoryId").val("");
    $("#txt_categoryname").val("");
    $("#txt_description").val("");
    bootbox.success('succes');
}

function Delete(categoryidTodelete) {
    alert(categoryidTodelete);
    $("#warningtext").text("Are you sure you want to delete this category?");
    $("#btnModal").click();
}

function Edit(categoryidToEdit) {
    //alert(categoryidToEdit);
    LoadCategory(categoryidToEdit);
}
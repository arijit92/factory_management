﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FMS.BL.Utilities;

namespace FMS.BL.Buisness
{
   public class CustomerBuisness : Common
    {
        #region Customer region

        public Status AddUpdateCustomer(Customer pmodel, long UserID)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    para.Add("@CustomerId", pmodel.CustomerId);
                    para.Add("@PointOfPersonName", pmodel.PointOfPersonName);
                    para.Add("@Address", pmodel.Address);
                    para.Add("@Phone", pmodel.Phone);
                    para.Add("@Email", pmodel.Email);
                    para.Add("@Block", pmodel.Block);
                    para.Add("@IsActive", Convert.ToInt32(pmodel.IsActive == false ? 0 : 1));
                    para.Add("@CreatedBy", UserID);
                    para.Add("@CompanyId", pmodel.CompanyId);
                    const string procName = "[dbo].[sp_InsertUpdateCustomer]";
                    connection.Open();
                    return connection.Query<Status>(procName, para, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to insert or update Customer", ex, "AddUpdateCustomer");
                return null;
            }

        }
        public List<Customer> GetCustomer(long? CustomerId, long UserID)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    if (CustomerId == 0)
                    {
                        CustomerId = null;
                    }
                    para.Add("@CustomerId", CustomerId);
                    para.Add("@OperationId", 2);
                    para.Add("@UserId", UserID);
                    const string procName = "[dbo].[sp_DeleteSelectCustomer]";
                    connection.Open();
                    return connection.Query<Customer>(procName, para, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Customer", ex, "GetCustomer");
                return null;
            }

        }
        public Status DeleteCustomer(long? CustomerId, long UserID)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    para.Add("@CustomerId", CustomerId);
                    para.Add("@OperationId", 1);
                    para.Add("@UserId", UserID);
                    const string procName = "[dbo].[sp_DeleteSelectCustomer]";
                    connection.Open();
                    return connection.Query<Status>(procName, para, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to delete Customer", ex, "DeleteCustomer");
                return null;
            }

        }
        #endregion
    }
}

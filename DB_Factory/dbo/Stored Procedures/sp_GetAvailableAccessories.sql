﻿CREATE PROCEDURE  [dbo].[sp_GetAvailableAccessories]
-- Add the parameters for the stored procedure here

AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY



BEGIN
SELECT tbl_Accessories.AccessoriesId,tbl_Accessories.AccessoriesName FROM tbl_Accessories WITH(NOLOCK) INNER JOIN tbl_StockMaster on tbl_StockMaster.AccessoriesId = tbl_Accessories.AccessoriesId
WHERE tbl_StockMaster.AvailableQuantity != 0
END

END TRY
BEGIN CATCH
SELECT 'Sorry something went wrong, unable to process,' as Msg,0 as SuccessFlag
RETURN
END CATCH
END
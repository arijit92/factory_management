﻿Create PROCEDURE  [dbo].[sp_DeleteSelectAccessoriesType]
-- Add the parameters for the stored procedure here
 @AccessoriesTypeId [bigint] NULL
,@OperationId [bigint]  --1=delete,2=Select
,@UserId [bigint] NULL

AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
IF(@OperationId  = 1 AND @AccessoriesTypeId >0)
BEGIN
UPDATE tbl_AccessoriesType
SET
IsDeleted = 1
   ,Date = GETDATE()
    WHERE AccessoriesTypeId = @AccessoriesTypeId
SELECT 'AccessoriesType deleted successfully.' as Msg,1 as SuccessFlag
END
ELSE
BEGIN
SELECT * FROM tbl_AccessoriesType WITH(NOLOCK)
WHERE AccessoriesTypeId = ISNULL(@AccessoriesTypeId,AccessoriesTypeId)
END

END TRY
BEGIN CATCH
SELECT 'Sorry something went wrong, unable to process,' as Msg,0 as SuccessFlag
RETURN
END CATCH
END
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FMS.BL;
using FMS.BL.Buisness;
using FMS.BL.Utilities;
namespace FMS.Controllers
{
    public class BagController : Controller
    {
        // GET: Bag 
        public ActionResult List()
        {
            ViewBag.ParentMenu = "master";
            ViewBag.ChildMenu = "bag";
            return View();
        }

        [HttpGet]
        public ActionResult Add(int? bagid)
        {
            ViewBag.ParentMenu = "master";
            ViewBag.ChildMenu = "bag";
            Bag bag = null;
            BagBuisness bagBuisness = null;
            long userid = 11;
            try
            {
                bag = new Bag();
                bagBuisness = new BagBuisness();
                if (bagid > 0)
                {
                    bag = bagBuisness.GetBag(bagid, userid).SingleOrDefault();
                }


            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Bag list", ex, "Bag");
            }
            return View(bag);
        }
        [HttpPost]
        public ActionResult Add(Bag bag)
        {
            ViewBag.ParentMenu = "master";
            ViewBag.ChildMenu = "bag";
            if (ModelState.IsValid)
            {
                BagBuisness buisness = new BagBuisness();
                Status status = new Status();
                status = buisness.AddUpdateBag(bag, 1);
                TempData["SuccessFlag"] = status.SuccessFlag;
                TempData["Msg"] = status.Msg;
                if (status.SuccessFlag == "1")
                {
                    return RedirectToAction("List");
                }
                else
                {
                    return View(bag);
                }
            }
            return View(bag);
        }

        [HttpPost]
        public JsonResult LoadBag(int BagId)
        {
            ViewBag.ParentMenu = "master";
            ViewBag.ChildMenu = "bag";
            long userid = 11;
            List<Bag> bag = null;
            BagBuisness bagBuisness = null;
            try
            {
                bag = new List<Bag>();
                bagBuisness = new BagBuisness();
                bag = bagBuisness.GetBag(BagId, userid).Where(i => i.IsDeleted == false).ToList();
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Bag list", ex, "Bag");
            }

            return Json(bag, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(int bagid)
        {
            ViewBag.ParentMenu = "master";
            ViewBag.ChildMenu = "bag";
            BagBuisness bagBuisness = null;
            Status status = null;
            long userid = 11;
            try
            {
                bagBuisness = new BagBuisness();
                status = new Status();
                status = bagBuisness.DeleteBag(bagid, userid);
                TempData["SuccessFlag"] = status.SuccessFlag;
                TempData["Msg"] = status.Msg;
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to delete Bag ", ex, "Delete");
            }
            return RedirectToAction("List");
        }

    }
}
﻿CREATE PROCEDURE  [dbo].[sp_GetLoginUser]
-- Add the parameters for the stored procedure here
@UserId NVARCHAR(MAX)

AS
BEGIN
SELECT TU.*,TR.Name as RoleName FROM tbl_User TU INNER JOIN tbl_Role TR ON TR.RoleId = TU.Role WHERE UserId = @UserId
END
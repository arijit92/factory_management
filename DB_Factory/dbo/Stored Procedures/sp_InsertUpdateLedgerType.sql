﻿CREATE PROCEDURE  [dbo].[sp_InsertUpdateLedgerType]
-- Add the parameters for the stored procedure here
 @LedgerTypeId [bigint]
,@LedgerTypeName [NVARCHAR](MAX)
,@LedgerTypeDescription [NVARCHAR](MAX)
,@IsActive [int] NULL
,@CreatedBy [bigint] NULL
,@CompanyId [bigint] NULL
AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
--update or insert checking
IF(@LedgerTypeId>0)
BEGIN
--Duplicate value check
IF EXISTS(SELECT * FROM tbl_LedgerType WITH(NOLOCK) WHERE LedgerTypeName = @LedgerTypeName AND LedgerTypeId != @LedgerTypeId AND IsDeleted = 0)
BEGIN
SELECT 'Already exists.' as Msg,0 as SuccessFlag
END
ELSE
BEGIN
--update
UPDATE tbl_LedgerType
SET
LedgerTypeName =UPPER(@LedgerTypeName)
,LedgerTypeDescription = @LedgerTypeDescription
,IsActive =@IsActive
,UpdatedBy =@CreatedBy
,UpdatedOn      =GETDATE()
,CompanyId =@CompanyId
WHERE LedgerTypeId =@LedgerTypeId
SELECT 'LedgerType updated successfully.' as Msg,1 as SuccessFlag
END
END
ELSE
BEGIN
--Duplicate value check
IF EXISTS (SELECT * FROM tbl_LedgerType WITH(NOLOCK) WHERE LedgerTypeName = @LedgerTypeName AND IsDeleted = 0)
BEGIN
SELECT 'Already exists.' as Msg,0 as SuccessFlag
END
ELSE
BEGIN
--insert
INSERT INTO tbl_LedgerType
(
 LedgerTypeName
,LedgerTypeDescription 
,IsActive
,IsDeleted
,CreatedBy
,CreatedOn
,CompanyId
)
VALUES
(
UPPER(@LedgerTypeName)
,@LedgerTypeDescription
,@IsActive
,0
,@CreatedBy
,GETDATE()
,@CompanyId
)
SELECT 'LedgerType created successfully.' as Msg,1 as SuccessFlag
END
END
END TRY
BEGIN CATCH
SELECT 'Sorry something went wrong, unable to process,' as Msg,0 as SuccessFlag
RETURN
END CATCH
END
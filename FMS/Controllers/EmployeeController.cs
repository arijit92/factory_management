﻿using FMS.BL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FMS.BL.Buisness;
using FMS.BL.Utilities;
using System.IO;
using System.Text;
using iTextSharp.text.pdf;
using iTextSharp.text;

namespace FMS.Controllers
{
    public class EmployeeController : Controller
    {
        // GET: Employee
        public ActionResult List()
        {
            ViewBag.ParentMenu = "EmployeeMaster";
            ViewBag.ChildMenu = "employee";
            return View();
        }

        // ADD: Employee
        public ActionResult Add(int? employeeid)
        {
            ViewBag.ParentMenu = "EmployeeMaster";
            ViewBag.ChildMenu = "employee";
            //List<Branch> branches = null;
            //BranchBuisness branchBuisness = null;
            EmployeeBuisness employeeBuisness = null;
            List<EmployeeType> employeeTypes = null;
            Employee employee = null;
            long userid = 11;
            try
            {
                //branchBuisness = new BranchBuisness();
                employeeBuisness = new EmployeeBuisness();
                //branches = new List<Branch>();
                employeeTypes = new List<EmployeeType>();
                employee = new Employee();
                if(employeeid>0)
                {
                    employee = employeeBuisness.GetAllEmployee(employeeid, userid).SingleOrDefault();
                }
                //branches = branchBuisness.GetBranch(0, 11).Where(i => i.IsActive == true && i.IsDeleted == false).ToList();
                employeeTypes = employeeBuisness.GetEmployeeType(0, 11).Where(i => i.IsActive == 1 && i.IsDeleted == 0).ToList();
                //ViewBag.Branch = new SelectList(branches, "BranchId", "BranchName", 0);
                ViewBag.EmployeeType = new SelectList(employeeTypes, "EmployeeTypeId", "EmployeeTypeName", 0);


            }
            catch(Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to render Employee Add page", ex, "Add");
            }
            return View(employee);
        }

        // ADD: Employee : POST
        [HttpPost]
        public ActionResult Add(Employee employee)
        {
            ViewBag.ParentMenu = "EmployeeMaster";
            ViewBag.ChildMenu = "employee";
            //List<Branch> branches = null;
            //BranchBuisness branchBuyisness = null;
            EmployeeBuisness employeeBuisness = null;
            List<EmployeeType> employeeTypes = null;
            Status status = null;
            try
            {
                //branchBuyisness = new BranchBuisness();
                //branches = new List<Branch>();
                employeeTypes = new List<EmployeeType>();
                employeeBuisness = new EmployeeBuisness();
                status = new Status();
                //branches = branchBuyisness.GetBranch(0, 11).Where(i => i.IsActive ==true && i.IsDeleted == false).ToList();
                employeeTypes = employeeBuisness.GetEmployeeType(0, 11).Where(i => i.IsActive == 1 && i.IsDeleted == 0).ToList();
                //ViewBag.Branch = new SelectList(branches, "BranchId", "BranchName", 0);
                ViewBag.EmployeeType = new SelectList(employeeTypes, "EmployeeTypeId", "EmployeeTypeName", 0);
                employee.CompanyId = 12;
                //// string uid = Session["UserBE"].UserId;
                long userid = 11;
                var errors = ModelState.Values.SelectMany(v => v.Errors);
                if (ModelState.IsValid)
                {
                    status = employeeBuisness.AddUpdateEmployee(employee, userid);
                   // ViewBag.Status = status;
                    TempData["SuccessFlag"] = status.SuccessFlag;
                    TempData["Msg"] = status.Msg;
                    if (status.SuccessFlag == "1")
                    {
                        return RedirectToAction("List");
                    }
                    else
                    {
                        return View(employee);

                    }
                    
                }
                else
                {
                    return View(employee);
                }

            }
            catch(Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to post Employee Add page", ex, "Add");
                return View(employee);
            }
            
        }

        [HttpPost]
        public JsonResult LoadEmployee(int employeeId)
        {
            ViewBag.ParentMenu = "EmployeeMaster";
            ViewBag.ChildMenu = "employee";
            //// string uid = Session["UserBE"].UserId;
            long userid = 11;
            List<Employee> employees = null;
            EmployeeBuisness employeeBuisness = null; 

            try
            {
                employees = new List<Employee>();
                employeeBuisness = new EmployeeBuisness();
                // AppLogger.Instance.Info(this.GetType(), "Fetch the list of all category", "LoadCategory");aa
                employees = employeeBuisness.GetAllEmployee(employeeId, userid).Where(i => i.IsDeleted == false).ToList() ;
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch employee list", ex, "LoadEmployee");
            }

            return Json(employees, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult LoadEmployeeLoanDetails(int employeeId)
        {
            ViewBag.ParentMenu = "EmployeeMaster";
            ViewBag.ChildMenu = "employee";
            //// string uid = Session["UserBE"].UserId;
            long userid = 11;
            List<LoanStatus> loanStatuses = null;
            EmployeeBuisness employeeBuisness = null;

            try
            {
                loanStatuses = new List<LoanStatus>();
                employeeBuisness = new EmployeeBuisness();
                // AppLogger.Instance.Info(this.GetType(), "Fetch the list of all category", "LoadCategory");aa
                loanStatuses = employeeBuisness.GetLaonDetails(employeeId, userid).ToList();
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch employee list", ex, "LoadEmployee");
            }

            return Json(loanStatuses, JsonRequestBehavior.AllowGet);
        }
        


        protected List<Employee> GetAllEmployee()
        {
            ViewBag.ParentMenu = "EmployeeMaster";
            ViewBag.ChildMenu = "employee";
            long userid = 11;
            List<Employee> employees = null;
            EmployeeBuisness employeeBuisness = null;
            try
            {
                employees = new List<Employee>();
                employeeBuisness = new EmployeeBuisness();
                // AppLogger.Instance.Info(this.GetType(), "Fetch the list of all category", "LoadCategory");aa
                employees = employeeBuisness.GetAllEmployee(0, userid).Where(i => i.IsDeleted == false).ToList();
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch employee list", ex, "LoadEmployee");
            }

            return employees;

        }

        public ActionResult Delete(int employeeid)
        {
            ViewBag.ParentMenu = "EmployeeMaster";
            ViewBag.ChildMenu = "employee";
            EmployeeBuisness employeeBuisness = null;
            Status status = null;
            long userid = 11;
            try
            {
                employeeBuisness = new EmployeeBuisness();
                status = new Status();
                status =  employeeBuisness.DeleteEmployee(employeeid, userid);
                TempData["SuccessFlag"] = status.SuccessFlag;
                TempData["Msg"] = status.Msg;

            }
            catch(Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to delete employee ", ex, "Delete");
            }
            return RedirectToAction("List"); 
        }

        public ActionResult LoadPDFForEmployee()
        {
            try
            {
                ViewBag.ParentMenu = "EmployeeMaster";
                ViewBag.ChildMenu = "employee";
                MemoryStream workStream = new MemoryStream();
                StringBuilder status = new StringBuilder("");
                DateTime dTime = DateTime.Now;
                //file name to be created   
                string strPDFFileName = string.Format("EmployeeList" + dTime.ToString("yyyyMMdd") + "-" + ".pdf");
                iTextSharp.text.Document doc = new iTextSharp.text.Document();
                //      doc.SetMargins(0f, 0f, 0f, 0f);
                //Create PDF Table with 5 columns  
                PdfPTable tableLayout = new PdfPTable(9);
                doc.SetMargins(0f, 0f, 0f, 0f);
                //Create PDF Table  

                //file will created in this path  
                string strAttachment = Server.MapPath("~/Downloads/" + strPDFFileName);


                 PdfWriter.GetInstance(doc, workStream).CloseStream = false;
                doc.Open();

                float[] headers = { 50, 40, 50, 35, 50, 50, 35,40,30 }; //Header Widths  
                tableLayout.SetWidths(headers); //Set the pdf headers  
                tableLayout.WidthPercentage = 90; //Set the PDF File witdh percentage  
                tableLayout.HeaderRows = 1;

                tableLayout.AddCell(new PdfPCell(new Phrase(" "))
                {
                    Colspan = 12,
                    Border = 0,
                    PaddingBottom = 5,
                    BackgroundColor = new iTextSharp.text.BaseColor(250, 250, 252)
                });

                tableLayout.AddCell(new PdfPCell(new Phrase("Employee List", new Font(Font.FontFamily.HELVETICA, 14, 1, new iTextSharp.text.BaseColor(250, 250, 252))))
                {
                    Colspan = 12,
                    Border = 0,
                    PaddingBottom = 5,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    BackgroundColor = new iTextSharp.text.BaseColor(15, 77, 144)
                });

                ////Add header  
                
                BL.Utilities.Common.AddCellToHeader(tableLayout, "Name");
                BL.Utilities.Common.AddCellToHeader(tableLayout, "Mobile");
                BL.Utilities.Common.AddCellToHeader(tableLayout, "Email");
                BL.Utilities.Common.AddCellToHeader(tableLayout, "Address");
                BL.Utilities.Common.AddCellToHeader(tableLayout, "Type");
                BL.Utilities.Common.AddCellToHeader(tableLayout, "Kacha Salary");
                BL.Utilities.Common.AddCellToHeader(tableLayout, "Paka salary");
                BL.Utilities.Common.AddCellToHeader(tableLayout, "PF");
                BL.Utilities.Common.AddCellToHeader(tableLayout, "Status");

                //Get the table data
                List<Employee> employees = new List<Employee>();
                employees = GetAllEmployee();
                foreach (var item in employees)
                {

                    BL.Utilities.Common.AddCellToBody(tableLayout, item.FirstName.ToString()+" "+item.LastName.ToString(), 1);
                    BL.Utilities.Common.AddCellToBody(tableLayout, item.Mobile, 2);
                    BL.Utilities.Common.AddCellToBody(tableLayout, item.Email, 3);
                    BL.Utilities.Common.AddCellToBody(tableLayout, item.CAddress, 4);
                    BL.Utilities.Common.AddCellToBody(tableLayout, item.EmployeeTypeName, 5);
                    BL.Utilities.Common.AddCellToBody(tableLayout, item.KACHA_SALARY.ToString(), 6);
                    BL.Utilities.Common.AddCellToBody(tableLayout, item.PAKA_SALARY.ToString(), 7); ;
                    BL.Utilities.Common.AddCellToBody(tableLayout, item.PF.ToString(), 8);
                    if(item.IsActive==true)
                    {
                        BL.Utilities.Common.AddCellToBody(tableLayout, "Active", 9);
                    }
                    else
                    {
                        BL.Utilities.Common.AddCellToBody(tableLayout, "In Active", 9);
                    }
                    

                }

                //Add Content to PDF   
                doc.Add(tableLayout);

                // Closing the document  
                doc.Close();

                byte[] byteInfo = workStream.ToArray();
                workStream.Write(byteInfo, 0, byteInfo.Length);
                workStream.Position = 0;

                Session["DownloadPDF_EmployeeList"] = byteInfo;
                return Json("", JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return View("Exception", ex.Message);
            }
        }

        public ActionResult DownloadPDFForEmployee()
        {
            if (Session["DownloadPDF_EmployeeList"] != null)
            {
                byte[] data = Session["DownloadPDF_EmployeeList"] as byte[];
                return File(data, "application/pdf", "EmployeeList.pdf");
            }
            else
            {
                return View("Exception");

            }
        }

        public ActionResult Salary()
        {
            ViewBag.ParentMenu = "EmployeeMaster";
            ViewBag.ChildMenu = "salarymanagement";
            UtilityBuisness utilityBuisness = new UtilityBuisness();
            DDLList data = new DDLList();
            List<DDLList> ddlList = null;
            List<Employee> employees = null;
            EmployeeBuisness employeeBuisness = new EmployeeBuisness();
            try
            {
                data.tableName = "tbl_Year";
                data.Value = "Id";
                data.Text = "Year";
                ddlList = utilityBuisness.GetDDLList(data);
                ViewBag.YearList = new SelectList(ddlList, "Text", "Text", 0);
                data.tableName = "tbl_Month";
                data.Value = "Id";
                data.Text = "Name";
                ddlList = utilityBuisness.GetDDLList(data);
                ViewBag.MonthList = new SelectList(ddlList, "value", "Text", 0);
                employees = employeeBuisness.GetAllEmployee(0, 1).Where(i => i.IsDeleted == false).ToList();
                ViewBag.EmployeeList = new SelectList(employees, "EmployeeId", "Fullname", 0);
                return View();
            }
            catch(Exception ex)
            {
                return View();
            }
            
            
        }

        public ActionResult EmployeeLoan()
        {
            ViewBag.ParentMenu = "EmployeeMaster";
            ViewBag.ChildMenu = "employeeloanlist";
            long userid = 11;
            List<Employee> employees = null;
            EmployeeBuisness employeeBuisness = null;
            try
            {
                employees = new List<Employee>();
                employeeBuisness = new EmployeeBuisness();
                // AppLogger.Instance.Info(this.GetType(), "Fetch the list of all category", "LoadCategory");aa
                employees = employeeBuisness.GetAllEmployee(0, userid).Where(i => i.IsDeleted == false).ToList();
                ViewBag.Employee = new SelectList(employees, "EmployeeId", "Fullname", 0);
            }
            catch(Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch employee list", ex, "EmployeeLoan");
            }

            return View();
        }

        [HttpPost]
        public ActionResult EmployeeLoan(LoanManagement loanManagement)
        {
            ViewBag.ParentMenu = "EmployeeMaster";
            ViewBag.ChildMenu = "employeeloanlist";
            long userid = 11;
            List<Employee> employees = null;
            EmployeeBuisness employeeBuisness = null;
            try
            {
                employees = new List<Employee>();
                employeeBuisness = new EmployeeBuisness();
                // AppLogger.Instance.Info(this.GetType(), "Fetch the list of all category", "LoadCategory");aa
                employees = employeeBuisness.GetAllEmployee(0, userid).Where(i => i.IsDeleted == false).ToList();
                ViewBag.Employee = new SelectList(employees, "EmployeeId", "Fullname", 0);
                if (ModelState.IsValid)
                {
                    EmployeeBuisness buisness = new EmployeeBuisness();
                    Status status = new Status();
                    status = buisness.AddUpdateLoan(loanManagement, 1);
                    TempData["SuccessFlag"] = status.SuccessFlag;
                    TempData["Msg"] = status.Msg;
                    if (status.SuccessFlag == "1")
                    {
                        return RedirectToAction("EmployeeLoanList");
                    }
                    else
                    {
                        return View(loanManagement);
                    }
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch employee list", ex, "EmployeeLoan");
            }

            return View();
        }

        public ActionResult EmployeeLoanList()
        {
            ViewBag.ParentMenu = "EmployeeMaster";
            ViewBag.ChildMenu = "employeeloanlist";
            return View();
        }

        public ActionResult LoanList()
        {
            ViewBag.ParentMenu = "EmployeeMaster";
            ViewBag.ChildMenu = "LoanList";
            return View();
        }

        public ActionResult RePaymentList()
        {
            ViewBag.ParentMenu = "EmployeeMaster";
            ViewBag.ChildMenu = "RepaymentList";
            return View();
        }

        [HttpPost]
        public JsonResult LoadLoan(int BranchId)
        {
            ViewBag.ParentMenu = "EmployeeMaster";
            ViewBag.ChildMenu = "employee";
            long userid = 11;
            List<LoanManagement> loanManagements = null;
            EmployeeBuisness employeeBuisness = null;
            try
            {
                loanManagements = new List<LoanManagement>();
                employeeBuisness = new EmployeeBuisness();
                loanManagements = employeeBuisness.GetLoan();
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch loan list", ex, "loanManagements");
            }

            return Json(loanManagements, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetLoanList(int BranchId)
        {
            ViewBag.ParentMenu = "EmployeeMaster";
            ViewBag.ChildMenu = "employee";
            long userid = 11;
            List<LoanManagement> loanManagements = null;
            EmployeeBuisness employeeBuisness = null;
            try
            {
                loanManagements = new List<LoanManagement>();
                employeeBuisness = new EmployeeBuisness();
                loanManagements = employeeBuisness.GetLoanList(BranchId);
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch loan list", ex, "loanManagements");
            }

            return Json(loanManagements, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetRepaymentList(int BranchId)
        {
            ViewBag.ParentMenu = "EmployeeMaster";
            ViewBag.ChildMenu = "GetRepaymentList";
            long userid = 11;
            List<LoanManagement> loanManagements = null;
            EmployeeBuisness employeeBuisness = null;
            try
            {
                loanManagements = new List<LoanManagement>();
                employeeBuisness = new EmployeeBuisness();
                loanManagements = employeeBuisness.GetRepaymentList(BranchId);
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch loan list", ex, "loanManagements");
            }

            return Json(loanManagements, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult SalaryList(string Year,int? Month)
        {
            ViewBag.ParentMenu = "EmployeeMaster";
            ViewBag.ChildMenu = "employee";
            long userid = 11;
            List<EmployeeSalary> employees = null;
            EmployeeBuisness employeeBuisness = null;
            try
            {
                employees = new List<EmployeeSalary>();
                employeeBuisness = new EmployeeBuisness();
                employees = employeeBuisness.LoadSalaryList(Year,Month,0);
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch loan list", ex, "loanManagements");
            }

            return Json(employees, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Salary(EmployeeSalary employeeSalary)
        {
            ViewBag.ParentMenu = "EmployeeMaster";
            ViewBag.ChildMenu = "employee";
            
            long userid = 11;
            UtilityBuisness utilityBuisness = new UtilityBuisness();
            DDLList data = new DDLList();
            List<DDLList> ddlList = null;
            List<Employee> employees = null;
            //List<Employee> employees = null;
            EmployeeBuisness employeeBuisness = null;
            Status status = null;
            try
            {
                //employees = new List<Employee>();
                employeeBuisness = new EmployeeBuisness();
                status = new Status();
                data.tableName = "tbl_Year";
                data.Value = "Id";
                data.Text = "Year";
                ddlList = utilityBuisness.GetDDLList(data);
                ViewBag.YearList = new SelectList(ddlList, "Text", "Text", 0);
                data.tableName = "tbl_Month";
                data.Value = "Id";
                data.Text = "Name";
                ddlList = utilityBuisness.GetDDLList(data);
                ViewBag.MonthList = new SelectList(ddlList, "value", "Text", 0);
                employees = employeeBuisness.GetAllEmployee(0, 1).Where(i => i.IsDeleted == false).ToList();
                ViewBag.EmployeeList = new SelectList(employees, "EmployeeId", "Fullname", 0);
                if (employeeSalary.DueAmount < employeeSalary.LoanDeduction)
                {
                    TempData["SuccessFlag"] = 0;
                    TempData["Msg"] = "Loan deduction amount is greater than due amount";
                    return View(employeeSalary);
                }
                if(ModelState.IsValid)
                {
                    status = employeeBuisness.SalaryDisburse(employeeSalary);
                }
                TempData["SuccessFlag"] = status.SuccessFlag;
                TempData["Msg"] = status.Msg;
                if (status.SuccessFlag == "1")
                {
 
                    return View("EmployeeSalaryList");
                }
                else
                {
                    return View(employeeSalary);
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch loan SalaryDisburse", ex, "SalaryDisburse");
            }

            return Json(status, JsonRequestBehavior.AllowGet);
        }

        public ActionResult EmployeeSalaryList()
        {
            ViewBag.ParentMenu = "EmployeeMaster";
            ViewBag.ChildMenu = "EmployeeSalaryList";
            return View();
        }

        public ActionResult SalarySlip(int EmployeeId,int Month,int Year)
        {
            ViewBag.EmployeeId = EmployeeId;
            ViewBag.Month = Month;
            ViewBag.Year = Year;
            return View();
        }

        [HttpPost]
        public JsonResult SalarySlipDetails(string EmployeeId,string Month,string Year)
        {
            ViewBag.ParentMenu = "EmployeeMaster";
            ViewBag.ChildMenu = "employee";
            long userid = 11;
            List<EmployeeSalary> employees = null;
            EmployeeBuisness employeeBuisness = null;
            Status status = null;
            try
            {
                employees = new List<EmployeeSalary>();
                employeeBuisness = new EmployeeBuisness();
                status = new Status();
                employees = employeeBuisness.LoadSalaryList(Year, Convert.ToInt32(Month),Convert.ToInt32(EmployeeId));
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch loan SalaryDisburse", ex, "SalaryDisburse");
            }

            return Json(employees, JsonRequestBehavior.AllowGet);
        }

    }
}
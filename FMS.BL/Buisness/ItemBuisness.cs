﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FMS.BL.Utilities;

namespace FMS.BL.Buisness
{
   public class ItemBuisness : Common
    {
        #region get all Items
        public List<Item> GetItem(long? itemId, long UserID)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    if (itemId == 0)
                    {
                        itemId = null;
                    }
                    para.Add("@ItemId", itemId);
                    para.Add("@OperationId", 2);
                    para.Add("@UserId", UserID);
                    const string procName = "[dbo].[sp_DeleteSelectItem]";
                    connection.Open();
                    return connection.Query<Item>(procName, para, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Item", ex, "GetItem");
                return null;
            }

        }
        #endregion

        #region add upadte item
        public Status AddUpdateitem(Item item, long UserID)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    para.Add("@ItemId", item.ItemId);
                    para.Add("@ItemName", item.ItemName);
                    para.Add("@CategoryId", item.CategoryId);
                    para.Add("@GSTId", item.GSTId);
                    para.Add("@unitId", item.UnitId);
                    para.Add("@ItemDescription", item.ItemDescription);
                    para.Add("@IsActive", item.IsActive);
                    para.Add("@CreatedBy", UserID);
                    para.Add("@CompanyId", item.CompanyId);
                    const string procName = "[dbo].[sp_InsertUpdateItem]";
                    connection.Open();
                    return connection.Query<Status>(procName, para, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to insert or update item", ex, "AddUpdateitem");
                return null;
            }

        }
        #endregion

        #region delete Item
        public Status DeleteItem(long? itemidTodelete, long UserID)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    para.Add("@ItemId", itemidTodelete);
                    para.Add("@OperationId", 1);
                    para.Add("@UserId", UserID);
                    const string procName = "[dbo].[sp_DeleteSelectItem]";
                    connection.Open();
                    return connection.Query<Status>(procName, para, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to delete Item", ex, "DeleteItem");
                return null;
            }

        }
        #endregion

        #region sale and purchase
        public List<ItemMaster> GetItems(long? itemId, long UserID)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    if (itemId == 0)
                    {
                        itemId = null;
                    }
                    para.Add("@Id", itemId);
                    para.Add("@OperationId", 2);
                    const string procName = "[dbo].[sp_DeleteSelectItemMaster]";
                    connection.Open();
                    return connection.Query<ItemMaster>(procName, para, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Item", ex, "GetItem");
                return null;
            }

        }


        public Status AddUpdateitems(ItemMaster item, long UserID)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    para.Add("@Id", item.Id);
                    para.Add("@ItemName", item.ItemName);
                    para.Add("@CategoryId", item.CategoryId);
                    para.Add("@GST", item.GST);
                    para.Add("@unit", item.Unit);
                    para.Add("@Description", item.Description);
                    const string procName = "[dbo].[sp_InsertUpdateItemMaster]";
                    connection.Open();
                    return connection.Query<Status>(procName, para, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to insert or update item", ex, "AddUpdateitem");
                return null;
            }

        }

        public List<ItemMaster> GetItemsList(long? itemId, long UserID)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    if (itemId == 0)
                    {
                        itemId = null;
                    }
                    para.Add("@Id", itemId);
                    para.Add("@OperationId", 2);
                    const string procName = "[dbo].[sp_DeleteSelectItemMaster]";
                    connection.Open();
                    return connection.Query<ItemMaster>(procName, para, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Item", ex, "GetItem");
                return null;
            }



        }
        public Status DeleteItems(long? itemidTodelete, long UserID)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    para.Add("@Id", itemidTodelete);
                    para.Add("@OperationId", 1);
                    
                    const string procName = "[dbo].[sp_DeleteSelectItemMaster]";
                    connection.Open();
                    return connection.Query<Status>(procName, para, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to delete Item", ex, "DeleteItem");
                return null;
            }

        }
        #endregion
    }
}

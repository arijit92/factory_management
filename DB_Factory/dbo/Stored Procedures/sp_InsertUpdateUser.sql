﻿CREATE PROCEDURE  [dbo].[sp_InsertUpdateUser]
-- Add the parameters for the stored procedure here
 @Id [bigint] null
,@FullName [NVARCHAR](MAX)
,@UserId [NVARCHAR](50)
,@Address [NVARCHAR] (MAX)
,@Password [NVARCHAR] (MAX)
,@Phone [NVARCHAR] (50)
,@Email [NVARCHAR] (100)
,@Role [bigint]
,@IsActive [int] NULL
,@CreatedBy [bigint] NULL
,@CompanyId [bigint] NULL
AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
--update or insert checking
IF(@Id>0)
BEGIN
--Duplicate value check
IF EXISTS(SELECT * FROM tbl_User WITH(NOLOCK) WHERE (UserId = @UserId OR Phone = @Phone OR Email = @Email) AND Id != @Id)
BEGIN
SELECT 'Already exists.' as Msg,0 as SuccessFlag
END
ELSE
BEGIN
--update
UPDATE tbl_User
SET
FullName =UPPER(@FullName)
,UserId = @UserId
,Address =@Address
,Phone =@Phone
,Email =@Email
,Role = @Role
,IsActive =@IsActive
,UpdatedBy =@CreatedBy
,UpdatedOn      =GETDATE()
,CompanyId =@CompanyId
WHERE Id  =@Id
SELECT 'User updated successfully.' as Msg,1 as SuccessFlag
END
END
ELSE
BEGIN
--Duplicate value check
IF EXISTS (SELECT * FROM tbl_User WITH(NOLOCK) WHERE UserId = @UserId OR Phone = @Phone OR Email = @Email)
BEGIN
SELECT 'Already exists.' as Msg,0 as SuccessFlag
END
ELSE
BEGIN
--insert
INSERT INTO tbl_User
(
 FullName
,UserId
,Password
,Address
,Phone 
,Email 
,Role
,IsActive
,IsDeleted
,CreatedBy
,CreatedOn
,CompanyId
)
VALUES
(
UPPER(@FullName)
,@UserId
,@Password
,@Address
,@Phone
,@Email
,@Role
,@IsActive
,0
,@CreatedBy
,GETDATE()
,@CompanyId
)
SELECT 'User created successfully.' as Msg,1 as SuccessFlag
END
END
END TRY
BEGIN CATCH
SELECT ERROR_MESSAGE() as Msg,0 as SuccessFlag
RETURN
END CATCH
END
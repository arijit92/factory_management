﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using FMS.BL;
using FMS.BL.Buisness;
using FMS.BL.Utilities;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace FMS.Controllers
{
    public class WeightDetailsController : Controller
    {
        // GET: WeightDetails  
        public ActionResult List()
        {
            ViewBag.ParentMenu = "WeightMaster";
            ViewBag.ChildMenu = "weightDetails";
            return View();
        }

        // GET: WeightDetails  Invoice
        public ActionResult Invoice(int TransactionId)
        {
            ViewBag.ParentMenu = "WeightMaster";
            ViewBag.ChildMenu = "weightDetails";
            ViewBag.TransactionId = TransactionId;
            return View();
        }

        // ADD: WeightDetails
        [HttpGet]
        public ActionResult Add(int? TransactionId)
        {
            ViewBag.ParentMenu = "WeightMaster";
            ViewBag.ChildMenu = "weightDetails";
            List<Unit> units = null;
            UnitBuisness unitBuisness = null;

            WeightDetails weightDetails = null;
            WeightDetailsBuisness weightDetailsBuisness = null;
            long userid = 11;
            try
            {
                units = new List<Unit>();
                unitBuisness = new UnitBuisness();

                weightDetails = new WeightDetails();
                weightDetailsBuisness = new WeightDetailsBuisness();
                if (TransactionId > 0)
                {
                    weightDetails = weightDetailsBuisness.GetWeightDetails(TransactionId, userid).SingleOrDefault();
                    //weightDetails.First_Weight_Time = (weightDetails.First_Weight_Time).ToString("HH:mm");
                    //weightDetails.Time_Second_Weight = (weightDetails.Time_Second_Weight).ToString("HH:mm");
                    //dt.ToString("HH:mm");
                }
                units = unitBuisness.GetUnit(0, 11).Where(i => i.IsActive == true && i.IsDeleted == false).ToList();
                ViewBag.Unit = new SelectList(units, "UnitId", "UnitName", 0);

            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Weight Details list", ex, "WeightDetails");
            }
            return View(weightDetails);
        }

        //// ADD: Item : WeightDetails
        [HttpPost]
        public ActionResult Add(WeightDetails weightDetails)
        {
            ViewBag.ParentMenu = "WeightMaster";
            ViewBag.ChildMenu = "weightDetails";
            List<Unit> units = null;
            UnitBuisness unitBuisness = null;
            units = new List<Unit>();
            unitBuisness = new UnitBuisness();
            units = unitBuisness.GetUnit(0, 11).Where(i => i.IsActive == true && i.IsDeleted == false).ToList();
            ViewBag.Unit = new SelectList(units, "UnitId", "UnitName", 0);
            var errors = ViewData.ModelState.Where(n => n.Value.Errors.Count > 0).ToList();
            if (ModelState.IsValid)
            {
                WeightDetailsBuisness buisness = new WeightDetailsBuisness();
                Status status = new Status();
               // string firstdate = (weightDetails.Date_First_Weight).ToString("MM/dd/yyyy") + " " + carBooking.PickUpTime;
                //string firstdate = (weightDetails.Date_First_Weight).Value.ToString("dd/mm/yyyy") + " " +weightDetails.First_Weight_Time;
                //string seconddate = (weightDetails.Second_Weight_Time).Value.ToString("dd/mm/yyyy") + " " + weightDetails.Time_Second_Weight;
                //weightDetails.Date_First_Weight = Convert.ToDateTime(firstdate);
                //weightDetails.Second_Weight_Time = Convert.ToDateTime(seconddate);
                status = buisness.AddUpdateWeightDetails(weightDetails, 1);
                TempData["SuccessFlag"] = status.SuccessFlag;
                TempData["Msg"] = status.Msg;
                if (status.SuccessFlag == "1")
                {
                    return RedirectToAction("List");
                }
                else
                {
                    return View(weightDetails);
                }
            }
            return View(weightDetails);
        }


        [HttpPost]
        public JsonResult LoadWeightDetails(int TransactionId)
        {
            ViewBag.ParentMenu = "WeightMaster";
            ViewBag.ChildMenu = "weightDetails";
            long userid = 11;
            List<WeightDetails> weightDetails = null;
            WeightDetailsBuisness weightDetailsBuisness = null;
            try
            {
                weightDetails = new List<WeightDetails>();
                weightDetailsBuisness = new WeightDetailsBuisness();
                weightDetails = weightDetailsBuisness.GetWeightDetails(TransactionId, userid).Where(i => i.IsDeleted == false).ToList();
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Weight Details list", ex, "WeightDetails");
            }

            return Json(weightDetails, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(int TransactionId)
        {
            ViewBag.ParentMenu = "WeightMaster";
            ViewBag.ChildMenu = "weightDetails";
            WeightDetailsBuisness weightDetailsBuisness = null;
            Status status = null;
            long userid = 11;
            try
            {
                weightDetailsBuisness = new WeightDetailsBuisness();
                status = new Status();
                status = weightDetailsBuisness.DeleteWeightDetails(TransactionId, userid);
                TempData["SuccessFlag"] = status.SuccessFlag;
                TempData["Msg"] = status.Msg;
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to delete WeightDetails ", ex, "Delete");
            }
            return RedirectToAction("List");
        }

        public ActionResult LoadPDFForWeightDetails()
        {
            ViewBag.ParentMenu = "master";
            ViewBag.ChildMenu = "weightDetails";
            try
            {
                MemoryStream workStream = new MemoryStream();
                StringBuilder status = new StringBuilder("");
                DateTime dTime = DateTime.Now;
                //file name to be created   
                string strPDFFileName = string.Format("WeightDetailsList" + dTime.ToString("yyyyMMdd") + "-" + ".pdf");
                iTextSharp.text.Document doc = new iTextSharp.text.Document();
                //      doc.SetMargins(0f, 0f, 0f, 0f);
                //Create PDF Table with 5 columns  
                PdfPTable tableLayout = new PdfPTable(7);
                doc.SetMargins(0f, 0f, 0f, 0f);
                //Create PDF Table  

                //file will created in this path  
                string strAttachment = Server.MapPath("~/Downloads/" + strPDFFileName);


                PdfWriter.GetInstance(doc, workStream).CloseStream = false;
                doc.Open();

                float[] headers = { 50, 40, 50, 35, 50 ,40,50}; //Header Widths  
                tableLayout.SetWidths(headers); //Set the pdf headers  
                tableLayout.WidthPercentage = 90; //Set the PDF File witdh percentage  
                tableLayout.HeaderRows = 1;

                tableLayout.AddCell(new PdfPCell(new Phrase(" "))
                {
                    Colspan = 12,
                    Border = 0,
                    PaddingBottom = 5,
                    BackgroundColor = new iTextSharp.text.BaseColor(250, 250, 252)
                });

                tableLayout.AddCell(new PdfPCell(new Phrase("WeightDetails List", new Font(Font.FontFamily.HELVETICA, 14, 1, new iTextSharp.text.BaseColor(250, 250, 252))))
                {
                    Colspan = 12,
                    Border = 0,
                    PaddingBottom = 5,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    BackgroundColor = new iTextSharp.text.BaseColor(15, 77, 144)
                });

                ////Add header  

                BL.Utilities.Common.AddCellToHeader(tableLayout, "Customer Name");
                BL.Utilities.Common.AddCellToHeader(tableLayout, "Vehicle Number");
                BL.Utilities.Common.AddCellToHeader(tableLayout, "Product");
                BL.Utilities.Common.AddCellToHeader(tableLayout, "First weight");
                BL.Utilities.Common.AddCellToHeader(tableLayout, "Second weight");
                BL.Utilities.Common.AddCellToHeader(tableLayout, "Net weight");
                BL.Utilities.Common.AddCellToHeader(tableLayout, "Charges");


                //Get the table data
                List<WeightDetails> weightDetails = new List<WeightDetails>();
                weightDetails = GetAllweightDetails();
                foreach (var item in weightDetails)
                {

                    BL.Utilities.Common.AddCellToBody(tableLayout, item.CustomerName, 1);
                    BL.Utilities.Common.AddCellToBody(tableLayout, item.VehicleNo, 2);
                    BL.Utilities.Common.AddCellToBody(tableLayout, item.Product.ToString(), 3);
                    BL.Utilities.Common.AddCellToBody(tableLayout, item.First_weight.ToString()+" "+item.UnitName, 4);
                    BL.Utilities.Common.AddCellToBody(tableLayout, item.Second_weight.ToString() + " " + item.UnitName, 5);
                    BL.Utilities.Common.AddCellToBody(tableLayout, item.NetWeight.ToString() + " " + item.UnitName, 6);
                    BL.Utilities.Common.AddCellToBody(tableLayout, item.WeightCharges.ToString(), 7);

                }

                //Add Content to PDF   
                doc.Add(tableLayout);

                // Closing the document  
                doc.Close();

                byte[] byteInfo = workStream.ToArray();
                workStream.Write(byteInfo, 0, byteInfo.Length);
                workStream.Position = 0;

                Session["DownloadPDF_WeightDetailsList"] = byteInfo;
                return Json("", JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return View("Exception", ex.Message);
            }

          
        }

        public ActionResult DownloadPDFForWeightDetails()
        {
            ViewBag.ParentMenu = "master";
            ViewBag.ChildMenu = "weightDetails";
            if (Session["DownloadPDF_WeightDetailsList"] != null)
            {
                byte[] data = Session["DownloadPDF_WeightDetailsList"] as byte[];
                return File(data, "application/pdf", "WeightDetailsList.pdf");
            }
            else
            {
                return View("Exception");

            }
        }

        protected List<WeightDetails> GetAllweightDetails()
        {
            long userid = 11;
            List<WeightDetails> weightDetails = null;
            WeightDetailsBuisness weightDetailsBuisness = null;
            try
            {
                weightDetails = new List<WeightDetails>();
                weightDetailsBuisness = new WeightDetailsBuisness();
                weightDetails = weightDetailsBuisness.GetWeightDetails(0, userid).Where(i => i.IsDeleted == false).ToList();
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Weight Details list", ex, "WeightDetails");
            }

            return weightDetails;

        }

    }
}
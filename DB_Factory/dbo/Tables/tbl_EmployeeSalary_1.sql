﻿CREATE TABLE [dbo].[tbl_EmployeeSalary] (
    [Id]               INT             IDENTITY (1, 1) NOT NULL,
    [Year]             NVARCHAR (4)    NOT NULL,
    [Month]            INT             NOT NULL,
    [EmployeeId]       INT             NOT NULL,
    [IsPaid]           BIT             NULL,
    [Paka_Salary]      DECIMAL (18, 2) NULL,
    [Kacha_Salary]     DECIMAL (18, 2) NULL,
    [LoanAmount]       DECIMAL (18, 2) NULL,
    [DueAmount]        DECIMAL (18, 2) NULL,
    [LoanDeduction]    DECIMAL (18, 2) NULL,
    [PF]               DECIMAL (18, 2) NULL,
    [Bonus]            DECIMAL (18, 2) NULL,
    [BonusRemarks]     NVARCHAR (50)   NULL,
    [Deduction]        DECIMAL (18, 2) NULL,
    [DeductionRemarks] NVARCHAR (50)   NULL,
    [Date]             DATETIME        NOT NULL
);


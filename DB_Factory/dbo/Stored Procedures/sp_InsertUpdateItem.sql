﻿CREATE PROCEDURE  [dbo].[sp_InsertUpdateItem]
-- Add the parameters for the stored procedure here
 @ItemId [bigint]
,@ItemName [nvarchar](100)
,@CategoryId [bigint]
,@GSTId [bigint] NULL
,@unitId [bigint] NULL
,@ItemDescription [nvarchar](max) NULL
,@IsActive [int] NULL
,@CreatedBy [bigint] NULL
,@CompanyId [bigint] NULL
AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
--update or insert checking
IF(@ItemId>0)
BEGIN
--Duplicate value check
IF EXISTS(SELECT * FROM tbl_Item WITH(NOLOCK) WHERE ItemName = @ItemName AND ItemId != @ItemId)
BEGIN
SELECT 'Already exists.' as Msg,0 as SuccessFlag
END
ELSE
BEGIN
--update
UPDATE tbl_Item
SET
ItemName =UPPER(@ItemName)
,ItemDescription =@ItemDescription
,CategoryId = @CategoryId
,GSTId = @GSTId
,UnitId = @unitId
,IsActive =@IsActive
,UpdatedBy =@CreatedBy
,UpdatedOn      =GETDATE()
,CompanyId =@CompanyId
WHERE ItemId =@ItemId
SELECT 'Item updated successfully.' as Msg,1 as SuccessFlag
END
END
ELSE
BEGIN
--Duplicate value check
IF EXISTS (SELECT * FROM tbl_Item WITH(NOLOCK) WHERE ItemName = @ItemName)
BEGIN
SELECT 'Already exists.' as Msg,0 as SuccessFlag
END
ELSE
BEGIN
--insert
INSERT INTO tbl_Item
(
 ItemName
,ItemDescription
,CategoryId
,GSTId
,UnitId
,IsActive
,IsDeleted
,CreatedBy
,CreatedOn
,CompanyId
)
VALUES
(
UPPER(@ItemName)
,@ItemDescription
,@CategoryId
,@GSTId
,@unitId
,@IsActive
,0
,@CreatedBy
,GETDATE()
,@CompanyId
)
SELECT 'Item created successfully.' as Msg,1 as SuccessFlag
END
END
END TRY
BEGIN CATCH
SELECT 'Sorry something went wrong, unable to process,' as Msg,0 as SuccessFlag
RETURN
END CATCH
END
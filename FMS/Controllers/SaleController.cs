﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FMS.BL;
using FMS.BL.Buisness;
using FMS.BL.Utilities;

namespace FMS.Controllers
{
    public class SaleController : Controller
    {
        // GET: Sale
        public ActionResult SaleStockEntry()
        {
            ViewBag.ParentMenu = "production";
            ViewBag.ChildMenu = "SaleStockEntry";
            List<ItemMaster> itemMasters = new List<ItemMaster>();
            ItemBuisness itemBuisness = new ItemBuisness();
            itemMasters = itemBuisness.GetItems(0, 1).Where(i => i.CategoryName == "PURCHASE" && i.ItemName != "Vitamin").ToList();
            ViewBag.ItemList = new SelectList(itemMasters, "Id", "DropdownValue", 0);
            return View();
        }

        [HttpPost]
        public ActionResult SaleStockEntry(ProductionStockEntry productionStockEntry)
        {
            ViewBag.ParentMenu = "production";
            ViewBag.ChildMenu = "SaleStockEntry";
            List<ItemMaster> itemMasters = new List<ItemMaster>();
            ItemBuisness itemBuisness = new ItemBuisness();
            itemMasters = itemBuisness.GetItems(0, 1).Where(i => i.CategoryName == "PURCHASE" && i.ItemName != "Vitamin").ToList();
            ViewBag.ItemList = new SelectList(itemMasters, "Id", "DropdownValue", 0);
            Status status = new Status();
            SaleBuisness saleBuisness = new SaleBuisness();
            if (ModelState.IsValid)
            {
                status = saleBuisness.AddUpdateProductionStockEntryDetails(productionStockEntry);
                TempData["SuccessFlag"] = status.SuccessFlag;
                TempData["Msg"] = status.Msg;
                if (status.SuccessFlag == "1")
                {
                    return RedirectToAction("SaleStockList");
                }
                else
                {
                    return View(productionStockEntry);

                }
            }
            else
            {
                return View(productionStockEntry);
            }
        }

        public ActionResult SaleStockList()
        {
            ViewBag.ParentMenu = "production";
            ViewBag.ChildMenu = "SaleStockList";
            return View();
        }

        [HttpPost]
        public JsonResult GetSaleStockDetails()
        {
            long userid = 11;
            List<ProductionStockEntry> productionStockEntries = null;
            SaleBuisness buisness = null;
            try
            {
                productionStockEntries = new List<ProductionStockEntry>();
                buisness = new SaleBuisness();
                productionStockEntries = buisness.GetSaleStockDetails().ToList();
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Role list", ex, "Role");
            }

            return Json(productionStockEntries, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetSaleStockCount()
        {
            long userid = 11;
            List<FlourBranRefStock> flourBranRefStocks = null;
            SaleBuisness buisness = null;
            try
            {
                flourBranRefStocks = new List<FlourBranRefStock>();
                buisness = new SaleBuisness();
                flourBranRefStocks = buisness.GetSaleStockCount().ToList();
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Role list", ex, "Role");
            }

            return Json(flourBranRefStocks, JsonRequestBehavior.AllowGet);
        }



        // sale entry
        // sale entry
        public ActionResult SaleEntry()
        {
            ViewBag.ParentMenu = "sales";
            ViewBag.ChildMenu = "SaleEntry";
            List<Packet> packets = null;
            packets = new List<Packet>();
            PacketBuisness packetBuisness = new PacketBuisness();
            packets = packetBuisness.GetPacketDetails().ToList();
            Sales sales = new Sales();
            UtilityBuisness utilityBuisness = new UtilityBuisness();
            DDLList data = new DDLList();
            List<DDLList> ddlList = null;
            List<ItemMaster> itemMasters = new List<ItemMaster>();
            List<SupplierType> supplierTypes = new List<SupplierType>();
            List<Customer> customers = new List<Customer>();
            ItemBuisness itemBuisness = new ItemBuisness();
            SupplierTypeBuisness supplierTypeBuisness = new SupplierTypeBuisness();
            CustomerBuisness customerBuisness = new CustomerBuisness();
            itemMasters = itemBuisness.GetItems(0, 1).Where(i => i.CategoryId == 2).ToList();
            supplierTypes = supplierTypeBuisness.GetSupplierType(0, 1);
            customers = customerBuisness.GetCustomer(0, 1);
            ViewBag.Packet = new SelectList(packets, "PacketId", "Description", 0);
            ViewBag.ItemList = new SelectList(itemMasters, "Id", "DropdownValue", 0);
            ViewBag.SupplierType = new SelectList(supplierTypes, "SupplierTypeId", "SupplierTypeName", 0);
            ViewBag.Supplier = new SelectList(customers, "CustomerId", "PointOfPersonName", 0);
            data.tableName = "tbl_PaymentMode";
            data.Value = "ModeId";
            data.Text = "ModeName";
            ddlList = utilityBuisness.GetDDLList(data);
            ViewBag.Mode = new SelectList(ddlList, "value", "Text", 0);
            data.tableName = "tbl_Bank";
            data.Value = "Id";
            data.Text = "BankName";
            ddlList = utilityBuisness.GetDDLList(data);
            ViewBag.Bank = new SelectList(ddlList, "Text", "Text", 0);

            return View();

        }

        [HttpPost]
        public ActionResult SaleEntry(Sales sales)
        {
            ViewBag.ParentMenu = "sales";
            ViewBag.ChildMenu = "SaleEntry";
            List<Packet> packets = null;
            packets = new List<Packet>();
            PacketBuisness packetBuisness = new PacketBuisness();
            packets = packetBuisness.GetPacketDetails().ToList();
            UtilityBuisness utilityBuisness = new UtilityBuisness();
            DDLList data = new DDLList();
            List<DDLList> ddlList = null;
            List<ItemMaster> itemMasters = new List<ItemMaster>();
            List<SupplierType> supplierTypes = new List<SupplierType>();
            List<Supplier> suppliers = new List<Supplier>();
            ItemBuisness itemBuisness = new ItemBuisness();
            SupplierTypeBuisness supplierTypeBuisness = new SupplierTypeBuisness();
            SupplierBuisness supplierBuisness = new SupplierBuisness();
            SaleBuisness saleBuisness = new SaleBuisness();
            Status status = new Status();
            itemMasters = itemBuisness.GetItems(0, 1).Where(i => i.CategoryId == 4).ToList();
            supplierTypes = supplierTypeBuisness.GetSupplierType(0, 1);
            suppliers = supplierBuisness.GetSupplier(0, 1);
            ViewBag.Packet = new SelectList(packets, "PacketId", "Description", 0);
            ViewBag.ItemList = new SelectList(itemMasters, "Id", "DropdownValue", 0);
            ViewBag.SupplierType = new SelectList(supplierTypes, "SupplierTypeId", "SupplierTypeName", 0);
            ViewBag.Supplier = new SelectList(suppliers, "SupplierId", "PointOfPersonName", 0);
            data.tableName = "tbl_PaymentMode";
            data.Value = "ModeId";
            data.Text = "ModeName";
            ddlList = utilityBuisness.GetDDLList(data);
            ViewBag.Mode = new SelectList(ddlList, "value", "Text", 0);
            data.tableName = "tbl_Bank";
            data.Value = "Id";
            data.Text = "BankName";
            ddlList = utilityBuisness.GetDDLList(data);
            ViewBag.Bank = new SelectList(ddlList, "Text", "Text", 0);
            if (ModelState.IsValid)
            {
                status = saleBuisness.AddUpdateSaleDetails(sales);
                TempData["SuccessFlag"] = status.SuccessFlag;
                TempData["Msg"] = status.Msg;
                if (status.SuccessFlag == "1")
                {
                    return RedirectToAction("SalesList");
                }
                else
                {
                    return View(sales);
                }
            }
            else
            {
                return View(sales);
            }

        }

        public ActionResult SalesList()
        {
            ViewBag.ParentMenu = "sales";
            ViewBag.ChildMenu = "SalesList";
            return View();
        }

        [HttpPost]
        public JsonResult GetSaleDetails()
        {
            long userid = 11;
            List<Sales> sales = null;
            SaleBuisness buisness = null;
            try
            {
                sales = new List<Sales>();
                buisness = new SaleBuisness();
                sales = buisness.GetSaleDetails().ToList();
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Role list", ex, "Role");
            }

            return Json(sales, JsonRequestBehavior.AllowGet);
        }


    }
}
﻿CREATE TABLE [dbo].[tbl_Alert] (
    [AlertId]          BIGINT         IDENTITY (1, 1) NOT NULL,
    [AlertName]        NVARCHAR (50)  NOT NULL,
    [AlertDescription] NVARCHAR (MAX) NOT NULL,
    [IssueDate]        DATETIME       NOT NULL,
    [NextRenewalDate]  DATETIME       NOT NULL,
    [IsActive]         INT            NULL,
    [IsDeleted]        INT            NULL,
    [CreatedBy]        BIGINT         NULL,
    [CreatedOn]        DATETIME       NULL,
    [UpdatedBy]        BIGINT         NULL,
    [UpdatedOn]        DATETIME       NULL,
    [CompanyId]        BIGINT         NULL
);




﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FMS.BL.Utilities;

namespace FMS.BL.Buisness
{
  public  class EmployeeBuisness : Common
    {
        #region ---get all employeetype---
        public List<EmployeeType> GetEmployeeType(long? EmployeeTypeId, long UserID)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    if (EmployeeTypeId == 0)
                    {
                        EmployeeTypeId = null;
                    }
                    para.Add("@EmployeeTypeId", EmployeeTypeId);
                    para.Add("@OperationId", 2);
                    para.Add("@UserId", UserID);
                    const string procName = "[dbo].[sp_DeleteSelectEmployeeType]";
                    connection.Open();
                    return connection.Query<EmployeeType>(procName, para, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch employee type", ex, "GetEmployeeType");
                return null;
            }

        }
        #endregion

        #region ---add upadte employee
        public Status AddUpdateEmployee(Employee employee, long UserID)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    para.Add("@EmployeeId", employee.EmployeeId);
                    para.Add("@FirstName", employee.FirstName);
                    para.Add("@LastName", employee.LastName);
                    para.Add("@Mobile", employee.Mobile);
                    para.Add("@Email", employee.Email);
                    para.Add("@CAddress", employee.CAddress);
                    para.Add("@EmployeeTypeId", employee.EmployeeTypeId);
                    para.Add("@BranchId", employee.BranchId);
                    para.Add("@PAKA_SALARY", employee.PAKA_SALARY);
                    para.Add("@KACHA_SALARY", employee.KACHA_SALARY);
                    para.Add("@PF", employee.PF);
                    para.Add("@PF", employee.PF);
                    para.Add("@IsActive", employee.IsActive);
                    para.Add("@CreatedBy", UserID);
                    para.Add("@CompanyId", employee.CompanyId);
                    const string procName = "[dbo].[sp_InsertUpdateEmployee]";
                    connection.Open();
                    return connection.Query<Status>(procName, para, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to insert or update employee", ex, "AddUpdateEmployee");
                return null;
            }

        }
        #endregion

        #region  ---get all employee
        public List<Employee> GetAllEmployee(long? emloyeeId, long userID)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    if (emloyeeId == 0)
                    {
                        emloyeeId = null;
                    }
                    para.Add("@EmployeeId", emloyeeId);
                    para.Add("@OperationId", 2);
                    para.Add("@UserId", userID);
                    const string procName = "[dbo].[sp_DeleteSelectEmployee]";
                    connection.Open();
                    return connection.Query<Employee>(procName, para, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch employee", ex, "GetAllEmployee");
                return null;
            }

        }
        #endregion

        #region ---delete employee
        public Status DeleteEmployee(long? employeeidTodelete, long UserID)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    para.Add("@EmployeeId", employeeidTodelete);
                    para.Add("@OperationId", 1);
                    para.Add("@UserId", UserID);
                    const string procName = "[dbo].[sp_DeleteSelectEmployee]";
                    connection.Open();
                    return connection.Query<Status>(procName, para, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to delete employee", ex, "DeleteEmployee");
                return null;
            }

        }
        #endregion

        #region Loan
        public Status AddUpdateLoan(LoanManagement loanManagement, long UserID)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    para.Add("@BranchId", loanManagement.BranchId);
                    para.Add("@EmployeeId", loanManagement.EmployeeId);
                    para.Add("@Amount", loanManagement.Amount);
                    para.Add("@CreatedBy", UserID);
                    const string procName = "[dbo].[sp_InsertUpdateLoan]";
                    connection.Open();
                    return connection.Query<Status>(procName, para, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to insert or update loan", ex, "AddUpdateLoan");
                return null;
            }

        }

        public List<LoanManagement> GetLoan()
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    
                    const string procName = "[dbo].[sp_SelectLoan]";
                    connection.Open();
                    return connection.Query<LoanManagement>(procName, para, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch employee type", ex, "GetEmployeeType");
                return null;
            }

        }

        public List<LoanManagement> GetLoanList(int? employeeid)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    if (employeeid == 0)
                        employeeid = null;
                    var para = new DynamicParameters();
                    para.Add("@EmployeeId", employeeid);
                    const string procName = "[dbo].[sp_SelectLoanList]";
                    connection.Open();
                    return connection.Query<LoanManagement>(procName, para, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch employee laon", ex, "GetLoanList");
                return null;
            }

        }


        public List<LoanManagement> GetRepaymentList(int? employeeid)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    if (employeeid == 0)
                        employeeid = null;
                    var para = new DynamicParameters();
                    para.Add("@EmployeeId", employeeid);
                    const string procName = "[dbo].[sp_SelectRepaymentList]";
                    connection.Open();
                    return connection.Query<LoanManagement>(procName, para, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch employee laon", ex, "GetLoanList");
                return null;
            }

        }

        public List<EmployeeSalary> LoadSalaryList(string Year,int? Month,int EmployeeId)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    if (Year == "")
                        Year = null;
                    para.Add("@EmployeeId", EmployeeId);
                    para.Add("@Year", Year);
                    para.Add("@Month", Month);
                    const string procName = "[dbo].[sp_SelectEmployeeSalary]";
                    connection.Open();
                    return connection.Query<EmployeeSalary>(procName, para, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch LoadSalaryList", ex, "LoadSalaryList");
                return null;
            }

        }

        public Status SalaryDisburse(EmployeeSalary employeeSalary)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    para.Add("@Year", employeeSalary.Year);
                    para.Add("@Month", employeeSalary.Month);
                    para.Add("@EmployeeId", employeeSalary.EmployeeId);
                    para.Add("@Kacha_Salary", employeeSalary.Kacha_Salary);
                    para.Add("@Paka_Salary", employeeSalary.Paka_Salary);
                    para.Add("@PF", employeeSalary.PF);
                    para.Add("@Bonus", employeeSalary.Bonus);
                    para.Add("@BonusRemarks", employeeSalary.BonusRemarks);
                    para.Add("@Deduction", employeeSalary.Deduction);
                    para.Add("@DeductionRemarks", employeeSalary.DeductionRemarks);
                    para.Add("@LoanAmount", employeeSalary.LoanAmount);
                    para.Add("@DueAmount", employeeSalary.DueAmount);
                    para.Add("@LoanDeduction", employeeSalary.LoanDeduction);
                    para.Add("@Gross", employeeSalary.Gross);
                    const string procName = "[dbo].[sp_SalaryDisburse]";
                    connection.Open();
                    return connection.Query<Status>(procName, para, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to insert or update SalaryDisburse", ex, "SalaryDisburse");
                return null;
            }

        }

        public List<LoanStatus> GetLaonDetails(long? emloyeeId, long userID)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    if (emloyeeId == 0)
                    {
                        emloyeeId = null;
                    }
                    para.Add("@EmployeeId", emloyeeId);
                    const string procName = "[dbo].[sp_LoanStatus]";
                    connection.Open();
                    return connection.Query<LoanStatus>(procName, para, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch employee", ex, "GetAllEmployee");
                return null;
            }

        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FMS.BL;
using FMS.BL.Buisness;
using FMS.BL.Utilities;

namespace FMS.Controllers
{
    public class ProductionController : Controller
    {
        // GET: Production
        public ActionResult ProductionEntry()
        {
            ViewBag.ParentMenu = "production";
            ViewBag.ChildMenu = "ProductionEntry";
            ProdEntry prodEntry = new ProdEntry();
            List<ItemMaster> itemMasters = new List<ItemMaster>();
            ItemBuisness itemBuisness = new ItemBuisness();
            itemMasters = itemBuisness.GetItems(0, 1).Where(i => i.CategoryName == "PURCHASE" && i.ItemName !="Vitamin").ToList();
            ViewBag.ItemList = new SelectList(itemMasters, "Id", "DropdownValue", 0);
            return View();
        }

        [HttpPost]
        public ActionResult ProductionEntry(ProdEntry prodEntry)
        {
            ViewBag.ParentMenu = "production";
            ViewBag.ChildMenu = "ProductionEntry";
            List<ItemMaster> itemMasters = new List<ItemMaster>();
            ItemBuisness itemBuisness = new ItemBuisness();
            itemMasters = itemBuisness.GetItems(0, 1).Where(i => i.CategoryName == "PURCHASE" && i.ItemName != "Vitamin").ToList();
            ViewBag.ItemList = new SelectList(itemMasters, "Id", "DropdownValue", 0);
            Status status = new Status();
            ProductionBuissness productionBuissness = new ProductionBuissness();
            if (ModelState.IsValid)
            {
                status = productionBuissness.AddUpdateProductionEntryDetails(prodEntry);
                TempData["SuccessFlag"] = status.SuccessFlag;
                TempData["Msg"] = status.Msg;
                if (status.SuccessFlag == "1")
                {
                    return RedirectToAction("ProductionStockList");
                }
                else
                {
                    return View(prodEntry);

                }
            }
            else
            {
                return View(prodEntry);
            }


        }

        public ActionResult ProductionStockList()
        {

            ViewBag.ParentMenu = "production";
            ViewBag.ChildMenu = "ProductionStockList";
            return View();
        }

        [HttpPost]
        public JsonResult ProductionStockCount()
        {
            long userid = 11;
            List<ProdEntry> itemMasters = null;
            ProductionBuissness buisness = null;
            try
            {
                itemMasters = new List<ProdEntry>();
                buisness = new ProductionBuissness();
                itemMasters = buisness.GetProductionStockCount().ToList();
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Role list", ex, "Role");
            }

            return Json(itemMasters, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetProductionStockDetails()
        {
            long userid = 11;
            List<ProdEntry> itemMasters = null;
            ProductionBuissness buisness = null;
            try
            {
                itemMasters = new List<ProdEntry>();
                buisness = new ProductionBuissness();
                itemMasters = buisness.GetProductionStockDetails().ToList();
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Role list", ex, "Role");
            }

            return Json(itemMasters, JsonRequestBehavior.AllowGet);
        }
    }
}
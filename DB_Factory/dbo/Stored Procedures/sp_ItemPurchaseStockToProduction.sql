﻿CREATE PROCEDURE  [dbo].[sp_ItemPurchaseStockToProduction]
-- Add the parameters for the stored procedure here
 @Id int = null
,@packetId     [bigint]
,@BostaQuantity  decimal(18,2)
,@Weight         decimal(18,2)
AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
Declare @StockBostaQuantity decimal
SET @StockBostaQuantity = (SELECT BostaQuantity from tbl_ItemPurchaseStock where PacketId = @packetId)
IF(@StockBostaQuantity>=@BostaQuantity)
BEGIN
--check existing or not
if(@Id>0)
--if exixting update the record
BEGIN
UPDATE  tbl_ItemProductionStockTransaction
SET  	
 PacketId      	  = 	@PacketId      	
,BostaQuantity 	  = 	@BostaQuantity 	
,Weight        	  = 	@Weight        		
,Date			  = 	GETDATE()		
 where Id = @Id

END
ELSE
BEGIN 
--if not exixts insert the record
INSERT INTO tbl_ItemProductionStockTransaction
(    
 PacketId      
,BostaQuantity 
,Weight        
,Date
)
values
(    
 @PacketId      
,@BostaQuantity 
,@Weight        
,GETDATE()
)
END
Declare @TotalBosta decimal(18,2),@TotalWeight decimal(18,2)
SET @TotalBosta = (SELECT SUM(BostaQuantity) from tbl_ItemProductionStockTransaction where PacketId = @PacketId)
SET @TotalWeight = (SELECT SUM(Weight) from tbl_ItemProductionStockTransaction where PacketId = @PacketId)
--check the main stock table
IF EXISTS(select ProductionStockId from tbl_ItemProductionStock where PacketId = @PacketId)
BEGIN
UPDATE tbl_ItemProductionStock
SET 
BostaQuantity = @TotalBosta,
TotalWeight = @TotalWeight,
UpdatedOn = GETDATE()
WHERE PacketId = @PacketId
END
ELSE
BEGIN
INSERT INTO tbl_ItemProductionStock
(
PacketId
,TotalWeight
,BostaQuantity
,UpdatedOn
)
VALUES
(
@PacketId
,@TotalWeight
,@TotalBosta
,GETDATE()
)
END
SELECT 'Production Stock updated successfully,' as Msg,1 as SuccessFlag
UPDATE tbl_ItemPurchaseStock
SET
BostaQuantity = BostaQuantity-@BostaQuantity
,TotalWeight = TotalWeight - @Weight
where PacketId = @packetId
END
ELSE
BEGIN
SELECT 'Stock not available,' as Msg,0 as SuccessFlag
END
END TRY
BEGIN CATCH
SELECT 'Sorry something went wrong, unable to process,' as Msg,0 as SuccessFlag
RETURN
END CATCH
END
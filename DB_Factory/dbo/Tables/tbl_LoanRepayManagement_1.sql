﻿CREATE TABLE [dbo].[tbl_LoanRepayManagement] (
    [Id]         INT             IDENTITY (1, 1) NOT NULL,
    [Year]       NCHAR (4)       NULL,
    [Month]      INT             NULL,
    [EmployeeId] INT             NOT NULL,
    [Amount]     DECIMAL (18, 2) NOT NULL,
    [DueAmount]  DECIMAL (18, 2) NULL,
    [Date]       DATETIME        NULL
);


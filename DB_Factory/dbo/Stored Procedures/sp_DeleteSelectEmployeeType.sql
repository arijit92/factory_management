﻿CREATE PROCEDURE  [dbo].[sp_DeleteSelectEmployeeType]
-- Add the parameters for the stored procedure here
 @EmployeeTypeId [bigint] NULL
,@OperationId [bigint]  --1=delete,2=Select
,@UserId [bigint] NULL

AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
IF(@OperationId  = 1 AND @EmployeeTypeId >0)
BEGIN
UPDATE tbl_EmployeeType
SET
IsDeleted = 1
   ,UpdatedBy = @UserId
   ,UpdatedOn = GETDATE()
    WHERE EmployeeTypeId = @EmployeeTypeId
SELECT 'Customer Type deleted successfully.' as Msg,1 as SuccessFlag
END
ELSE
BEGIN
SELECT * FROM tbl_EmployeeType WITH(NOLOCK)
WHERE EmployeeTypeId = ISNULL(@EmployeeTypeId,EmployeeTypeId)
END

END TRY
BEGIN CATCH
SELECT 'Sorry something went wrong, unable to process,' as Msg,0 as SuccessFlag
RETURN
END CATCH
END
﻿CREATE PROCEDURE  [dbo].[sp_DeleteSelectProductCategory]
-- Add the parameters for the stored procedure here
@CategoryId [bigint] NULL
,@OperationId [bigint]  --1=delete,2=Select
,@UserId [bigint] NULL

AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
IF(@OperationId  = 1 AND @CategoryId >0)
BEGIN
UPDATE tbl_ProductCategory
SET
IsDeleted = 1
   ,UpdatedBy = @UserId
   ,UpdatedOn = GETDATE()
    WHERE CategoryId = @CategoryId
SELECT 'Product category deleted successfully.' as Msg,1 as SuccessFlag
END
ELSE
BEGIN
SELECT * FROM tbl_ProductCategory WITH(NOLOCK)
WHERE CategoryId = ISNULL(@CategoryId,CategoryId) AND IsDeleted = 0
ORDER BY CategoryId DESC;
END

END TRY
BEGIN CATCH
SELECT 'Sorry something went wrong, unable to process,' as Msg,0 as SuccessFlag
RETURN
END CATCH
END

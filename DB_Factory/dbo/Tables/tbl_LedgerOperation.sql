﻿CREATE TABLE [dbo].[tbl_LedgerOperation] (
    [LedgerOperationId]    INT            NULL,
    [LedgerId]             INT            NULL,
    [OperationName]        NVARCHAR (50)  NULL,
    [OperationDescription] NVARCHAR (MAX) NULL
);


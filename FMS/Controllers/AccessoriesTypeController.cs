﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FMS.BL;
using FMS.BL.Buisness;
using FMS.BL.Utilities;

namespace FMS.Controllers
{
    public class AccessoriesTypeController : Controller
    {
        // GET: AccessoriesType
        public ActionResult List()
        {
            ViewBag.ParentMenu = "master";
            ViewBag.ChildMenu = "accessoriestype";
            return View();
        }

        [HttpGet]
        public ActionResult Add(int? accessoriestypeid)
        {
            ViewBag.ParentMenu = "master";
            ViewBag.ChildMenu = "accessoriestype";
            AccessoriesType accessories = null;
            AccessoriesBuisness accessoriesBuisness = null;
            List<AccessoriesType> accessoriesTypes = null;
            long userid = 11;
            try
            {
                accessories = new AccessoriesType();
                accessoriesBuisness = new AccessoriesBuisness();
                accessoriesTypes = new List<AccessoriesType>();
                if (accessoriestypeid > 0)
                {
                    accessories = accessoriesBuisness.GetAccessoriesType(accessoriestypeid, userid).SingleOrDefault();
                }
               // accessoriesTypes = accessoriesBuisness.GetAccessoriesType(0, 1).ToList();
               // ViewBag.AccessoriesTypes = new SelectList(accessoriesTypes, "AccessoriesTypeId", "AccessoriesTypeName", 0);


            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Accessories list", ex, "Accessories");
            }
            return View(accessories);
        }

        [HttpPost]
        public ActionResult Add(AccessoriesType accessories)
        {
            ViewBag.ParentMenu = "master";
            ViewBag.ChildMenu = "accessoriestype";
            if (ModelState.IsValid)
            {
                AccessoriesBuisness buisness = new AccessoriesBuisness();
                Status status = new Status();
               // List<AccessoriesType> accessoriesTypes = new List<AccessoriesType>();
                status = buisness.AddUpdateAccessoriesType(accessories);
                TempData["SuccessFlag"] = status.SuccessFlag;
                TempData["Msg"] = status.Msg;
                if (status.SuccessFlag == "1")
                {
                    return RedirectToAction("List");
                }
                else
                {
                    return View(accessories);
                }
            }
            return View(accessories);
        }

        [HttpPost]
        public JsonResult LoadAccessoriesType(int AccessoriesTypeId)
        {
            long userid = 11;
            List<AccessoriesType> accessories = null;
            AccessoriesBuisness accessoriesBuisness = null;
            try
            {
                accessories = new List<AccessoriesType>();
                accessoriesBuisness = new AccessoriesBuisness();
                accessories = accessoriesBuisness.GetAccessoriesType(AccessoriesTypeId, userid).Where(i => i.IsDeleted == false).ToList();
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Accessories list", ex, "Accessories");
            }

            return Json(accessories, JsonRequestBehavior.AllowGet);
        }
    }
}
﻿CREATE TABLE [dbo].[tblPurchaseStockWastage] (
    [Id]         BIGINT          IDENTITY (1, 1) NOT NULL,
    [ItemTypeId] BIGINT          NOT NULL,
    [Weight]     DECIMAL (18, 2) NOT NULL,
    [StockDate]  DATETIME        NOT NULL,
    CONSTRAINT [PK_tblPurchaseStockWastage] PRIMARY KEY CLUSTERED ([Id] ASC)
);


﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FMS.BL.Utilities;


namespace FMS.BL.Buisness
{
    public class CategoryBuisness : Common
    {
        #region category region

        public Status AddUpdateProductCategory(ProductCategory pmodel, long UserID)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    para.Add("@CategoryId", pmodel.CategoryId);
                    para.Add("@CategoryName", pmodel.CategoryName);
                    para.Add("@Description", pmodel.Description);
                    para.Add("@IsActive", pmodel.IsActive);
                    para.Add("@CreatedBy", UserID);
                    para.Add("@CompanyId", pmodel.CompanyId);
                    const string procName = "[dbo].[sp_InsertUpdateProductCategory]";
                    connection.Open();
                    return connection.Query<Status>(procName, para, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to insert or update category", ex, "AddUpdateProductCategory");
                return null;
            }

        }
        public List<ProductCategory> GetProductCategory(long? CategoryId, long UserID)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    if (CategoryId == 0)
                    {
                        CategoryId = null;
                    }
                    para.Add("@CategoryId", CategoryId);
                    para.Add("@OperationId", 2);
                    para.Add("@UserId", UserID);
                    const string procName = "[dbo].[sp_DeleteSelectProductCategory]";
                    connection.Open();
                    return connection.Query<ProductCategory>(procName, para, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch category", ex, "GetProductCategory");
                return null;
            }

        }

        public Status DeleteProductCategory(long? CategoryId, long UserID)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    para.Add("@CategoryId", CategoryId);
                    para.Add("@OperationId", 1);
                    para.Add("@UserId", UserID);
                    const string procName = "[dbo].[sp_DeleteSelectProductCategory]";
                    connection.Open();
                    return connection.Query<Status>(procName, para, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to delete category", ex, "DeleteProductCategory");
                return null;
            }

        }
        #endregion
    }
}

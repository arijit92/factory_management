using System;
using System.Collections.Generic;
using System.Text;

namespace FMS.BL.Functions
{
    public enum Month
    {
        NONE =0,
        January =1,
        February=2,
        March=3,
        April=4,
        May=5,
        June=6,
        July=7,
        August=8,
        September=9,
        October=10,
        November=11,
        December =12
    }
    public enum DateFormat
    {
        MM_dd_yyyy_d=1,
        dddd_MMMMdd_yyyy_D =2,
        dddd_MMMMdd_yyyy_HH_mm_f=3,
        dddd_MMMMdd_yyyy_HH_mm_ss_F=4,
        MM_dd_yyyy_HH_mm_g=5,
        MM_dd_yyyy_HH_mm_ss_G=6,
        MMMMdd_m=7,
        Ddd_ddMMM_yyyy_HH_mm_ss_GMT_r =8,
        yyyy_MM_dd_HH_mm_ss_s=9,
        yyyy_MM_dd_HH_mm_ss_GMT_S=10,
        HH_mm_t=11,
        HH_mm_ss_T=12,
        yyyy_MM_dd_HH_mm_ss_u=13,
        dddd_MMMM_dd_yyyy_HH_mm_ss_U=14,
        MMMM_yyyy_Y=15 ,
        DD_MM_YYYY=16,
        DD_MM_YY=17,
        WK_MM_DD_YYYY = 18 //Monday August 23, 2010
    }	
	
    public class DateFunctions
    {
        public static bool IsDate(string vDate)
        {
            try
            {
                DateTime dt = Convert.ToDateTime(vDate);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static string PadZero(int iValue)
        {
            if (iValue < 10)
            {
                return "0" + Convert.ToString(iValue);
            }
            else
            {
                return Convert.ToString(iValue);
            }

        }

        public static string ShortMonth(Month month)
        {
            switch (month)
            {
                case Month.January:
                    return "Jan";
                case Month.February:
                    return "Feb";
                case Month.March:
                    return "Mar";
                case Month.April:
                    return "Apr";
                case Month.May:
                    return "May";
                case Month.June:
                    return "Jun";
                case Month.July:
                    return "Jul";
                case Month.August:
                    return "Aug";
                case Month.September:
                    return "Sep";
                case Month.October:
                    return "Oct";
                case Month.November:
                    return "Nov";
                case Month.December:
                    return "Dec";
            }
            return "Jan";
        }

        public static string LongMonth(Month month)
        {
            switch (month)
            {
                case Month.January:
                    return "January";
                case Month.February:
                    return "February";
                case Month.March:
                    return "March";
                case Month.April:
                    return "April";
                case Month.May:
                    return "May";
                case Month.June:
                    return "June";
                case Month.July:
                    return "July";
                case Month.August:
                    return "August";
                case Month.September:
                    return "September";
                case Month.October:
                    return "October";
                case Month.November:
                    return "November";
                case Month.December:
                    return "December";
            }
            return "January";
        }

        public static string FormatDate(object vDate,DateFormat df)
        {
            if (vDate.ToString().Trim().Length==0) return "";
            DateTime bDate = Convert.ToDateTime(vDate);
            switch (df)
            {
                case DateFormat.DD_MM_YY:
                    return PadZero(bDate.Day) + "/" + PadZero(bDate.Month) + "/" + bDate.Year.ToString().Substring(2);
                case DateFormat.DD_MM_YYYY:
                    return PadZero(bDate.Day) + "/" + PadZero(bDate.Month) + "/" + bDate.Year.ToString();
                case DateFormat.Ddd_ddMMM_yyyy_HH_mm_ss_GMT_r:
                    return String.Format("{0:r}", bDate);
                case DateFormat.dddd_MMMM_dd_yyyy_HH_mm_ss_U:
                    return String.Format("{0:U}", bDate);
                case DateFormat.dddd_MMMMdd_yyyy_D:
                    return String.Format("{0:D}", bDate);
                case DateFormat.dddd_MMMMdd_yyyy_HH_mm_f :
                    return String.Format("{0:f}", bDate);
                case DateFormat.dddd_MMMMdd_yyyy_HH_mm_ss_F :
                    return String.Format("{0:F}", bDate);
                case DateFormat.HH_mm_ss_T :
                    return String.Format("{0:T}", bDate);
                case DateFormat.HH_mm_t :
                    return String.Format("{0:t}", bDate);
                case DateFormat.MM_dd_yyyy_d :
                    return String.Format("{0:d}", bDate);
                case DateFormat.MM_dd_yyyy_HH_mm_g :
                    return String.Format("{0:g}", bDate);
                case DateFormat.MM_dd_yyyy_HH_mm_ss_G :
                    return String.Format("{0:G}", bDate);
                case DateFormat.MMMM_yyyy_Y :
                    return String.Format("{0:Y}", bDate);
                case DateFormat.MMMMdd_m :
                    return String.Format("{0:m}", bDate);
                case DateFormat.yyyy_MM_dd_HH_mm_ss_GMT_S :
                    return String.Format("{0:S}", bDate);
                case DateFormat.yyyy_MM_dd_HH_mm_ss_s :
                    return String.Format("{0:s}", bDate);
                case DateFormat.yyyy_MM_dd_HH_mm_ss_u :
                    return String.Format("{0:u}", bDate);
                case DateFormat.WK_MM_DD_YYYY:
                    return String.Format("{0:D}", bDate);      
            }
            return  String.Format("{0:d}", bDate);
        }

        //public static string FormatDateDDMMYYYY(string vDate)
        //{            
        //    string rValue = "";
            
        //    if (IsDate(vDate) == true)
        //    {
        //        return FormatDateDDMMYYYY(Convert.ToDateTime(vDate));
        //    }
        //    return rValue;
        //}

        //public static string FormatDateDDMMYYYY(object vDate)
        //{
        //    DateTime bDate = (DateTime)vDate;
           
        //    bMonth = PadZero(bDate.Month);
        //    bDay = PadZero(bDate.Day);
        //    bYear = PadZero(bDate.Year);
        //    return  rValue = bDay + "/" + bMonth + "/" + bYear;
            
        //}


        //public static string FormatDateMMDDYYYY(string vDate)
        //{
        //    string bMonth, bDay, bYear;
        //    string rValue = "";
        //    DateTime bDate;
        //    if (IsDate(vDate) == true)
        //    {
        //        bDate = Convert.ToDateTime(vDate);
        //        bMonth = PadZero(bDate.Month);
        //        bDay = PadZero(bDate.Day);
        //        bYear = PadZero(bDate.Year);
        //        rValue = bMonth + "/" + bDay + "/" + bYear;
        //    }
        //    return rValue;
        //}

        //public static string FormatDateDDMMYYYY(string vDate)
        //{
        //    string bMonth, bDay, bYear;
        //    string rValue = "";
        //    DateTime bDate;
        //    if (IsDate(vDate) == true)
        //    {
        //        bDate = Convert.ToDateTime(vDate);
        //        bMonth = Convert.ToString(bDate.Month);
        //        bDay = Convert.ToString(bDate.Day);
        //        bYear = Convert.ToString(bDate.Year);
        //        rValue = bDay + "/" + bMonth + "/" + bYear;
        //    }
        //    return rValue;
        //}

        //public static string FormatDateMMDDYYYY(string vDate)
        //{
        //    string bMonth, bDay, bYear;
        //    string rValue = "";
        //    DateTime bDate;
        //    if (IsDate(vDate) == true)
        //    {
        //        bDate = Convert.ToDateTime(vDate);
        //        bMonth = Convert.ToString(bDate.Month);
        //        bDay = Convert.ToString(bDate.Day);
        //        bYear = Convert.ToString(bDate.Year);
        //        rValue = bMonth + "/" + bDay + "/" + bYear;
        //    }
        //    return rValue;
        //}

       

        public static string DisplayFormatToDB(DateTime  vDate)
        {
            return vDate.Year.ToString() + "-" + PadZero(vDate.Month) + "-" + PadZero(vDate.Day);       
        }

        public static string GetDisplayFormat()
        {
            return "DD/MM/YYYY"; //Configuration.AppSettings("DateDisplayFormat");
        }

        public static long DateDiff(DateInterval Interval, System.DateTime StartDate, System.DateTime EndDate)
        {
            long lngDateDiffValue = 0;
            System.TimeSpan TS = new System.TimeSpan(EndDate.Ticks - StartDate.Ticks);
            switch (Interval)
            {
                case DateInterval.Day:
                    lngDateDiffValue = (long)TS.Days;
                    break;
                case DateInterval.Hour:
                    lngDateDiffValue = (long)TS.TotalHours;
                    break;
                case DateInterval.Minute:
                    lngDateDiffValue = (long)TS.TotalMinutes;
                    break;
                case DateInterval.Month:
                    lngDateDiffValue = (long)(TS.Days / 30);
                    break;
                case DateInterval.Quarter:
                    lngDateDiffValue = (long)((TS.Days / 30) / 3);
                    break;
                case DateInterval.Second:
                    lngDateDiffValue = (long)TS.TotalSeconds;
                    break;
                case DateInterval.Week:
                    lngDateDiffValue = (long)(TS.Days / 7);
                    break;
                case DateInterval.Year:
                    lngDateDiffValue = (long)(TS.Days / 365);
                    break;
            }
            return (lngDateDiffValue);
        }
       
    }

    public enum DateInterval
    {
        Second, Minute, Hour, Day, Week, Month, Quarter, Year
    }
}

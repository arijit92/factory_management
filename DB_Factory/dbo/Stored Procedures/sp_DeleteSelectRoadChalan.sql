﻿CREATE PROCEDURE  [dbo].[sp_DeleteSelectRoadChalan]
-- Add the parameters for the stored procedure here
 @RoadChalanId [bigint] NULL
,@OperationId [bigint]  --1=delete,2=Select


AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
IF(@OperationId  = 1 AND @RoadChalanId >0)
BEGIN
UPDATE tbl_RoadChalan
SET
IsDeleted = 1
  
   ,Date = GETDATE()
    WHERE Id = @RoadChalanId
SELECT 'Road Chalan deleted successfully.' as Msg,1 as SuccessFlag
END
ELSE
BEGIN
SELECT * FROM tbl_RoadChalan WITH(NOLOCK)
WHERE Id = ISNULL(@RoadChalanId,Id)
END

END TRY
BEGIN CATCH
SELECT 'Sorry something went wrong, unable to process,' as Msg,0 as SuccessFlag
RETURN
END CATCH
END
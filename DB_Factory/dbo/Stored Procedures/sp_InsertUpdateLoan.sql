﻿CREATE PROCEDURE  [dbo].[sp_InsertUpdateLoan]
-- Add the parameters for the stored procedure here
 @BranchId [bigint] null
,@EmployeeId [bigint] null
,@Amount decimal
,@CreatedBy bigint
AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
--update or insert checking
INSERT INTO tbl_LoanManagement
(
 BranchId 
,EmployeeId           
,Amount           
,Date     
)
VALUES
(
 @BranchId           
,@EmployeeId           
,@Amount     
,GETDATE()
)
SELECT 'Loan added successfully,' as Msg,1 as SuccessFlag
END TRY
BEGIN CATCH
SELECT 'Sorry something went wrong, unable to process,' as Msg,0 as SuccessFlag
RETURN
END CATCH
END
﻿$(function () {
    LoadEmployee(0);
});

function DownloadPDF() {
    $.ajax({
        type: "POST",
       // url: "/AdminReports/ExportESCROWStatementToPDF?year=" + $('#Year').val() + "&month=" + $('#Month').val() + "&corporateName=" + $('#CorporateID').val(),
        url: "/Employee/LoadPDFForEmployee",
        cache: false,
        success: function (data) {
            window.location = '/Employee/DownloadPDFForEmployee';
        },
        error: function (data) {
          //  Materialize.toast("Something went wrong. ", 3000, 'rounded');
            toastr.error('Something went wrong.');
        }
    });
}



$(function () {
    $("#btnExport").click(function () {
        $("#hiddentable").table2excel({
            filename: "EmployeeList.xlsx"
        });
    });



    $("body").on("click", "#btnExporttoPDF", function () {
        html2canvas($('#hiddentable')[0], {
            onrendered: function (canvas) {
                var data = canvas.toDataURL();
                var docDefinition = {
                    content: [{
                        image: data,
                        width: 500
                    }]
                };
                pdfMake.createPdf(docDefinition).download("EmployeeList.pdf");
            }
        });
    });
});

function LoadEmployee(employeeid) {

    var requestData = {
        employeeId: employeeid
    };
    $.ajax({
        url: '/Employee/LoadEmployee',
        type: 'POST',
        data: JSON.stringify(requestData),
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        async: true,
        processData: true,
        cache: false,
        error: function (xhr) {
            alert('Error: ' + xhr.statusText);
        },
        success: function (result) {

            $('#tblEmployee').html("");
            $('#tblEmployeeHidden').html("");
            var tblHead = '<table class="table table-bordered table-striped" id="example1" cellspacing="0" cellpadding="4" align="Center" style="color:#333333;border-collapse:collapse;"><thead><tr style = "color:White;background-color:#1C5E55;font-weight:bold;" ><th scope="col">Name</th><th scope="col">Mobile</th><th scope="col">Email</th><th scope="col">Contact Address</th><th scope="col">Employee Type</th><th scope="col">Kacha salary</th><th scope="col">Paka Salary</th><th scope="col">PF</th>  <th scope="col">Status</th><th scope="col">Action</th></tr ></thead><tbody>';
            var tblHeadHidden = '<table class="table table-bordered table-striped" id="hiddentable" cellspacing="0" cellpadding="4" align="Center" style="color:#333333;border-collapse:collapse;"><thead><tr style = "color:White;background-color:#1C5E55;font-weight:bold;" ><th scope="col">Name</th><th scope="col">Mobile</th><th scope="col">Email</th><th scope="col">Contact Address</th><th scope="col">Employee Type</th><th scope="col">Kacha salary</th><th scope="col">Paka Salary</th><th scope="col">PF</th>  <th scope="col">Status</th></tr ></thead><tbody>';

            //alert(tblHead);
            var tbody = ""
            var tbodyHidden = ""
            for (var i = 0; i < result.length; i++) {
                var status = null;
                if (result[i].IsActive == 0) {
                    status = "Inactive"
                }
                else {
                    status = "Active"
                }
                if (result[i].Description == null) {
                    result[i].Description = "";
                }
                //  tbody = tbody + '<tbody><tr><td align="left" id=id_' + result[i].roleTitle + '>' + result[i].roleTitle + '</td><td id=id_' + result[i].Permissions + '>' + result[i].Permissions + '</td><td class="td-actions text-center"><a href="#" class="" id="btnCreateRole" onclick="EditRole(\'' + result[i].roleID + '\');"><i class="far fa-edit text-warning"></i> </a>&nbsp;<a href="#" class="" id="btnCreateRole" onclick="DeleteRole(\'' + result[i].roleID + '\')"><i class="fas fa-times text-danger"></i> </a></td></tr>';
                tbody = tbody + '<tr style="background-color:White;"><td>' + result[i].FirstName + ' ' + result[i].LastName + '</td ><td>' + result[i].Mobile + '</td><td>' + result[i].Email + '</td><td>' + result[i].CAddress + '</td><td>' + result[i].EmployeeTypeName + '</td><td>' + result[i].PAKA_SALARY + '</td><td>' + result[i].KACHA_SALARY + '</td><td>' + result[i].PF + '</td><td>' + status + '</td><td><a href="#" onclick="Delete(' + result[i].EmployeeId + ')"><i class="far fa-trash-alt"></i></i></a >&nbsp;&nbsp;<a href="#" onclick="Edit(' + result[i].EmployeeId + ');"><i class="fa fa-fw fa-edit" ></i></a ></td></tr >'
                tbodyHidden = tbodyHidden + '<tr style="background-color:White;"><td>' + result[i].FirstName + ' ' + result[i].LastName + '</td ><td>' + result[i].Mobile + '</td><td>' + result[i].Email + '</td><td>' + result[i].CAddress + '</td><td>' + result[i].EmployeeTypeName + '</td><td>' + result[i].PAKA_SALARY + '</td><td>' + result[i].KACHA_SALARY + '</td><td>' + result[i].PF + '</td><td>' + status + '</td></tr >'
            }
            $('#tblEmployee').html(tblHead + tbody + '</tbody></table>');
            $("#example1").DataTable();
            $('#tblEmployeeHidden').html(tblHeadHidden + tbodyHidden + '</tbody></table>');

        },
    });
}

function Edit(employeeid) {
    window.location.href = '/Employee/Add?employeeid=' + employeeid + '';
}

function Delete(employeeidTodelete) {
    
    bootbox.confirm("Are you sure you want to delete this employee?", function (confirm) {
        if (confirm == true) {
            window.location.href = '/Employee/Delete?employeeid=' + employeeidTodelete + '';
           // LoadEmployee(0);
        }


    })
}
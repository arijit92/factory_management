﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FMS.BL.Utilities;
namespace FMS.BL.Buisness
{
   public class StockBuisness : Common
    {
        public Status UpdateStock(StockMaster stockMaster)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    para.Add("@AccessoriesTypeId", stockMaster.AccessoriesTypeId);
                    para.Add("@AccessoriesId", stockMaster.AccessoriesId);
                    para.Add("@Remarks", stockMaster.Remarks);
                    para.Add("@Quantity", stockMaster.AvailableQuantity);
                    para.Add("@Type", stockMaster.Type);

                    const string procName = "[dbo].[sp_InsertUpdatetockMaster]";
                    connection.Open();
                    return connection.Query<Status>(procName, para, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to UpdateStock", ex, "UpdateStock");
                return null;
            }

        }

        public List<StockMaster> GetAvailableStocks(int? AccessoriesId)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    if(AccessoriesId==0)
                    {
                        AccessoriesId = null;
                    }
                    para.Add("@AccessoriesId", AccessoriesId);

                    const string procName = "[dbo].[sp_GetAvailableStock]";
                    connection.Open();
                    return connection.Query<StockMaster>(procName, para, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable GetAvailableStocks", ex, "GetAvailableStocks");
                return null;
            }

        }
    }
}

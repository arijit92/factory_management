using System;
using System.Collections;
using System.Text;
using System.Data;
using FMS.BL.Utilities;


namespace FMS.BL.Functions
{
    public class CommonFunctions
    {
        public static string GetColumnSeparator()
        {
            return "|";
        }

        public static string GetRowSeparator()
        {
            return ";";
        }

        public static string GetExceptionSeparator()
        {
            return "|";
        }

        public static string GetDelimeter()
        {
            return ",";
        }


        public static SortedList PopulateBooleanCombo(BoolanTextType boolanTextType, SortedList slCultureInfo)
        {
            SortedList sortedList = new SortedList();

            if (boolanTextType == BoolanTextType.TrueFalse)
            {
                sortedList.Add(slCultureInfo["True"], 1);
                sortedList.Add(slCultureInfo["False"], 0);
            }
            else if (boolanTextType == BoolanTextType.YesNo)
            {
                sortedList.Add(slCultureInfo["Yes"], 1);
                sortedList.Add(slCultureInfo["No"], 0);
            }
            return sortedList;
        }

        private static Hashtable BindToEnum(Type enumType)
        {
            string[] names = Enum.GetNames(enumType);
            Array values = Enum.GetValues(enumType);
            Hashtable ht = new Hashtable();
            for (int i = 0; i < names.Length; i++)
                ht.Add(names[i], (int)values.GetValue(i));
            return ht;
        }

        public static SortedList PopulateMonthCombo(MonthString monthString)
        {
            Hashtable hashtable = new Hashtable();
            SortedList sortedList = new SortedList();

            if (monthString == MonthString.LongMonth)
            {
                hashtable = BindToEnum(typeof(LongMonthString));
            }
            else if (monthString == MonthString.LongMonth)
            {
                hashtable = BindToEnum(typeof(ShortMonthString));
            }
            foreach (object key in hashtable.Keys)
            {
                sortedList.Add(key, hashtable[key]);
            }
            sortedList.TrimToSize();
            return sortedList;
        }

        public static String CreateChildString(DataTable dataTable)
        {
            return CreateChildString(dataTable, '|', ';');
        }

        public static String CreateChildString(DataTable dataTable, int[] fields, Fields field)
        {
            return CreateChildString(dataTable, '|', ';', fields, field);
        }

        private static String CreateChildString(DataTable dataTable, Char fieldSeparator, Char rowSeparator)
        {
            int[] fields = new int[0];
            return CreateChildString(dataTable, fieldSeparator, rowSeparator, fields, Fields.All);
        }

        private static String CreateChildString(DataTable dataTable, Char fieldSeparator, Char rowSeparator, int[] fields, Fields field)
        {
            StringBuilder stringBuilder = new StringBuilder();
            Object[] itemArray;
            bool fieldRequiredOrToBeEscaped = false;

            for (int rowCount = 0; rowCount < dataTable.Rows.Count; rowCount++)
            {
                itemArray = dataTable.Rows[rowCount].ItemArray;
                for (int fieldCount = 0; fieldCount < itemArray.Length; fieldCount++)
                {
                    if (fields.Length != 0)
                    {
                        for (int fieldRequiredOrToBeEscapedCount = 0; fieldRequiredOrToBeEscapedCount < fields.Length; fieldRequiredOrToBeEscapedCount++)
                        {
                            if (fields[fieldRequiredOrToBeEscapedCount] == fieldCount)
                            {
                                fieldRequiredOrToBeEscaped = true;
                                break;
                            }
                        }
                    }

                    if (field == Fields.Required)
                    {
                        if (fieldRequiredOrToBeEscaped)
                        {
                            stringBuilder.Append(itemArray[fieldCount].ToString().Trim());
                            if (fieldCount <(itemArray.Length))
                            {
                                stringBuilder.Append(fieldSeparator);
                            }
                        }
                        fieldRequiredOrToBeEscaped = false;
                    }
                    else if (field == Fields.Escape)
                    {
                        if (!fieldRequiredOrToBeEscaped)
                        {
                            stringBuilder.Append(itemArray[fieldCount].ToString().Trim());
                            if (fieldCount < (itemArray.Length))
                            {
                                stringBuilder.Append(fieldSeparator);
                            }
                        }
                        fieldRequiredOrToBeEscaped = false;
                    }
                    else
                    {
                        stringBuilder.Append(itemArray[fieldCount].ToString().Trim());
                        if (fieldCount < (itemArray.Length))
                        {
                            stringBuilder.Append(fieldSeparator);
                        }
                        fieldRequiredOrToBeEscaped = false;
                    }
                }
                stringBuilder.Append(rowSeparator);
            }
            return stringBuilder.ToString();
        }

        public static string GetDay(int iDayNo, int iMonthNo, int iYearNo)
        {
            int iRem = 0;
            string sDate = "";
            Math.DivRem(iMonthNo, 2, out iRem);
            if (iRem == 0)
            {
                if (iMonthNo == 2)
                {
                    int iRemYear = 0;
                    Math.DivRem(iYearNo, 4, out iRemYear);
                    if (iRemYear == 0)
                    {
                        if (iDayNo < 30)
                        {
                            DateTime dt = new DateTime(iYearNo, iMonthNo, iDayNo);
                            sDate = dt.DayOfWeek.ToString().Remove(3);
                        }
                    }
                    else
                    {
                        if (iDayNo < 29)
                        {
                            DateTime dt = new DateTime(iYearNo, iMonthNo, iDayNo);
                            sDate = dt.DayOfWeek.ToString().Remove(3);
                        }
                    }
                }
                else
                {
                    if (iDayNo < 31)
                    {
                        DateTime dt = new DateTime(iYearNo, iMonthNo, iDayNo);
                        sDate = dt.DayOfWeek.ToString().Remove(3);
                    }
                }
            }
            else
            {
                if (iDayNo < 32)
                {
                    DateTime dt = new DateTime(iYearNo, iMonthNo, iDayNo);
                    sDate = dt.DayOfWeek.ToString().Remove(3);
                }
            }
            return sDate;
        }

        public static string CreateMonthData(int iDayNo, int iMonthNo, int iYearNo)
        {
            string dayOfYear;
            dayOfYear = GetDay(iDayNo, iMonthNo, iYearNo);
            return dayOfYear;
        }
        public static DataTable CreateDataTable(int iColumnNo, int iMonthNo, int iYearNo)
        {
            DataTable dt = new DataTable();
            for (int i = 1; i <= iColumnNo; i++)
            {
                DataColumn dtCol = new DataColumn(System.String.Concat(i.ToString(), "</br>", CreateMonthData(i, iMonthNo, iYearNo)));
                dt.Columns.Add(dtCol);
            }
            dt.AcceptChanges();
            return dt;
        }

        public static DataTable ExtendDataTable(DataTable dtExtend, int iColumnNo)
        {
            int iMonthNo, iYearNo;

            iMonthNo = Convert.ToInt32(dtExtend.Rows[0][12]);
            iYearNo = Convert.ToInt32(dtExtend.Rows[0][13]);

            for (int i = 1; i <= iColumnNo; i++)
            {
                DataColumn dtCol = new DataColumn(System.String.Concat(i.ToString(), "</br>", CreateMonthData(i, iMonthNo, iYearNo)));
                dtExtend.Columns.Add(dtCol);
            }
            dtExtend.AcceptChanges();
            return dtExtend;
        }

        public static DataTable CreateYearDataTable(long iStartYear, long iEndYear)
        {
            DataTable dtYear = new DataTable();
            dtYear.Columns.Add("YearText");
            dtYear.Columns.Add("YearValue");
            DataRow nYearRow;
            long i = 0;
            for (i = iStartYear; i <= iEndYear; i++)
            {
                nYearRow = dtYear.NewRow();
                nYearRow["YearText"] = i;
                nYearRow["YearValue"] = i;
                dtYear.Rows.Add(nYearRow);
            }
            return dtYear;
        }
    }
}

﻿CREATE TABLE [dbo].[tbl_Module] (
    [ModuleId]       BIGINT         IDENTITY (1, 1) NOT NULL,
    [ModuleLevel]    INT            NOT NULL,
    [ModuleName]     NVARCHAR (100) NOT NULL,
    [SubURL]         NVARCHAR (MAX) NOT NULL,
    [SortOrder]      NVARCHAR (50)  NOT NULL,
    [ParentModId]    INT            NOT NULL,
    [IsDeleted]      INT            NOT NULL,
    [ControllerName] NVARCHAR (100) NULL
);


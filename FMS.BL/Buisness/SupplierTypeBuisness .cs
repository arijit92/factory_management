﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FMS.BL.Utilities;


namespace FMS.BL.Buisness
{
    public class SupplierTypeBuisness : Common
    {
        #region SupplierType region

        public Status AddUpdateSupplierType(SupplierType pmodel, long UserID)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    para.Add("@SupplierTypeId", pmodel.SupplierTypeId);
                    para.Add("@SupplierTypeName", pmodel.SupplierTypeName);
                    para.Add("@SupplierTypeDescription", pmodel.SupplierTypeDescription);
                    para.Add("@IsActive", Convert.ToInt32(pmodel.IsActive == false ? 0 : 1));
                    para.Add("@CreatedBy", UserID);
                    para.Add("@CompanyId", pmodel.CompanyId);
                    const string procName = "[dbo].[sp_InsertUpdateSupplierType]";
                    connection.Open();
                    return connection.Query<Status>(procName, para, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to insert or update SupplierType", ex, "AddUpdateSupplierType");
                return null;
            }

        }
        public List<SupplierType> GetSupplierType(long? SupplierTypeId, long UserID)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    if (SupplierTypeId == 0)
                    {
                        SupplierTypeId = null;
                    }
                    para.Add("@SupplierTypeId", SupplierTypeId);
                    para.Add("@OperationId", 2);
                    para.Add("@UserId", UserID);
                    const string procName = "[dbo].[sp_DeleteSelectSupplierType]";
                    connection.Open();
                    return connection.Query<SupplierType>(procName, para, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch SupplierType", ex, "GetSupplierType");
                return null;
            }

        }
        public Status DeleteSupplierType(long? SupplierTypeId, long UserID)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    para.Add("@SupplierTypeId", SupplierTypeId);
                    para.Add("@OperationId", 1);
                    para.Add("@UserId", UserID);
                    const string procName = "[dbo].[sp_DeleteSelectSupplierType]";
                    connection.Open();
                    return connection.Query<Status>(procName, para, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to delete SupplierType", ex, "DeleteSupplierType");
                return null;
            }

        }
        #endregion
    }
}

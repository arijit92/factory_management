﻿CREATE TABLE [dbo].[tbl_CustomerType] (
    [CustomerTypeId]          BIGINT         IDENTITY (1, 1) NOT NULL,
    [CustomerTypeName]        NVARCHAR (50)  NOT NULL,
    [CustomerTypeDescription] NVARCHAR (MAX) NULL,
    [IsActive]                INT            NULL,
    [IsDeleted]               INT            NULL,
    [CreatedBy]               BIGINT         NULL,
    [CreatedOn]               DATETIME       NULL,
    [UpdatedBy]               BIGINT         NULL,
    [UpdatedOn]               DATETIME       NULL,
    [CompanyId]               BIGINT         NULL
);




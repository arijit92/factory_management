﻿CREATE TABLE [dbo].[tblPurchaseEntry] (
    [Id]           BIGINT          IDENTITY (1, 1) NOT NULL,
    [ItemTypeId]   BIGINT          NOT NULL,
    [NoOfBosta]    BIGINT          NOT NULL,
    [Weight]       DECIMAL (18, 2) NOT NULL,
    [Price]        DECIMAL (18, 2) NOT NULL,
    [PurchaseDate] DATETIME        NOT NULL,
    CONSTRAINT [PK_tblPurchaseEntry] PRIMARY KEY CLUSTERED ([Id] ASC)
);


﻿CREATE PROCEDURE  [dbo].[sp_SelectOperationByLedger]
-- Add the parameters for the stored procedure here
 @LedgerId [bigint] NULL


AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
select * from tbl_OperationMaster where LedgerID = @LedgerId
select TTD.TransactionDetailsID,TTD.OperationID,TTD.LedgerID,TTD.Ammount,TOM.OperationName from tbl_TransactionDetails TTD inner join tbl_OperationMaster TOM on TOM.OperationID = TTD.OperationID
where TTD.LedgerID = @LedgerId


END TRY
BEGIN CATCH
SELECT 'Sorry something went wrong, unable to process,' as Msg,0 as SuccessFlag
RETURN
END CATCH
END
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FMS.BL;
using FMS.BL.Buisness;
using FMS.BL.Utilities;
namespace FMS.Controllers
{
    public class LabourWorkTypeController : Controller
    {
        // GET: LabourWorkType  
        public ActionResult List()
        {
            ViewBag.ParentMenu = "master";
            ViewBag.ChildMenu = "labourWorkType";
            return View();
        }

        [HttpGet]
        public ActionResult Add(int? labourWorkTypeid)
        {
            ViewBag.ParentMenu = "master";
            ViewBag.ChildMenu = "labourWorkType";
            LabourWorkType labourWorkType = null;
            LabourWorkTypeBuisness labourWorkTypeBuisness = null;
            long userid = 11;
            try
            {
                labourWorkType = new LabourWorkType();
                labourWorkTypeBuisness = new LabourWorkTypeBuisness();
                if (labourWorkTypeid > 0)
                {
                    labourWorkType = labourWorkTypeBuisness.GetLabourWorkType(labourWorkTypeid, userid).SingleOrDefault();
                }


            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch LabourWorkType list", ex, "LabourWorkType");
            }
            return View(labourWorkType);
        }
        [HttpPost]
        public ActionResult Add(LabourWorkType labourWorkType)
        {
            ViewBag.ParentMenu = "master";
            ViewBag.ChildMenu = "labourWorkType";
            if (ModelState.IsValid)
            {
                LabourWorkTypeBuisness buisness = new LabourWorkTypeBuisness();
                Status status = new Status();
                status = buisness.AddUpdateLabourWorkType(labourWorkType, 1);
                TempData["SuccessFlag"] = status.SuccessFlag;
                TempData["Msg"] = status.Msg;
                if (status.SuccessFlag == "1")
                {
                    return RedirectToAction("List");
                }
                else
                {
                    return View(labourWorkType);
                }
            }
            return View(labourWorkType);
        }

        [HttpPost]
        public JsonResult LoadLabourWorkType(int LabourWorkTypeId)
        {
            ViewBag.ParentMenu = "master";
            ViewBag.ChildMenu = "labourWorkType";
            long userid = 11;
            List<LabourWorkType> labourWorkType = null;
            LabourWorkTypeBuisness labourWorkTypeBuisness = null;
            try
            {
                labourWorkType = new List<LabourWorkType>();
                labourWorkTypeBuisness = new LabourWorkTypeBuisness();
                labourWorkType = labourWorkTypeBuisness.GetLabourWorkType(LabourWorkTypeId, userid).Where(i => i.IsDeleted == false).ToList();
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch LabourWorkType list", ex, "LabourWorkType");
            }

            return Json(labourWorkType, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(int labourWorkTypeid)
        {
            ViewBag.ParentMenu = "LabourMaster";
            ViewBag.ChildMenu = "labourWorkType";
            LabourWorkTypeBuisness labourWorkTypeBuisness = null;
            Status status = null;
            long userid = 11;
            try
            {
                labourWorkTypeBuisness = new LabourWorkTypeBuisness();
                status = new Status();
                status = labourWorkTypeBuisness.DeleteLabourWorkType(labourWorkTypeid, userid);
                TempData["SuccessFlag"] = status.SuccessFlag;
                TempData["Msg"] = status.Msg;
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to delete LabourWorkType ", ex, "Delete");
            }
            return RedirectToAction("List");
        }

    }
}
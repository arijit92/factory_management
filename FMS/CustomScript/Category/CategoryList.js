﻿
$(function () {

    $("#example1").DataTable();
    LoadCategory(0);
   
});

//--Bind all category for table----//
function LoadCategory(categoryid) {

    var requestData = {
        categoryId: categoryid
    };
    $.ajax({
        url: '/Category/LoadCategory',
        type: 'POST',
        data: JSON.stringify(requestData),
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        async: true,
        processData: true,
        cache: false,
        error: function (xhr) {
            alert('Error: ' + xhr.statusText);
        },
        success: function (result) {

                $('#tblCategory').html("");
                var tblHead = '<table class="table table-bordered table-striped" id="example1" cellspacing="0" cellpadding="4" align="Center" style="color:#333333;border-collapse:collapse;"><thead><tr style = "color:White;background-color:#1C5E55;font-weight:bold;" ><th scope="col">Category Name</th><th scope="col"> Category Description</th> <th scope="col">Status</th><th scope="col">Action</th></tr ></thead><tbody>';
                //alert(tblHead);
                var tbody = ""
                for (var i = 0; i < result.length; i++) {
                    var status = null;
                    if (result[i].IsActive == 0) {
                        status = "Inactive"
                    }
                    else {
                        status = "Active"
                    }
                    if (result[i].Description == null) {
                        result[i].Description = "";
                    }
                    //  tbody = tbody + '<tbody><tr><td align="left" id=id_' + result[i].roleTitle + '>' + result[i].roleTitle + '</td><td id=id_' + result[i].Permissions + '>' + result[i].Permissions + '</td><td class="td-actions text-center"><a href="#" class="" id="btnCreateRole" onclick="EditRole(\'' + result[i].roleID + '\');"><i class="far fa-edit text-warning"></i> </a>&nbsp;<a href="#" class="" id="btnCreateRole" onclick="DeleteRole(\'' + result[i].roleID + '\')"><i class="fas fa-times text-danger"></i> </a></td></tr>';
                    tbody = tbody + '<tr style="background-color:White;"><td>' + result[i].CategoryName + '</td ><td>' + result[i].Description + '</td><td>' + status + '</td><td><a href="#" onclick="Delete(' + result[i].CategoryId + ')"><i class="far fa-trash-alt"></i></i></a >&nbsp;&nbsp;<a href="#" onclick="Edit(' + result[i].CategoryId + ');"><i class="fa fa-fw fa-edit" ></i></a ></td></tr >'
                }
                $('#tblCategory').html(tblHead + tbody + '</tbody></table>');
                $("#example1").DataTable();
        },
    });
}
///--------------delete category----------------//
function Delete(categoryidTodelete) {
    //alert(categoryidTodelete);
    bootbox.confirm("Are you sure you want to delete this category?", function (confirm) {
        if (confirm == true) {
            var requestData = {
                categoryId: categoryidTodelete
            };
            $.ajax({
                url: '/Category/DeleteCategory',
                type: 'POST',
                data: JSON.stringify(requestData),
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                async: true,
                processData: true,
                cache: false,
                error: function (xhr) {
                    alert('Error: ' + xhr.statusText);
                },
                success: function (result) {
                    if (result.SuccessFlag == 1) {
                        bootbox.alert({
                            size: "small",
                            message: result.Msg,
                            callback: function () { LoadCategory(0) }
                        })

                        //toastr.success(result.Msg);
                        // Reset();
                        LoadCategory(0);
                    }
                    else {
                        toastr.error(result.Msg);
                    }
                }
            });
        }
        
       
    })
}

function Edit(categoryid) {
    window.location.href = '/Category/Redirect?categoryid=' + categoryid + '';
}

function Add()
{
    window.location.href = '/Category/Redirect?categoryid=' + 0 + '';
}
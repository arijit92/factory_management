﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FMS.BL.Utilities;


namespace FMS.BL.Buisness
{
    public class BranchBuisness : Common
    {
        #region Branch region

        public Status AddUpdateBranch(Branch pmodel, long UserID)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    para.Add("@BranchId", pmodel.BranchId);
                    para.Add("@BranchName", pmodel.BranchName);
                    para.Add("@Address", pmodel.Address);
                    para.Add("@Phone", pmodel.Phone);
                    para.Add("@Email", pmodel.Email);
                    para.Add("@IsActive", Convert.ToInt32(pmodel.IsActive == false ? 0 : 1));
                    para.Add("@CreatedBy", UserID);
                    para.Add("@CompanyId", pmodel.CompanyId);
                    const string procName = "[dbo].[sp_InsertUpdateBranch]";
                    connection.Open();
                    return connection.Query<Status>(procName, para, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to insert or update Branch", ex, "AddUpdateBranch");
                return null;
            }

        }
        public List<Branch> GetBranch(long? BranchId, long UserID)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    if (BranchId == 0)
                    {
                        BranchId = null;
                    }
                    para.Add("@BranchId", BranchId);
                    para.Add("@OperationId", 2);
                    para.Add("@UserId", UserID);
                    const string procName = "[dbo].[sp_DeleteSelectBranch]";
                    connection.Open();
                    return connection.Query<Branch>(procName, para, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Branch", ex, "GetBranch");
                return null;
            }

        }
        public Status DeleteBranch(long? BranchId, long UserID)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    para.Add("@BranchId", BranchId);
                    para.Add("@OperationId", 1);
                    para.Add("@UserId", UserID);
                    const string procName = "[dbo].[sp_DeleteSelectBranch]";
                    connection.Open();
                    return connection.Query<Status>(procName, para, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to delete Branch", ex, "DeleteBranch");
                return null;
            }

        }
        #endregion
    }
}

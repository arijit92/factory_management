﻿CREATE PROCEDURE  [dbo].[sp_InsertUpdateRoadChalan]
-- Add the parameters for the stored procedure here
 @RoadChalanId				bigint null,
 @NameOfDistributor			nvarchar(100),
 @AddressOfDistributor		nvarchar(MAX),
 @Destination				nvarchar(50),
 @Quantity					int,
 @TruckNo					nvarchar(50)
AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
--update or insert checking
IF(@RoadChalanId>0)

BEGIN
--update
UPDATE tbl_RoadChalan
SET
 NameOfDistributor					= @NameOfDistributor
,AddressOfDistributor               = @AddressOfDistributor
,Destination					    = @Destination
,Quantity						    = @Quantity
,TruckNo							=@TruckNo
,Date								= Getdate()
WHERE Id =@RoadChalanId
SELECT 'Road Chalan updated successfully.' as Msg,1 as SuccessFlag
END

ELSE
BEGIN


BEGIN
--insert
INSERT INTO tbl_RoadChalan
(
 NameOfDistributor		
,AddressOfDistributor   
,Destination			
,Quantity	
,TruckNo
,IsDeleted
,Date					
)
VALUES
(
 @NameOfDistributor		
,@AddressOfDistributor              
,@Destination			           
,@Quantity	
,@TruckNo
,0
,GETDATE()				
)
SELECT 'Road Chalan created successfully.' as Msg,1 as SuccessFlag
END
END
END TRY
BEGIN CATCH
SELECT 'Sorry something went wrong, unable to process,' as Msg,0 as SuccessFlag
RETURN
END CATCH
END
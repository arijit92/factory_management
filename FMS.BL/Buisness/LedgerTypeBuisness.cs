﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FMS.BL.Utilities;


namespace FMS.BL.Buisness
{
    public class LedgerTypeBuisness : Common
    {
        #region Accessories region

        public Status AddUpdateLedgerType(LedgerType pmodel, long UserID)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    para.Add("@LedgerTypeId", pmodel.LedgerTypeId);
                    para.Add("@LedgerTypeName", pmodel.LedgerTypeName);
                    para.Add("@LedgerTypeDescription", pmodel.LedgerTypeDescription);
                    para.Add("@IsActive", Convert.ToInt32(pmodel.IsActive == false ? 0 : 1));
                    para.Add("@CreatedBy", UserID);
                    para.Add("@CompanyId", pmodel.CompanyId);
                    const string procName = "[dbo].[sp_InsertUpdateLedgerType]";
                    connection.Open();
                    return connection.Query<Status>(procName, para, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to insert or update LedgerType", ex, "AddUpdateLedgerType");
                return null;
            }

        }
        public List<LedgerType> GetLedgerType(long? LedgerTypeId, long UserID)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    if (LedgerTypeId == 0)
                    {
                        LedgerTypeId = null;
                    }
                    para.Add("@LedgerTypeId", LedgerTypeId);
                    para.Add("@OperationId", 2);
                    para.Add("@UserId", UserID);
                    const string procName = "[dbo].[sp_DeleteSelectLedgerType]";
                    connection.Open();
                    return connection.Query<LedgerType>(procName, para, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch LedgerType", ex, "GetLedgerType");
                return null;
            }

        }
        public Status DeleteLedgerType(long? LedgerTypeId, long UserID)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    para.Add("@LedgerTypeId", LedgerTypeId);
                    para.Add("@OperationId", 1);
                    para.Add("@UserId", UserID);
                    const string procName = "[dbo].[sp_DeleteSelectLedgerType]";
                    connection.Open();
                    return connection.Query<Status>(procName, para, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to delete LedgerType", ex, "DeleteLedgerType");
                return null;
            }

        }
        #endregion
    }
}

﻿CREATE PROCEDURE  [dbo].[sp_InsertUpdateLedger]
-- Add the parameters for the stored procedure here
 @LedgerId [bigint]
,@LedgerName [NVARCHAR](MAX)
,@LedgerTypeId [bigint]
,@LedgerDescription [NVARCHAR](MAX)
,@IsActive [int] NULL
,@CreatedBy [bigint] NULL
,@CompanyId [bigint] NULL
AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
--update or insert checking
IF(@LedgerId>0)
BEGIN
--Duplicate value check
IF EXISTS(SELECT * FROM tbl_Ledger WITH(NOLOCK) WHERE LedgerName = @LedgerName AND LedgerId != @LedgerId AND IsDeleted = 0)
BEGIN
SELECT 'Already exists.' as Msg,0 as SuccessFlag
END
ELSE
BEGIN
--update
UPDATE tbl_Ledger
SET
LedgerName =UPPER(@LedgerName)
,LedgerTypeId = @LedgerTypeId
,LedgerDescription = @LedgerDescription
,IsActive =@IsActive
,UpdatedBy =@CreatedBy
,UpdatedOn      =GETDATE()
,CompanyId =@CompanyId
WHERE LedgerId =@LedgerId
SELECT 'Ledger updated successfully.' as Msg,1 as SuccessFlag
END
END
ELSE
BEGIN
--Duplicate value check
IF EXISTS (SELECT * FROM tbl_Ledger WITH(NOLOCK) WHERE LedgerName = @LedgerName AND IsDeleted = 0)
BEGIN
SELECT 'Already exists.' as Msg,0 as SuccessFlag
END
ELSE
BEGIN
--insert
INSERT INTO tbl_Ledger
(
 LedgerName
 ,LedgerTypeId
,LedgerDescription 
,IsActive
,IsDeleted
,CreatedBy
,CreatedOn
,CompanyId
)
VALUES
(
UPPER(@LedgerName)
,@LedgerTypeId
,@LedgerDescription
,@IsActive
,0
,@CreatedBy
,GETDATE()
,@CompanyId
)
SELECT 'Ledger created successfully.' as Msg,1 as SuccessFlag
END
END
END TRY
BEGIN CATCH
SELECT 'Sorry something went wrong, unable to process,' as Msg,0 as SuccessFlag
RETURN
END CATCH
END
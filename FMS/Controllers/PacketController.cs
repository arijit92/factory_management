﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FMS.BL;
using FMS.BL.Buisness;
using FMS.BL.Utilities;

namespace FMS.Controllers
{
    public class PacketController : Controller
    {
        // GET: Packet
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult LoadPacket(int PacketId)
        {
           
            long userid = 11;
            List<Packet> packets = null;
            PacketBuisness packetBuisness = null;
            try
            {
                packets = new List<Packet>();
                packetBuisness = new PacketBuisness();
                packets = packetBuisness.GetPacketDetails().Where(i => i.PacketId == PacketId).ToList();
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Customer list", ex, "Customer");
            }

            return Json(packets, JsonRequestBehavior.AllowGet);
        }
    }
}
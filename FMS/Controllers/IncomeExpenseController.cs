﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FMS.BL;
using FMS.BL.Buisness;
using FMS.BL.Utilities;

namespace FMS.Controllers
{
    public class IncomeExpenseController : Controller
    {
        // GET: IncomeExpense
        public ActionResult Index()
        {
            ViewBag.ParentMenu = "LedgerMaster";
            ViewBag.ChildMenu = "Index";

            return View();
        }

        public ActionResult Details()
        {
            ViewBag.ParentMenu = "LedgerMaster";
            ViewBag.ChildMenu = "Details";

            return View();
        }

        [HttpPost]
        public JsonResult InsertUpdateOperationDetails(List<OperationValue> OperationValue, string IFSC, string ChequeNo, string DraftNo, string BankName,string PaymentMode,string Partyname,string Date)
        {
        
            ViewBag.ParentMenu = "LedgerMaster";
            ViewBag.ChildMenu = "ledger";
            long userid = 11;
            IncomeExpenseBuisness incomeExpenseBuisness = null;
            Status status = null;
            try
            {
                status = new Status();
                incomeExpenseBuisness = new IncomeExpenseBuisness();
                status = incomeExpenseBuisness.AddUpdateOperationMaster(OperationValue, IFSC, ChequeNo, DraftNo, BankName,0,PaymentMode, Partyname,Date);
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Ledger LoadOperation", ex, "LoadOperation");
            }

            return Json(status, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult LoadDetails(int LedgerId)
        {

            ViewBag.ParentMenu = "LedgerMaster";
            ViewBag.ChildMenu = "ledger";
            long userid = 11;
            IncomeExpenseBuisness incomeExpenseBuisness = null;
            OperationByLedger operationByLedgers = null;
            try
            {
                operationByLedgers = new OperationByLedger();
                incomeExpenseBuisness = new IncomeExpenseBuisness();
                //dynamic list
                var list = incomeExpenseBuisness.GetDetails(LedgerId);
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Ledger LoadOperation", ex, "LoadOperation");
            }

            return Json(operationByLedgers, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult LoadLedgerValue(int LedgerId,DateTime? date)
        {

            ViewBag.ParentMenu = "LedgerMaster";
            ViewBag.ChildMenu = "ledger";
            long userid = 11;
            IncomeExpenseBuisness incomeExpenseBuisness = null;
            
            List<TransactionDetails> transactionDetails = null;
            try
            {
                
                incomeExpenseBuisness = new IncomeExpenseBuisness();
                transactionDetails = new List<TransactionDetails>();
                //dynamic list
                transactionDetails = incomeExpenseBuisness.LoadLedgerValue(LedgerId,date);
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Ledger LoadLedgerValue", ex, "LoadLedgerValue");
            }

            return Json(transactionDetails, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult LoadLedgerType(int LedgerId)
        {
            ViewBag.ParentMenu = "LedgerMaster";
            ViewBag.ChildMenu = "ledger";
            long userid = 11;
            List<Ledger> ledger = null;
            LedgerBuisness ledgerBuisness = null;
            try
            {
                ledger = new List<Ledger>();
                ledgerBuisness = new LedgerBuisness();
                ledger = ledgerBuisness.GetLedger(LedgerId, userid).Where(i => i.IsDeleted == false).ToList();
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Ledger list", ex, "Ledger");
            }

            return Json(ledger, JsonRequestBehavior.AllowGet);
        }

        public ActionResult List()
        {
            return View();
        }

        [HttpPost]
        public JsonResult GetReport(string SearchType, int LedgerId)
        {

            ViewBag.ParentMenu = "LedgerMaster";
            ViewBag.ChildMenu = "ledger";
            long userid = 11;
            IncomeExpenseBuisness incomeExpenseBuisness = null;
            Status status = null;
            List<TransactionMaster> list = null;
            try
            {
                status = new Status();
                incomeExpenseBuisness = new IncomeExpenseBuisness();
                list = new List<TransactionMaster>();
                list = incomeExpenseBuisness.GetReport(SearchType, LedgerId);
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Ledger LoadOperation", ex, "LoadOperation");
            }

            return Json(status, JsonRequestBehavior.AllowGet);
        }

        //  [HttpPost]
        //public JsonResult LoadIncomeExpenseList(string Mode,string Type)
        //{
        //    ViewBag.ParentMenu = "LedgerMaster";
        //    ViewBag.ChildMenu = "ledger";
        //    long userid = 11;
        //    List<Ledger> ledger = null;
        //    LedgerBuisness ledgerBuisness = null;
        //    try
        //    {
        //        ledger = new List<Ledger>();
        //        ledgerBuisness = new LedgerBuisness();
        //        ledger = ledgerBuisness.GetLedger(LedgerId, userid).Where(i => i.IsDeleted == false).ToList();
        //    }
        //    catch (Exception ex)
        //    {
        //        AppLogger.Instance.Error(this.GetType(), "Unable to fetch Ledger list", ex, "Ledger");
        //    }

        //    return Json(ledger, JsonRequestBehavior.AllowGet);
        //}

    }
}
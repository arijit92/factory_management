﻿CREATE TABLE [dbo].[tbl_StockMaster] (
    [AccessoriesId]     INT             NOT NULL,
    [AvailableQuantity] NUMERIC (18, 2) NULL,
    [UpdatedOn]         DATETIME        NULL
);


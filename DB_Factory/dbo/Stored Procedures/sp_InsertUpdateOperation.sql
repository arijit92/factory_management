﻿CREATE PROCEDURE  [dbo].[sp_InsertUpdateOperation]
 @OperationID [bigint]
,@OperationName [NVARCHAR](500)
,@OperationDesc [NVARCHAR](MAX)
,@LedgerID BIGINT
,@IsActive [int] NULL
,@CreatedBy [bigint] NULL
AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
IF(@OperationID>0)
BEGIN
IF EXISTS(SELECT * FROM [dbo].[tbl_OperationMaster] WITH(NOLOCK) WHERE OperationName = @OperationName AND OperationID != @OperationID)
BEGIN
SELECT 'Already exists.' as Msg,0 as SuccessFlag
END
ELSE
BEGIN
--update
UPDATE tbl_OperationMaster
SET
 OperationName =UPPER(@OperationName)
,OperationDesc =@OperationDesc
,LedgerID =@LedgerID
,IsActive =@IsActive
,UpdatedBy =@CreatedBy
,UpdatedOn =GETDATE()
WHERE OperationID =@OperationID
SELECT 'Operation updated successfully.' as Msg,1 as SuccessFlag
END
END
ELSE
BEGIN
IF EXISTS (SELECT * FROM tbl_OperationMaster WITH(NOLOCK) WHERE OperationName = @OperationName)
BEGIN
SELECT 'Already exists.' as Msg,0 as SuccessFlag
END
ELSE
BEGIN
--insert
INSERT INTO tbl_OperationMaster
(
 OperationName
,OperationDesc
,LedgerID
,IsActive
,IsDeleted
,CreatedBy
,CreatedOn
)
VALUES
(
UPPER(@OperationName)
,@OperationDesc
,@LedgerID
,@IsActive
,0
,@CreatedBy
,GETDATE()
)
SELECT 'Operation created successfully.' as Msg,1 as SuccessFlag
END
END
END TRY
BEGIN CATCH
SELECT 'Sorry something went wrong, unable to process,' as Msg,0 as SuccessFlag
RETURN
END CATCH
END
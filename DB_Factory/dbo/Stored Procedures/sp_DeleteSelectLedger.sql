﻿CREATE PROCEDURE  [dbo].[sp_DeleteSelectLedger]
-- Add the parameters for the stored procedure here
 @LedgerId [bigint] NULL
,@OperationId [bigint]  --1=delete,2=Select
,@UserId [bigint] NULL

AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
IF(@OperationId  = 1 AND @LedgerId >0)
BEGIN
UPDATE tbl_Ledger
SET
IsDeleted = 1
   ,UpdatedBy = @UserId
   ,UpdatedOn = GETDATE()
    WHERE LedgerId = @LedgerId
SELECT 'Ledger deleted successfully.' as Msg,1 as SuccessFlag
END
ELSE
BEGIN
SELECT TL.*,TLT.LedgerTypeName FROM tbl_Ledger TL WITH(NOLOCK) INNER JOIN tbl_LedgerType TLT ON TLT.LedgerTypeId = TL.LedgerTypeId
WHERE LedgerId = ISNULL(@LedgerId,LedgerId)
END

END TRY
BEGIN CATCH
SELECT 'Sorry something went wrong, unable to process,' as Msg,0 as SuccessFlag
RETURN
END CATCH
END
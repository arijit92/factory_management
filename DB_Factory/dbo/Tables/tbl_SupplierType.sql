﻿CREATE TABLE [dbo].[tbl_SupplierType] (
    [SupplierTypeId]          BIGINT         IDENTITY (1, 1) NOT NULL,
    [SupplierTypeName]        NVARCHAR (100) NOT NULL,
    [SupplierTypeDescription] NVARCHAR (MAX) NULL,
    [IsActive]                INT            NULL,
    [IsDeleted]               INT            NULL,
    [CreatedBy]               BIGINT         NULL,
    [CreatedOn]               DATETIME       NULL,
    [UpdatedBy]               BIGINT         NULL,
    [UpdatedOn]               DATETIME       NULL,
    [CompanyId]               BIGINT         NULL
);




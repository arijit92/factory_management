﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FMS.BL.Utilities;

namespace FMS.BL.Buisness
{
   public class AssetBuisness : Common
    {
        #region Assets region

        public Status AddUpdateAssets(Assets pmodel, long UserID)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                   
                    para.Add("@AssetsId"        , pmodel.AssetsId);
                    para.Add("@SerialNumber"    , pmodel.SerialNumber);
                    para.Add("@DeedNumber"      , pmodel.DeedNumber);
                    para.Add("@PlotNumber"      , pmodel.PlotNumber);
                    para.Add("@Moja"             ,pmodel.Moja);
                    para.Add("@AreaOfLand"       ,pmodel.AreaOfLand);
                    para.Add("@ValueOfLand"      ,pmodel.ValueOfLand);
                    para.Add("@Remarks"          , pmodel.Remarks);
                    const string procName = "[dbo].[sp_InsertUpdateAssets]";
                    connection.Open();
                    return connection.Query<Status>(procName, para, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to insert or update Assets", ex, "AddUpdateAssets");
                return null;
            }

        }
        public List<Assets> GetAssets(long? AssetsId, long UserID)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    if (AssetsId == 0)
                    {
                        AssetsId = null;
                    }
                    para.Add("@AssetsId", AssetsId);
                    para.Add("@OperationId", 2);
                    para.Add("@UserId", UserID);
                    const string procName = "[dbo].[sp_DeleteSelectAssets]";
                    connection.Open();
                    return connection.Query<Assets>(procName, para, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Assets", ex, "GetAssets");
                return null;
            }

        }
        public Status DeleteAssets(long? AssetsId, long UserID)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    para.Add("@AssetsId", AssetsId);
                    para.Add("@OperationId", 1);
                    para.Add("@UserId", UserID);
                    const string procName = "[dbo].[sp_DeleteSelectAssets]";
                    connection.Open();
                    return connection.Query<Status>(procName, para, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to delete Assets", ex, "DeleteAssets");
                return null;
            }

        }
        #endregion
    }
}

﻿CREATE PROCEDURE  [dbo].[sp_InsertUpdateWeightDetails]
-- Add the parameters for the stored procedure here
 @TransactionId [bigint]
,@CustomerName [NVARCHAR](200)
,@Product [NVARCHAR](50) NULL
,@Misc_1st_weight [NVARCHAR](50) NULL
,@Misc_2nd_weight [NVARCHAR](50) NULL
,@Date_First_Weight datetime
,@Date_Second_Weight datetime
,@First_weight decimal(18,2)
,@Second_weight decimal(18,2)
,@UnitId bigint	
,@NetWeight decimal(18,2)
,@WeightCharges decimal(18,2)
,@IsActive [int] NULL
,@CreatedBy [bigint] NULL
,@CompanyId [bigint] NULL
AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
--update or insert checking
IF(@TransactionId>0)
BEGIN
--update
UPDATE tbl_WeightDetails
SET
 CustomerName         =UPPER(@CustomerName)
,Product              =@Product
,Misc_1st_weight      =@Misc_1st_weight
,Misc_2nd_weight      =@Misc_2nd_weight
,Date_First_Weight    =@Date_First_Weight
,Date_Second_Weight   =@Date_Second_Weight
,First_weight         =@First_weight
,Second_weight        =@Second_weight
,UnitId               =@UnitId
,NetWeight            =@NetWeight
,WeightCharges        =@WeightCharges
,IsActive             =@IsActive
,UpdatedBy            =@CreatedBy
,UpdatedOn            =GETDATE()
,CompanyId            =@CompanyId
WHERE TransactionId =@TransactionId
SELECT 'WeightDetails updated successfully.' as Msg,1 as SuccessFlag
END
ELSE
BEGIN

--insert
INSERT INTO tbl_WeightDetails
(
 CustomerName      
,Product           
,Misc_1st_weight   
,Misc_2nd_weight   
,Date_First_Weight 
,Date_Second_Weight
,First_weight      
,Second_weight     
,UnitId            
,NetWeight         
,WeightCharges  
,IsActive
,IsDeleted
,CreatedBy
,CreatedOn
,CompanyId
)
VALUES
(
 UPPER(@CustomerName)
,@Product
,@Misc_1st_weight
,@Misc_2nd_weight
,@Date_First_Weight
,@Date_Second_Weight
,@First_weight
,@Second_weight
,@UnitId
,@NetWeight
,@WeightCharges
,@IsActive
,0
,@CreatedBy
,GETDATE()
,@CompanyId
)
SELECT 'WeightDetails created successfully.' as Msg,1 as SuccessFlag
END
END TRY
BEGIN CATCH
SELECT ERROR_MESSAGE() as Msg,0 as SuccessFlag
RETURN
END CATCH
END
﻿CREATE TABLE [dbo].[tbl_Role] (
    [RoleId]          BIGINT         IDENTITY (1, 1) NOT NULL,
    [Name]            NVARCHAR (200) NOT NULL,
    [RoleDescription] NVARCHAR (MAX) NULL,
    [IsActive]        INT            NOT NULL,
    [IsDeleted]       INT            NOT NULL,
    [CreatedBy]       BIGINT         NULL,
    [CreatedOn]       DATETIME       NULL,
    [UpdatedBy]       BIGINT         NULL,
    [UpdatedOn]       DATETIME       NULL,
    [CompanyId]       BIGINT         NULL
);




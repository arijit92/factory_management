﻿CREATE TABLE [dbo].[tbl_IncomeExpense] (
    [IncomeExpenseId]   INT             NULL,
    [LedgerOperationId] INT             NULL,
    [Amount]            DECIMAL (18, 2) NULL,
    [TransactionId]     INT             NULL,
    [Date]              DATETIME        NULL
);


﻿CREATE TABLE [dbo].[tbl_Documents] (
    [DocumentId]   BIGINT         IDENTITY (1, 1) NOT NULL,
    [DocumentName] NVARCHAR (MAX) NOT NULL,
    [DocumentPath] NVARCHAR (MAX) NULL,
    [IsActive]     INT            NULL,
    [IsDeleted]    INT            NULL,
    [CreatedBy]    BIGINT         NULL,
    [CreatedOn]    DATETIME       NULL,
    [UpdatedBy]    BIGINT         NULL,
    [UpdatedOn]    DATETIME       NULL,
    [CompanyId]    BIGINT         NULL
);




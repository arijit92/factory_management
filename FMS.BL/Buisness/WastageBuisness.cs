﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FMS.BL.Utilities;

namespace FMS.BL.Buisness
{
    public class WastageBuisness : Common
    {
        public Status AddUpdateWastage(Wastage wastage)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    para.Add("@WastageId", wastage.WastageId);
                    para.Add("@ItemTypeId ", wastage.ItemTypeId);
                    para.Add("@Weight", wastage.Weight);
                    const string procName = "[dbo].[insertupdateWastage]";
                    connection.Open();
                    return connection.Query<Status>(procName, para, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to insert or update Purchase Details", ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                return null;
            }

        }

        public List<Wastage> GetWasteDetails()
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();

                    const string procName = "[dbo].[spWastageDetails]";
                    connection.Open();
                    return connection.Query<Wastage>(procName, para, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch purchase stock", ex, "GetPurchaseStockCount");
                return null;
            }

        }
    }
}

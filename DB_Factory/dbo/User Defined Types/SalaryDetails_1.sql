﻿CREATE TYPE [dbo].[SalaryDetails] AS TABLE (
    [EmployeeId]    BIGINT          NULL,
    [IsPaid]        BIT             NULL,
    [LoanAmount]    DECIMAL (18, 2) NULL,
    [Paka_Salary]   DECIMAL (18, 2) NULL,
    [Kacha_Salary]  DECIMAL (18, 2) NULL,
    [PF]            DECIMAL (18, 2) NULL,
    [LoanDeduction] DECIMAL (18, 2) NULL);


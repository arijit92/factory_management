﻿CREATE PROCEDURE  [dbo].[sp_InsertUpdateEmployee]
-- Add the parameters for the stored procedure here
 @EmployeeId [bigint]
,@FirstName [NVARCHAR](50)
,@LastName [NVARCHAR](50)
,@Mobile [NVARCHAR] (50)
,@Email [NVARCHAR] (100)
,@CAddress [NVARCHAR] (MAX)
,@EmployeeTypeId [bigint]
,@BranchId [bigint]
,@PAKA_SALARY numeric(18,2) NULL
,@KACHA_SALARY numeric(18,2) NULL
,@PF numeric(18,2) NULL
,@IsActive [int] NULL
,@CreatedBy [bigint] NULL
,@CompanyId [bigint] NULL
AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
--update or insert checking
IF(@EmployeeId>0)
BEGIN
--Duplicate value check
IF EXISTS(SELECT * FROM tbl_Employee WITH(NOLOCK) WHERE (Mobile = @Mobile OR Email = @Email) AND EmployeeId != @EmployeeId)
BEGIN
SELECT 'Duplicate mobile or email.' as Msg,0 as SuccessFlag
END
ELSE
BEGIN
--update
UPDATE tbl_Employee
SET
 FirstName         = UPPER(@FirstName)
,LastName          = UPPER(@LastName)
,CAddress		   = @CAddress
,Mobile            = @Mobile
,Email             = @Email
,BranchId          = @BranchId
,EmployeeTypeId    = @EmployeeTypeId
,PAKA_SALARY       = @PAKA_SALARY
,KACHA_SALARY      = @KACHA_SALARY
,PF                = @PF
,IsActive          = @IsActive
,UpdatedBy         = @CreatedBy
,UpdatedOn         = GETDATE()
,CompanyId         = @CompanyId
WHERE EmployeeId   = @EmployeeId
SELECT 'Employee updated successfully.' as Msg,1 as SuccessFlag
END
END
ELSE
BEGIN
--Duplicate value check
IF EXISTS (SELECT * FROM tbl_Employee WITH(NOLOCK) WHERE Mobile = @Mobile OR Email = @Email)
BEGIN
SELECT 'Already exists.' as Msg,0 as SuccessFlag
END
ELSE
BEGIN
--insert
INSERT INTO tbl_Employee
(
FirstName      
,LastName       
,CAddress		
,Mobile         
,Email          
,BranchId       
,EmployeeTypeId 
,PAKA_SALARY   
,KACHA_SALARY   
,PF             
,IsActive 
,IsDeleted
,CreatedBy      
,CreatedOn      
,CompanyId  
)
VALUES
(
UPPER(@FirstName)
,UPPER(@LastName)
,@CAddress
,@Mobile
,@Email
,@BranchId
,@EmployeeTypeId
,@PAKA_SALARY
,@KACHA_SALARY
,@PF
,@IsActive
,0
,@CreatedBy
,GETDATE()
,@CompanyId
)
SELECT 'Employee created successfully.' as Msg,1 as SuccessFlag
END
END
END TRY
BEGIN CATCH
SELECT ERROR_MESSAGE() as Msg,0 as SuccessFlag
RETURN
END CATCH
END
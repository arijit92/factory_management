﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FMS.BL;
using FMS.BL.Buisness;
using FMS.BL.Utilities;

namespace FMS.Controllers
{
    public class PurchaseController : Controller
    {

        public ActionResult PurchaseStockList()
        {

            ViewBag.ParentMenu = "purchase";
            ViewBag.ChildMenu = "PurchaseStockList";
            return View();
        }

        [HttpPost]
        public JsonResult PurchaseStockCount()
        {
            long userid = 11;
            List<PurchaseEntry> itemMasters = null;
            PurchaseBuisness buisness = null;
            try
            {
                itemMasters = new List<PurchaseEntry>();
                buisness = new PurchaseBuisness();
                itemMasters = buisness.GetPurchaseStockCount().ToList();
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Role list", ex, "Role");
            }

            return Json(itemMasters, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetPurchaseStockDetails()
        {
            long userid = 11;
            List<PurchaseEntry> itemMasters = null;
            PurchaseBuisness buisness = null;
            try
            {
                itemMasters = new List<PurchaseEntry>();
                buisness = new PurchaseBuisness();
                itemMasters = buisness.GetPurchaseStockDetails().ToList();
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Role list", ex, "Role");
            }

            return Json(itemMasters, JsonRequestBehavior.AllowGet);
        }

        // GET: Purchase
        public ActionResult PurchaseEntry()
        {
            ViewBag.ParentMenu = "purchase";
            ViewBag.ChildMenu = "PurchaseEntry";
            PurchaseEntry purchaseEntry = new PurchaseEntry();
            UtilityBuisness utilityBuisness = new UtilityBuisness();
            DDLList data = new DDLList();
            List<DDLList> ddlList = null;
            List<ItemMaster> itemMasters = new List<ItemMaster>();
            List<SupplierType> supplierTypes = new List<SupplierType>();
            List<Supplier> suppliers = new List<Supplier>();
            ItemBuisness itemBuisness = new ItemBuisness();
            SupplierTypeBuisness supplierTypeBuisness = new SupplierTypeBuisness();
            SupplierBuisness supplierBuisness = new SupplierBuisness();
            itemMasters = itemBuisness.GetItems(0, 1).Where(i => i.CategoryId == 1).ToList();
            supplierTypes = supplierTypeBuisness.GetSupplierType(0, 1);
            suppliers = supplierBuisness.GetSupplier(0,1);
            ViewBag.ItemList = new SelectList(itemMasters, "Id", "DropdownValue", 0);
            ViewBag.SupplierType = new SelectList(supplierTypes, "SupplierTypeId", "SupplierTypeName", 0);
            ViewBag.Supplier = new SelectList(suppliers, "SupplierId", "PointOfPersonName", 0);
            data.tableName = "tbl_PaymentMode";
            data.Value = "ModeId";
            data.Text = "ModeName";
            ddlList = utilityBuisness.GetDDLList(data);
            ViewBag.Mode = new SelectList(ddlList, "value", "Text", 0);
            data.tableName = "tbl_Bank";
            data.Value = "Id";
            data.Text = "BankName";
            ddlList = utilityBuisness.GetDDLList(data);
            ViewBag.Bank = new SelectList(ddlList, "Text", "Text", 0);

            return View(purchaseEntry);
        }

        [HttpPost]
        public ActionResult PurchaseEntry(PurchaseEntry purchaseEntry)
        {
            ViewBag.ParentMenu = "purchase";
            ViewBag.ChildMenu = "PurchaseEntry";
            UtilityBuisness utilityBuisness = new UtilityBuisness();
            DDLList data = new DDLList();
            List<DDLList> ddlList = null;
            List<ItemMaster> itemMasters = new List<ItemMaster>();
            List<SupplierType> supplierTypes = new List<SupplierType>();
            List<Supplier> suppliers = new List<Supplier>();
            ItemBuisness itemBuisness = new ItemBuisness();
            SupplierTypeBuisness supplierTypeBuisness = new SupplierTypeBuisness();
            SupplierBuisness supplierBuisness = new SupplierBuisness();
            PurchaseBuisness purchaseBuisness = new PurchaseBuisness();
            Status status = new Status();
            itemMasters = itemBuisness.GetItems(0, 1).Where(i => i.CategoryId == 1).ToList();
            supplierTypes = supplierTypeBuisness.GetSupplierType(0, 1);
            suppliers = supplierBuisness.GetSupplier(0, 1);
            ViewBag.ItemList = new SelectList(itemMasters, "Id", "DropdownValue", 0);
            ViewBag.SupplierType = new SelectList(supplierTypes, "SupplierTypeId", "SupplierTypeName", 0);
            ViewBag.Supplier = new SelectList(suppliers, "SupplierId", "PointOfPersonName", 0);
            data.tableName = "tbl_PaymentMode";
            data.Value = "ModeId";
            data.Text = "ModeName";
            ddlList = utilityBuisness.GetDDLList(data);
            ViewBag.Mode = new SelectList(ddlList, "value", "Text", 0);
            data.tableName = "tbl_Bank";
            data.Value = "Id";
            data.Text = "BankName";
            ddlList = utilityBuisness.GetDDLList(data);
            ViewBag.Bank = new SelectList(ddlList, "Text", "Text", 0);
            if(ModelState.IsValid)
            {
                status = purchaseBuisness.AddUpdatePurchaseEntryDetails(purchaseEntry);
                TempData["SuccessFlag"] = status.SuccessFlag;
                TempData["Msg"] = status.Msg;
                if (status.SuccessFlag == "1")
                {
                    return RedirectToAction("PurchaseStockList");
                }
                else
                {
                    return View(purchaseEntry);

                }
            }
            else
            {
                return View(purchaseEntry);
            }
            
            
        }
    }
}
﻿CREATE TABLE [dbo].[tbl_TransactionDetails] (
    [TransactionDetailsID] BIGINT          IDENTITY (1, 1) NOT NULL,
    [TransactionID]        BIGINT          NOT NULL,
    [OperationID]          BIGINT          NULL,
    [LedgerID]             BIGINT          NULL,
    [Ammount]              DECIMAL (18, 2) NULL,
    CONSTRAINT [PK_tbl_TransactionDetails] PRIMARY KEY CLUSTERED ([TransactionDetailsID] ASC)
);


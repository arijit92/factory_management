﻿CREATE PROCEDURE  [dbo].[sp_DeleteSelectDocument]
-- Add the parameters for the stored procedure here
 @DocumentId [bigint] NULL
,@OperationId [bigint]  --1=delete,2=Select
,@UserId [bigint] NULL

AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
IF(@OperationId  = 1 AND @DocumentId >0)
BEGIN
UPDATE tbl_Documents
SET
IsDeleted = 1
   ,UpdatedBy = @UserId
   ,UpdatedOn = GETDATE()
    WHERE DocumentId = @DocumentId
SELECT 'Document deleted successfully.' as Msg,1 as SuccessFlag
END
ELSE
BEGIN
SELECT * FROM tbl_Documents WITH(NOLOCK)
WHERE DocumentId = ISNULL(@DocumentId,DocumentId)
END

END TRY
BEGIN CATCH
SELECT 'Sorry something went wrong, unable to process,' as Msg,0 as SuccessFlag
RETURN
END CATCH
END
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FMS.BL;
using FMS.BL.Buisness;
using FMS.BL.Utilities;
namespace FMS.Controllers
{
    public class LedgerTypeController : Controller
    {
        // GET: Role  
        public ActionResult List()
        {
            ViewBag.ParentMenu = "superadmin";
            ViewBag.ChildMenu = "ledgerType";
            return View();
        }

        [HttpGet]
        public ActionResult Add(int? ledgerTypeid)
        {
            ViewBag.ParentMenu = "superadmin";
            ViewBag.ChildMenu = "ledgerType";
            LedgerType ledgerType = null;
            LedgerTypeBuisness ledgerTypeBuisness = null;
            long userid = 11;
            try
            {
                ledgerType = new LedgerType();
                ledgerTypeBuisness = new LedgerTypeBuisness();
                if (ledgerTypeid > 0)
                {
                    ledgerType = ledgerTypeBuisness.GetLedgerType(ledgerTypeid, userid).SingleOrDefault();
                }


            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch LedgerType list", ex, "LedgerType");
            }
            return View(ledgerType);
        }
        [HttpPost]
        public ActionResult Add(LedgerType ledgerType)
        {
            ViewBag.ParentMenu = "superadmin";
            ViewBag.ChildMenu = "ledgerType";
            if (ModelState.IsValid)
            {
                LedgerTypeBuisness buisness = new LedgerTypeBuisness();
                Status status = new Status();
                status = buisness.AddUpdateLedgerType(ledgerType, 1);
                TempData["SuccessFlag"] = status.SuccessFlag;
                TempData["Msg"] = status.Msg;
                if (status.SuccessFlag == "1")
                {
                    return RedirectToAction("List");
                }
                else
                {
                    return View(ledgerType);
                }
            }
            return View(ledgerType);
        }


        [HttpPost]
        public JsonResult LoadLedgerType(int LedgerTypeId)
        {
            ViewBag.ParentMenu = "superadmin";
            ViewBag.ChildMenu = "ledgerType";
            long userid = 11;
            List<LedgerType> ledgerType = null;
            LedgerTypeBuisness ledgerTypeBuisness = null;
            try
            {
                ledgerType = new List<LedgerType>();
                ledgerTypeBuisness = new LedgerTypeBuisness();
                ledgerType = ledgerTypeBuisness.GetLedgerType(LedgerTypeId, userid).Where(i => i.IsDeleted == false).ToList();
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch LedgerType list", ex, "LedgerType");
            }

            return Json(ledgerType, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(int ledgerTypeid)
        {
            ViewBag.ParentMenu = "LedgerMaster";
            ViewBag.ChildMenu = "ledgerType";
            LedgerTypeBuisness ledgerTypeBuisness = null;
            Status status = null;
            long userid = 11;
            try
            {
                ledgerTypeBuisness = new LedgerTypeBuisness();
                status = new Status();
                status = ledgerTypeBuisness.DeleteLedgerType(ledgerTypeid, userid);
                TempData["SuccessFlag"] = status.SuccessFlag;
                TempData["Msg"] = status.Msg;
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to delete LedgerType ", ex, "LedgerType");
            }
            return RedirectToAction("List");
        }

    }
}
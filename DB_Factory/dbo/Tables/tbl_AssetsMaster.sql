﻿CREATE TABLE [dbo].[tbl_AssetsMaster] (
    [AssetsId]     INT            IDENTITY (1, 1) NOT NULL,
    [SerialNumber] NVARCHAR (50)  NULL,
    [DeedNumber]   NVARCHAR (50)  NULL,
    [Date]         DATETIME       NULL,
    [PlotNumber]   NVARCHAR (50)  NULL,
    [Moja]         NVARCHAR (50)  NULL,
    [AreaOfLand]   NVARCHAR (50)  NULL,
    [ValueOfLand]  NVARCHAR (50)  NULL,
    [Remarks]      NVARCHAR (MAX) NULL,
    [IsActive]     INT            NULL,
    [IsDeleted]    INT            NULL,
    [CreatedBy]    BIGINT         NULL,
    [CreatedOn]    DATETIME       NULL,
    [UpdatedBy]    BIGINT         NULL,
    [UpdatedOn]    DATETIME       NULL,
    [CompanyId]    BIGINT         NULL
);




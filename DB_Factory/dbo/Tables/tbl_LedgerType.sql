﻿CREATE TABLE [dbo].[tbl_LedgerType] (
    [LedgerTypeId]          BIGINT         IDENTITY (1, 1) NOT NULL,
    [LedgerTypeName]        NVARCHAR (50)  NOT NULL,
    [LedgerTypeDescription] NVARCHAR (100) NULL,
    [IsActive]              INT            NULL,
    [IsDeleted]             INT            NULL,
    [CreatedBy]             BIGINT         NULL,
    [CreatedOn]             DATETIME       NULL,
    [UpdatedBy]             BIGINT         NULL,
    [UpdatedOn]             DATETIME       NULL,
    [CompanyId]             BIGINT         NULL
);




﻿CREATE TYPE [dbo].[EmployeeSalaryDetails] AS TABLE (
    [EmployeeId]    BIGINT          NULL,
    [IsPaid]        BIT             NULL,
    [IsChecked]     BIT             NULL,
    [LoanAmount]    DECIMAL (18, 2) NULL,
    [Paka_Salary]   DECIMAL (18, 2) NULL,
    [Kacha_Salary]  DECIMAL (18, 2) NULL,
    [PF]            DECIMAL (18, 2) NULL,
    [LoanDeduction] DECIMAL (18, 2) NULL);


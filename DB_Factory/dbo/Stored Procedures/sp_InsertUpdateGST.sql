﻿CREATE PROCEDURE  [dbo].[sp_InsertUpdateGST]
-- Add the parameters for the stored procedure here
 @GSTId [bigint]
,@HSN [NVARCHAR](MAX)
,@CGST NUMERIC(18,2)
,@IGST NUMERIC(18,2)
,@Percentage NUMERIC(18,2)
,@GSTDescription [NVARCHAR](MAX)
,@IsActive [int] NULL
,@CreatedBy [bigint] NULL
,@CompanyId [bigint] NULL
AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
--update or insert checking
IF(@GSTId>0)
BEGIN
--Duplicate value check
IF EXISTS(SELECT * FROM tbl_GST WITH(NOLOCK) WHERE HSN = @HSN AND GSTId != @GSTId AND IsDeleted=0)
BEGIN
SELECT 'Already exists.' as Msg,0 as SuccessFlag
END
ELSE
BEGIN
--update
UPDATE tbl_GST
SET
 HSN =UPPER(@HSN)
,CGST              = @CGST
,IGST              = @IGST
,Percentage        = @Percentage
,GSTDescription    = @GSTDescription
,IsActive =@IsActive
,UpdatedBy =@CreatedBy
,UpdatedOn      =GETDATE()
,CompanyId =@CompanyId
WHERE GSTId =@GSTId
SELECT 'GST updated successfully.' as Msg,1 as SuccessFlag
END
END
ELSE
BEGIN
--Duplicate value check
IF EXISTS (SELECT * FROM tbl_GST WITH(NOLOCK) WHERE HSN = @HSN AND IsDeleted=0)
BEGIN
SELECT 'Already exists.' as Msg,0 as SuccessFlag
END
ELSE
BEGIN
--insert
INSERT INTO tbl_GST
(
 HSN 
,CGST           
,IGST           
,Percentage     
,GSTDescription
,IsActive
,IsDeleted
,CreatedBy
,CreatedOn
,CompanyId
)
VALUES
(
UPPER(@HSN)
,@CGST           
,@IGST           
,@Percentage     
,@GSTDescription
,@IsActive
,0
,@CreatedBy
,GETDATE()
,@CompanyId
)
SELECT 'GST created successfully.' as Msg,1 as SuccessFlag
END
END
END TRY
BEGIN CATCH
SELECT 'Sorry something went wrong, unable to process,' as Msg,0 as SuccessFlag
RETURN
END CATCH
END
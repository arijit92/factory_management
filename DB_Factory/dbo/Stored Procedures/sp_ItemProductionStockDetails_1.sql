﻿CREATE PROCEDURE  [dbo].[sp_ItemProductionStockDetails]
@Date datetime = null
AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
SELECT P.Description,P.Unit,IPST.Weight as Weight,IPST.BostaQuantity,IPST.PacketId,IPST.Date FROM 
tbl_ItemProductionStockTransaction IPST INNER JOIN tbl_Packet P 
ON P.PacketId = IPST.PacketId where Date = ISNULL(@Date,Date)
--SELECT 'Alert deleted successfully.' as Msg,1 as SuccessFlag
END TRY
BEGIN CATCH
SELECT 'Sorry something went wrong, unable to process,' as Msg,0 as SuccessFlag
RETURN
END CATCH
END
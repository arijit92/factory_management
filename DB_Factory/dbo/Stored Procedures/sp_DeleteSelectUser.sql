﻿CREATE PROCEDURE  [dbo].[sp_DeleteSelectUser]
-- Add the parameters for the stored procedure here
 @Id [bigint] NULL
,@OperationId [bigint]  --1=delete,2=Select
,@UserId [bigint] NULL

AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
IF(@OperationId  = 1 AND @Id >0)
BEGIN
UPDATE tbl_User
SET
IsDeleted = 1
   ,UpdatedBy = @UserId
   ,UpdatedOn = GETDATE()
    WHERE Id = @Id
SELECT 'User deleted successfully.' as Msg,1 as SuccessFlag
END
ELSE
BEGIN
SELECT TU.*,TR.Name AS RoleName FROM tbl_User TU WITH(NOLOCK)  INNER JOIN tbl_Role TR WITH(NOLOCK)
ON TU.Role = tr.RoleId
WHERE TU.Id = ISNULL(@Id,TU.Id)
END

END TRY
BEGIN CATCH
SELECT 'Sorry something went wrong, unable to process,' as Msg,0 as SuccessFlag
RETURN
END CATCH
END
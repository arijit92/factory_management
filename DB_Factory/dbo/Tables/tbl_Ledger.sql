﻿CREATE TABLE [dbo].[tbl_Ledger] (
    [LedgerId]          BIGINT         IDENTITY (1, 1) NOT NULL,
    [LedgerName]        NVARCHAR (100) NOT NULL,
    [LedgerTypeId]      BIGINT         NOT NULL,
    [LedgerDescription] NVARCHAR (200) NULL,
    [IsActive]          INT            NULL,
    [IsDeleted]         INT            NULL,
    [CreatedBy]         BIGINT         NULL,
    [CreatedOn]         DATETIME       NULL,
    [UpdatedBy]         BIGINT         NULL,
    [UpdatedOn]         DATETIME       NULL,
    [CompanyId]         BIGINT         NULL
);




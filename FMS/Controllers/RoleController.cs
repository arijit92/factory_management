﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FMS.BL;
using FMS.BL.Buisness;
using FMS.BL.Utilities;
namespace FMS.Controllers
{
    public class RoleController : Controller
    {
        // GET: Role  
        public ActionResult List()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Add(int? roleid)
        {
            Role role = null;
            RoleBuisness roleBuisness = null;
            long userid = 11;
            try
            {
                role = new Role();
                roleBuisness = new RoleBuisness();
                if (roleid > 0)
                {
                    role = roleBuisness.GetRole(roleid, userid).SingleOrDefault();
                }


            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Role list", ex, "Role");
            }
            return View(role);
        }
        [HttpPost]
        public ActionResult Add(Role role)
        {
            if (ModelState.IsValid)
            {
                RoleBuisness buisness = new RoleBuisness();
                Status status = new Status();
                status = buisness.AddUpdateRole(role, 1);
                TempData["SuccessFlag"] = status.SuccessFlag;
                TempData["Msg"] = status.Msg;
                if (status.SuccessFlag == "1")
                {
                    return RedirectToAction("List");
                }
                else
                {
                    return View(role);
                }
            }
            return View(role);
        }


        [HttpPost]
        public JsonResult LoadRole(int RoleId)
        {
            long userid = 11;
            List<Role> role = null;
            RoleBuisness roleBuisness = null;
            try
            {
                role = new List<Role>();
                roleBuisness = new RoleBuisness();
                role = roleBuisness.GetRole(RoleId, userid).Where(i => i.IsDeleted == false).ToList();
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Role list", ex, "Role");
            }

            return Json(role, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(int roleid)
        {
            RoleBuisness roleBuisness = null;
            Status status = null;
            long userid = 11;
            try
            {
                roleBuisness = new RoleBuisness();
                status = new Status();
                status = roleBuisness.DeleteRole(roleid, userid);
                TempData["SuccessFlag"] = status.SuccessFlag;
                TempData["Msg"] = status.Msg;
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to delete Role ", ex, "Role");
            }
            return RedirectToAction("List");
        }

        public ActionResult RoleModuleMapping()
        {

            return View();
        }

    }
}
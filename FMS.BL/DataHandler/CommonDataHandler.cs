﻿//***Contains Functionality of CommonDataHandler.cs***//

using System;
using System.Globalization;
using FMS.BL.Utilities;


namespace FMS.BL.DataHandler
{
    /// <summary>
    /// Summary description for CommonDataHandler
    /// </summary>

    public class CommonDataHandler
    {
        #region:Constructor
        public CommonDataHandler()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        #endregion

        #region Combo Population

        /// <summary>
        /// Set Drop Down List Selected.
        /// </summary>
        /// <param name="ddl">Rad Combo box of Telerik Webcontrol</param>
        /// <param name="value">String type value</param>


        #endregion
        #region Get Integer Value from Object

        /// <summary>
        /// Get Integer Value.
        /// </summary>
        /// <param name="obj">Object type obj</param>
        /// <returns></returns>
        public int GetIntegerValue(object obj)
        {
            return GetIntegerValue(obj, 0);
        }
        public int GetIntegerValue(object obj, int defaultReturnValue)
        {
            try
            {
                defaultReturnValue = Convert.ToInt32(obj);
            }
            catch
            {

            }
            return defaultReturnValue;
        }
        /// <summary>
        /// Get Long Value.
        /// </summary>
        /// <param name="obj">Object type obj</param>
        /// <returns></returns>
        public long GetLontValue(object obj)
        {
            return GetLontValue(obj, 0);
        }
        public long GetLontValue(object obj, long defaultReturnValue)
        {
            try
            {
                defaultReturnValue = Convert.ToInt64(obj);
            }
            catch
            {

            }
            return defaultReturnValue;
        }
        #endregion
        #region Get Decimel Value from Object

        public decimal GetDecimelValue(object obj, decimal defaultReturnValue)
        {
            try
            {
                defaultReturnValue = Convert.ToDecimal(obj);
            }
            catch
            {

            }
            return defaultReturnValue;
        }
        public decimal GetDecimelValue(object obj)
        {
            return GetDecimelValue(obj, 0);
        }

        #endregion
        #region Get Boolean Value from Object

        public bool GetBooleanValue(object obj)
        {
            return GetBooleanValue(obj, false);
        }
        public bool GetBooleanValue(object obj, bool defaultValue)
        {
            try
            {
                if (!(obj == null || obj == DBNull.Value))
                    defaultValue = Convert.ToBoolean(obj);
            }
            catch
            {
            }
            return defaultValue;
        }

        #endregion
        #region Get DateTime Value from Object


        public DateTime GetDateTimeValue(object obj)
        {
            return GetDateTimeValue(obj, Convert.ToDateTime("1900-01-01"));
        }

        public DateTime GetDateTimeValue(object obj, DateTime defaultValue)
        {
            try
            {
                if (!(obj == null || obj == DBNull.Value))
                {
                    defaultValue = Convert.ToDateTime(obj);
                }
            }
            catch
            {
            }
            return defaultValue;
        }



        #endregion
        #region Get String Value from Object

        public string GetStringValue(object obj)
        {
            return GetStringValue(obj, "");
        }
        public string GetStringValue(object obj, string defaultValue)
        {
            if (!(obj == null || obj == DBNull.Value))
                defaultValue = obj.ToString();
            return defaultValue;
        }
        public long GetLongValue(object obj)
        {
            return GetLongValue(obj, 0);
        }
        public long GetLongValue(object obj, long defaultValue)
        {
            if (!(obj == null || obj == DBNull.Value))
                try
                {
                    defaultValue = Convert.ToInt64(obj);
                }
                catch { }
            return defaultValue;
        }

        #endregion


        #region Decimal Value Handling
        public decimal GetDecimalValue(Object value)
        {
            try
            {
                return Convert.ToDecimal(value);
            }
            catch
            {
                return 0;
            }
        }
        public decimal GetDecimalPlaceValue(string Value, int DecimalPlace)
        {
            return MakeRoundOffDecimal(Value.ToString(), DecimalPlace);
        }
        public string GetDecimalPlaceValue(object Value, object DecimalPlace)
        {
            return GetDecimalPlaceValue(GetDecimalValue(Value), GetIntegerValue(DecimalPlace)).ToString();
        }
        public decimal GetDecimalPlaceValue(decimal Value, int DecimalPlace)
        {
            return MakeRoundOffDecimal(Value.ToString(), DecimalPlace);
        }

        public decimal MakeRoundOffDecimal(decimal inPutVal, int decimalPlace)
        {
            decimal outPut = 0M;
            try
            {
                decimal mval = Math.Round(inPutVal, MidpointRounding.AwayFromZero);
                outPut = decimal.Parse(mval.ToString("N2", CultureInfo.InvariantCulture));

            }
            catch { }
            return outPut;
        }

        /// <summary>
        /// This function for decimal round off.
        /// </summary>
        /// <param name="inPutVal">This is input value</param>
        /// <param name="decimalPlace">This is decimal place value</param>
        /// <returns></returns>
        public decimal MakeRoundOffDecimal(string inPutVal, int decimalPlace)
        {
            decimal val = decimal.Round(Convert.ToDecimal(inPutVal.ToString(CultureInfo.InvariantCulture)), decimalPlace);
            decimal mval = Math.Round(Convert.ToDecimal(inPutVal.ToString(CultureInfo.InvariantCulture)), decimalPlace);
            decimal outPut = Convert.ToDecimal(inPutVal.ToString(CultureInfo.InvariantCulture));
            try
            {
                string ValBeforePoint = String.Empty;
                string valAfterPoint = String.Empty;
                string tempValAfterPoint = String.Empty;
                string valMultiplier = ".";

                for (int i = 0; i < decimalPlace; i++)
                {
                    if (i == (decimalPlace - 1))
                    {
                        valMultiplier += "1";
                    }
                    else
                    {
                        valMultiplier += "0";
                    }
                }

                if (inPutVal.IndexOf(".") != -1)
                {
                    ValBeforePoint = inPutVal.Substring(0, inPutVal.IndexOf('.'));
                    if (inPutVal.Substring(inPutVal.IndexOf('.') + 1).Length <= decimalPlace)
                    {
                        for (int j = inPutVal.Substring(inPutVal.IndexOf('.') + 1).Length; j <= decimalPlace; j++)
                        { inPutVal += "0"; }
                        valAfterPoint = inPutVal.Substring(inPutVal.IndexOf('.') + 1, decimalPlace);
                    }
                    valAfterPoint = inPutVal.Substring(inPutVal.IndexOf('.') + 1, decimalPlace + 1);
                }
                else if (inPutVal.IndexOf(",") != -1) // This condition Added by Partha . For handling Eurpe Version
                {
                    ValBeforePoint = inPutVal.Substring(0, inPutVal.IndexOf(','));
                    if (inPutVal.Substring(inPutVal.IndexOf(',') + 1).Length <= decimalPlace)
                    {
                        for (int j = inPutVal.Substring(inPutVal.IndexOf(',') + 1).Length; j <= decimalPlace; j++)
                        { inPutVal += "0"; }
                        valAfterPoint = inPutVal.Substring(inPutVal.IndexOf(',') + 1, decimalPlace);
                    }
                    valAfterPoint = inPutVal.Substring(inPutVal.IndexOf(',') + 1, decimalPlace + 1);
                }
                else
                {
                    ValBeforePoint = inPutVal;
                }
                if (valAfterPoint.Trim().Length > 0)
                {
                    char[] arr = valAfterPoint.ToCharArray();

                    string lastItemValue = GetStringValue(arr[decimalPlace].ToString());


                    if (inPutVal.Contains(","))
                    {
                        valMultiplier = valMultiplier.Replace('.', ',');
                        if (GetIntegerValue(lastItemValue) >= 5)
                        {
                            tempValAfterPoint = inPutVal.Substring(inPutVal.IndexOf(',') + 1, decimalPlace);
                            valAfterPoint = GetStringValue(GetIntegerValue(tempValAfterPoint) + 1);
                            outPut = Convert.ToDecimal(ValBeforePoint) + (Convert.ToDecimal(valAfterPoint) * Convert.ToDecimal(valMultiplier));
                        }
                        else
                        {
                            tempValAfterPoint = inPutVal.Substring(inPutVal.IndexOf(',') + 1, decimalPlace);
                            if (Convert.ToDecimal(ValBeforePoint) >= 0)
                            {
                                outPut = Convert.ToDecimal(ValBeforePoint) + (Convert.ToDecimal(tempValAfterPoint) * Convert.ToDecimal(valMultiplier));
                            }
                            else
                            {
                                outPut = Convert.ToDecimal(ValBeforePoint) - (Convert.ToDecimal(tempValAfterPoint) * Convert.ToDecimal(valMultiplier));
                            }
                        }
                    }
                    else
                    {

                        if (GetIntegerValue(lastItemValue) >= 5)
                        {
                            tempValAfterPoint = inPutVal.Substring(inPutVal.IndexOf('.') + 1, decimalPlace);
                            valAfterPoint = GetStringValue(GetIntegerValue(tempValAfterPoint) + 1);
                            outPut = Convert.ToDecimal(ValBeforePoint) + (Convert.ToDecimal(valAfterPoint) * Convert.ToDecimal(valMultiplier));
                        }
                        else
                        {
                            tempValAfterPoint = inPutVal.Substring(inPutVal.IndexOf('.') + 1, decimalPlace);
                            //outPut = Convert.ToDecimal(ValBeforePoint) + (Convert.ToDecimal(tempValAfterPoint) * Convert.ToDecimal(valMultiplier));
                            if (Convert.ToDecimal(ValBeforePoint) >= 0)
                            {
                                outPut = Convert.ToDecimal(ValBeforePoint) + (Convert.ToDecimal(tempValAfterPoint) * Convert.ToDecimal(valMultiplier));
                            }
                            else
                            {
                                outPut = Convert.ToDecimal(ValBeforePoint) - (Convert.ToDecimal(tempValAfterPoint) * Convert.ToDecimal(valMultiplier));
                            }
                        }
                    }
                }
            }
            catch { }
            outPut = decimal.Parse(outPut.ToString("N2", CultureInfo.InvariantCulture));
            return outPut;
        }


        /// <summary>
        /// This Methods for round off item weight for(USPS) 
        /// EX: .15 Pound =1 Pound
        /// </summary>
        /// <param name="inPutVal"></param>
        /// <returns></returns>

        public int MakeRoundOffDecimal(string inPutVal)
        {
            int contenerVal = 0;
            int intValAfterPoint = 0;

            string ValBeforePoint = inPutVal.Substring(0, inPutVal.IndexOf('.'));
            string valAfterPoint = inPutVal.Substring(inPutVal.IndexOf('.') + 1);

            try
            {
                contenerVal = Convert.ToInt32(ValBeforePoint);
                intValAfterPoint = Convert.ToInt32(valAfterPoint);
                if (intValAfterPoint > 0)
                {
                    contenerVal += 1;
                }
            }
            catch
            {
                contenerVal += 1;
            }
            return contenerVal;
        }
        private string AddOne(int DecimalPlace)
        {
            string Outpout = ".";
            for (int i = 1; i < DecimalPlace; i++)
            {
                Outpout = Outpout + "0";
            }
            Outpout = Outpout + "1";
            return Outpout;
        }

        #endregion
        #region GUID - String Handler

        public Guid StringToGuid(string sValue)
        {
            System.Guid guid = new Guid(sValue);
            return guid;
        }

        #endregion
        #region GetResourceText

        #endregion
        #region Get QueryString Value

        //public string GetQueryStringValue(string key)
        //{
        //    return GetQueryStringValue(key, "");
        //}

        //public string GetQueryStringValue(string key, string defaultValue)
        //{
        //    if (HttpContext.Current.Request.QueryString[key] != null)
        //        defaultValue = HandleQuota.HandleInputString(GetStringValue(HttpContext.Current.Request.QueryString[key]));
        //    return defaultValue;
        //}

        #endregion

        public string GetDecimalPlaceValueWithoutRound(object inPutVal)
        {
            int decimalPlace = 2;
            decimal outPut = Convert.ToDecimal(inPutVal);
            try
            {
                ////outPut = decimal.Round(Convert.ToDecimal(inPutVal), decimalPlace);
                outPut = decimal.Round(Convert.ToDecimal(inPutVal), decimalPlace);
            }
            catch { }
            return Convert.ToString(outPut);
        }
    }
}

﻿CREATE PROCEDURE  [dbo].[sp_DeleteSelectAlert]
-- Add the parameters for the stored procedure here
 @AlertId [bigint] NULL
,@OperationId [bigint]  --1=delete,2=Select
,@UserId [bigint] NULL

AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
IF(@OperationId  = 1 AND @AlertId >0)
BEGIN
UPDATE tbl_Alert
SET
IsDeleted = 1
   ,UpdatedBy = @UserId
   ,UpdatedOn = GETDATE()
    WHERE AlertId = @AlertId
SELECT 'Alert deleted successfully.' as Msg,1 as SuccessFlag
END
ELSE
BEGIN
SELECT *,CONVERT(VARCHAR(100),IssueDate,103 ) AS StrIssueDate,CONVERT(VARCHAR(100),NextRenewalDate,103 ) AS StrNextRenewalDate FROM tbl_Alert WITH(NOLOCK)
WHERE AlertId = ISNULL(@AlertId,AlertId)
END

END TRY
BEGIN CATCH
SELECT 'Sorry something went wrong, unable to process,' as Msg,0 as SuccessFlag
RETURN
END CATCH
END
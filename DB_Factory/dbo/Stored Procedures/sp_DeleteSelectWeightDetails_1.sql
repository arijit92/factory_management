﻿CREATE PROCEDURE  [dbo].[sp_DeleteSelectWeightDetails]
-- Add the parameters for the stored procedure here
 @TransactionId [bigint] NULL
--,@OperationId [bigint]  --1=delete,2=Select


AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY

SELECT TWD.*,TU.UnitName,CONVERT(VARCHAR(100),TWD.Date_First_Weight,22 ) AS str_Date_First_Weight,CONVERT(VARCHAR(100),TWD.Date_Second_Weight,22 ) AS str_Date_Second_Weight FROM tbl_WeightDetails TWD WITH(NOLOCK) INNER JOIN tbl_Unit TU ON TU.UnitId = TWD.UnitId 
WHERE TransactionId = ISNULL(@TransactionId,TransactionId)


END TRY
BEGIN CATCH
SELECT 'Sorry something went wrong, unable to process,' as Msg,0 as SuccessFlag
RETURN
END CATCH
END
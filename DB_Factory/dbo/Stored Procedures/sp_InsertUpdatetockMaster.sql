﻿CREATE PROCEDURE  [dbo].[sp_InsertUpdatetockMaster]
-- Add the parameters for the stored procedure here
 @AccessoriesId [bigint]
,@Remarks [NVARCHAR](MAX) NULL
,@Quantity numeric(18,2)
,@Type [NVARCHAR](20)
AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
--update or insert checking
IF EXISTS(Select * from tbl_StockMaster where AccessoriesId=@AccessoriesId)
BEGIN
DECLARE @AvailableQuantity numeric(18,2)
SET @AvailableQuantity = (SELECT AvailableQuantity from tbl_StockMaster  where AccessoriesId=@AccessoriesId)
IF(@Type='Add')
BEGIN
Update tbl_StockMaster
SET AvailableQuantity = @AvailableQuantity+@Quantity
 where AccessoriesId=@AccessoriesId
 END
 ELSE
 BEGIN
 Update tbl_StockMaster
SET AvailableQuantity = @AvailableQuantity-@Quantity
 where AccessoriesId=@AccessoriesId

 END
 
INSERT INTO tbl_StockHistory (Accessories,Quantity,EntryDate,UpdatedBy,Type,Remarks) VALUES(@AccessoriesId,@Quantity,GETDATE(),1,@Type,@Remarks)
SELECT 'Stock updated successfully.' as Msg,1 as SuccessFlag
END
ELSE
BEGIN
INSERT INTO tbl_StockMaster(AccessoriesId,AvailableQuantity,UpdatedOn) VALUES (@AccessoriesId,@Quantity,GETDATE())
INSERT INTO tbl_StockHistory (Accessories,Quantity,EntryDate,UpdatedBy,Type,Remarks) VALUES(@AccessoriesId,@Quantity,GETDATE(),1,@Type,@Remarks)
SELECT 'Stock updated successfully.' as Msg,1 as SuccessFlag
END
END TRY
BEGIN CATCH
SELECT 'Sorry something went wrong, unable to process,' as Msg,0 as SuccessFlag
RETURN
END CATCH
END
﻿CREATE PROCEDURE  [dbo].[sp_InsertUpdateTransaction]
 @TransactionID [bigint],
 @IFSC [Nvarchar](50) NULL,
 @ChequeNo [Nvarchar](50) NULL,
 @DraftNo [Nvarchar](50) NULL,
 @BankName [Nvarchar](50) NULL,
 @LedgerId int null,
 @Amount decimal(18,2) null,
 @PaymentMode nvarchar(50),
 @Partyname nvarchar(100) null,
 @Operations_Details [Operations] ReadOnly
AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
IF(@TransactionID>0)
BEGIN
UPDATE tbl_TransactionMaster 
SET
IFSC = @IFSC,
ChequeNo= @ChequeNo,
DraftNo = @DraftNo,
BankName = @BankName,
Amount = @Amount,
LedgerId = @LedgerId,
PaymentMode = @PaymentMode,
Partyname = @Partyname,
TransactionDate =  GETDATE()
WHERE TransactionID = @TransactionID;

Delete from [dbo].[tbl_TransactionDetails] WHERE TransactionID=@TransactionID

END
ELSE
BEGIN
INSERT INTO [dbo].[tbl_TransactionMaster]
(TransactionDate
,IFSC
,ChequeNo
,DraftNo
,BankName
,Amount
,LedgerId
,PaymentMode
,Partyname)
VALUES
(GETDATE()
,@IFSC
,@ChequeNo
,@DraftNo
,@BankName
,@Amount
,@LedgerId
,@PaymentMode
,@Partyname)
SET @TransactionID=(SELECT SCOPE_IDENTITY())
    END
INSERT INTO dbo.tbl_TransactionDetails([TransactionID],[OperationID],[LedgerID],[Ammount])  
    SELECT @TransactionID,E.[OperationID],E.[LedgerID],E.[Ammount] FROM @Operations_Details AS E


SELECT 'Operation updated successfully.' as Msg,1 as SuccessFlag
END TRY
BEGIN CATCH
SELECT 'Sorry something went wrong, unable to process,' as Msg,0 as SuccessFlag
RETURN
END CATCH
END
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FMS.BL;
using FMS.BL.Buisness;
using FMS.BL.Utilities;

namespace FMS.Controllers
{
    public class WastageController : Controller
    {
        // GET: Wastage
        public ActionResult Release()
        {
            ViewBag.ParentMenu = "Wastage";
            ViewBag.ChildMenu = "Release";
            List<ItemMaster> itemMasters = new List<ItemMaster>();
            ItemBuisness itemBuisness = new ItemBuisness();
            itemMasters = itemBuisness.GetItems(0, 1).Where(i => i.CategoryId==3).ToList();
            ViewBag.ItemList = new SelectList(itemMasters, "Id", "DropdownValue", 0);
            return View();
        }

        [HttpPost]
        public ActionResult Release(Wastage wastage)
        {
            ViewBag.ParentMenu = "Wastage";
            ViewBag.ChildMenu = "Release";
            List<ItemMaster> itemMasters = new List<ItemMaster>();
            ItemBuisness itemBuisness = new ItemBuisness();
            itemMasters = itemBuisness.GetItems(0, 1).Where(i => i.CategoryId == 3).ToList();
            ViewBag.ItemList = new SelectList(itemMasters, "Id", "DropdownValue", 0);
            Status status = new Status();
            WastageBuisness wastageBuisness = new WastageBuisness();
            if (ModelState.IsValid)
            {
                status = wastageBuisness.AddUpdateWastage(wastage);
                TempData["SuccessFlag"] = status.SuccessFlag;
                TempData["Msg"] = status.Msg;
                if (status.SuccessFlag == "1")
                {
                    return RedirectToAction("WastageList");
                }
                else
                {
                    return View(wastage);
                }
            }
            else
            {
                return View(wastage);
            }
        }

        public ActionResult WastageList()
        {
            ViewBag.ParentMenu = "Wastage";
            ViewBag.ChildMenu = "WastageList";

            return View();
        }

        [HttpPost]
        public JsonResult GetWasteDetails()
        {
            long userid = 11;
            List<Wastage> wastages = null;
            WastageBuisness buisness = null;
            try
            {
                wastages = new List<Wastage>();
                buisness = new WastageBuisness();
                wastages = buisness.GetWasteDetails().ToList();
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Role list", ex, "Role");
            }

            return Json(wastages, JsonRequestBehavior.AllowGet);
        }
    }
}
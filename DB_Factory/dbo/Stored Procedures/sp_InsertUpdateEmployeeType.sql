﻿CREATE PROCEDURE  [dbo].[sp_InsertUpdateEmployeeType]
-- Add the parameters for the stored procedure here
 @EmployeeTypeId [bigint]
,@EmployeeTypeName [NVARCHAR](MAX)
,@EmployeeTypeDescription [NVARCHAR](MAX)
,@IsActive [int] NULL
,@CreatedBy [bigint] NULL
,@CompanyId [bigint] NULL
AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
--update or insert checking
IF(@EmployeeTypeId>0)
BEGIN
--Duplicate value check
IF EXISTS(SELECT * FROM tbl_EmployeeType WITH(NOLOCK) WHERE EmployeeTypeName = @EmployeeTypeName AND EmployeeTypeId != @EmployeeTypeId)
BEGIN
SELECT 'Already exists.' as Msg,0 as SuccessFlag
END
ELSE
BEGIN
--update
UPDATE tbl_EmployeeType
SET
EmployeeTypeName =UPPER(@EmployeeTypeName)
,EmployeeTypeDescription = @EmployeeTypeDescription
,IsActive =@IsActive
,UpdatedBy =@CreatedBy
,UpdatedOn      =GETDATE()
,CompanyId =@CompanyId
WHERE EmployeeTypeId =@EmployeeTypeId
SELECT 'EmployeeType updated successfully.' as Msg,1 as SuccessFlag
END
END
ELSE
BEGIN
--Duplicate value check
IF EXISTS (SELECT * FROM tbl_EmployeeType WITH(NOLOCK) WHERE EmployeeTypeName = @EmployeeTypeName)
BEGIN
SELECT 'Already exists.' as Msg,0 as SuccessFlag
END
ELSE
BEGIN
--insert
INSERT INTO tbl_EmployeeType
(
 EmployeeTypeName
,EmployeeTypeDescription 
,IsActive
,IsDeleted
,CreatedBy
,CreatedOn
,CompanyId
)
VALUES
(
UPPER(@EmployeeTypeName)
,@EmployeeTypeDescription
,@IsActive
,0
,@CreatedBy
,GETDATE()
,@CompanyId
)
SELECT 'EmployeeType created successfully.' as Msg,1 as SuccessFlag
END
END
END TRY
BEGIN CATCH
SELECT 'Sorry something went wrong, unable to process,' as Msg,0 as SuccessFlag
RETURN
END CATCH
END
﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FMS.BL.Utilities;


namespace FMS.BL.Buisness
{
    public class MachineBuisness : Common
    {
        #region Machine region

        public Status AddUpdateMachine(Machine pmodel, long UserID)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    para.Add("@MachineId", pmodel.MachineId);
                    para.Add("@MachineName", pmodel.MachineName);
                    para.Add("@Quantity", pmodel.Quantity);
                    para.Add("@UnitId", pmodel.UnitId);
                    para.Add("@CostOfMachineries", pmodel.CostOfMachineries);
                    para.Add("@IsActive", Convert.ToInt32(pmodel.IsActive == false ? 0 : 1));
                    para.Add("@CreatedBy", UserID);
                    para.Add("@CompanyId", pmodel.CompanyId);
                    const string procName = "[dbo].[sp_InsertUpdateMachine]";
                    connection.Open();
                    return connection.Query<Status>(procName, para, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to insert or update Machine", ex, "AddUpdateMachine");
                return null;
            }

        }
        public List<Machine> GetMachine(long? MachineId, long UserID)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    if (MachineId == 0)
                    {
                        MachineId = null;
                    }
                    para.Add("@MachineId", MachineId);
                    para.Add("@OperationId", 2);
                    para.Add("@UserId", UserID);
                    const string procName = "[dbo].[sp_DeleteSelectMachine]";
                    connection.Open();
                    return connection.Query<Machine>(procName, para, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Machine", ex, "GetMachine");
                return null;
            }

        }
        public Status DeleteMachine(long? MachineId, long UserID)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    para.Add("@MachineId", MachineId);
                    para.Add("@OperationId", 1);
                    para.Add("@UserId", UserID);
                    const string procName = "[dbo].[sp_DeleteSelectMachine]";
                    connection.Open();
                    return connection.Query<Status>(procName, para, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to delete Machine", ex, "DeleteMachine");
                return null;
            }

        }
        #endregion
    }
}

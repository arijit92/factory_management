using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Collections;
using System.Text.RegularExpressions;
using System.Reflection;
using System.IO;
using System.Security.Cryptography;
using iTextSharp.text.pdf;
using iTextSharp.text;

namespace FMS.BL.Utilities
{
    public class Common
    {
        public static string Encrypt(string input)
        {
            //  string CryptoKey = System.Configuration.ConfigurationManager.AppSettings["CryptoKey"];
            string CryptoKey = "ihn-chaps-dcgind";

            byte[] inputArray = UTF8Encoding.UTF8.GetBytes(input);
            TripleDESCryptoServiceProvider tripleDES = new TripleDESCryptoServiceProvider();
            tripleDES.Key = UTF8Encoding.UTF8.GetBytes(CryptoKey);
            tripleDES.Mode = CipherMode.ECB;
            tripleDES.Padding = PaddingMode.PKCS7;
            ICryptoTransform cTransform = tripleDES.CreateEncryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(inputArray, 0, inputArray.Length);
            tripleDES.Clear();
            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }
        public static string Decrypt(string input)
        {
          //  string CryptoKey = System.Configuration.ConfigurationManager.AppSettings["CryptoKey"];
            string CryptoKey = "ihn-chaps-dcgind";
            byte[] inputArray = Convert.FromBase64String(input);
            TripleDESCryptoServiceProvider tripleDES = new TripleDESCryptoServiceProvider();
            tripleDES.Key = UTF8Encoding.UTF8.GetBytes(CryptoKey);
            tripleDES.Mode = CipherMode.ECB;
            tripleDES.Padding = PaddingMode.PKCS7;
            ICryptoTransform cTransform = tripleDES.CreateDecryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(inputArray, 0, inputArray.Length);
            tripleDES.Clear();
            return UTF8Encoding.UTF8.GetString(resultArray);
        }

        public static void AddCellToHeader(PdfPTable tableLayout, string cellText)
        {

            tableLayout.AddCell(new PdfPCell(new Phrase(cellText, new Font(Font.FontFamily.HELVETICA, 8, 1, iTextSharp.text.BaseColor.BLACK)))
            {

                HorizontalAlignment = Element.ALIGN_CENTER,
                Padding = 5,
                BackgroundColor = new iTextSharp.text.BaseColor(207, 207, 207)
            });
        }

        public static void AddCellToBody(PdfPTable tableLayout, string cellText, int cell)
        {
            tableLayout.AddCell(new PdfPCell(new Phrase(cellText, new Font(Font.FontFamily.HELVETICA, 8, 1, iTextSharp.text.BaseColor.BLACK)))
            {
                HorizontalAlignment = (cell == 1 ? Element.ALIGN_LEFT : Element.ALIGN_CENTER),
                Padding = 5,
                BackgroundColor = new iTextSharp.text.BaseColor(250, 250, 252)
            });
        }

        public static bool IsDate(string vDate)
        {
            try
            {
                DateTime dt = Convert.ToDateTime(vDate);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool IsNumeric(object o)
        {
            try
            {
                Convert.ToDouble(o);
            }
            catch
            {
                return false;
            }
            return true;
        }

        public static bool IsNull(object s)
        {
            try
            {
                if (s == null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }

        public static bool IsEmpty(string s)
        {
            try
            {
                if (s.Trim().Equals(String.Empty))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }

        public static bool IsBoolean(object o)
        {
            try
            {
                Convert.ToBoolean(o);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool IsValidType(string stringToMatch, ValidationType validationType)
        {
            bool validType = false;
            Regex regEx = new Regex("");
            if (validationType == ValidationType.Email)
            {
                regEx = new Regex("^[\\w-\\.]{1,}\\@([\\da-zA-Z-]{1,}\\.){1,}[\\da-zA-Z-]{2,3}$");
            }
            else if (validationType == ValidationType.PhoneNumber)
            {
                regEx = new Regex("((\\(\\d{3}\\) ?)|(\\d{3}-))?\\d{3}-\\d{4}( x\\d{0,})?");
            }
            MatchCollection matchCollection = regEx.Matches(stringToMatch);

            foreach (Match match in matchCollection)
            {
                if (match.Success)
                {
                    validType = true;
                }
                else
                {
                    validType = false;
                }
            }
            return validType;
        }

        public static ArrayList ConvertSingleColumnToArrayList(DataTable dataTable)
        {
            return ConvertSingleColumnToArrayList(dataTable, 0);
        }

        public static ArrayList ConvertSingleColumnToArrayList(DataTable dataTable, int columnIndex)
        {
            ArrayList arrayList = new ArrayList();
            for (int rowCount = 0; rowCount < dataTable.Rows.Count; rowCount++)
            {
                arrayList.Add(dataTable.Rows[rowCount][columnIndex]);
            }
            return arrayList;
        }

        public static Hashtable ConvertDataRowToHashtable(DataTable dataTable)
        {
            Hashtable hashTable = new Hashtable();
            for (int i = 0; i < dataTable.Columns.Count; i++)
            {
                hashTable.Add(dataTable.Columns[i].ColumnName, dataTable.Rows[0][i].ToString());
            }
            return hashTable;
        }

        public static Hashtable ConvertDataRowToHashtable(DataTable dataTable, int rowNumber)
        {
            Hashtable hashTable = new Hashtable();
            for (int i = 0; i < dataTable.Columns.Count; i++)
            {
                hashTable.Add(dataTable.Columns[i].ColumnName, dataTable.Rows[rowNumber][i].ToString());
            }
            return hashTable;
        }

        public static Hashtable ConvertDataTableToHashtable(DataTable dataTable, string keyField, string valueField)
        {
            return ConvertDataTableToHashtable(dataTable, dataTable.Columns[keyField].Ordinal, dataTable.Columns[valueField].Ordinal);
        }

        public static Hashtable ConvertDataTableToHashtable(DataTable dataTable, int keyFieldIndex, int valueFieldIndex)
        {
            int[] arrkeyFieldIndex = new int[1];
            arrkeyFieldIndex[0] = keyFieldIndex;
            return ConvertDataTableToHashtable(dataTable, arrkeyFieldIndex, valueFieldIndex, null, SeparatorType.NoSeparator);
        }

        public static Hashtable ConvertDataTableToHashtable(DataTable dataTable, string[] keyField, string valueField)
        {
            int[] keyFieldIndex = new int[keyField.Length];
            for (int i = 0; i < keyFieldIndex.Length; i++)
            {
                keyFieldIndex[i] = dataTable.Columns[keyField[i]].Ordinal;
            }
            return ConvertDataTableToHashtable(dataTable, keyFieldIndex, dataTable.Columns[valueField].Ordinal);
        }

        public static Hashtable ConvertDataTableToHashtable(DataTable dataTable, int[] keyFieldIndex, int valueFieldIndex)
        {
            string[] keyFieldSeparator = new string[keyFieldIndex.Length];
            keyFieldSeparator[0] = " [";
            for (int i = 1; i < (keyFieldSeparator.Length - 1); i++)
            {
                keyFieldSeparator[i] = "] [";
            }
            keyFieldSeparator[keyFieldSeparator.Length - 1] = "]";
            return ConvertDataTableToHashtable(dataTable, keyFieldIndex, valueFieldIndex, keyFieldSeparator, SeparatorType.ClosedSeparator);
        }

        public static Hashtable ConvertDataTableToHashtable(DataTable dataTable, string[] keyField, string valueField, string[] keyFieldSeparator, SeparatorType separatorType)
        {
            int[] keyFieldIndex = new int[keyField.Length];
            for (int i = 0; i < keyFieldIndex.Length; i++)
            {
                keyFieldIndex[i] = dataTable.Columns[keyField[i]].Ordinal;
            }
            return ConvertDataTableToHashtable(dataTable, keyFieldIndex, dataTable.Columns[valueField].Ordinal, keyFieldSeparator, separatorType);
        }

        public static Hashtable ConvertDataTableToHashtable(DataTable dataTable, int[] keyFieldIndex, int valueFieldIndex, string[] keyFieldSeparator, SeparatorType separatorType)
        {
            if (separatorType == SeparatorType.ClosedSeparator)
            {
                if (keyFieldIndex.Length != keyFieldSeparator.Length)
                {
                    throw new ArgumentException("Number of KeyFields mismatches number of Separators");
                }
            }
            else if (separatorType == SeparatorType.OpenSeparator)
            {
                if (keyFieldIndex.Length != (keyFieldSeparator.Length - 1))
                {
                    throw new ArgumentException("Number of KeyFields mismatches number of Separators");
                }
            }

            StringBuilder sKey = new StringBuilder();
            Hashtable hashTable = new Hashtable();
            SortedList sortedList = new SortedList();

            for (int j = 0; j < dataTable.Rows.Count; j++)
            {
                for (int i = 0; i < keyFieldIndex.Length; i++)
                {
                    sKey.Append(dataTable.Rows[j][keyFieldIndex[i]].ToString());
                    if (keyFieldSeparator != null)
                    {
                        if (i < keyFieldSeparator.Length)
                        {
                            sKey.Append(keyFieldSeparator[i].ToString());
                        }
                    }
                }
                sortedList.Add(sKey.ToString(), dataTable.Rows[j][valueFieldIndex]);
                sKey.Remove(0, sKey.Length);
            }
            sortedList.TrimToSize();
            for (int k = 0; k < sortedList.Count; k++)
            {
                hashTable.Add(sortedList.GetKey(k), sortedList.GetByIndex(k));
            }
            return hashTable;
        }

        public static SortedList ConvertDataTableToSortedList(DataTable dataTable, string keyField, string valueField)
        {
            return ConvertDataTableToSortedList(dataTable, dataTable.Columns[keyField].Ordinal, dataTable.Columns[valueField].Ordinal);
        }

        public static SortedList ConvertDataTableToSortedList(DataTable dataTable, int keyFieldIndex, int valueFieldIndex)
        {
            int[] arrkeyFieldIndex = new int[1];
            arrkeyFieldIndex[0] = keyFieldIndex;
            return ConvertDataTableToSortedList(dataTable, arrkeyFieldIndex, valueFieldIndex, null, SeparatorType.NoSeparator);
        }

        public static SortedList ConvertDataTableToSortedList(DataTable dataTable, string[] keyField, string valueField)
        {
            int[] keyFieldIndex = new int[keyField.Length];
            for (int i = 0; i < keyFieldIndex.Length; i++)
            {
                keyFieldIndex[i] = dataTable.Columns[keyField[i]].Ordinal;
            }
            return ConvertDataTableToSortedList(dataTable, keyFieldIndex, dataTable.Columns[valueField].Ordinal);
        }

        public static SortedList ConvertDataTableToSortedList(DataTable dataTable, int[] keyFieldIndex, int valueFieldIndex)
        {
            string[] keyFieldSeparator = new string[keyFieldIndex.Length];
            keyFieldSeparator[0] = " [";
            for (int i = 1; i < (keyFieldSeparator.Length - 1); i++)
            {
                keyFieldSeparator[i] = "] [";
            }
            keyFieldSeparator[keyFieldSeparator.Length - 1] = "]";
            return ConvertDataTableToSortedList(dataTable, keyFieldIndex, valueFieldIndex, keyFieldSeparator, SeparatorType.ClosedSeparator);
        }

        public static SortedList ConvertDataTableToSortedList(DataTable dataTable, string[] keyField, string valueField, string[] keyFieldSeparator, SeparatorType separatorType)
        {
            int[] keyFieldIndex = new int[keyField.Length];
            for (int i = 0; i < keyFieldIndex.Length; i++)
            {
                keyFieldIndex[i] = dataTable.Columns[keyField[i]].Ordinal;
            }
            return ConvertDataTableToSortedList(dataTable, keyFieldIndex, dataTable.Columns[valueField].Ordinal, keyFieldSeparator, separatorType);
        }

        public static SortedList ConvertDataTableToSortedList(DataTable dataTable, int[] keyFieldIndex, int valueFieldIndex, string[] keyFieldSeparator, SeparatorType separatorType)
        {
            if (separatorType == SeparatorType.ClosedSeparator)
            {
                if (keyFieldIndex.Length != keyFieldSeparator.Length)
                {
                    throw new ArgumentException("Number of KeyFields mismatches number of Separators");
                }
            }
            else if (separatorType == SeparatorType.OpenSeparator)
            {
                if (keyFieldIndex.Length != (keyFieldSeparator.Length - 1))
                {
                    throw new ArgumentException("Number of KeyFields mismatches number of Separators");
                }
            }

            StringBuilder sKey = new StringBuilder();
            SortedList sortedList = new SortedList();

            for (int j = 0; j < dataTable.Rows.Count; j++)
            {
                for (int i = 0; i < keyFieldIndex.Length; i++)
                {
                    sKey.Append(dataTable.Rows[j][keyFieldIndex[i]].ToString());
                    if (keyFieldSeparator != null)
                    {
                        if (i < keyFieldSeparator.Length)
                        {
                            sKey.Append(keyFieldSeparator[i].ToString());
                        }
                    }
                }
                sortedList.Add(sKey.ToString(), dataTable.Rows[j][valueFieldIndex]);
                sKey.Remove(0, sKey.Length);
            }
            sortedList.TrimToSize();

            return sortedList;
        }

        public static DataTable CopySelectedRowsFromDataSetToDatatable(DataSet ds, string filterString, int tableIndex)
        {
            DataTable dt = new DataTable();
            DataRow[] dr;

            dt = ds.Tables[tableIndex].Clone();
            dr = ds.Tables[tableIndex].Select(filterString);
            for (int j = 0; j < dr.Length; j++)
            {
                dt.ImportRow(dr[j]);
            }
            dt.AcceptChanges();

            return dt;
        }

        public static DataTable AddNewRowToDataTable(DataTable dataTable, Object[] propertyArray)
        {
            DataRow dataRow = dataTable.NewRow();
            dataRow.ItemArray = propertyArray;
            dataTable.Rows.Add(dataRow);
            dataTable.AcceptChanges();
            return dataTable;
        }

        public static DataTable UpdateDataTableRow(DataTable dataTable, Object[] propertyArray, int condition, String DataTableKeyField)
        {
            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                if (dataTable.Rows[i][DataTableKeyField].ToString().Equals(condition.ToString()))
                {
                    dataTable.Rows[i].BeginEdit();
                    for (int j = 0; j < propertyArray.Length; j++)
                    {
                        dataTable.Rows[i][j] = propertyArray[j];
                    }
                    dataTable.Rows[i].EndEdit();
                    dataTable.Rows[i].AcceptChanges();
                    break;
                }
            }
            return dataTable;
        }

        public static DataTable DeleteDataTableRow(DataTable dataTable, int condition, String DataTableKeyField)
        {
            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                if (dataTable.Rows[i][DataTableKeyField].ToString().Equals(condition.ToString()))
                {
                    dataTable.Rows[i].Delete();
                    dataTable.Rows[i].AcceptChanges();
                    break;
                }
            }
            return dataTable;
        }

        public static DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            return dataTable;

        }

        public static string HandleError(Exception exception)
        {
            String errorMessage = "";
            try
            {
                ErrorType errorType = (ErrorType)Convert.ToInt32(exception.Message.Substring(0, 1));
                if (errorType == ErrorType.Null)
                {
                    errorMessage = exception.Message.Substring(3, exception.Message.Length) + "is NULL.";
                }
                else if (errorType == ErrorType.NonNumeric)
                {
                    errorMessage = exception.Message.Substring(3, exception.Message.Length) + "is NonNumeric.";
                }
                else if (errorType == ErrorType.NonDate)
                {
                    errorMessage = exception.Message.Substring(3, exception.Message.Length) + "is Not a Date.";
                }
                else if (errorType == ErrorType.NonBoolean)
                {
                    errorMessage = exception.Message.Substring(3, exception.Message.Length) + "is Not a Boolean value.";
                }
                else if (errorType == ErrorType.Empty)
                {
                    errorMessage = exception.Message.Substring(3, exception.Message.Length) + "is Empty.";
                }
            }
            catch
            {
                errorMessage = exception.Message;
            }
            return errorMessage;
        }

        public static int LanguageConverter(object obj)
        {
            int iLanguageID = 0;
            if (Convert.ToString(obj) == "fr")
            {
                iLanguageID = 1;
            }
            else if (Convert.ToString(obj) == "en-US")
            {
                iLanguageID = 2;
            }
            else if (Convert.ToString(obj) == "ar")
            {
                iLanguageID = 3;
            }
            return iLanguageID;
        }

        #region:: Change Status Text 
        public static string GetStatus(int IsActive)
        {
            string StatusText = "<span style='color:#228B22;font-weight:bold;'>Active</span>";
            if(IsActive==0)
            {
                StatusText = "<span style='color:#FF0000;font-weight:bold;'>Inactive</span>";
            }
            return StatusText;
        }
        #endregion

        #region:: Change Status Text(Yes/No) 
        public static string GetYNStatus(int IsActive)
        {
            string StatusText = "Yes";
            if (IsActive == 0)
            {
                StatusText = "No";
            }
            return StatusText;
        }
        #endregion

        #region:: Regex to check only one space between words
        public static void CheckOneSpaceBetweenWords(out string regex,out string errormessage)
        {
            regex= "^\\S+(?: \\S+)*$";
            errormessage = "Only one space is allowed between words";
        }

        public static bool ReturnCheckOneSpaceBetweenWords(string texttocheck)
        {
            string regex = "^\\S+(?: \\S+)*$";
            bool status = Regex.IsMatch(texttocheck, regex);
            return status;
        }
        #endregion

        #region:: Regex to check decimal value
        public static void CheckDecimalValue(out string regex, out string errormessage)
        {
            regex = "^[0-9]\\d{0,9}(\\.\\d{1,3})?%?$|^[0-9]\\d{0,9}(\\.\\d{1,3})?%?$";
            errormessage = "Only decimal value allowed";
        }

        public static bool ReturnCheckDecimalValue(string texttocheck)
        {
            string regex = "^[0-9]\\d{0,9}(\\.\\d{1,3})?%?$|^[0-9]\\d{0,9}(\\.\\d{1,3})?%?$";
            bool status = Regex.IsMatch(texttocheck, regex);
            return status;
        }

        #endregion

        #region:: Regex to check numberic value
        public static void CheckNumericValue(out string regex, out string errormessage)
        {
            regex = "\\d+";
            errormessage = "Only numeric value allowed";
        }

        public static bool ReturnCheckNumericValue(string texttocheck)
        {
            string regex = "\\d+";
            bool status = Regex.IsMatch(texttocheck, regex);
            return status;
        }

        #endregion

        #region:: Regex to check + or - with decimal value
        public static void CheckDecimalValuePreceedingCharacter(out string regex, out string errormessage)
        {
            regex = "^[\\-\\+][0-9]\\d{0,9}(\\.\\d{1,3})?%?$|^[0-9]\\d{0,9}(\\.\\d{1,3})?%?$";
            errormessage = "Only decimal value allowed";
        }

        public static bool ReturnCheckDecimalValuePreceedingCharacter(string texttocheck)
        {
            string regex = "^[\\-\\+][0-9]\\d{0,9}(\\.\\d{1,3})?%?$|^[0-9]\\d{0,9}(\\.\\d{1,3})?%?$";
            bool status = Regex.IsMatch(texttocheck, regex);
            return status;
        }

        #endregion

        #region:: Regex to check GST number
        public static void CheckGSTNumber(out string regex,out string errormessage)
        {
            regex = "^([0][1-9]|[1-2][0-9]|[3][0-7])([a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}[1-9a-zA-Z]{1}[zZ]{1}[0-9a-zA-Z]{1})+$";//"\\d{2}[A-Z]{5}\\d{4}[A-Z]{1}\\d[Z]{1}[A-Z\\d]{1}";
            errormessage = "Invalid GST number";
        }
        #endregion

        #region:: Regex to check Zip code
        public static void CheckZipCode(out string regex, out string errormessage)
        {
            regex = "\\d{6}";
            errormessage = "Invalid Zip code";
        }
        #endregion

        #region:: Regex to check Contact number
        public static void CheckContactNumber(out string regex, out string errormessage)
        {
            regex = "\\d{10}|\\d{11}";
            errormessage = "Invalid Contact number";
        }
        #endregion

        #region::Regex to check date (dd/mm/yyyy)
        public static void CheckDateFormatDDMMYYYY(out string regex, out string errormessage)
        {
            regex = "(((0|1)[0-9]|2[0-9]|3[0-1])\\/(0[1-9]|1[0-2])\\/((19|20)\\d\\d))$";
            errormessage = "Invalid date format";
        }

        public static bool ReturnCheckDateFormatDDMMYYYY(string texttocheck)
        {
            string regex = "(((0|1)[0-9]|2[0-9]|3[0-1])\\/(0[1-9]|1[0-2])\\/((19|20)\\d\\d))$";
            bool status = Regex.IsMatch(texttocheck, regex);
            return status;
        }

        public static void CheckDateFormatDDMMYYYYHHMIN(out string regex, out string errormessage)
        {
            regex = "(0?[1-9]|[12][0-9]|3[01])\\/(0?[1-9]|1[0-2])\\/(19|20\\d\\d) (0?[0-9]|1[0-9]|2[0-3]):([0-9]|[0-5][0-9])$";
            errormessage = "Invalid date time format";
        }

        public static bool ReturnCheckDateFormatDDMMYYYYHHMIN(string texttocheck)
        {
            string regex = "(0?[1-9]|[12][0-9]|3[01])\\/(0?[1-9]|1[0-2])\\/(19|20\\d\\d) (0?[0-9]|1[0-9]|2[0-3]):([0-9]|[0-5][0-9])$";
            bool status = Regex.IsMatch(texttocheck, regex);
            return status;
        }

        #endregion

        #region::Regex to check for no special characters or space        
        public static void CheckNoSpace(out string regex, out string errormessage)
        {
            regex = @"^[^\s]+$";
            errormessage = "No space or special characters allowed";
        }
        #endregion

        #region::Regex to check for no special characters or space        
        public static void CheckNoSpaceOrSpecialCharacter(out string regex, out string errormessage)
        {
            regex = @"^[^<>.,?;:'()!~%\-_@#/*""\s]+$";
            errormessage = "No space or special characters allowed";
        }
        #endregion 

        #region::Regex to check minimum characters length
        public static void CheckMinimumCharacterLength(out string regex, out string errormessage,string len)
        {
            regex = @"^[\s\S]{"+ len + ",}$";
            errormessage = "Minimum "+len+" character";
        }
        #endregion
    }

    public enum ErrorType
    {
        Null = 1,
        NonNumeric = 2,
        NonDate = 3,
        NonBoolean = 4,
        Empty = 5,
        Numeric = 6,
        Invalid = 7,
        Required = 8
    }
    public enum PaymentMethod
    {
        Manual = 1,
        PayPal = 2
    }
    public enum ActionType
    {
        Insert = 1,
        Update = 2,
        Fetch = 3,
        Delete = 4,
        Accept = 5,
        Reject = 6
    }
    public enum FetchBy
    {
        NumaricVal = 5,
        TextVal = 6
    }
    public enum ReaderType
    {
        DataReader = 1,
        XMLReader = 2
    }

    public enum SeparatorType
    {
        ClosedSeparator = 1,
        OpenSeparator = 2,
        NoSeparator = 3
    }

    public enum BoolanTextType
    {
        TrueFalse = 1,
        YesNo = 2
    }

    public enum ControlPosition
    {
        SearchPanel = 1,
        EntryPanel = 2
    }
    public enum StatusText
    {
        YesNo = 1,
        ActiveInActive = 2
    }
    public enum MonthString
    {
        ShortMonth = 1,
        LongMonth = 2
    }

    public enum ShortMonthString
    {
        Jan = 1,
        Feb = 2,
        Mar = 3,
        Apr = 4,
        May = 5,
        Jun = 6,
        Jul = 7,
        Aug = 8,
        Sep = 9,
        Oct = 10,
        Nov = 11,
        Dec = 12
    }

    public enum LongMonthString
    {
        January = 1,
        February = 2,
        March = 3,
        April = 4,
        May = 5,
        June = 6,
        July = 7,
        August = 8,
        September = 9,
        October = 10,
        November = 11,
        December = 12
    }

    public enum Fields
    {
        Escape = 1,
        Required = 2,
        All = 3
    }

    public enum ValidationType
    {
        PhoneNumber = 1,
        Email = 2
    }

    public enum PageSize
    {
        Max = 100,
        Standerd = 7
    }

    public enum Language
    {
        fr = 1,
        enUS = 2
    }
    public enum Sequence
    {
        UP = 1,
        Down = 2

    }
    public enum EmploymentStatus
    {
        Service = 1,
        Self = 2
    }
    public enum Currency
    {
        Rs = 1,
        USD = 2
    }
    public enum ImageSize
    {
        ThumbnailSize_Default = 0,
        ThumbnailSize_Small = 1,
        ThumbnailSize_Medium = 2,
        ThumbnailSize_Large = 3
    }
    public enum ImageLocationType
    {
        ImageLocationType_Gallery = 1,
        ImageLocationType_Groups = 2,
        ImageLocationType_Profile = 3,
        ImageLocationType_Default = 4,
        ImageLocationType_HomePageScrolling = 5

    }
    public enum SearchType
    {
        Name = 1,
        NickName = 2,
        Hostail_Single_ = 3,
        Hostail_Multi = 4,
        Organisation
    }
    public enum FileUploadFormat
    {
        JPG = 1,
        JPEG = 2,
        PDF = 3
    }
    public enum FetchEmployeeBy
    {
        None = 0,
        UserName = 1,
        Email = 2
    }
    public enum Module
    {
        All = 0,
        Sales = 1,
        Purchase = 2,
        Finance = 3,
        Administration = 4,
        HR = 5
    }
    public enum Statuses
    {
        //This enum is for maintaining the status of sales Enquery, Order, Delivery & Invoice
        Created = 0,
        Open = 1,
        Partial = 2,
        MoveToAdmin = 3,
        Confirm = 4,
        Closed = 5,
        Pending = 6,
        Approved = 7,
        Processed = 8,
        Picked = 9,
        Canceled = 10,
        InTransit=11
    }
    public enum NoImageSize
    {
        Large = 0,
        Thumb = 1,
        Small = 2
    }

    //public enum SupplierType
    //{
    //    AllSupplier = 0,
    //    Group = 1,
    //    Particular = 2,
    //}

   

   
    public enum MenuPosition
    {
        Control = 1,
        Create = 2,
        Communicate = 3,
    }

    public enum EmailTemplate
    {
        Birthday = 1,
        Anniversary = 2,
        Career = 3,
        ToDo = 4,
        Resignation = 5,
        ResignHrApproval = 6,
        RejectResignation = 7,
        Release = 8,
        Invite = 9,
        SalesInquiry = 50,
        SalesQuotation = 51,
        SalesNegotiation = 52,
        SalesOrder = 53,
        SalesProformaInvoice = 54,
        SalesPickList = 55,
        SalesDelivery = 56,
        SalesConfirmDelivery = 57,
        SalesInvoice = 58,
        ConsignmentOrder = 70,
        ConsignmentPickList = 71,
        ConsignmentDelivery = 72,
        ConsignmentInvoice = 73,
        ConsignmentReturn = 74,
        PurchaseRequisition = 100,
        PurchaseOrder = 101,
        PurchaseAcknowledgement = 102,
        CashPurchase = 103,
        CustomerWelcome = 200,
        LoginInfo=201,
        Nobody=202,
        Trip=203,
        NewsEmail= 204
    }


    /// <summary>
    /// This Enum for ScrapType
    /// </summary>
    public enum ScrapType
    {
        Sales = 1,
        Consignment = 2,
        Other = 3
    }
    public enum ChatDisplay
    {
        Two_D = 1,
        Three_D = 2
    }
    public enum ChartType
    {
        Area = 1,
        Bar = 2,
        Column = 3,
        Line = 4,
        Pie = 5,
        Pyramid = 6
    }
    public enum ChartDrowStyle
    {
        CircleBase = 1,
        SquareBase = 2
    }

    public enum FITransationType
    {
        PaymentVoucher = 1,
        ReciveVoucher = 2,
        GST = 3,
        BankCharge = 4,
        Journal = 5,
        Contra = 6,
        CashSales = 7,
        CreditSale = 8,
        CashPurchase = 9,
        CreditPurchase = 10,
        CreditSalesInvoice = 11,
        CreditPurchaseInvoice = 12,
        CustomerDebitNote = 13,
        CustomerCreditNote = 14,
        CustomerContra = 15,
        SupplerContra = 16,
        SupplierDebitNote = 17,
        SupplierCreditNote = 18,
        SalaryPayment = 19
    }
    public enum TransationBy
    {
        Cash = 1,
        Bank = 2,
        Both = 3,
        None = 4
    }
    public enum ApprovalStatus
    {
        //0- Not Approve, 1-Approve,2-Hold,4 -Cancel
        NotApprove = 0,
        Approve = 1,
        Hold = 2,
        Cancel = 4
    }

    public enum CustomerSpecification
    {
        General = 0,
        Consignment = 1,
    }


    public enum CrystalReports
    {
        Finance_AccountWiseLedgerDetails = 0,
        Finance_AccountWiseTrialBalanceDetails = 1,
        Finance_AccountWiseTrialBalanceSummary = 2,
        Finance_ProfitLossStatement = 3,
        TradingAndPLAndBS = 4,
        TradingAndPL = 5,
        BalanceSheet = 6,
        Finance_AccountWiseLedgerSummary = 7,
        Finance_AccountWiseLedgerDetailsWithFC = 8,
        Finance_AccountWiseLedgerSummaryWithFC = 9,
        Finance_CustomerWiseOutstandingDetails = 10,
        Finance_CustomerwiseOutstandingSummary = 11,
        Finance_SupplierWiseOutstandingDetails = 12,
        Finance_SupplierwiseOutstandingSummary = 13,

        Sales_CustomerStatement = 14,
        Purchase_SupplierStatement = 15,

        Inventory_StockBalanceDetails = 16,
        Inventory_StockBalanceSummary = 17,

        Sales_InquiryReport = 18,
        Sales_QuotationReport = 19,
        Sales_OrderReport = 20,
        Sales_ConfirmDelivaryReport = 21,
        Sales_InvoiceReport = 22,
        Sales_CashSalesReport = 23,
        Sales_DebitNoteReport = 24,
        Sales_CreditNoteReport = 25,
        Finance_CustomerBalanceReport = 26,
        Finance_ReceiptPaymentReport = 27,
        Finance_SupplierBalanceReport = 28,
        Finance_JournalOfTransactionReport = 29,
        Finance_CustomerDueDocumentListing = 30,
        Finance_SupplierDueDocumentListing = 31,
        Finance_SupplierAnalysisByDocument = 32,
        Finance_CustomerAnalysisByDocument = 33,
        Finance_MothWiseCollectionPaymentAnalysis = 34,
        Finance_ChartOfAccount = 35,
        Finance_ChartOfAccountGroupWise = 36,
        Finance_BRSReport = 37,
        Sales_OutStandingInquiryReport = 38,
        Sales_OutStandingQuotationReport = 39,
        Sales_OutStandingOrderReport = 40,
        Sales_OutStandingInvoiceReport = 41,

        Purchase_RequisitionReport = 42,
        Purchase_QuotationReport = 43,
        Purchase_OrderReport = 44,
        Purchase_AcknowledgementReport = 45,
        Purchase_InvoiceReport = 46,
        Purchase_DebitNoteReport = 47,
        Purchase_CreditNoteReport = 48,

        Purchase_OutStandingRequisitionReport = 49,
        Purchase_OutStandingQuotationReport = 50,
        Purchase_OutStandingOrderReport = 51,
        Purchase_OutStandingAcknowledgementReport = 52,
        Purchase_OutStandingInvoiceReport = 53,
        Finance_JournalVoucherListing = 54,
        Finance_ReceiptVoucherListing = 55,
        Finance_PaymentVoucherListing = 56,

        Inventory_ItemWarehouseWiseStock = 57,
        Inventory_StockAgingDetails = 58,
        Inventory_StockAgingSummary = 59,

        Service_EasyLeadAnalysis = 60,
        Service_EasyQuotationAnalysis = 61,
        Service_Invoice = 62,

        HR_Payslip = 63,
        POS_SALE=64
    }

    public enum enumFinalACType
    {
        TradingAndPLAndBS = 0,
        TradingAndPL = 1,
        BalanceSheet = 2
    }
    public enum LadgerType
    {
        All = 0,
        General = 1,
        Sales = 2,
        Purchase = 3
    }

    public enum DrCr
    {
        Dr = 0,
        Cr = 1

    }
    public enum WarehouseStockTransactionType
    {
        OpeningInventory = 0,
        GoodsReceivedNote = 1,
        SalesDelivery = 2,
        SalesReturn = 3,
        FOCSampleDelivery = 4,
        StockTransfer = 5,
        CashSalesDelivery = 6,
        PurchaseReturn = 7,
        PurchaseDebitNote = 8,
        SalesDebitNote = 9,
        ConsignmentDelivery = 10,
        ConsignmentReturn = 11

    }

    public enum WarehouseType
    {
        Normal = 0,
        Retailer = 1
    }
    public enum VoucherType
    {
        PurchaseInvoice = 1,
        SaleInvoice = 2,
        PurchaseReturn = 3,
        SaleReturn = 4,
        SupplierDebitNote = 5,
        SupplierCreditNote = 6,
        SupplierPayment = 7,
        CustomerReceive = 8,
        CashSale = 9,
        CashPurchase = 10,
        Journal = 11,
        PaymentVoucher = 12,
        ReceiptVoucher = 13,
        CustomerDebitNote = 14,
        CustomerCreditNote = 15,
        CashSaleReceive = 16,
        CashPurchaseReceive = 17,
        CustomerContra = 18,
        SupplierContra = 19,
        CustomerRefund = 20,
        SupplierRefund = 21,
        CustomerInvoice = 22,
        ConsignmentInvoice = 23
    }

    public enum Modules
    {
        PaymentVoucherEntry = 84,
        ReceiptVoucherEntry = 85,
        CustomerDebitNoteEntry = 86,
        CustomerCreditNoteEntry = 87,
        JournalEntry = 90,
        SupplierDebitNoteEntry = 221,
        SupplierCreditNoteEntry = 222,
        SalesReceiveEntry = 237,
        CustomerContraEntry = 341,
        PurchasePaymentEntry = 444,
        SupplierRefundEntry = 445,
        SupplierContraEntry = 446,
        CashPurchaseEntry = 447
    }

    public enum CardType
    {
        Visa = 1,
        MasterCard = 2,
        Discover = 3,
        AmericanExpress = 4
    }
    public enum PaymentType
    {
        //PayByCard = 1,
        PayByPaypal = 2,
    }
    public enum CampaignStatus
    {
        InProgress=1,
        Completed=2,
        Absorted=3,
        Planned=4
    }
    public enum QuotationStatus
    {
        Created = 0,
        Close = 5        
    }
	
	public enum AppointmentStatus
    {
        Pending = 1,
        Completed = 2
    }
    public enum OrderType
    {
        WEB = 1,
        POS = 2,
        ECOMM = 3
    }
    public enum CustomerSource
    {
        WEB = 1,
        POS = 2,
        ECOMM = 3
    }

    /// <summary>
    /// 0=default, 2=megento,1=woocommerce,3=ebay,4=amazon,5=quickbook, 6=ecommerce
    /// </summary>
    public enum InventoryImportSource
    {
        Default = 0,
        Woocommerce = 1,
        Megento = 2,
        EBay = 3,
        Amazon = 4,
        Quickbook = 5,
        ECommerce = 6,
        MountainBrook =7
    }

    public enum RedeemOptions
    {
        PromotionalOffer = 1,
        StoreItem = 2,
        GiftCard = 3,
        MemberPointsDeducted = 4,
        MemberPointsAdded = 5,
        GiftCardRedeem = 6
    }

   
    //public enum POSType
    //{
    //    Retail = 1,
    //    Resturent = 2
    //}

}
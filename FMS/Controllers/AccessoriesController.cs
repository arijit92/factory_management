﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FMS.BL;
using FMS.BL.Buisness;
using FMS.BL.Utilities;

namespace FMS.Controllers
{
    public class AccessoriesController : Controller
    {
        // GET: Accessories  
        public ActionResult List()
        {
            ViewBag.ParentMenu = "master";
            ViewBag.ChildMenu = "accessories";
            return View();
        }

        [HttpGet]
        public ActionResult Add(int? accessoriesid)
        {
            ViewBag.ParentMenu = "master";
            ViewBag.ChildMenu = "accessories";
            Accessories accessories = null;
            AccessoriesBuisness accessoriesBuisness = null;
            List<AccessoriesType> accessoriesTypes = null;
            long userid = 11;
            try
            {
                accessories = new Accessories();
                accessoriesBuisness = new AccessoriesBuisness();
                accessoriesTypes = new List<AccessoriesType>();
                if (accessoriesid > 0)
                {
                    accessories = accessoriesBuisness.GetAccessories(accessoriesid, userid).SingleOrDefault();
                }
                accessoriesTypes = accessoriesBuisness.GetAccessoriesType(0, 1).ToList();
                ViewBag.AccessoriesTypes = new SelectList(accessoriesTypes, "AccessoriesTypeId", "AccessoriesTypeName", 0);


            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Accessories list", ex, "Accessories");
            }
            return View(accessories);
        }
        [HttpPost]
    public ActionResult Add(Accessories accessories)
    {
            ViewBag.ParentMenu = "master";
            ViewBag.ChildMenu = "accessories";
            List<AccessoriesType> accessoriesTypes = new List<AccessoriesType>();
            AccessoriesBuisness buisness = new AccessoriesBuisness();
            accessoriesTypes = buisness.GetAccessoriesType(0, 1).ToList();
            ViewBag.AccessoriesTypes = new SelectList(accessoriesTypes, "AccessoriesTypeId", "AccessoriesTypeName", 0);
            if (ModelState.IsValid)
        {
            
            Status status = new Status();
                status = buisness.AddUpdateAccessories(accessories, 1);
            TempData["SuccessFlag"] = status.SuccessFlag;
            TempData["Msg"] = status.Msg;
            if (status.SuccessFlag == "1")
            {
                return RedirectToAction("List");
            }
            else
            {    
                return View(accessories);
            }
        }
        return View(accessories);
    }

    [HttpPost]
    public JsonResult LoadAccessories(int AccessoriesId)
    {
        long userid = 11;
        List<Accessories> accessories = null;
        AccessoriesBuisness accessoriesBuisness = null;
        try
        {
            accessories = new List<Accessories>();
            accessoriesBuisness = new AccessoriesBuisness();
            accessories = accessoriesBuisness.GetAccessories(AccessoriesId, userid).Where(i => i.IsDeleted == false).ToList();
        }
        catch (Exception ex)
        {
            AppLogger.Instance.Error(this.GetType(), "Unable to fetch Accessories list", ex, "Accessories");
        }

        return Json(accessories, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(int accessoriesid)
        {
            ViewBag.ParentMenu = "master";
            ViewBag.ChildMenu = "accessories";
            AccessoriesBuisness accessoriesBuisness = null;
            Status status = null;
            long userid = 11;
            try
            {
                accessoriesBuisness = new AccessoriesBuisness();
                status = new Status();
                status = accessoriesBuisness.DeleteAccessories(accessoriesid, userid);
                TempData["SuccessFlag"] = status.SuccessFlag;
                TempData["Msg"] = status.Msg;
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to delete Accessories ", ex, "Delete");
            }
            return RedirectToAction("List");
        }
       
    }
}
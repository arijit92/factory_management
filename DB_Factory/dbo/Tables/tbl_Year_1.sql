﻿CREATE TABLE [dbo].[tbl_Year] (
    [Id]       INT          IDENTITY (1, 1) NOT NULL,
    [Year]     NVARCHAR (4) NOT NULL,
    [IsActive] BIT          NULL
);


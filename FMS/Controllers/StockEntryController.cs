﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FMS.BL;
using FMS.BL.Buisness;
using FMS.BL.Utilities;

namespace FMS.Controllers
{
    public class StockEntryController : Controller
    {
        // GET: StockEntry
        [HttpGet]
        public ActionResult AddPurchaseEntry()
        {
            ViewBag.ParentMenu = "salepurchase";
            ViewBag.ChildMenu = "purchase";
            return View();
        }
        //[HttpPost]
        //public ActionResult AddPurchaseEntry(PurchaseEntry purchaseEntry)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        SalePurchaseBuisness buisness = new SalePurchaseBuisness();
        //        Status status = new Status();
        //        status = buisness.AddUpdatePurchaseEntryDetails(purchaseEntry);
        //        TempData["SuccessFlag"] = status.SuccessFlag;
        //        TempData["Msg"] = status.Msg;
        //        if (status.SuccessFlag == "1")
        //        {
        //            return RedirectToAction("ListOfEntries");
        //        }
        //        else
        //        {
        //            return View(purchaseEntry);
        //        }
        //    }
        //    return View();
        //}
        [HttpGet]
        public ActionResult ListOfEntries()
        {
            return View();
        }
        [HttpPost]
        public JsonResult GetStockEntryDetails()
        {
            List<PurchaseEntry> lst = new List<PurchaseEntry>();
            SalePurchaseBuisness salesBuisness = new SalePurchaseBuisness();
            try
            {
                lst = salesBuisness.GetStockEntryDetails().ToList();
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Accessories list", ex, "Accessories");
            }

            return Json(lst, JsonRequestBehavior.AllowGet);
        }

        //public ActionResult MoveToStock(int itemTypeId)
        //{
        //    List<PurchaseEntry> lst = new List<PurchaseEntry>();
        //    SalePurchaseBuisness salesBuisness = new SalePurchaseBuisness();
        //    try
        //    {
        //        lst = salesBuisness.GetStockEntryDetails().ToList();
        //        TempData["ItemName"] = lst.Select(e=>e.ItemTypeId).FirstOrDefault();
        //        TempData["Weight"] = lst.Where(e => e.ItemTypeId == itemTypeId).Sum(e => e.Weight);
        //    }
        //    catch (Exception ex)
        //    {
        //        AppLogger.Instance.Error(this.GetType(), "Unable to fetch Accessories list", ex, "Accessories");
        //    }
        //    return View();
        //}
    }
}

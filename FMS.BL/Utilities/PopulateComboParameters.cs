using System;
using System.Collections.Generic;
using System.Text;

namespace FMS.BL.Utilities
{
    public class PopulateComboParameters
    {
        private string tableName;
        private string whereClause;
        private string mainTextField;
        private string subTextField;
        private string valueField;

        public string TableName
        {
            get
            {
                return tableName;
            }
            set
            {
                this.tableName = value;
            }
        }

        public string WhereClause
        {
            get
            {
                return whereClause;
            }
            set
            {
                this.whereClause = value;
            }
        }

        public string MainTextField
        {
            get
            {
                return mainTextField;
            }
            set
            {
                this.mainTextField = value;
            }
        }

        public string SubTextField
        {
            get
            {
                return subTextField;
            }
            set
            {
                this.subTextField = value;
            }
        }

        public string ValueField
        {
            get
            {
                return valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }
}

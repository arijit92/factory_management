﻿using FMS.BL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FMS.BL.Buisness;
using FMS.BL.Utilities;


namespace FMS.Controllers
{
    public class ItemController : Controller
    {
        // GET: item
        public ActionResult List()
        {
            ViewBag.ParentMenu = "master";
            ViewBag.ChildMenu = "item";
            return View();
        }

        // ADD: item
        public ActionResult Add(int? itemid)
        {
            ViewBag.ParentMenu = "master";
            ViewBag.ChildMenu = "item";
            List<ProductCategory> productCategories = null;
            CategoryBuisness categoryBuisness = null;
            List<Unit> units = null;
            UnitBuisness unitBuisness = null;
            List<GST> gSTs = null;
            GSTBuisness gSTBuisness = null;
            ItemBuisness itemBuisness = null;
            Item item = null;
            long userid = 11;
            try
            {
                productCategories = new List<ProductCategory>();
                categoryBuisness = new CategoryBuisness();
                units = new List<Unit>();
                unitBuisness = new UnitBuisness();
                gSTs = new List<GST>();
                gSTBuisness = new GSTBuisness();

                itemBuisness = new ItemBuisness();                                             
                item = new Item();
                if (itemid > 0)
                {
                    item = itemBuisness.GetItem(itemid, userid).SingleOrDefault();
                }
                productCategories = categoryBuisness.GetProductCategory(0, 11).Where(i => i.IsActive == 1 && i.IsDeleted == 0).ToList();
                units = unitBuisness.GetUnit(0, 11).Where(i => i.IsActive == true && i.IsDeleted == false).ToList();
                gSTs = gSTBuisness.GetGST(0, 11).Where(i => i.IsActive == true && i.IsDeleted == false).ToList();
                ViewBag.Category = new SelectList(productCategories, "CategoryId", "CategoryName", 0);
                ViewBag.Unit = new SelectList(units, "UnitId", "UnitName", 0);
                ViewBag.GST = new SelectList(gSTs, "GSTId", "ddlPer", 0);


            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to render Item Add page", ex, "Add");
            }
            return View(item);
        }

        //// ADD: Item : POST
        [HttpPost]
        public ActionResult Add(Item Item)
        {
            ViewBag.ParentMenu = "master";
            ViewBag.ChildMenu = "item";
            List<ProductCategory> productCategories = null;
            CategoryBuisness categoryBuisness = null;
            List<Unit> units = null;
            UnitBuisness unitBuisness = null;
            List<GST> gSTs = null;
            GSTBuisness gSTBuisness = null;
            ItemBuisness itemBuisness = null;
            Status status = null;
            long userid = 11;
            Item.CompanyId = 12;
            try
            {
                status = new Status();
                productCategories = new List<ProductCategory>();
                categoryBuisness = new CategoryBuisness();
                units = new List<Unit>();
                unitBuisness = new UnitBuisness();
                gSTs = new List<GST>();
                gSTBuisness = new GSTBuisness();
                itemBuisness = new ItemBuisness();
                productCategories = categoryBuisness.GetProductCategory(0, 11).Where(i => i.IsActive == 1 && i.IsDeleted == 0).ToList();
                units = unitBuisness.GetUnit(0, 11).Where(i => i.IsActive == true && i.IsDeleted == false).ToList();
                gSTs = gSTBuisness.GetGST(0, 11).Where(i => i.IsActive == true && i.IsDeleted == false).ToList();
                ViewBag.Category = new SelectList(productCategories, "CategoryId", "CategoryName", 0);
                ViewBag.Unit = new SelectList(units, "UnitId", "UnitName", 0);
                ViewBag.GST = new SelectList(gSTs, "GSTId", "HSN", 0);
                //// string uid = Session["UserBE"].UserId
                var errors = ModelState.Values.SelectMany(v => v.Errors);
                if (ModelState.IsValid)
                {
                    status = itemBuisness.AddUpdateitem(Item, userid);
                    // ViewBag.Status = status;
                    TempData["SuccessFlag"] = status.SuccessFlag;
                    TempData["Msg"] = status.Msg;
                    if (status.SuccessFlag == "1")
                    {
                        return RedirectToAction("List");
                    }
                    else
                    {
                        return View(Item);

                    }

                }
                else
                {
                    return View(Item);
                }

            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to post Item Add page", ex, "Add");
                return View(Item);
            }

        }

        [HttpPost]
        public JsonResult LoadItem(int itemId)
        {
            ViewBag.ParentMenu = "master";
            ViewBag.ChildMenu = "item";
            //// string uid = Session["UserBE"].UserId;
            long userid = 11;
            List<Item> Items = null;
            ItemBuisness ItemBuisness = null;
            try
            {
                Items = new List<Item>();
                ItemBuisness = new ItemBuisness();
                // AppLogger.Instance.Info(this.GetType(), "Fetch the list of all category", "LoadCategory");aa
                Items = ItemBuisness.GetItem(itemId, userid).Where(i => i.IsDeleted == false).ToList();
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch item list", ex, "LoadItem");
            }

            return Json(Items, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(int itemid)
        {
            ViewBag.ParentMenu = "master";
            ViewBag.ChildMenu = "item";
            ItemBuisness ItemBuisness = null;
            Status status = null;
            long userid = 11;
            try
            {
                ItemBuisness = new ItemBuisness();
                status = new Status();
                status = ItemBuisness.DeleteItem(itemid, userid);
                TempData["SuccessFlag"] = status.SuccessFlag;
                TempData["Msg"] = status.Msg;

            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to delete Item ", ex, "Delete");
            }
            return RedirectToAction("List");
        }

        #region sale and purchase
        public ActionResult AddItems(int? itemid)
        {
            ViewBag.ParentMenu = "superadmin";
            ViewBag.ChildMenu = "ItemsList";
            List<ProductCategory> productCategories = null;
            CategoryBuisness categoryBuisness = null;
            List<Unit> units = null;
            UnitBuisness unitBuisness = null;
            //List<GST> gSTs = null;
           // GSTBuisness gSTBuisness = null;
            ItemBuisness itemBuisness = null;
            ItemMaster item = null;
            long userid = 11;
            try
            {
                productCategories = new List<ProductCategory>();
                categoryBuisness = new CategoryBuisness();
                units = new List<Unit>();
                unitBuisness = new UnitBuisness();
               // gSTs = new List<GST>();
                //gSTBuisness = new GSTBuisness();

                itemBuisness = new ItemBuisness();
                item = new ItemMaster();
                if (itemid > 0)
                {
                    item = itemBuisness.GetItems(itemid, userid).SingleOrDefault();
                }
                productCategories = categoryBuisness.GetProductCategory(0, 11).Where(i => i.IsActive == 1 && i.IsDeleted == 0).ToList();
                //units = unitBuisness.GetUnit(0, 11).Where(i => i.IsActive == true && i.IsDeleted == false).ToList();
                //gSTs = gSTBuisness.GetGST(0, 11).Where(i => i.IsActive == true && i.IsDeleted == false).ToList();
                ViewBag.Category = new SelectList(productCategories, "CategoryId", "CategoryName", 0);
                ViewBag.Unit = new SelectList(units, "UnitId", "UnitName", 0);
               // ViewBag.GST = new SelectList(gSTs, "GSTId", "ddlPer", 0);


            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to render Item Add page", ex, "Add");
            }
            return View(item);
        }

        //// ADD: Item : POST
        [HttpPost]
        public ActionResult AddItems(ItemMaster Item)
        {
            ViewBag.ParentMenu = "superadmin";
            ViewBag.ChildMenu = "ItemsList";
            List<ProductCategory> productCategories = null;
            CategoryBuisness categoryBuisness = null;
           // List<Unit> units = null;
            UnitBuisness unitBuisness = null;
           // List<GST> gSTs = null;
            //GSTBuisness gSTBuisness = null;
            ItemBuisness itemBuisness = null;
            Status status = null;
            long userid = 11;
            //Item.CompanyId = 12;
            try
            {
                status = new Status();
                productCategories = new List<ProductCategory>();
                categoryBuisness = new CategoryBuisness();
               // units = new List<Unit>();
                unitBuisness = new UnitBuisness();
               // gSTs = new List<GST>();
               // gSTBuisness = new GSTBuisness();
                itemBuisness = new ItemBuisness();
                productCategories = categoryBuisness.GetProductCategory(0, 11).Where(i => i.IsActive == 1 && i.IsDeleted == 0).ToList();
              //  units = unitBuisness.GetUnit(0, 11).Where(i => i.IsActive == true && i.IsDeleted == false).ToList();
               // gSTs = gSTBuisness.GetGST(0, 11).Where(i => i.IsActive == true && i.IsDeleted == false).ToList();
                ViewBag.Category = new SelectList(productCategories, "CategoryId", "CategoryName", 0);
               // ViewBag.Unit = new SelectList(units, "UnitId", "UnitName", 0);
               // ViewBag.GST = new SelectList(gSTs, "GSTId", "HSN", 0);
                //// string uid = Session["UserBE"].UserId
                var errors = ModelState.Values.SelectMany(v => v.Errors);
                if (ModelState.IsValid)
                {
                    status = itemBuisness.AddUpdateitems(Item, userid);
                    // ViewBag.Status = status;
                    TempData["SuccessFlag"] = status.SuccessFlag;
                    TempData["Msg"] = status.Msg;
                    if (status.SuccessFlag == "1")
                    {
                        return RedirectToAction("ItemsList");
                    }
                    else
                    {
                        return View(Item);

                    }

                }
                else
                {
                    return View(Item);
                }

            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to post Item Add page", ex, "Add");
                return View(Item);
            }

        }

        public ActionResult ItemsList()
        {
            ViewBag.ParentMenu = "superadmin";
            ViewBag.ChildMenu = "ItemsList";
            return View();
        }

        [HttpPost]
        public JsonResult LoadItems(int itemId)
        {
            ViewBag.ParentMenu = "superadmin";
            ViewBag.ChildMenu = "ItemsList";
            //// string uid = Session["UserBE"].UserId;
            long userid = 11;
            List<ItemMaster> Items = null;
            ItemBuisness ItemBuisness = null;
            try
            {
                Items = new List<ItemMaster>();
                ItemBuisness = new ItemBuisness();
                // AppLogger.Instance.Info(this.GetType(), "Fetch the list of all category", "LoadCategory");aa
                Items = ItemBuisness.GetItemsList(itemId, userid).Where(i => i.IsActive ==true).ToList();
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch item list", ex, "LoadItem");
            }

            return Json(Items, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteItems(int itemid)
        {
            ViewBag.ParentMenu = "superadmin";
            ViewBag.ChildMenu = "ItemsList";
            ItemBuisness ItemBuisness = null;
            Status status = null;
            long userid = 11;
            try
            {
                ItemBuisness = new ItemBuisness();
                status = new Status();
                status = ItemBuisness.DeleteItems(itemid, userid);
                TempData["SuccessFlag"] = status.SuccessFlag;
                TempData["Msg"] = status.Msg;

            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to delete Item ", ex, "Delete");
            }
            return RedirectToAction("ItemsList");
        }
        #endregion

    }

}
﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FMS.BL.Utilities;

namespace FMS.BL.Buisness
{
   public class DocumentBuisness : Common
    {
        public Status AddUpdateDocument(Documents documents, long UserID)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    para.Add("@DocumentId", documents.DocumentId);
                    para.Add("@DocumentName", documents.DocumentName);
                    para.Add("@DocumentPath", documents.DocumentPath);
                    para.Add("@IsActive",1);
                    para.Add("@CreatedBy", UserID);
                    para.Add("@CompanyId", documents.CompanyId);
                    const string procName = "[dbo].[sp_InsertUpdateDocument]";
                    connection.Open();
                    return connection.Query<Status>(procName, para, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to insert or update Unit", ex, "AddUpdateUnit");
                return null;
            }

        }

        public List<Documents> GetDocuments(long? DocumentsId, long UserID)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    if (DocumentsId == 0)
                    {
                        DocumentsId = null;
                    }
                    para.Add("@DocumentId", DocumentsId);
                    para.Add("@OperationId", 2);
                    para.Add("@UserId", UserID);
                    const string procName = "[dbo].[sp_DeleteSelectDocument]";
                    connection.Open();
                    return connection.Query<Documents>(procName, para, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch documents", ex, "GetDocuments");
                return null;
            }

        }

        public Status DeleteDocument(long? documentidTodelete, long UserID)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    para.Add("@DocumentId", documentidTodelete);
                    para.Add("@OperationId", 1);
                    para.Add("@UserId", UserID);
                    const string procName = "[dbo].[sp_DeleteSelectDocument]";
                    connection.Open();
                    return connection.Query<Status>(procName, para, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to delete document", ex, "DeleteDocument");
                return null;
            }

        }

    }
}

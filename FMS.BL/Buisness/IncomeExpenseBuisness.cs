﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FMS.BL.Utilities;

namespace FMS.BL.Buisness
{
   public class IncomeExpenseBuisness : Common
    {
        public Status AddUpdateOperationMaster(List<OperationValue> OperationValue, string IFSC, string ChequeNo, string DraftNo, string BankName,int TransactionId,string PaymentMode,string Partyname,string Date)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    DataTable List = new DataTable();
                    List.Columns.Add("OperationID", typeof(int));
                    List.Columns.Add("LedgerID", typeof(int));
                    List.Columns.Add("Ammount", typeof(decimal));
                    decimal amount = 0;
                    int ledgerId = 0;
                    foreach (var MD in OperationValue)
                    {
                        var dr = List.NewRow();
                        dr["OperationID"] = MD.OperationId;
                        dr["LedgerID"] = MD.LedgerId;
                        ledgerId = MD.LedgerId;
                        dr["Ammount"] = MD.OperationAmount;
                        amount = amount + MD.OperationAmount;
                        List.Rows.Add(dr);
                    }
                    para.Add("@TransactionID", TransactionId);
                    para.Add("@IFSC", IFSC);
                    para.Add("@ChequeNo", ChequeNo);
                    para.Add("@DraftNo", DraftNo);
                    para.Add("@BankName", BankName);
                    para.Add("@Amount", amount);
                    para.Add("@PaymentMode", PaymentMode);
                    para.Add("@LedgerId", ledgerId);
                    para.Add("@Partyname", Partyname);
                    para.Add("@Date", Convert.ToDateTime(Date));
                    para.Add("@Operations_Details", List, DbType.Object, ParameterDirection.Input, null);
                    const string procName = "[dbo].[sp_InsertUpdateTransaction]";
                    connection.Open();
                    return connection.Query<Status>(procName, para, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to insert or update AddUpdaeOperationMaster", ex, "AddUpdateOperationMaster");
                return null;
            }

        }

        public object GetDetails(long? LedgerId)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    
                    para.Add("@LedgerId", LedgerId);
                    
                    const string procName = "[dbo].[sp_SelectOperationByLedger]";
                    connection.Open();
                    return connection.Query<object>(procName, para, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Ledger", ex, "GetLedger");
                return null;
            }

        }

        public List<TransactionDetails> LoadLedgerValue(long? LedgerId,DateTime? date)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();

                    para.Add("@LedgerId", LedgerId);
                    para.Add("@Date", date);

                    const string procName = "[dbo].[sp_GetLedgerValue]";
                    connection.Open();
                    return connection.Query<TransactionDetails>(procName, para, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Ledger", ex, "GetLedger");
                return null;
            }

        }

        public List<TransactionMaster> GetReport(string SearchType, int LedgerId)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();

                    para.Add("@LedgerId", LedgerId);

                    const string procName = "[dbo].[sp_GetLedgerValue]";
                    connection.Open();
                    return connection.Query<TransactionMaster>(procName, para, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Ledger", ex, "GetLedger");
                return null;
            }

        }
    }
}

﻿CREATE TABLE [dbo].[tbl_Item] (
    [ItemId]          BIGINT         IDENTITY (1, 1) NOT NULL,
    [CategoryId]      BIGINT         NOT NULL,
    [ItemName]        NVARCHAR (100) NOT NULL,
    [GSTId]           BIGINT         NULL,
    [UnitId]          BIGINT         NOT NULL,
    [ItemDescription] NVARCHAR (MAX) NULL,
    [IsActive]        INT            NULL,
    [IsDeleted]       INT            NULL,
    [CreatedBy]       BIGINT         NULL,
    [CreatedOn]       DATETIME       NULL,
    [UpdatedBy]       BIGINT         NULL,
    [UpdatedOn]       DATETIME       NULL,
    [CompanyId]       BIGINT         NULL
);






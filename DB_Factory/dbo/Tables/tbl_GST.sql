﻿CREATE TABLE [dbo].[tbl_GST] (
    [GSTId]          BIGINT          IDENTITY (1, 1) NOT NULL,
    [HSN]            NVARCHAR (50)   NOT NULL,
    [CGST]           NUMERIC (18, 2) NULL,
    [IGST]           NUMERIC (18, 2) NULL,
    [Percentage]     NUMERIC (18, 2) NOT NULL,
    [GSTDescription] NVARCHAR (MAX)  NULL,
    [IsActive]       INT             NULL,
    [IsDeleted]      INT             NULL,
    [CreatedBy]      BIGINT          NULL,
    [CreatedOn]      DATETIME        NULL,
    [UpdatedBy]      BIGINT          NULL,
    [UpdatedOn]      DATETIME        NULL,
    [CompanyId]      BIGINT          NULL
);






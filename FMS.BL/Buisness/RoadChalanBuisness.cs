﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FMS.BL.Utilities;

namespace FMS.BL.Buisness
{
   public class RoadChalanBuisness : Common
    {
        #region RoadChalan region

        public Status AddUpdateRoadChalan(RoadChalan pmodel, long UserID)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    para.Add("@RoadChalanId"	,		  pmodel.Id);
                    para.Add("@NameOfDistributor"	,	  pmodel.NameOfDistributor);
                    para.Add("@AddressOfDistributor",  pmodel.AddressOfDistributor);
                    para.Add("@Destination"		,	  pmodel.Destination);
                    para.Add("@Quantity",				  pmodel.Quantity);
                    para.Add("@TruckNo"		,		  pmodel.TruckNo);

                    para.Add("@Product", pmodel.Product);
                    para.Add("@VehicleNumber", pmodel.VehicleNumber);
                    para.Add("@TotalWeight", pmodel.TotalWeight);
                    para.Add("@DriverName", pmodel.DriverName);
                    para.Add("@DriverMobile", pmodel.DriverMobile);

                    const string procName = "[dbo].[sp_InsertUpdateRoadChalan]";
                    connection.Open();
                    return connection.Query<Status>(procName, para, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to insert or update RoadChalan", ex, "AddUpdateRoadChalan");
                return null;
            }

        }
        public List<RoadChalan> GetRoadChalan(long? RoadChalanId, long UserID)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    if (RoadChalanId == 0)
                    {
                        RoadChalanId = null;
                    }
                    para.Add("@RoadChalanId", RoadChalanId);
                    para.Add("@OperationId", 2);
                    const string procName = "[dbo].[sp_DeleteSelectRoadChalan]";
                    connection.Open();
                    return connection.Query<RoadChalan>(procName, para, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch RoadChalan", ex, "GetRoadChalan");
                return null;
            }

        }
        public Status DeleteRoadChalan(long? RoadChalanId, long UserID)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    para.Add("@RoadChalanId", RoadChalanId);
                    para.Add("@OperationId", 1);
                    const string procName = "[dbo].[sp_DeleteSelectRoadChalan]";
                    connection.Open();
                    return connection.Query<Status>(procName, para, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to delete RoadChalan", ex, "DeleteRoadChalan");
                return null;
            }

        }
        #endregion
    }
}

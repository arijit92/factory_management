﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using FMS.BL;
using FMS.BL.Buisness;
using FMS.BL.Utilities;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace FMS.Controllers
{
    public class MachineController : Controller
    {
        // GET: Machine  
        public ActionResult List()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Add(int? machineid)
        {
            List<Unit> unit = null;
            Machine machine = null;
            MachineBuisness machineBuisness = null;
            UnitBuisness unitBuisness = null;
            long userid = 11;
            try
            {
                unit = new List<Unit>();
                machine = new Machine();
                machineBuisness = new MachineBuisness();
                unitBuisness = new UnitBuisness();
                if (machineid > 0)
                {
                    machine = machineBuisness.GetMachine(machineid, userid).SingleOrDefault();
                }
                unit = unitBuisness.GetUnit(0, 11).Where(i => i.IsActive == true && i.IsDeleted == false).ToList();
                ViewBag.Unit = new SelectList(unit, "UnitId", "UnitName", 0);

            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Machine list", ex, "Machine");
            }
            return View(machine);
        }
        [HttpPost]
        public ActionResult Add(Machine machine)
        {
            List<Unit> unit = null;
            UnitBuisness unitBuisness = null;
            unit = new List<Unit>();
            unitBuisness = new UnitBuisness();
            unit = unitBuisness.GetUnit(0, 11).Where(i => i.IsActive == true && i.IsDeleted == false).ToList();
            ViewBag.Unit = new SelectList(unit, "UnitId", "UnitName", 0);
            if (ModelState.IsValid)
            {
                MachineBuisness buisness = new MachineBuisness();
                Status status = new Status();
                status = buisness.AddUpdateMachine(machine, 1);
                TempData["SuccessFlag"] = status.SuccessFlag;
                TempData["Msg"] = status.Msg;
                if (status.SuccessFlag == "1")
                {
                    return RedirectToAction("List");
                }
                else
                {
                    return View(machine);
                }
            }
            return View(machine);
        }


        [HttpPost]
        public JsonResult LoadMachine(int MachineId)
        {
            long userid = 11;
            List<Machine> machine = null;
            MachineBuisness machineBuisness = null;
           
            try
            {
                machine = new List<Machine>();
                machineBuisness = new MachineBuisness();
                machine = machineBuisness.GetMachine(MachineId, userid).Where(i => i.IsDeleted == false).ToList();
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Machine list", ex, "Machine");
            }

            return Json(machine, JsonRequestBehavior.AllowGet);
        }

        protected List<Machine> GetAllMachine()
        {
            long userid = 11;
            List<Machine> machine = null;
            MachineBuisness machineBuisness = null;

            try
            {
                machine = new List<Machine>();
                machineBuisness = new MachineBuisness();
                machine = machineBuisness.GetMachine(0, userid).Where(i => i.IsDeleted == false).ToList();
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Machine list", ex, "Machine");
            }

            return machine;

        }

        public ActionResult Delete(int machineid)
        {
            MachineBuisness machineBuisness = null;
            Status status = null;
            long userid = 11;
            try
            {
                machineBuisness = new MachineBuisness();
                status = new Status();
                status = machineBuisness.DeleteMachine(machineid, userid);
                TempData["SuccessFlag"] = status.SuccessFlag;
                TempData["Msg"] = status.Msg;
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to delete Machine ", ex, "Delete");
            }
            return RedirectToAction("List");
        }

        public ActionResult LoadPDFForMachine()
        {
            try
            {
                MemoryStream workStream = new MemoryStream();
                StringBuilder status = new StringBuilder("");
                DateTime dTime = DateTime.Now;
                //file name to be created   
                string strPDFFileName = string.Format("MachineList" + dTime.ToString("yyyyMMdd") + "-" + ".pdf");
                iTextSharp.text.Document doc = new iTextSharp.text.Document();
                //      doc.SetMargins(0f, 0f, 0f, 0f);
                //Create PDF Table with 5 columns  
                PdfPTable tableLayout = new PdfPTable(5);
                doc.SetMargins(0f, 0f, 0f, 0f);
                //Create PDF Table  

                //file will created in this path  
                string strAttachment = Server.MapPath("~/Downloads/" + strPDFFileName);


                PdfWriter.GetInstance(doc, workStream).CloseStream = false;
                doc.Open();

                float[] headers = { 50, 40, 50, 35, 50 }; //Header Widths  
                tableLayout.SetWidths(headers); //Set the pdf headers  
                tableLayout.WidthPercentage = 90; //Set the PDF File witdh percentage  
                tableLayout.HeaderRows = 1;

                tableLayout.AddCell(new PdfPCell(new Phrase(" "))
                {
                    Colspan = 12,
                    Border = 0,
                    PaddingBottom = 5,
                    BackgroundColor = new iTextSharp.text.BaseColor(250, 250, 252)
                });

                tableLayout.AddCell(new PdfPCell(new Phrase("Machine List", new Font(Font.FontFamily.HELVETICA, 14, 1, new iTextSharp.text.BaseColor(250, 250, 252))))
                {
                    Colspan = 12,
                    Border = 0,
                    PaddingBottom = 5,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    BackgroundColor = new iTextSharp.text.BaseColor(15, 77, 144)
                });

                ////Add header  

                BL.Utilities.Common.AddCellToHeader(tableLayout, "Machine Name");
                BL.Utilities.Common.AddCellToHeader(tableLayout, "Quantity");
                BL.Utilities.Common.AddCellToHeader(tableLayout, "Unit");
                BL.Utilities.Common.AddCellToHeader(tableLayout, "Cost of Machineries");
                BL.Utilities.Common.AddCellToHeader(tableLayout, "Status");
                

                //Get the table data
                List<Machine> machines = new List<Machine>();
                machines = GetAllMachine();
                foreach (var item in machines)
                {

                    BL.Utilities.Common.AddCellToBody(tableLayout, item.MachineName, 1);
                    BL.Utilities.Common.AddCellToBody(tableLayout, item.Quantity.ToString(), 2);
                    BL.Utilities.Common.AddCellToBody(tableLayout, item.UnitName, 3);
                    BL.Utilities.Common.AddCellToBody(tableLayout, item.CostOfMachineries.ToString(), 4);
                    
                    if (item.IsActive == true)
                    {
                        BL.Utilities.Common.AddCellToBody(tableLayout, "Active", 5);
                    }
                    else
                    {
                        BL.Utilities.Common.AddCellToBody(tableLayout, "In Active", 5);
                    }


                }

                //Add Content to PDF   
                doc.Add(tableLayout);

                // Closing the document  
                doc.Close();

                byte[] byteInfo = workStream.ToArray();
                workStream.Write(byteInfo, 0, byteInfo.Length);
                workStream.Position = 0;

                Session["DownloadPDF_MachineList"] = byteInfo;
                return Json("", JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return View("Exception", ex.Message);
            }
        }
        public ActionResult DownloadPDFForMachine()
        {
            if (Session["DownloadPDF_MachineList"] != null)
            {
                byte[] data = Session["DownloadPDF_MachineList"] as byte[];
                return File(data, "application/pdf", "MachineList.pdf");
            }
            else
            {
                return View("Exception");

            }
        }
    }
}
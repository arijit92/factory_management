﻿CREATE TABLE [dbo].[tbl_ItemSalesStockTransaction] (
    [Id]            INT             IDENTITY (1, 1) NOT NULL,
    [PacketId]      INT             NOT NULL,
    [BostaQuantity] DECIMAL (18, 2) NOT NULL,
    [Weight]        DECIMAL (18, 2) NOT NULL,
    [Date]          DATETIME        NULL
);


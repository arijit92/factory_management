using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Collections;
using System.Text.RegularExpressions;

namespace FMS.BL.Utilities
{
    public class HandleQuota
    {
        private static string sInputChar;
        private static string sOutputChar;
        private static string sSingleQuota = "'";
        private static string sDoubleQuota = "\''";

        private static System.Type tString = System.Type.GetType("System.String");

        #region Private Static Methods


        private static void SetInputReplacement()
        {
            sInputChar = sSingleQuota;
            sOutputChar = sDoubleQuota;
        }
        private static void SetOutputReplacement()
        {
            sInputChar = sDoubleQuota;
            sOutputChar = sSingleQuota;
        }

        private static string Replace(object obj)
        {
            string str = Convert.ToString(obj);
            return str.Replace(sInputChar, sOutputChar);
        }



        private static SortedList HandleSortedList(SortedList sl)
        {
            SortedList oSl = new SortedList();
            foreach (string key in sl.Keys)
            {
                string sKey, sValue;
                sKey = Replace(key);
                sValue = Replace(sl[key]);
                oSl[sKey] = sValue;
            }
            return oSl;
        }

        private static ArrayList HandleArrayList(ArrayList al)
        {
            ArrayList oAl = new ArrayList();
            int count;
            for (count = 0; count < al.Count; count++)
            {
                oAl.Add(Replace(al[count]));
            }
            return oAl;
        }


        private static Hashtable HandleHashtable(Hashtable ht)
        {
            Hashtable oHt = new Hashtable();
            foreach (string key in ht.Keys)
            {
                string sKey, sValue;
                sKey = Replace(key);
                sValue = Replace(ht[key]);
                oHt[sKey] = sValue;
            }
            return oHt;
        }

        private static bool IsDBNull(object o)
        {
            if (o == DBNull.Value)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        private static bool IsValidSystemType(object o)
        {
            if (Utilities.Common.IsNull(o))
            {
                return true;
            }
            else if (o == DBNull.Value)
            {
                return true;
            }
            else if (Utilities.Common.IsNumeric(o))
            {
                return true;
            }
            else if (Utilities.Common.IsBoolean(o))
            {
                return true;
            }
            else if (Utilities.Common.IsDate(Convert.ToString(o)))
            {
                return true;
            }

            else
            {
                return false;
            }
        }
        //Old
        //private static Object[] HandleArrayObject(params  object[] obj)
        //{
        //    Object[] oObj = new Object[obj.Length];

        //    int count;
        //    for (count = 0; count < obj.Length; count++)
        //    {
        //        if (IsValidSystemType(obj[count]))
        //        {
        //            oObj[count] = obj[count];
        //        }
        //        else
        //        {
        //            oObj[count] = Replace(obj[count]);
        //        }
        //    }
        //    return oObj;
        //}

        //New
        private static Object[] HandleArrayObject(params  object[] obj)
        {
            Object[] oObj = new Object[obj.Length];

            int count;
            // System.Type tString = System.Type.GetType("System.String");
            for (count = 0; count < obj.Length; count++)
            {
                //if (IsValidSystemType(obj[count]))
                if (tString != obj[count].GetType())
                {
                    oObj[count] = obj[count];
                }
                else
                {
                    oObj[count] = Replace(obj[count]);
                }
            }
            return oObj;
        }

        //New

        private static DataTable HandleDataTable(DataTable dt)
        {

            int count;
            //ArrayList al = new ArrayList();
            List<int> lstGeneric = new List<int>();

            DataTable oDT = dt.Clone();
            //System.Type tString = System.Type.GetType("System.String");
            int iColNum = 0;

            foreach (DataColumn dc in dt.Columns)
            {
                if (dc.DataType == tString)
                    lstGeneric.Add(iColNum);
                iColNum++;
            }
            int ListCount = lstGeneric.Count;

            foreach (DataRow dr in dt.Rows)
            {

                for (count = 0; count < ListCount; count++)
                {
                    //iColNum = ;
                    if (!IsDBNull(dr[lstGeneric[count]]))
                    {
                        //oDR[count] = dr[count];
                        dr[lstGeneric[count]] = Replace(dr[lstGeneric[count]]);
                    }
                }
                //DataRow oDR = oDT.NewRow();
                //for (count = 0; count < columnCount; count++)
                //{
                //    if (al.Contains(dt.Columns[count].ColumnName))
                //    {
                //        if (!IsDBNull(dr[count]))
                //        {
                //            //oDR[count] = dr[count];
                //            dr[count] = Replace(dr[count]);
                //        }                        
                //    }                    
                //}
                oDT.ImportRow(dr);
            }
            //dt.AcceptChanges();
            return oDT;
        }


        //Old
        //private static DataTable HandleDataTable(DataTable dt)
        //{
        //    int columnCount = dt.Columns.Count;
        //    int count;
        //    DataTable oDT = dt.Clone();
        //    foreach (DataRow dr in dt.Rows)
        //    {
        //        DataRow oDR = oDT.NewRow();
        //        for (count = 0; count < columnCount; count++)
        //        {
        //            if (IsValidSystemType(dr[count]))
        //            {
        //                oDR[count] = dr[count];
        //            }
        //            else
        //            {
        //                oDR[count] = Replace(dr[count]);
        //            }
        //        }
        //        oDT.Rows.Add(oDR);
        //    }
        //    return oDT;
        //}




        #endregion

        public static string HandleInputString(string str)
        {
            str = HandleOutputString(str);
            SetInputReplacement();
            return Replace(str).Trim();
        }
        public static string HandleInputString(object obj)
        {
            if (!(obj == null || obj == DBNull.Value))
            {
                string str = obj.ToString().Trim();
                str = HandleOutputString(str);
                SetInputReplacement();
                return Replace(str).Trim();
            }
            else
                return String.Empty;
        }
        public static string HandleOutputString(string str)
        {
            SetOutputReplacement();
            return Replace(str);
        }

        public static SortedList HandleInputSortedList(SortedList sl)
        {
            SetInputReplacement();
            return HandleSortedList(sl);
        }

        public static SortedList HandleOutputSortedList(SortedList sl)
        {
            SetOutputReplacement();
            return HandleSortedList(sl);
        }

        public static ArrayList HandleInputArrayList(ArrayList al)
        {
            SetInputReplacement();
            return HandleArrayList(al);
        }

        public static ArrayList HandleOutputArrayList(ArrayList al)
        {
            SetOutputReplacement();
            return HandleArrayList(al);
        }

        public static Hashtable HandleInputHashtable(Hashtable ht)
        {
            SetInputReplacement();
            return HandleHashtable(ht);
        }

        public static Hashtable HandleOutputHashtable(Hashtable ht)
        {
            SetOutputReplacement();
            return HandleHashtable(ht);
        }

        public static Object[] HandleInputArrayObject(params  object[] obj)
        {
            SetInputReplacement();
            Object[] oObj = HandleArrayObject(obj);
            return oObj;
        }

        public static Object[] HandleOutputArrayObject(params  object[] obj)
        {
            SetOutputReplacement();
            return HandleArrayObject(obj);
        }

        public static DataTable HandleInputDataTable(DataTable dt)
        {
            SetInputReplacement();
            return HandleDataTable(dt);
        }

        public static DataTable HandleOutputDataTable(DataTable dt)
        {
            SetOutputReplacement();
            return HandleDataTable(dt);
        }


        public static DataSet HandleInputDataSet(DataSet ds)
        {
            DataSet oDs = new DataSet();
            foreach (DataTable dt in ds.Tables)
            {
                //DataTable oDt = dt.Clone();
                //oDt = HandleInputDataTable(oDt);
                oDs.Tables.Add(HandleInputDataTable(dt));
            }
            return oDs;
        }

        public static DataSet HandleOutputDataSet(DataSet ds)
        {
            DataSet oDs = new DataSet();
            foreach (DataTable dt in ds.Tables)
            {
                //DataTable oDt = dt.Clone();
                //oDt = HandleOutputDataTable(oDt);
                //oDs.Tables.Add(oDt);
                oDs.Tables.Add(HandleOutputDataTable(dt));
            }
            return oDs;
        }


    }
}

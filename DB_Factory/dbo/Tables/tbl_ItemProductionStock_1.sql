﻿CREATE TABLE [dbo].[tbl_ItemProductionStock] (
    [ProductionStockId] INT             IDENTITY (1, 1) NOT NULL,
    [PacketId]          INT             NOT NULL,
    [TotalWeight]       DECIMAL (18, 2) NOT NULL,
    [BostaQuantity]     INT             NOT NULL,
    [UpdatedOn]         DATETIME        NOT NULL
);


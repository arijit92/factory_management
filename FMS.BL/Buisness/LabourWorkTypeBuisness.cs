﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FMS.BL.Utilities;


namespace FMS.BL.Buisness
{
    public class LabourWorkTypeBuisness : Common
    {
        #region LabourWorkType region

        public Status AddUpdateLabourWorkType(LabourWorkType pmodel, long UserID)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    para.Add("@LabourWorkTypeId", pmodel.LabourWorkTypeId);
                    para.Add("@LabourWorkTypeName", pmodel.LabourWorkTypeName);
                    para.Add("@LabourWorkTypeDescription", pmodel.LabourWorkTypeDescription);
                    para.Add("@PerBagrRate", pmodel.PerBagrRate);
                    para.Add("@IsActive", Convert.ToInt32(pmodel.IsActive == false ? 0 : 1));
                    para.Add("@CreatedBy", UserID);
                    para.Add("@CompanyId", pmodel.CompanyId);
                    const string procName = "[dbo].[sp_InsertUpdateLabourWorkType]";
                    connection.Open();
                    return connection.Query<Status>(procName, para, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to insert or update Labour Work Type", ex, "AddUpdateLabourWorkType");
                return null;
            }

        }
        public List<LabourWorkType> GetLabourWorkType(long? LabourWorkTypeId, long UserID)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    if (LabourWorkTypeId == 0)
                    {
                        LabourWorkTypeId = null;
                    }
                    para.Add("@LabourWorkTypeId", LabourWorkTypeId);
                    para.Add("@OperationId", 2);
                    para.Add("@UserId", UserID);
                    const string procName = "[dbo].[sp_DeleteSelectLabourWorkType]";
                    connection.Open();
                    return connection.Query<LabourWorkType>(procName, para, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Labour Work Type", ex, "GetLabourWorkType");
                return null;
            }

        }
        public Status DeleteLabourWorkType(long? LabourWorkTypeId, long UserID)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    para.Add("@LabourWorkTypeId", LabourWorkTypeId);
                    para.Add("@OperationId", 1);
                    para.Add("@UserId", UserID);
                    const string procName = "[dbo].[sp_DeleteSelectLabourWorkType]";
                    connection.Open();
                    return connection.Query<Status>(procName, para, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to delete Labour Work Type", ex, "DeleteLabourWorkType");
                return null;
            }

        }
        #endregion
    }
}

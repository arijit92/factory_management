﻿CREATE PROCEDURE  [dbo].[sp_InsertUpdateAssets]
-- Add the parameters for the stored procedure here
 @AssetsId				[bigint]
,@SerialNumber			[NVARCHAR](50)
,@DeedNumber			[NVARCHAR](50) NULL
,@PlotNumber			[NVARCHAR](50) NULL
,@Moja					[NVARCHAR](50) NULL
,@AreaOfLand			[NVARCHAR](50) NULL
,@ValueOfLand			[NVARCHAR](50) NULL
,@Remarks				[NVARCHAR](MAX) NULL

AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
--update or insert checking
IF(@AssetsId>0)
BEGIN
--Duplicate value check
IF EXISTS(SELECT * FROM tbl_AssetsMaster WITH(NOLOCK) WHERE SerialNumber = @SerialNumber AND AssetsId != @AssetsId AND IsDeleted = 0)
BEGIN
SELECT 'Already exists.' as Msg,0 as SuccessFlag
END
ELSE
BEGIN
--update
UPDATE tbl_AssetsMaster
SET
 --AssetsId		=@AssetsId		
 SerialNumber	=@SerialNumber	
,DeedNumber		=@DeedNumber	
,PlotNumber		=@PlotNumber	
,Moja			=@Moja			
,AreaOfLand		=@AreaOfLand	
,ValueOfLand	=@ValueOfLand	
,Remarks		=@Remarks
,CreatedOn		=Getdate()
WHERE AssetsId =@AssetsId
SELECT 'Assets updated successfully.' as Msg,1 as SuccessFlag
END
END
ELSE
BEGIN
--Duplicate value check
IF EXISTS (SELECT * FROM tbl_AssetsMaster WITH(NOLOCK) WHERE SerialNumber = @SerialNumber AND IsDeleted=0)
BEGIN
SELECT 'Already exists.' as Msg,0 as SuccessFlag
END
ELSE
BEGIN
--insert
INSERT INTO tbl_AssetsMaster
(
 --AssetsId		
 SerialNumber	
,DeedNumber		
,PlotNumber		
,Moja			
,AreaOfLand		
,ValueOfLand	
,Remarks		
,CreatedOn		
)
VALUES
(
-- @AssetsId		
 @SerialNumber	
,@DeedNumber	
,@PlotNumber	
,@Moja			
,@AreaOfLand	
,@ValueOfLand	
,@Remarks
,Getdate()
)
SELECT 'Assets created successfully.' as Msg,1 as SuccessFlag
END
END
END TRY
BEGIN CATCH
SELECT 'Sorry something went wrong, unable to process,' as Msg,0 as SuccessFlag
RETURN
END CATCH
END
﻿CREATE PROCEDURE  [dbo].[sp_InsertUpdateSupplierType]
-- Add the parameters for the stored procedure here
 @SupplierTypeId [bigint]
,@SupplierTypeName [NVARCHAR](MAX)
,@SupplierTypeDescription [NVARCHAR](MAX)
,@IsActive [int] NULL
,@CreatedBy [bigint] NULL
,@CompanyId [bigint] NULL
AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
--update or insert checking
IF(@SupplierTypeId>0)
BEGIN
--Duplicate value check
IF EXISTS(SELECT * FROM tbl_SupplierType WITH(NOLOCK) WHERE SupplierTypeName = @SupplierTypeName AND SupplierTypeId != @SupplierTypeId AND IsDeleted = 0)
BEGIN
SELECT 'Already exists.' as Msg,0 as SuccessFlag
END
ELSE
BEGIN
--update
UPDATE tbl_SupplierType
SET
SupplierTypeName =UPPER(@SupplierTypeName)
,SupplierTypeDescription = @SupplierTypeDescription
,IsActive =@IsActive
,UpdatedBy =@CreatedBy
,UpdatedOn      =GETDATE()
,CompanyId =@CompanyId
WHERE SupplierTypeId =@SupplierTypeId
SELECT 'SupplierType updated successfully.' as Msg,1 as SuccessFlag
END
END
ELSE
BEGIN
--Duplicate value check
IF EXISTS (SELECT * FROM tbl_SupplierType WITH(NOLOCK) WHERE SupplierTypeName = @SupplierTypeName AND IsDeleted = 0)
BEGIN
SELECT 'Already exists.' as Msg,0 as SuccessFlag
END
ELSE
BEGIN
--insert
INSERT INTO tbl_SupplierType
(
 SupplierTypeName
,SupplierTypeDescription 
,IsActive
,IsDeleted
,CreatedBy
,CreatedOn
,CompanyId
)
VALUES
(
UPPER(@SupplierTypeName)
,@SupplierTypeDescription
,@IsActive
,0
,@CreatedBy
,GETDATE()
,@CompanyId
)
SELECT 'SupplierType created successfully.' as Msg,1 as SuccessFlag
END
END
END TRY
BEGIN CATCH
SELECT 'Sorry something went wrong, unable to process,' as Msg,0 as SuccessFlag
RETURN
END CATCH
END
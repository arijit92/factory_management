﻿CREATE PROCEDURE  [dbo].[sp_SelectEmployeeSalary]
-- Add the parameters for the stored procedure here
 @EmployeeId [bigint] NULL
,@Year nvarchar(4)  --1=delete,2=Select
,@Month [int] NULL

AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
IF(@EmployeeId>0)
BEGIN
SELECT E.EmployeeId
,E.FirstName
,E.LastName
,E.PAKA_SALARY
,E.KACHA_SALARY
,E.PF
,ES.IsPaid
,ISNULL(ES.Deduction,0) as Deduction
,ISNULL(ES.DeductionRemarks,'') DeductionRemarks
,ISNULL(ES.Bonus,0) Bonus
,ISNULL(ES.BonusRemarks,'') BonusRemarks
,ISNULL(ES.LoanDeduction,0) as LoanDeduction
--,ISNULL((select top 1 DueAmount from tbl_LoanRepayManagement where EmployeeId = E.EmployeeId order by Id desc),ISNULL(LM.Amount,0)) AS DueAmount
 from tbl_EmployeeSalary ES 
left join tbl_Employee E on ES.EmployeeId = E.EmployeeId
where ES.Year = @Year AND ES.Month = @Month AND ES.EmployeeId = @EmployeeId
END
ELSE
BEGIN
SELECT E.EmployeeId
,E.FirstName
,E.LastName
,E.PAKA_SALARY
,E.KACHA_SALARY
,E.PF
,ES.IsPaid
,ISNULL(ES.Deduction,0) as Deduction
,ISNULL(ES.DeductionRemarks,'') DeductionRemarks
,ISNULL(ES.Bonus,0) Bonus
,ISNULL(ES.BonusRemarks,'') BonusRemarks
,ISNULL(ES.LoanDeduction,0) as LoanDeduction
--,ISNULL((select top 1 DueAmount from tbl_LoanRepayManagement where EmployeeId = E.EmployeeId order by Id desc),ISNULL(LM.Amount,0)) AS DueAmount
 from tbl_EmployeeSalary ES 
left join tbl_Employee E on ES.EmployeeId = E.EmployeeId
where ES.Year = @Year AND ES.Month = @Month 
--AND ES.EmployeeId = @EmployeeId
END


END TRY
BEGIN CATCH
SELECT 'Sorry something went wrong, unable to process,' as Msg,0 as SuccessFlag
RETURN
END CATCH
END
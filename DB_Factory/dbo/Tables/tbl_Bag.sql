﻿CREATE TABLE [dbo].[tbl_Bag] (
    [BagId]          BIGINT          IDENTITY (1, 1) NOT NULL,
    [BagName]        NVARCHAR (50)   NOT NULL,
    [BagDescription] NVARCHAR (MAX)  NULL,
    [NetWeight]      NUMERIC (18, 2) NOT NULL,
    [PPWeight]       NUMERIC (18, 2) NOT NULL,
    [IsActive]       INT             NULL,
    [IsDeleted]      INT             NULL,
    [CreatedBy]      BIGINT          NULL,
    [CreatedOn]      DATETIME        NULL,
    [UpdatedBy]      BIGINT          NULL,
    [UpdatedOn]      DATETIME        NULL,
    [CompanyId]      BIGINT          NULL
);


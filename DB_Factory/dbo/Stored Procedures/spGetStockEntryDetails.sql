﻿CREATE Proc spGetStockEntryDetails
as
begin
	Select 
	 PE.ItemTypeId
	,PE.NoOfBosta
	,PE.Weight
	,PE.Price
	,FORMAT (PurchaseDate, 'dd/MM/yyyy ') as Date
	,I.ItemName 
	from tblPurchaseEntry PE
	inner join tbl_Item I on PE.ItemTypeId = I.ItemId order by PE.Id desc
end
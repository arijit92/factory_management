﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FMS.BL.Utilities;

namespace FMS.BL.Buisness
{
    public class SalePurchaseBuisness : Common
    {
        public List<ItemPurchaseStock> GetPurchaseStockCount()
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();

                    const string procName = "[dbo].[sp_ItemPurchaseStockCount]";
                    connection.Open();
                    return connection.Query<ItemPurchaseStock>(procName, para, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Branch", ex, "GetBranch");
                return null;
            }

        }
        // production stock count
        public List<ItemProductionStock> GetProductionStockCount()
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();

                    const string procName = "[dbo].[sp_ItemProductionStockCount]";
                    connection.Open();
                    return connection.Query<ItemProductionStock>(procName, para, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Branch", ex, "GetBranch");
                return null;
            }

        }
        public List<ItemProductionStockTransaction> GetProductionStockDetails()
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();

                    const string procName = "[dbo].[sp_ItemProductionStockDetails]";
                    connection.Open();
                    return connection.Query<ItemProductionStockTransaction>(procName, para, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Branch", ex, "GetBranch");
                return null;
            }

        }

        public List<Packet> GetPacketDetails()
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    const string procName = "[dbo].[sp_GetAllPackets]";
                    connection.Open();
                    return connection.Query<Packet>(procName, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Packets", ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                return null;
            }

        }

        public Status AddUpdatePurchaseDetails(ItemPurchaseStockTransaction purchaseModel)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    para.Add("@Id", purchaseModel.Id);
                    para.Add("@CustomerId ", purchaseModel.CustomerId);
                    para.Add("@PacketId", purchaseModel.PacketId);
                    para.Add("@BostaQuantity", purchaseModel.BostaQuantity);
                    para.Add("@Weight ", purchaseModel.Weight);
                    para.Add("@Price", purchaseModel.Price);
                    para.Add("@TransactionId", 1);
                    const string procName = "[dbo].[sp_InsertUpdateItemPurchaseStock]";
                    connection.Open();
                    return connection.Query<Status>(procName, para, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to insert or update Purchase Details", ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                return null;
            }

        }
        public Status AddUpdateSaleDetails(ItemPurchaseStockTransaction purchaseModel)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    para.Add("@Id", purchaseModel.Id);
                    para.Add("@CustomerId ", purchaseModel.CustomerId);
                    para.Add("@PacketId", purchaseModel.PacketId);
                    para.Add("@BostaQuantity", purchaseModel.BostaQuantity);
                    para.Add("@Weight ", purchaseModel.Weight);
                    para.Add("@Price", purchaseModel.Price);
                    para.Add("@TransactionId", 1);
                    const string procName = "[dbo].[sp_InsertUpdateItemCustomerSalesStock]";
                    connection.Open();
                    return connection.Query<Status>(procName, para, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to insert or update Sale Details", ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                return null;
            }

        }
        public Status AddUpdateProductionDetails(ItemProductionStockTransaction purchaseModel)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    para.Add("@Id", purchaseModel.Id);
                   // para.Add("@CustomerId ", purchaseModel.CustomerId);
                    para.Add("@PacketId", purchaseModel.PacketId);
                    para.Add("@BostaQuantity", purchaseModel.BostaQuantity);
                    para.Add("@Weight ", purchaseModel.Weight);
                    //para.Add("@Price", purchaseModel.Price);
                    //para.Add("@TransactionId", 1);
                    const string procName = "[dbo].[sp_ItemPurchaseStockToProduction]";
                    connection.Open();
                    return connection.Query<Status>(procName, para, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to insert or update Purchase Details", ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                return null;
            }

        }

        public Status AddUpdateSaleDetails(ItemSalesStockTransaction purchaseModel)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    para.Add("@Id", purchaseModel.Id);
                    // para.Add("@CustomerId ", purchaseModel.CustomerId);
                    para.Add("@PacketId", purchaseModel.PacketId);
                    para.Add("@BostaQuantity", purchaseModel.BostaQuantity);
                    para.Add("@Weight ", purchaseModel.Weight);
                    //para.Add("@Price", purchaseModel.Price);
                    //para.Add("@TransactionId", 1);
                    const string procName = "[dbo].[sp_ItemProductionToSales]";
                    connection.Open();
                    return connection.Query<Status>(procName, para, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to insert or update Purchase Details", ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                return null;
            }

        }

        public List<ItemPurchaseStockTransaction> GetAllPurchaseList(long? Id =null)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    if (Id.HasValue)
                        para.Add("@Id", Id);
                    const string procName = "[dbo].[sp_ItemPurchaseStockDetails]";
                    connection.Open();
                    return connection.Query<ItemPurchaseStockTransaction>(procName,para, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Purchase List", ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                return null;
            }

        }


        public List<ItemSalesStock> GetSaleStockCount()
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();

                    const string procName = "[dbo].[sp_ItemSalesStockCount]";
                    connection.Open();
                    return connection.Query<ItemSalesStock>(procName, para, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Branch", ex, "GetBranch");
                return null;
            }

        }
        public List<ItemSalesStockTransaction> GetSaleStockDetails()
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();

                    const string procName = "[dbo].[sp_ItemSalesStockDetails]";
                    connection.Open();
                    return connection.Query<ItemSalesStockTransaction>(procName, para, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Branch", ex, "GetBranch");
                return null;
            }

        }
        public List<ItemCustomerSalesStockTransaction> GetSaleDetails()
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();

                    const string procName = "[dbo].[sp_ItemCustomerSalesStockDetails]";
                    connection.Open();
                    return connection.Query<ItemCustomerSalesStockTransaction>(procName, para, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Branch", ex, "GetBranch");
                return null;
            }
        }

        //public Status AddUpdatePurchaseEntryDetails(PurchaseEntry purchaseModel)
        //{
        //    try
        //    {
        //        using (IDbConnection connection = base.GetConnection())
        //        {
        //            var para = new DynamicParameters();
        //            // para.Add("@Id", purchaseModel.Id);
        //            para.Add("@ItemTypeId ", purchaseModel.ItemTypeId);
        //            para.Add("@NoOfBosta", purchaseModel.NoOfBosta);
        //            para.Add("@Weight ", purchaseModel.Weight);
        //            para.Add("@Price", purchaseModel.Price);
        //            const string procName = "[dbo].[spInsertUpdatePurchaseEntryDetails]";
        //            connection.Open();
        //            return connection.Query<Status>(procName, para, commandType: CommandType.StoredProcedure).FirstOrDefault();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        AppLogger.Instance.Error(this.GetType(), "Unable to insert or update Purchase Details", ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
        //        return null;
        //    }

        //}

        public List<PurchaseEntry> GetStockEntryDetails()
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();

                    const string procName = "[dbo].[spGetStockEntryDetails]";
                    connection.Open();
                    return connection.Query<PurchaseEntry>(procName, para, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Branch", ex, "GetBranch");
                return null;
            }
        }
    }


}

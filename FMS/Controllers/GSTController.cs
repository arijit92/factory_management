﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FMS.BL;
using FMS.BL.Buisness;
using FMS.BL.Utilities;
namespace FMS.Controllers
{
    public class GSTController : Controller
    {
        // GET: GST 
        public ActionResult List()
        {
            ViewBag.ParentMenu = "master";
            ViewBag.ChildMenu = "gst";
            return View();
        }

        [HttpGet]
        public ActionResult Add(int? gstid)
        {
            ViewBag.ParentMenu = "master";
            ViewBag.ChildMenu = "gst";
            GST gst = null;
            GSTBuisness gstBuisness = null;
            long userid = 11;
            try
            {
                gst = new GST();
                gstBuisness = new GSTBuisness();
                if (gstid > 0)
                {
                    gst = gstBuisness.GetGST(gstid, userid).SingleOrDefault();
                }


            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch GST list", ex, "GST");
            }
            return View(gst);
        }
        [HttpPost]
        public ActionResult Add(GST gst)
        {
            ViewBag.ParentMenu = "master";
            ViewBag.ChildMenu = "gst";
            if (ModelState.IsValid)
            {
                GSTBuisness buisness = new GSTBuisness();
                Status status = new Status();
                status = buisness.AddUpdateGST(gst, 1);
                TempData["SuccessFlag"] = status.SuccessFlag;
                TempData["Msg"] = status.Msg;
                if (status.SuccessFlag == "1")
                {
                    return RedirectToAction("List");
                }
                else
                {
                    return View(gst);
                }
            }
            return View(gst);
        }

        [HttpPost]
        public JsonResult LoadGST(int GSTId)
        {
            ViewBag.ParentMenu = "master";
            ViewBag.ChildMenu = "gst";
            long userid = 11;
            List<GST> gst = null;
            GSTBuisness gstBuisness = null;
            try
            {
                gst = new List<GST>();
                gstBuisness = new GSTBuisness();
                gst = gstBuisness.GetGST(GSTId, userid).Where(i => i.IsDeleted == false).ToList();
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch GST list", ex, "GST");
            }

            return Json(gst, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(int gstid)
        {
            ViewBag.ParentMenu = "master";
            ViewBag.ChildMenu = "gst";
            GSTBuisness gstBuisness = null;
            Status status = null;
            long userid = 11;
            try
            {
                gstBuisness = new GSTBuisness();
                status = new Status();
                status = gstBuisness.DeleteGST(gstid, userid);
                TempData["SuccessFlag"] = status.SuccessFlag;
                TempData["Msg"] = status.Msg;
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to delete GST ", ex, "Delete");
            }
            return RedirectToAction("List");
        }

    }
}
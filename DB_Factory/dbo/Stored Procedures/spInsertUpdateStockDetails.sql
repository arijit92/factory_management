﻿
CREATE PROC [dbo].[spInsertUpdateStockDetails] --1,1,300,'STOCK',0
(
	@Id bigint = 0,
	@ItemTypeId bigint = NULL,
	@Weight decimal(18,2),
	@Type varchar(10) = NULL,
	@IsProductionEntry bit = 0
)
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY
		IF ((SELECT Weight FROM tblPurchaseEntryMaster WHERE ItemTypeId = @ItemTypeId) < @Weight)
			BEGIN
				SELECT 'Sorry something went wrong, unable to process,' as Msg,0 as SuccessFlag
				RETURN;
			END
		IF @Id>0
			BEGIN
				IF(@Type = 'STOCK')
					BEGIN
						UPDATE  tblPurchaseStock SET
						Weight = Weight + @Weight, 
						IsProductionEntry = @IsProductionEntry
						WHERE Id = @Id

						SELECT 'Stock Details updated successfully.' as Msg,1 as SuccessFlag
					END
				ELSE
					BEGIN
						UPDATE  tblPurchaseStockWastage SET
						Weight = @Weight 
						WHERE Id = @Id

						SELECT 'Stock Wastage Details updated successfully.' as Msg,1 as SuccessFlag
					END
			END

		ELSE
			BEGIN
				IF(@Type = 'STOCK')
				BEGIN
					INSERT INTO tblPurchaseStock VALUES
					(
						@ItemTypeId,
						@Weight,
						GETDATE(),
						@IsProductionEntry
					)
					SELECT 'Stock Entered successfully.' as Msg,1 as SuccessFlag
				END
				ELSE
				BEGIN
					INSERT INTO tblPurchaseStockWastage VALUES
					(
						@ItemTypeId,
						@Weight,
						GETDATE()
					)
					SELECT 'Stock Wastage Details Entered successfully.' as Msg,1 as SuccessFlag
				END
			END

		UPDATE tblPurchaseEntryMaster SET Weight = Weight - @Weight WHERE ItemTypeId = @ItemTypeId
	END TRY

	BEGIN CATCH
		SELECT 'Sorry something went wrong, unable to process,' as Msg,0 as SuccessFlag
	END CATCH
END
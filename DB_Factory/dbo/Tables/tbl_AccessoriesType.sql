﻿CREATE TABLE [dbo].[tbl_AccessoriesType] (
    [AccessoriesTypeId]   BIGINT        IDENTITY (1, 1) NOT NULL,
    [AccessoriesTypeName] NVARCHAR (50) NULL,
    [Description]         NVARCHAR (50) NULL,
    [IsDeleted]           BIT           NULL,
    [Date]                DATE          NULL
);


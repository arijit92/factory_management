﻿CREATE TABLE [dbo].[tbl_EmployeeType] (
    [EmployeeTypeId]          BIGINT         IDENTITY (1, 1) NOT NULL,
    [EmployeeTypeName]        NVARCHAR (50)  NOT NULL,
    [EmployeeTypeDescription] NVARCHAR (100) NULL,
    [IsActive]                INT            NULL,
    [IsDeleted]               INT            NULL,
    [CreatedBy]               BIGINT         NULL,
    [CreatedOn]               DATETIME       NULL,
    [UpdatedBy]               BIGINT         NULL,
    [UpdatedOn]               DATETIME       NULL,
    [CompanyId]               BIGINT         NULL
);




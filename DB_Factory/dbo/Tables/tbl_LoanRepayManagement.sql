﻿CREATE TABLE [dbo].[tbl_LoanRepayManagement] (
    [Id]         INT             IDENTITY (1, 1) NOT NULL,
    [EmployeeId] INT             NOT NULL,
    [Amount]     DECIMAL (18, 2) NOT NULL,
    [DueAmount]  DECIMAL (18, 2) NOT NULL,
    [Date]       DATETIME        NULL
);


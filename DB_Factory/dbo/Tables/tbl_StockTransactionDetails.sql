﻿CREATE TABLE [dbo].[tbl_StockTransactionDetails] (
    [StockTransactionDetailsId] INT             NOT NULL,
    [TransactionId]             INT             NOT NULL,
    [ItemId]                    INT             NULL,
    [Quantity]                  NUMERIC (18, 2) NULL,
    [TransactionType]           NVARCHAR (10)   NULL,
    [IsActive]                  BIT             NULL,
    [IsDeleted]                 BIT             NULL,
    [CreatedBy]                 BIGINT          NULL,
    [CreatedOn]                 DATETIME        NULL,
    [UpdatedBy]                 BIGINT          NULL,
    [UpdatedOn]                 DATETIME        NULL,
    [CompanyId]                 BIGINT          NULL
);


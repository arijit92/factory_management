﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FMS.BL.Utilities;

namespace FMS.BL.Buisness
{
    public class PacketBuisness : Common
    {

        public List<Packet> GetPacketDetails()
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    const string procName = "[dbo].[sp_GetAllPackets]";
                    connection.Open();
                    return connection.Query<Packet>(procName, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Packets", ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                return null;
            }

        }
    }
}

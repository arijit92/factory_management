﻿CREATE PROCEDURE  [dbo].[sp_InsertUpdateAccessories]
-- Add the parameters for the stored procedure here
 @AccessoriesId [bigint]
 ,@AccessoriesTypeId [bigint] null
,@AccessoriesName [NVARCHAR](MAX)
,@AccessoriesDescription [NVARCHAR](MAX)
,@IsActive [int] NULL
,@CreatedBy [bigint] NULL
,@CompanyId [bigint] NULL
AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
--update or insert checking
IF(@AccessoriesId>0)
BEGIN
--Duplicate value check
IF EXISTS(SELECT * FROM tbl_Accessories WITH(NOLOCK) WHERE AccessoriesName = @AccessoriesName AND AccessoriesId != @AccessoriesId)
BEGIN
SELECT 'Already exists.' as Msg,0 as SuccessFlag
END
ELSE
BEGIN
--update
UPDATE tbl_Accessories
SET
AccessoriesName =UPPER(@AccessoriesName)
,AccessoriesTypeId = @AccessoriesTypeId
,AccessoriesDescription = @AccessoriesDescription
,IsActive =@IsActive
,UpdatedBy =@CreatedBy
,UpdatedOn      =GETDATE()
,CompanyId =@CompanyId
WHERE AccessoriesId =@AccessoriesId
SELECT 'Accessories updated successfully.' as Msg,1 as SuccessFlag
END
END
ELSE
BEGIN
--Duplicate value check
IF EXISTS (SELECT * FROM tbl_Accessories WITH(NOLOCK) WHERE AccessoriesName = @AccessoriesName)
BEGIN
SELECT 'Already exists.' as Msg,0 as SuccessFlag
END
ELSE
BEGIN
--insert
INSERT INTO tbl_Accessories
(
 AccessoriesName
 ,AccessoriesTypeId
,AccessoriesDescription 
,IsActive
,IsDeleted
,CreatedBy
,CreatedOn
,CompanyId
)
VALUES
(
UPPER(@AccessoriesName)
,@AccessoriesTypeId
,@AccessoriesDescription
,@IsActive
,0
,@CreatedBy
,GETDATE()
,@CompanyId
)
SELECT 'Accessories created successfully.' as Msg,1 as SuccessFlag
END
END
END TRY
BEGIN CATCH
SELECT 'Sorry something went wrong, unable to process,' as Msg,0 as SuccessFlag
RETURN
END CATCH
END
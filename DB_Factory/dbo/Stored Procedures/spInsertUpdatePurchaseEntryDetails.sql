﻿
CREATE PROC [dbo].[spInsertUpdatePurchaseEntryDetails] --0,1,3,460.00,400.00,0
(
	@ItemTypeId bigint = NULL,
	@NoOfBosta bigint = NULL,
	@Weight decimal(18,2),
	@Price decimal(18,2),
	@IsStockEntry bit = 0
)
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY
		
		INSERT INTO tblPurchaseEntry VALUES
		(
			@ItemTypeId,
			@NoOfBosta,
			@Weight,
			@Price,
			GETDATE()
		)

		IF (SELECT COUNT(*) FROM tblPurchaseEntryMaster WHERE ItemTypeId = @ItemTypeId ) >0
			BEGIN
				UPDATE tblPurchaseEntryMaster SET
				NoOfBosta = NoOfBosta + @NoOfBosta,
				Weight = Weight + @Weight
				WHERE ItemTypeId = @ItemTypeId
			END
		ELSE
			BEGIN
				INSERT INTO tblPurchaseEntryMaster VALUES
				(
					@@IDENTITY,
					@ItemTypeId,
					@NoOfBosta,
					@Weight,
					GETDATE()
				)
			END
		SELECT 'Purchase entry created successfully.' as Msg,1 as SuccessFlag
			
	END TRY

	BEGIN CATCH
		SELECT 'Sorry something went wrong, unable to process,' as Msg,0 as SuccessFlag
	END CATCH
END
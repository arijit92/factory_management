﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FMS.BL.Utilities;

namespace FMS.BL.Buisness
{
   public class ProductionBuissness : Common
    {
        public Status AddUpdateProductionEntryDetails(ProdEntry prodEntry)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    para.Add("@PRODID", prodEntry.ProductionID);
                    para.Add("@ITEMTYPEID ", prodEntry.ItemId);
                   
                    para.Add("@NUMBEROFBAGS", prodEntry.NumberOfBags);
                    para.Add("@WEIGHT", prodEntry.Weight);
                    
                    const string procName = "[dbo].[insertupdateProdEntry]";
                    connection.Open();
                    return connection.Query<Status>(procName, para, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to insert or update Purchase Details", ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                return null;
            }

        }

        public List<ProdEntry> GetProductionStockCount()
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();

                    const string procName = "[dbo].[spProductionStockCount]";
                    connection.Open();
                    return connection.Query<ProdEntry>(procName, para, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch purchase stock", ex, "GetPurchaseStockCount");
                return null;
            }

        }

        public List<ProdEntry> GetProductionStockDetails()
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();

                    const string procName = "[dbo].[spGetProdEntryDetails]";
                    connection.Open();
                    return connection.Query<ProdEntry>(procName, para, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch purchase stock", ex, "GetPurchaseStockCount");
                return null;
            }

        }
    }
}

﻿CREATE PROCEDURE  [dbo].[sp_ItemProductionToSales]
-- Add the parameters for the stored procedure here
 @Id int = null
,@packetId     [bigint]
,@BostaQuantity  decimal(18,2)
,@Weight         decimal(18,2)
AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
Declare @StockBostaQuantity decimal
SET @StockBostaQuantity = (SELECT BostaQuantity from tbl_ItemProductionStock where PacketId = @packetId)
IF(@StockBostaQuantity>=@BostaQuantity)
BEGIN
--check existing or not
if(@Id>0)
--if exixting update the record
BEGIN
UPDATE  tbl_ItemSalesStockTransaction
SET  	
 PacketId      	  = 	@PacketId      	
,BostaQuantity 	  = 	@BostaQuantity 	
,Weight        	  = 	@Weight        		
,Date			  = 	GETDATE()		
 where Id = @Id

END
ELSE
BEGIN 
--if not exixts insert the record
INSERT INTO tbl_ItemSalesStockTransaction
(    
 PacketId      
,BostaQuantity 
,Weight        
,Date
)
values
(    
 @PacketId      
,@BostaQuantity 
,@Weight        
,GETDATE()
)
END
Declare @TotalBosta decimal(18,2),@TotalWeight decimal(18,2)
SET @TotalBosta = (SELECT SUM(BostaQuantity) from tbl_ItemSalesStockTransaction where PacketId = @PacketId)
SET @TotalWeight = (SELECT SUM(Weight) from tbl_ItemSalesStockTransaction where PacketId = @PacketId)
--check the main stock table
IF EXISTS(select SalesStockId from tbl_ItemSalesStock where PacketId = @PacketId)
BEGIN
UPDATE tbl_ItemSalesStock
SET 
--BostaQuantity = @TotalBosta,
--TotalWeight = @TotalWeight,
BostaQuantity = BostaQuantity + @BostaQuantity,
TotalWeight = TotalWeight + @Weight,
UpdatedOn = GETDATE()
WHERE PacketId = @PacketId
END
ELSE
BEGIN
INSERT INTO tbl_ItemSalesStock
(
PacketId
,TotalWeight
,BostaQuantity
,UpdatedOn
)
VALUES
(
@PacketId
,@Weight
,@BostaQuantity
,GETDATE()
)
END
SELECT 'Production Stock updated successfully,' as Msg,1 as SuccessFlag
UPDATE tbl_ItemProductionStock
SET
BostaQuantity = BostaQuantity-@BostaQuantity
,TotalWeight = TotalWeight - @Weight
where PacketId = @packetId
END
ELSE
BEGIN
SELECT 'Stock not available,' as Msg,0 as SuccessFlag
END
END TRY
BEGIN CATCH
SELECT 'Sorry something went wrong, unable to process,' as Msg,0 as SuccessFlag
RETURN
END CATCH
END
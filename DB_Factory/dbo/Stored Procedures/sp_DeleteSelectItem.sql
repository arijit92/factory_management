﻿CREATE PROCEDURE  [dbo].[sp_DeleteSelectItem]
-- Add the parameters for the stored procedure here
 @ItemId [bigint] NULL
,@OperationId [bigint]  --1=delete,2=Select
,@UserId [bigint] NULL

AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
IF(@OperationId  = 1 AND @ItemId >0)
BEGIN
UPDATE tbl_Item
SET
    IsDeleted = 1
   ,UpdatedBy = @UserId
   ,UpdatedOn = GETDATE()
    WHERE ItemId = @ItemId
SELECT 'Item deleted successfully.' as Msg,1 as SuccessFlag
END
ELSE
BEGIN
SELECT TI.*,TU.UnitName,TG.HSN,TPC.CategoryName FROM tbl_Item TI WITH(NOLOCK) 
INNER JOIN tbl_ProductCategory TPC WITH(NOLOCK) ON TI.CategoryId = TPC.CategoryId
INNER JOIN tbl_Unit TU WITH(NOLOCK) ON TU.UnitId = TI.UnitId
LEFT  JOIN tbl_GST TG WITH(NOLOCK) ON TG.GSTId = TI.GSTId
WHERE ItemId = ISNULL(@ItemId,ItemId)
END

END TRY
BEGIN CATCH
SELECT 'Sorry something went wrong, unable to process,' as Msg,0 as SuccessFlag
RETURN
END CATCH
END
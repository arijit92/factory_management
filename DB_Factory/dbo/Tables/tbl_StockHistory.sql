﻿CREATE TABLE [dbo].[tbl_StockHistory] (
    [StockHistoryId] INT             IDENTITY (1, 1) NOT NULL,
    [Accessories]    INT             NOT NULL,
    [Quantity]       NUMERIC (18, 2) NULL,
    [EntryDate]      DATETIME        NULL,
    [UpdatedBy]      INT             NULL,
    [Remarks]        NVARCHAR (MAX)  NULL,
    [Type]           NVARCHAR (20)   NULL
);


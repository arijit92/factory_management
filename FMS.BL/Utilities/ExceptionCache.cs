using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Web;

namespace FMS.BL.Utilities
{
    public class ExceptionCache
    {
        public ExceptionCache()
        {
        }

        private StringBuilder cacheManager = new StringBuilder();

        public void AddCustomErrorMsg(Exception  ex)
        {
            //AddCustomErrorMsg("Message :" +  ex.Message + "<BR /> Source : " + ex.Source +"<BR /> Stack Trace :" + ex.StackTrace + "<BR />");             
            AddCustomErrorMsg(ex.Message);
        }


        public void AddCustomErrorMsg(string errorMessage)
        {
            cacheManager.Append(errorMessage + ".<BR>");
        }

        public void AddCustomErrorMsg(string fieldName, ErrorType errorType)
        {
            string errorMessage = "";

            if (errorType == ErrorType.Null)
            {
                errorMessage = " is NULL.";
            }
            else if (errorType == ErrorType.Required)
            {
                errorMessage = " is Required.";
            }
            else if (errorType == ErrorType.NonNumeric)
            {
                errorMessage =  " is NonNumeric.";
            }
            else if (errorType == ErrorType.NonDate)
            {
                errorMessage =  " is Not a Date.";
            }
            else if (errorType == ErrorType.NonBoolean)
            {
                errorMessage =  " is Not a Boolean value.";
            }
            else if (errorType == ErrorType.Empty)
            {
                errorMessage = " is Empty.";
            }
            else if (errorType == ErrorType.Invalid)
            {
                errorMessage = " is Invalid.";
            }
            cacheManager.Append(fieldName + errorMessage + "<BR>");
        }

        public bool IsEmpty()
        {
            bool isEmpty = false;
            if (cacheManager.Length == 0)
            {
                return true;
            }
            return isEmpty;
        }             

        public string Flush(bool saveMessage)
        {
            if (saveMessage)
            {
                return cacheManager.ToString();
            }
            else
            {
                string strError = cacheManager.ToString();
                cacheManager.Remove(0, cacheManager.Length);
                return strError;
            }
        }
        public string Flush()
        {
            string strError = cacheManager.ToString();
            cacheManager.Remove(0, cacheManager.Length);
            return strError;
        }
        public void Clear()
        {
            cacheManager.Remove(0, cacheManager.Length);
        }

        public static string HandleError(Exception ex)
        {
            ExceptionCache ec = new ExceptionCache();
            ec.AddCustomErrorMsg(ex);
            return ec.Flush();
        }
    }
}

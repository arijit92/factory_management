﻿CREATE PROCEDURE  [dbo].[sp_SalaryDisburse]
 @Year nvarchar(4),
 @Month int,
 @EmployeeId int,
 @Kacha_Salary decimal(18,2),
 @Paka_Salary decimal (18,2),
 @PF decimal(18,2),
 @Bonus decimal(18,2),
 @BonusRemarks nvarchar(100),
 @Deduction decimal(18,2),
 @DeductionRemarks nvarchar(100),
 @LoanAmount decimal(18,2),
 @DueAmount decimal(18,2),
 @LoanDeduction decimal(18,2)
AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
IF EXISTS(SELECT EmployeeId from tbl_EmployeeSalary where Year=@Year AND Month = @Month AND EmployeeId = @EmployeeId)
BEGIN
UPDATE tbl_EmployeeSalary
SET
Kacha_Salary		= @Kacha_Salary,
Paka_Salary			= @Paka_Salary,
PF					= @PF,
Bonus				= @Bonus,
BonusRemarks		= @BonusRemarks,
Deduction			= @Deduction,
DeductionRemarks	= @DeductionRemarks,
LoanAmount			= @LoanAmount,
DueAmount			= @DueAmount,
LoanDeduction		= @LoanDeduction,
Date				= GETDATE()
where Year=@Year AND Month = @Month AND EmployeeId = @EmployeeId
END
ELSE
BEGIN
INSERT INTO tbl_EmployeeSalary
(
 Year
,Month
,EmployeeId
,Kacha_Salary	
,Paka_Salary		
,PF				
,Bonus			
,BonusRemarks	
,Deduction		
,DeductionRemarks
,LoanAmount		
,DueAmount		
,LoanDeduction	
,Date
)
VALUES
(
 @Year
,@Month
,@EmployeeId
,@Kacha_Salary	
,@Paka_Salary		
,@PF				
,@Bonus			
,@BonusRemarks	
,@Deduction		
,@DeductionRemarks
,@LoanAmount		
,@DueAmount		
,@LoanDeduction	
,GETDATE()
)
END
IF(@LoanDeduction>0)
BEGIN
IF EXISTS(SELECT EmployeeId from tbl_LoanRepayManagement where Year=@Year AND Month = @Month AND EmployeeId = @EmployeeId)
BEGIN
UPDATE tbl_LoanRepayManagement
SET 
Amount = @LoanDeduction,
Date = GETDATE()
where Year=@Year AND Month = @Month AND EmployeeId = @EmployeeId
END
ELSE
BEGIN
INSERT INTO tbl_LoanRepayManagement
(
Year,
Month,
EmployeeId,
Amount,
Date
)
VALUES
(
@Year,
@Month,
@EmployeeId,
@LoanDeduction,
GETDATE()
)
END
END

SELECT 'Salary disbursed successfully.' as Msg,1 as SuccessFlag
END TRY
BEGIN CATCH
SELECT ERROR_MESSAGE() as Msg,0 as SuccessFlag
RETURN
END CATCH
END
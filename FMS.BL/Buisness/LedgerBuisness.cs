﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FMS.BL.Utilities;


namespace FMS.BL.Buisness
{
    public class LedgerBuisness : Common
    {
        #region ledger region

        public Status AddUpdateLedger(Ledger pmodel, long UserID)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    para.Add("@LedgerId", pmodel.LedgerId);
                    para.Add("@LedgerName", pmodel.LedgerName);
                    para.Add("@LedgerTypeId", pmodel.LedgerTypeId);
                    para.Add("@LedgerDescription", pmodel.LedgerDescription);
                    para.Add("@IsActive", Convert.ToInt32(pmodel.IsActive == false ? 0 : 1));
                    para.Add("@CreatedBy", UserID);
                    para.Add("@CompanyId", pmodel.CompanyId);
                    const string procName = "[dbo].[sp_InsertUpdateLedger]";
                    connection.Open();
                    return connection.Query<Status>(procName, para, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to insert or update Ledger", ex, "AddUpdateLedger");
                return null;
            }

        }

        public List<Ledger> GetLedger(long? LedgerId, long UserID)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    if (LedgerId == 0)
                    {
                        LedgerId = null;
                    }
                    para.Add("@LedgerId", LedgerId);
                    para.Add("@OperationId", 2);
                    para.Add("@UserId", UserID);
                    const string procName = "[dbo].[sp_DeleteSelectLedger]";
                    connection.Open();
                    return connection.Query<Ledger>(procName, para, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Ledger", ex, "GetLedger");
                return null;
            }

        }

        public Status DeleteLedger(long? LedgerId, long UserID)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    para.Add("@LedgerId", LedgerId);
                    para.Add("@OperationId", 1);
                    para.Add("@UserId", UserID);
                    const string procName = "[dbo].[sp_DeleteSelectLedger]";
                    connection.Open();
                    return connection.Query<Status>(procName, para, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to delete Ledger", ex, "DeleteLedger");
                return null;
            }

        }
        #endregion

        #region operation master
        public Status AddUpdateOperationMaster(OperationMaster pmodel, long UserID)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    para.Add("@OperationID", pmodel.OperationID);
                    para.Add("@OperationName", pmodel.OperationName);
                    para.Add("@OperationDesc", pmodel.OperationDesc);
                    para.Add("@LedgerID", pmodel.LedgerID);
                    para.Add("@IsActive", 1);
                    para.Add("@CreatedBy", UserID);
                    const string procName = "[dbo].[sp_InsertUpdateOperation]";
                    connection.Open();
                    return connection.Query<Status>(procName, para, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to insert or update AddUpdaeOperationMaster", ex, "AddUpdateOperationMaster");
                return null;
            }

        }

        public List<OperationMaster> GetOperation(long? LedgerId, long UserID)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    //if (LedgerId == 0)
                    //{
                    //    LedgerId = null;
                    //}
                    para.Add("@LedgerId", LedgerId);
                    para.Add("@OperationId", 2);
                    para.Add("@LedgerOperationId", 5);
                    para.Add("@UserId", UserID);
                    const string procName = "[dbo].[sp_DeleteSelectOperation]";
                    connection.Open();
                    return connection.Query<OperationMaster>(procName, para, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to delete  operation", ex, "GetOperation");
                return null;
            }

        }
        #endregion

        #region income expense
        //public List<OperationMaster> GetOperation(long? LedgerId, long UserID)
        //{
        //    try
        //    {
        //        using (IDbConnection connection = base.GetConnection())
        //        {
        //            var para = new DynamicParameters();
        //            //if (LedgerId == 0)
        //            //{
        //            //    LedgerId = null;
        //            //}
        //            para.Add("@LedgerId", LedgerId);
        //            para.Add("@OperationId", 2);
        //            para.Add("@LedgerOperationId", 5);
        //            para.Add("@UserId", UserID);
        //            const string procName = "[dbo].[sp_DeleteSelectOperation]";
        //            connection.Open();
        //            return connection.Query<OperationMaster>(procName, para, commandType: CommandType.StoredProcedure).ToList();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        AppLogger.Instance.Error(this.GetType(), "Unable to delete  operation", ex, "GetOperation");
        //        return null;
        //    }

        //}
        #endregion


    }
}

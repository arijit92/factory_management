﻿CREATE TABLE [dbo].[tbl_LabourWorkType] (
    [LabourWorkTypeId]          BIGINT          IDENTITY (1, 1) NOT NULL,
    [LabourWorkTypeName]        NVARCHAR (MAX)  NOT NULL,
    [PerBagrRate]               DECIMAL (18, 2) NOT NULL,
    [LabourWorkTypeDescription] NVARCHAR (MAX)  NULL,
    [IsActive]                  INT             NULL,
    [IsDeleted]                 INT             NULL,
    [CreatedBy]                 BIGINT          NULL,
    [CreatedOn]                 DATETIME        NULL,
    [UpdatedBy]                 BIGINT          NULL,
    [UpdatedOn]                 DATETIME        NULL,
    [CompanyId]                 BIGINT          NULL
);




﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FMS.BL;
using FMS.BL.Buisness;
using FMS.BL.Utilities;
namespace FMS.Controllers
{
    public class ApplicationLogController : Controller
    {
        // GET: ApplicationLog 
        public ActionResult List()
        {
            return View();
        }

        [HttpPost]
        public JsonResult LoadApplictionLog()
        {
           
            List<ApplicationLog> applicationLog = null;
            ApplicationLogBuisness applicationLogBuisness = null;
            try
            {
                applicationLog = new List<ApplicationLog>();
                applicationLogBuisness = new ApplicationLogBuisness();
                applicationLog = applicationLogBuisness.GetApplicationLog().ToList();
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Bag list", ex, "Bag");
            }

            return Json(applicationLog, JsonRequestBehavior.AllowGet);
        }

        

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FMS.BL;
using FMS.BL.Buisness;
using FMS.BL.Utilities;
namespace FMS.Controllers
{
    public class SupplierController : Controller
    {
        // GET: Role  
        public ActionResult List()
        {
            ViewBag.ParentMenu = "master";
            ViewBag.ChildMenu = "supplier";
            return View();
        }

        [HttpGet]
        public ActionResult Add(int? supplierid)
        {
            ViewBag.ParentMenu = "master";
            ViewBag.ChildMenu = "supplier";
            List<SupplierType> suppliersType = null;
            Supplier supplier = null;
            SupplierBuisness supplierBuisness = null;
            SupplierTypeBuisness supplierTypeBuisness = null;
            long userid = 11;
            try
            {
                suppliersType = new List<SupplierType>();
                supplier = new Supplier();
                supplierBuisness = new SupplierBuisness();
                supplierTypeBuisness = new SupplierTypeBuisness();
                if (supplierid > 0)
                {
                    supplier = supplierBuisness.GetSupplier(supplierid, userid).SingleOrDefault();
                }
                suppliersType = supplierTypeBuisness.GetSupplierType(0, 11).Where(i => i.IsActive == true && i.IsDeleted == false).ToList();
                ViewBag.SupplierType = new SelectList(suppliersType, "SupplierTypeId", "SupplierTypeName", 0);
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Supplier list", ex, "Supplier");
            }
            return View(supplier);
        }
        [HttpPost]
        public ActionResult Add(Supplier supplier)
        {
            ViewBag.ParentMenu = "master";
            ViewBag.ChildMenu = "supplier";
            SupplierBuisness buisness = null;
            List<SupplierType> suppliersType = null;
            SupplierTypeBuisness supplierTypeBuisness = null;
            supplierTypeBuisness = new SupplierTypeBuisness();
            suppliersType = supplierTypeBuisness.GetSupplierType(0, 11).Where(i => i.IsActive == true && i.IsDeleted == false).ToList();
            ViewBag.SupplierType = new SelectList(suppliersType, "SupplierTypeId", "SupplierTypeName", 0);
            if (ModelState.IsValid)
            {
                buisness = new SupplierBuisness();
               
                Status status = new Status();
                status = buisness.AddUpdateSupplier(supplier, 1);
                TempData["SuccessFlag"] = status.SuccessFlag;
                TempData["Msg"] = status.Msg;
                if (status.SuccessFlag == "1")
                {
                    return RedirectToAction("List");
                }
                else
                {
                    return View(supplier);
                }
            }
            return View(supplier);
        }


        [HttpPost]
        public JsonResult LoadSupplier(int SupplierId)
        {
            ViewBag.ParentMenu = "master";
            ViewBag.ChildMenu = "supplier";
            long userid = 11;
            List<Supplier> supplier = null;
            SupplierBuisness supplierBuisness = null;
            try
            {
                supplier = new List<Supplier>();
                supplierBuisness = new SupplierBuisness();
                supplier = supplierBuisness.GetSupplier(SupplierId, userid).Where(i => i.IsDeleted == false).ToList();
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Supplier list", ex, "Supplier");
            }

            return Json(supplier, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(int supplierid)
        {
            SupplierBuisness supplierBuisness = null;
            Status status = null;
            long userid = 11;
            try
            {
                supplierBuisness = new SupplierBuisness();
                status = new Status();
                status = supplierBuisness.DeleteSupplier(supplierid, userid);
                TempData["SuccessFlag"] = status.SuccessFlag;
                TempData["Msg"] = status.Msg;
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to delete Supplier ", ex, "Supplier");
            }
            return RedirectToAction("List");
        }

    }
}
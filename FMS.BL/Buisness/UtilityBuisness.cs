﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FMS.BL.Utilities;

namespace FMS.BL.Buisness
{
    public class UtilityBuisness : Common
    {
        #region GetDDLList
        //public List<DDLList> GetDDLList(DDLList ddlList)
        //{
        //    List<DDLList> DDLObjList = new List<DDLList>();
        //    DDLList DDLObj = null;
        //    List<SqlParameter> arrParams = new List<SqlParameter>();
        //    arrParams.Add(new SqlParameter("@tableName", ddlList.tableName));
        //    arrParams.Add(new SqlParameter("@param", ddlList.param));
        //    arrParams.Add(new SqlParameter("@PId", ddlList.PId));
        //    arrParams.Add(new SqlParameter("@ColumnName", ddlList.ColumnName));
        //    arrParams.Add(new SqlParameter("@PId1", ddlList.PId1));
        //    arrParams.Add(new SqlParameter("@ColumnName1", ddlList.ColumnName1));
        //    arrParams.Add(new SqlParameter("@Text", ddlList.Text));
        //    arrParams.Add(new SqlParameter("@Value", ddlList.Value));
        //    SqlDataReader rdr = SqlHelper.ExecuteReader(GetConnectionString(), CommandType.StoredProcedure, "DropDown_For_ALL_SP", arrParams.ToArray());
        //    if (rdr != null)
        //    {
        //        while (rdr.Read())
        //        {
        //            DDLObj = new DDLList();
        //            DDLObj.Value = Convert.ToString(rdr["Value"]);
        //            DDLObj.Text = Convert.ToString(rdr["Text"]);
        //            DDLObjList.Add(DDLObj);
        //        }
        //        rdr.Close();
        //    }
        //    rdr.Dispose();
        //    return DDLObjList;
        //}


        public List<DDLList> GetDDLList(DDLList ddlList)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    para.Add("@tableName", ddlList.tableName);
                    para.Add("@param", ddlList.param);
                    para.Add("@PId", ddlList.PId);
                    para.Add("@ColumnName", ddlList.ColumnName);
                    para.Add("@PId1", ddlList.PId1);
                    para.Add("@ColumnName1", ddlList.ColumnName1);
                    para.Add("@Text", ddlList.Text);
                    para.Add("@Value", ddlList.Value);

                    const string procName = "[dbo].[sp_DropDown_For_ALL]";
                    connection.Open();
                    return connection.Query<DDLList>(procName, para, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch ddl list", ex, "GetDDLList");
                return null;
            }

        }

        #endregion
    }
}

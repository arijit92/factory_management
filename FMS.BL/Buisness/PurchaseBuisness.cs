﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FMS.BL.Utilities;

namespace FMS.BL.Buisness
{
   public class PurchaseBuisness : Common
    {
        public Status AddUpdatePurchaseEntryDetails(PurchaseEntry purchaseModel)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    para.Add("@PURCHASEID", purchaseModel.PurchaseId);
                    para.Add("@DONUMBER ", purchaseModel.DONumber);
                    para.Add("@CHALLANNUMBER", purchaseModel.ChallanNumber);
                    para.Add("@SUPPLIERID ", purchaseModel.SupplierId);
                    para.Add("@TRUCKNUMBER", purchaseModel.TruckNumber);
                    para.Add("@ITEMTYPE", purchaseModel.ItemType);
                    para.Add("@NUMBEROFBAG", purchaseModel.NumberOfBag);
                    para.Add("@WEIGHT", purchaseModel.Weight);
                    para.Add("@UNIT", purchaseModel.Unit);
                    para.Add("@PAYMENTMODE", purchaseModel.PaymentMode);
                    para.Add("@BANKNAME", purchaseModel.BankName);
                    para.Add("@CHEQUENUMBER", purchaseModel.ChequeNumber);
                    para.Add("@DRAFTNUMBER", purchaseModel.DraftNumber);
                    para.Add("@TOTAL", purchaseModel.Total);
                    const string procName = "[dbo].[insertupdatePurchaseEntry]";
                    connection.Open();
                    return connection.Query<Status>(procName, para, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to insert or update Purchase Details", ex, System.Reflection.MethodBase.GetCurrentMethod().Name);
                return null;
            }

        }


        public List<PurchaseEntry> GetPurchaseStockCount()
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();

                    const string procName = "[dbo].[spPurchaseStockDetails]";
                    connection.Open();
                    return connection.Query<PurchaseEntry>(procName, para, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch purchase stock", ex, "GetPurchaseStockCount");
                return null;
            }

        }

        public List<PurchaseEntry> GetPurchaseStockDetails()
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();

                    const string procName = "[dbo].[spGetStockEntryDetails]";
                    connection.Open();
                    return connection.Query<PurchaseEntry>(procName, para, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch purchase stock", ex, "GetPurchaseStockCount");
                return null;
            }

        }
    }
}

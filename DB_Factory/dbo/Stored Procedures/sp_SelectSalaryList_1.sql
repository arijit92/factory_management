﻿CREATE PROCEDURE  [dbo].[sp_SelectSalaryList]
-- Add the parameters for the stored procedure here
 @EmployeeId [bigint] NULL
,@Year nvarchar(4)  --1=delete,2=Select
,@Month [int] NULL

AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
IF(@EmployeeId>0)
BEGIN
SELECT E.EmployeeId
,E.FirstName
,E.LastName
,E.PAKA_SALARY
,E.KACHA_SALARY
,E.PF
,ES.IsPaid
,ISNULL(ES.LoanDeduction,0) as DeductionAmount
,ISNULL(LM.Amount,0) as LoanAmount
,ISNULL((select top 1 DueAmount from tbl_LoanRepayManagement where EmployeeId = E.EmployeeId order by Id desc),ISNULL(LM.Amount,0)) AS DueAmount
 from tbl_EmployeeSalary ES 
inner join tbl_Employee E on ES.EmployeeId = E.EmployeeId
left join tbl_LoanManagement LM on Lm.EmployeeId = e.EmployeeId
where ES.Year = @Year AND ES.Month = @Month AND ES.EmployeeId = @EmployeeId
END
IF EXISTS(SELECT * from tbl_EmployeeSalaryTrack where Year=@Year AND Month = @Month )
BEGIN
SELECT E.EmployeeId
,E.FirstName
,E.LastName
,E.PAKA_SALARY
,E.KACHA_SALARY
,E.PF
,ES.IsPaid

,ISNULL(ES.LoanDeduction,0) as DeductionAmount
,ISNULL(LM.Amount,0) as LoanAmount
,ISNULL((select top 1 DueAmount from tbl_LoanRepayManagement where EmployeeId = E.EmployeeId order by Id desc),ISNULL(LM.Amount,0)) AS DueAmount
from tbl_EmployeeSalary ES 
right join tbl_Employee E on ES.EmployeeId = E.EmployeeId
left join tbl_LoanManagement LM on Lm.EmployeeId = e.EmployeeId
where ES.Year = @Year AND ES.Month = @Month
END
ELSE
BEGIN
select E.EmployeeId
,E.FirstName
,E.LastName
,E.PAKA_SALARY
,E.KACHA_SALARY
,E.PF 
,0 AS DeductionAmount
,ISNULL(LM.Amount,0) as LoanAmount
,ISNULL((select top 1 DueAmount from tbl_LoanRepayManagement where EmployeeId = E.EmployeeId order by Id desc),ISNULL(LM.Amount,0)) AS DueAmount
from 
tbl_Employee E 
--left join tbl_EmployeeSalary ES 
--on E.EmployeeId = ES.EmployeeId
left join tbl_LoanManagement LM on Lm.EmployeeId = e.EmployeeId
END

END TRY
BEGIN CATCH
SELECT 'Sorry something went wrong, unable to process,' as Msg,0 as SuccessFlag
RETURN
END CATCH
END
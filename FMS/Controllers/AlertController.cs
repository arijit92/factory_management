﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using FMS.BL;
using FMS.BL.Buisness;
using FMS.BL.Utilities;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace FMS.Controllers
{
    public class AlertController : Controller
    {
        // GET: Alert  
        public ActionResult List()
        {
            ViewBag.ParentMenu = "administrator";
            ViewBag.ChildMenu = "alert";
            return View();
        }

        [HttpGet]
        public ActionResult Add(int? alertid)
        {
            ViewBag.ParentMenu = "administrator";
            ViewBag.ChildMenu = "alert";
            Alert alert = null;
            AlertBuisness alertBuisness = null;
            long userid = 11;
            try
            {
                alert = new Alert();
                alertBuisness = new AlertBuisness();
                if (alertid > 0)
                {
                    alert = alertBuisness.GetAlert(alertid, userid).SingleOrDefault();
                }
               

            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Alert list", ex, "Alert");
            }
            return View(alert);
        }
        [HttpPost]
        public ActionResult Add(Alert alert)
        {
            ViewBag.ParentMenu = "administrator";
            ViewBag.ChildMenu = "alert";
            if (ModelState.IsValid)
            {
                AlertBuisness buisness = new AlertBuisness();
                Status status = new Status();
                status = buisness.AddUpdateAlert(alert, 1);
                TempData["SuccessFlag"] = status.SuccessFlag;
                TempData["Msg"] = status.Msg;
                if (status.SuccessFlag == "1")
                {
                    return RedirectToAction("List");
                }
                else
                {
                    return View(alert);
                }
            }
            return View(alert);
        }


        [HttpPost]
        public JsonResult LoadAlert(int AlertId)
        {
            ViewBag.ParentMenu = "administrator";
            ViewBag.ChildMenu = "alert";
            long userid = 11;
            List<Alert> alert = null;
            AlertBuisness alertBuisness = null;
            try
            {
                alert = new List<Alert>();
                alertBuisness = new AlertBuisness();
                alert = alertBuisness.GetAlert(AlertId, userid).Where(i => i.IsDeleted == false).ToList();
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Alert list", ex, "Alert");
            }

            return Json(alert, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(int alertid)
        {
            ViewBag.ParentMenu = "administrator";
            ViewBag.ChildMenu = "alert";
            AlertBuisness alertBuisness = null;
            Status status = null;
            long userid = 11;
            try
            {
                alertBuisness = new AlertBuisness();
                status = new Status();
                status = alertBuisness.DeleteAlert(alertid, userid);
                TempData["SuccessFlag"] = status.SuccessFlag;
                TempData["Msg"] = status.Msg;
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to delete Alert ", ex, "Delete");
            }
            return RedirectToAction("List");
        }

        protected List<Alert> GetAllAlert()
        {
            ViewBag.ParentMenu = "administrator";
            ViewBag.ChildMenu = "alert";
            long userid = 11;
            List<Alert> alerts = null;
            AlertBuisness alertBuisness = null;
            try
            {
                alerts = new List<Alert>();
                alertBuisness = new AlertBuisness();
                // AppLogger.Instance.Info(this.GetType(), "Fetch the list of all category", "LoadCategory");aa
                alerts = alertBuisness.GetAlert(0, userid).Where(i => i.IsDeleted == false).ToList();
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch employee list", ex, "LoadEmployee");
            }

            return alerts;

        }

        public ActionResult LoadPDFForAlert()
        {
            try
            {
                ViewBag.ParentMenu = "administrator";
                ViewBag.ChildMenu = "alert";
                MemoryStream workStream = new MemoryStream();
                StringBuilder status = new StringBuilder("");
                DateTime dTime = DateTime.Now;
                //file name to be created   
                string strPDFFileName = string.Format("AlertList" + dTime.ToString("yyyyMMdd") + "-" + ".pdf");
                iTextSharp.text.Document doc = new iTextSharp.text.Document();
                //      doc.SetMargins(0f, 0f, 0f, 0f);
                //Create PDF Table with 4 columns  
                PdfPTable tableLayout = new PdfPTable(4);
                doc.SetMargins(0f, 0f, 0f, 0f);
                //Create PDF Table  

                //file will created in this path  
                string strAttachment = Server.MapPath("~/Downloads/" + strPDFFileName);


                PdfWriter.GetInstance(doc, workStream).CloseStream = false;
                doc.Open();

                float[] headers = { 50, 40, 50, 35}; //Header Widths  
                tableLayout.SetWidths(headers); //Set the pdf headers  
                tableLayout.WidthPercentage = 90; //Set the PDF File witdh percentage  
                tableLayout.HeaderRows = 1;

                tableLayout.AddCell(new PdfPCell(new Phrase(" "))
                {
                    Colspan = 12,
                    Border = 0,
                    PaddingBottom = 5,
                    BackgroundColor = new iTextSharp.text.BaseColor(250, 250, 252)
                });

                tableLayout.AddCell(new PdfPCell(new Phrase("Alert List", new Font(Font.FontFamily.HELVETICA, 14, 1, new iTextSharp.text.BaseColor(250, 250, 252))))
                {
                    Colspan = 12,
                    Border = 0,
                    PaddingBottom = 5,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    BackgroundColor = new iTextSharp.text.BaseColor(15, 77, 144)
                });

                ////Add header  

                BL.Utilities.Common.AddCellToHeader(tableLayout, "Alert Name");
                BL.Utilities.Common.AddCellToHeader(tableLayout, "Alert Description");
                BL.Utilities.Common.AddCellToHeader(tableLayout, "Issue Date");
                BL.Utilities.Common.AddCellToHeader(tableLayout, "Renewal date");
                

                //Get the table data
                List<Alert> alerts = new List<Alert>();
                alerts = GetAllAlert();
                foreach (var item in alerts)
                {

                    BL.Utilities.Common.AddCellToBody(tableLayout, item.AlertName.ToString(), 1);
                    BL.Utilities.Common.AddCellToBody(tableLayout, item.AlertDescription, 2);
                    BL.Utilities.Common.AddCellToBody(tableLayout, String.Format("{0:M/d/yyyy}", item.IssueDate), 3);
                    BL.Utilities.Common.AddCellToBody(tableLayout, String.Format("{0:M/d/yyyy}", item.NextRenewalDate), 4);
                    
                    //if (item.IsActive == true)
                    //{
                    //    BL.Utilities.Common.AddCellToBody(tableLayout, "Active", 9);
                    //}
                    //else
                    //{
                    //    BL.Utilities.Common.AddCellToBody(tableLayout, "In Active", 9);
                    //}


                }

                //Add Content to PDF   
                doc.Add(tableLayout);

                // Closing the document  
                doc.Close();

                byte[] byteInfo = workStream.ToArray();
                workStream.Write(byteInfo, 0, byteInfo.Length);
                workStream.Position = 0;

                Session["DownloadPDF_AlertList"] = byteInfo;
                return Json("", JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return View("Exception", ex.Message);
            }
        }

        public ActionResult DownloadPDFForAlert()
        {
            if (Session["DownloadPDF_AlertList"] != null)
            {
                byte[] data = Session["DownloadPDF_AlertList"] as byte[];
                return File(data, "application/pdf", "AlertList.pdf");
            }
            else
            {
                return View("Exception");

            }
        }

        [HttpPost]
        public JsonResult GetAlertNotification()
        {
          
            List<Alert> alert = null;
            AlertBuisness alertBuisness = null;
            try
            {
                alert = new List<Alert>();
                alertBuisness = new AlertBuisness();
                alert = alertBuisness.GetAlertNotification().ToList();
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Alert list", ex, "GetAlertNotification");
            }

            return Json(alert, JsonRequestBehavior.AllowGet);
        }

    }
}
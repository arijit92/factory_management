﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FMS.BL.Utilities;

namespace FMS.BL.Buisness
{
   public class HomeBuisness : Common
    {
        #region get all CompanyProfile
        public List<Company> CompanyProfile()
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {

                    const string procName = "[dbo].[sp_SelectCompany]";
                    connection.Open();
                    return connection.Query<Company>(procName,  commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Item", ex, "GetItem");
                return null;
            }

        }
        #endregion
 
    }
}

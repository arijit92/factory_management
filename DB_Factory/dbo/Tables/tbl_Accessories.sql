﻿CREATE TABLE [dbo].[tbl_Accessories] (
    [AccessoriesId]          BIGINT         IDENTITY (1, 1) NOT NULL,
    [AccessoriesTypeId]      BIGINT         NULL,
    [AccessoriesName]        NVARCHAR (100) NOT NULL,
    [AccessoriesDescription] NVARCHAR (MAX) NULL,
    [IsActive]               INT            NULL,
    [IsDeleted]              INT            NULL,
    [CreatedBy]              BIGINT         NULL,
    [CreatedOn]              DATETIME       NULL,
    [UpdatedBy]              BIGINT         NULL,
    [UpdatedOn]              DATETIME       NULL,
    [CompanyId]              BIGINT         NULL
);






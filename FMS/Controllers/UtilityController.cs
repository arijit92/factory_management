﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FMS.BL;
using FMS.BL.Buisness;
using FMS.BL.Utilities;

namespace FMS.Controllers
{
    public class UtilityController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult DropDownPopulation(DDLList data)
        {
            UtilityBuisness utilityBuisness = new UtilityBuisness();

            List<DDLList> ddlList = utilityBuisness.GetDDLList(data);


            return Json(ddlList, JsonRequestBehavior.AllowGet);

        }
    }
}
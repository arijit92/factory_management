﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMS.BL.Validation
{
    public class RequiredIf : ValidationAttribute
    {
        private readonly string Value;
        public RequiredIf(string _value)
        {
            Value = _value;
        }
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var _value = validationContext.ObjectInstance
                .GetType().GetProperty(Value).GetValue(validationContext.ObjectInstance);
            
            if (validationContext.DisplayName == "PaymentMode")
            {

                if (!string.IsNullOrEmpty(Convert.ToString(_value)) && Convert.ToString(_value).Equals("2")
                    && string.IsNullOrEmpty(Convert.ToString(value)))
                {
                    return new ValidationResult("Payment Mode is mandatory", new string[] { validationContext.DisplayName });
                }
                else
                    return ValidationResult.Success;
            }
            if (validationContext.DisplayName == "ChequeNumber")
            {

                if (!string.IsNullOrEmpty(Convert.ToString(_value)) && Convert.ToString(_value).Equals("2")
                    && string.IsNullOrEmpty(Convert.ToString(value)))
                {
                    return new ValidationResult("Cheque Number is mandatory", new string[] { validationContext.DisplayName });
                }
                else
                    return ValidationResult.Success;
            }

            if (validationContext.DisplayName == "DONumber")
            {

                if (!string.IsNullOrEmpty(Convert.ToString(_value)) && Convert.ToString(_value).Equals("1")
                    && string.IsNullOrEmpty(Convert.ToString(value)))
                {
                    return new ValidationResult("DO Number is mandatory when supplier is goverment ", new string[] { validationContext.DisplayName });
                }
                else
                    return ValidationResult.Success;
            }

            if (validationContext.DisplayName == "ChallanNumber")
            {

                if (!string.IsNullOrEmpty(Convert.ToString(_value)) && Convert.ToString(_value).Equals("1")
                    && string.IsNullOrEmpty(Convert.ToString(value)))
                {
                    return new ValidationResult("Challan Number is mandatory when supplier is goverment ", new string[] { validationContext.DisplayName });
                }
                else
                    return ValidationResult.Success;
            }

            if (validationContext.DisplayName == "Total")
            {

                if (!string.IsNullOrEmpty(Convert.ToString(_value)) && Convert.ToString(_value).Equals("2")
                    && string.IsNullOrEmpty(Convert.ToString(value)))
                {
                    return new ValidationResult("Total is mandatory ", new string[] { validationContext.DisplayName });
                }
                else
                    return ValidationResult.Success;
            }

            return ValidationResult.Success;
        }
    }
}

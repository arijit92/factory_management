﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FMS.BL.Utilities;


namespace FMS.BL.Buisness
{
    public class AccessoriesBuisness : Common
    {
        #region Accessories region

        public Status AddUpdateAccessories(Accessories pmodel, long UserID)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    para.Add("@AccessoriesId", pmodel.AccessoriesId);
                    para.Add("@AccessoriesName", pmodel.AccessoriesName);
                    para.Add("@AccessoriesTypeId", pmodel.AccessoriesTypeId);
                    para.Add("@AccessoriesDescription", pmodel.AccessoriesDescription);
                    para.Add("@IsActive", Convert.ToInt32(pmodel.IsActive == false ? 0 : 1));
                    para.Add("@CreatedBy", UserID);
                    para.Add("@CompanyId", pmodel.CompanyId);
                    const string procName = "[dbo].[sp_InsertUpdateAccessories]";
                    connection.Open();
                    return connection.Query<Status>(procName, para, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to insert or update Unit", ex, "AddUpdateUnit");
                return null;
            }

        }
        public List<Accessories> GetAccessories(long? AccessoriesId, long UserID)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    if (AccessoriesId == 0)
                    {
                        AccessoriesId = null;
                    }
                    para.Add("@AccessoriesId", AccessoriesId);
                    para.Add("@OperationId", 2);
                    para.Add("@UserId", UserID);
                    const string procName = "[dbo].[sp_DeleteSelectAccessories]";
                    connection.Open();
                    return connection.Query<Accessories>(procName, para, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch unit", ex, "GetUnit");
                return null;
            }

        }
        public List<Accessories> GetAvailableAccessories()
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    
                    const string procName = "[dbo].[sp_GetAvailableAccessories]";
                    connection.Open();
                    return connection.Query<Accessories>(procName, para, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch unit", ex, "GetUnit");
                return null;
            }

        }
        public Status DeleteAccessories(long? AccessoriesId, long UserID)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    para.Add("@AccessoriesId", AccessoriesId);
                    para.Add("@OperationId", 1);
                    para.Add("@UserId", UserID);
                    const string procName = "[dbo].[sp_DeleteSelectAccessories]";
                    connection.Open();
                    return connection.Query<Status>(procName, para, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to delete Unit", ex, "DeleteUnit");
                return null;
            }

        }
        #endregion

        #region accessories type
        public List<AccessoriesType> GetAccessoriesType(long? AccessoriesId, long UserID)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    if (AccessoriesId == 0)
                    {
                        AccessoriesId = null;
                    }
                    para.Add("@AccessoriesTypeId", AccessoriesId);
                    para.Add("@OperationId", 2);
                    para.Add("@UserId", UserID);
                    const string procName = "[dbo].[sp_DeleteSelectAccessoriesType]";
                    connection.Open();
                    return connection.Query<AccessoriesType>(procName, para, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch unit", ex, "GetUnit");
                return null;
            }

        }

        public Status AddUpdateAccessoriesType(AccessoriesType pmodel)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    para.Add("@AccessoriesTypeId", pmodel.AccessoriesTypeId);
                    para.Add("@AccessoriesTypeName", pmodel.AccessoriesTypeName);
                    para.Add("@Description", pmodel.Description);
                   
                    const string procName = "[dbo].[sp_InsertUpdateAccessoriesType]";
                    connection.Open();
                    return connection.Query<Status>(procName, para, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to sp_InsertUpdateAccessoriesType", ex, "AddUpdateAccessoriesType");
                return null;
            }

        }

        public Status DeleteAccessoriesType(long? AccessoriesTypeId)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    para.Add("@AccessoriesTypeId", AccessoriesTypeId);
                    para.Add("@OperationId", 1);
                    para.Add("@UserId", 0);
                    const string procName = "[dbo].[sp_DeleteSelectAccessoriesType]";
                    connection.Open();
                    return connection.Query<Status>(procName, para, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to delete Unit", ex, "DeleteUnit");
                return null;
            }

        }

        #endregion
    }
}

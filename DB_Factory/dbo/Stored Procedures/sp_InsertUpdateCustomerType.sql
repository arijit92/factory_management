﻿CREATE PROCEDURE  [dbo].[sp_InsertUpdateCustomerType]
-- Add the parameters for the stored procedure here
 @CustomerTypeId [bigint]
,@CustomerTypeName [NVARCHAR](MAX)
,@CustomerTypeDescription [NVARCHAR](MAX)
,@IsActive [int] NULL
,@CreatedBy [bigint] NULL
,@CompanyId [bigint] NULL
AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
--update or insert checking
IF(@CustomerTypeId>0)
BEGIN
--Duplicate value check
IF EXISTS(SELECT * FROM tbl_CustomerType WITH(NOLOCK) WHERE CustomerTypeName = @CustomerTypeName AND CustomerTypeId != @CustomerTypeId)
BEGIN
SELECT 'Already exists.' as Msg,0 as SuccessFlag
END
ELSE
BEGIN
--update
UPDATE tbl_CustomerType
SET
CustomerTypeName =UPPER(@CustomerTypeName)
,CustomerTypeDescription = @CustomerTypeDescription
,IsActive =@IsActive
,UpdatedBy =@CreatedBy
,UpdatedOn      =GETDATE()
,CompanyId =@CompanyId
WHERE CustomerTypeId =@CustomerTypeId
SELECT 'CustomerType updated successfully.' as Msg,1 as SuccessFlag
END
END
ELSE
BEGIN
--Duplicate value check
IF EXISTS (SELECT * FROM tbl_CustomerType WITH(NOLOCK) WHERE CustomerTypeName = @CustomerTypeName)
BEGIN
SELECT 'Already exists.' as Msg,0 as SuccessFlag
END
ELSE
BEGIN
--insert
INSERT INTO tbl_CustomerType
(
 CustomerTypeName
,CustomerTypeDescription 
,IsActive
,IsDeleted
,CreatedBy
,CreatedOn
,CompanyId
)
VALUES
(
UPPER(@CustomerTypeName)
,@CustomerTypeDescription
,@IsActive
,0
,@CreatedBy
,GETDATE()
,@CompanyId
)
SELECT 'CustomerType created successfully.' as Msg,1 as SuccessFlag
END
END
END TRY
BEGIN CATCH
SELECT 'Sorry something went wrong, unable to process,' as Msg,0 as SuccessFlag
RETURN
END CATCH
END
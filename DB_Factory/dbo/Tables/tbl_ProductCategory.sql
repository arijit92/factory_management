﻿CREATE TABLE [dbo].[tbl_ProductCategory] (
    [CategoryId]   BIGINT         IDENTITY (1, 1) NOT NULL,
    [CategoryName] NVARCHAR (100) NOT NULL,
    [Description]  NVARCHAR (MAX) NULL,
    [IsActive]     INT            NULL,
    [IsDeleted]    INT            NULL,
    [CreatedBy]    BIGINT         NULL,
    [CreatedOn]    DATETIME       NULL,
    [UpdatedBy]    BIGINT         NULL,
    [UpdatedOn]    DATETIME       NULL,
    [CompanyId]    BIGINT         NULL
);


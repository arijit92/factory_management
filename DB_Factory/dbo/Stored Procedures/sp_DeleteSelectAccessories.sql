﻿CREATE PROCEDURE  [dbo].[sp_DeleteSelectAccessories]
-- Add the parameters for the stored procedure here
 @AccessoriesId [bigint] NULL
,@OperationId [bigint]  --1=delete,2=Select
,@UserId [bigint] NULL

AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
IF(@OperationId  = 1 AND @AccessoriesId >0)
BEGIN
UPDATE tbl_Accessories
SET
IsDeleted = 1
   ,UpdatedBy = @UserId
   ,UpdatedOn = GETDATE()
    WHERE AccessoriesId = @AccessoriesId
SELECT 'Accessories deleted successfully.' as Msg,1 as SuccessFlag
END
ELSE
BEGIN
SELECT * FROM tbl_Accessories WITH(NOLOCK)
left join tbl_AccessoriesType on tbl_AccessoriesType.AccessoriesTypeId = tbl_Accessories.AccessoriesTypeId
WHERE AccessoriesId = ISNULL(@AccessoriesId,AccessoriesId)
END

END TRY
BEGIN CATCH
SELECT 'Sorry something went wrong, unable to process,' as Msg,0 as SuccessFlag
RETURN
END CATCH
END
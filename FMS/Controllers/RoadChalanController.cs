﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FMS.BL;
using FMS.BL.Buisness;
using FMS.BL.Utilities;

namespace FMS.Controllers
{
    public class RoadChalanController : Controller
    {
        // GET: RoadChalan 
        public ActionResult List()
        {
            ViewBag.ParentMenu = "WeightMaster";
            ViewBag.ChildMenu = "RoadChalanList";
            return View();
        }

        [HttpGet]
        public ActionResult Add(int? RoadChalanid)
        {
            ViewBag.ParentMenu = "WeightMaster";
            ViewBag.ChildMenu = "RoadChalanList";
            RoadChalan RoadChalan = null;
            RoadChalanBuisness RoadChalanBuisness = null;
            long userid = 11;
            try
            {
                RoadChalan = new RoadChalan();
                RoadChalanBuisness = new RoadChalanBuisness();
                if (RoadChalanid > 0)
                {
                    RoadChalan = RoadChalanBuisness.GetRoadChalan(RoadChalanid, userid).SingleOrDefault();
                }


            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch RoadChalan list", ex, "RoadChalan");
            }
            return View(RoadChalan);
        }
        [HttpPost]
        public ActionResult Add(RoadChalan RoadChalan)
        {
            ViewBag.ParentMenu = "WeightMaster";
            ViewBag.ChildMenu = "RoadChalanList";
            if (ModelState.IsValid)
            {
                RoadChalanBuisness buisness = new RoadChalanBuisness();
                Status status = new Status();
                status = buisness.AddUpdateRoadChalan(RoadChalan, 1);
                TempData["SuccessFlag"] = status.SuccessFlag;
                TempData["Msg"] = status.Msg;
                if (status.SuccessFlag == "1")
                {
                    return RedirectToAction("List");
                }
                else
                {
                    return View(RoadChalan);
                }
            }
            return View(RoadChalan);
        }

        [HttpPost]
        public JsonResult LoadRoadChalan(int RoadChalanId)
        {
            ViewBag.ParentMenu = "WeightMaster";
            ViewBag.ChildMenu = "RoadChalanList";
            long userid = 11;
            List<RoadChalan> RoadChalan = null;
            RoadChalanBuisness RoadChalanBuisness = null;
            try
            {
                RoadChalan = new List<RoadChalan>();
                RoadChalanBuisness = new RoadChalanBuisness();
                RoadChalan = RoadChalanBuisness.GetRoadChalan(RoadChalanId, userid).Where(i => i.IsDeleted == false).ToList();
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch RoadChalan list", ex, "RoadChalan");
            }

            return Json(RoadChalan, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(int RoadChalanid)
        {
            ViewBag.ParentMenu = "WeightMaster";
            ViewBag.ChildMenu = "RoadChalanList";
            RoadChalanBuisness RoadChalanBuisness = null;
            Status status = null;
            long userid = 11;
            try
            {
                RoadChalanBuisness = new RoadChalanBuisness();
                status = new Status();
                status = RoadChalanBuisness.DeleteRoadChalan(RoadChalanid, userid);
                TempData["SuccessFlag"] = status.SuccessFlag;
                TempData["Msg"] = status.Msg;
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to delete RoadChalan ", ex, "Delete");
            }
            return RedirectToAction("List");
        }

        // GET: RoadChalan  Invoice
        public ActionResult Invoice(int Id)
        {
            ViewBag.ParentMenu = "WeightMaster";
            ViewBag.ChildMenu = "weightDetails";
            ViewBag.Id = Id;
            return View();
        }
    }
}
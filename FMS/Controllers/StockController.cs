﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FMS.BL;
using FMS.BL.Buisness;
using FMS.BL.Utilities;

namespace FMS.Controllers
{
    public class StockController : Controller
    {
        // GET: Stock
        public ActionResult Add()
        {
            ViewBag.ParentMenu = "stock";
            ViewBag.ChildMenu = "add";
            return View();
        }
        public ActionResult Release()
        {
            ViewBag.ParentMenu = "stock";
            ViewBag.ChildMenu = "release";
            return View();
        }
        public ActionResult List()
        {
            ViewBag.ParentMenu = "stock";
            ViewBag.ChildMenu = "list";
            AccessoriesBuisness accessoriesBuisness = new AccessoriesBuisness();
            List<AccessoriesType> accessories = new List<AccessoriesType>();
            accessories = accessoriesBuisness.GetAccessoriesType(0,0).ToList();
            ViewBag.Accessories = new SelectList(accessories, "AccessoriesTypeId", "AccessoriesTypeName", 0);
            return View();
        }

        public ActionResult StockAdd()
        {
            ViewBag.ParentMenu = "stock";
            ViewBag.ChildMenu = "add";
            AccessoriesBuisness accessoriesBuisness = new AccessoriesBuisness();
            List<Accessories> accessories = new List<Accessories>();
            
            List<AccessoriesType> accessoriestype = new List<AccessoriesType>();
            accessories = accessoriesBuisness.GetAccessories(0, 11).Where(i => i.IsActive == false && i.IsDeleted == false).ToList();
            ViewBag.Accessories = new SelectList(accessories, "AccessoriesId", "AccessoriesName", 0);
            accessoriestype = accessoriesBuisness.GetAccessoriesType(0, 11).Where(i => i.IsDeleted == false).ToList();
            ViewBag.AccessoriesType = new SelectList(accessoriestype, "AccessoriesTypeId", "AccessoriesTypeName", 0);

            //items.Add(new SelectListItem() { Text = "Add", Value = "Add" });
            //items.Add(new SelectListItem() { Text = "Release", Value = "Release" });
            //ViewBag.Type = new SelectList(items, "Value", "Text", 0);

            return View();
        }

        [HttpPost]
        public ActionResult StockAdd(StockMaster stockMaster)
        {
            ViewBag.ParentMenu = "stock";
            ViewBag.ChildMenu = "add";

            if (ModelState.IsValid)
            {
                StockBuisness stockBuisness = new StockBuisness();
                Status status = new Status();
                stockMaster.Type = "Add";
                status = stockBuisness.UpdateStock(stockMaster);
                TempData["SuccessFlag"] = status.SuccessFlag;
                TempData["Msg"] = status.Msg;
                if (status.SuccessFlag == "1")
                {
                    return RedirectToAction("List");
                }
                else
                {
                   
                    return View(stockMaster);
                }
            }
            AccessoriesBuisness accessoriesBuisness = new AccessoriesBuisness();
            List<Accessories> accessories = new List<Accessories>();
            List<AccessoriesType> accessoriestype = new List<AccessoriesType>();
            accessories = accessoriesBuisness.GetAccessories(0, 11).Where(i => i.IsActive == false).ToList();
            ViewBag.Accessories = new SelectList(accessories, "AccessoriesId", "AccessoriesName", 0);
            accessoriestype = accessoriesBuisness.GetAccessoriesType(0, 11).Where(i => i.IsDeleted == false).ToList();
            ViewBag.AccessoriesType = new SelectList(accessoriestype, "AccessoriesTypeId", "AccessoriesTypeName", 0);
            return View(stockMaster);
        }

        public ActionResult StockRelease()
        {
            ViewBag.ParentMenu = "stock";
            ViewBag.ChildMenu = "release";
            AccessoriesBuisness accessoriesBuisness = new AccessoriesBuisness();
            List<Accessories> accessories = new List<Accessories>();
            List<AccessoriesType> accessoriestype = new List<AccessoriesType>();
            accessories = accessoriesBuisness.GetAccessories(0,0).Where(i =>i.IsActive == false).ToList();
            ViewBag.Accessories = new SelectList(accessories, "AccessoriesId", "AccessoriesName", 0);
            accessoriestype = accessoriesBuisness.GetAccessoriesType(0, 0).ToList();
            ViewBag.AccessoriesType = new SelectList(accessoriestype, "AccessoriesTypeId", "AccessoriesTypeName", 0);

            //items.Add(new SelectListItem() { Text = "Add", Value = "Add" });
            //items.Add(new SelectListItem() { Text = "Release", Value = "Release" });
            //ViewBag.Type = new SelectList(items, "Value", "Text", 0);

            return View();
        }

        [HttpPost]
        public ActionResult StockRelease(StockMaster stockMaster)
        {
            ViewBag.ParentMenu = "stock";
            ViewBag.ChildMenu = "release";
            if (ModelState.IsValid)
            {
                StockBuisness stockBuisness = new StockBuisness();
                Status status = new Status();
                stockMaster.Type = "Release";
                status = stockBuisness.UpdateStock(stockMaster);
                TempData["SuccessFlag"] = status.SuccessFlag;
                TempData["Msg"] = status.Msg;
                if (status.SuccessFlag == "1")
                {
                    return RedirectToAction("List");
                }
                else
                {
                   
                    return View(stockMaster);
                }
            }
            AccessoriesBuisness accessoriesBuisness = new AccessoriesBuisness();
            List<Accessories> accessories = new List<Accessories>();
            List<AccessoriesType> accessoriestype = new List<AccessoriesType>();
            accessories = accessoriesBuisness.GetAccessories(0,0).Where(i => i.IsDeleted == false).ToList();
            
            ViewBag.Accessories = new SelectList(accessories, "AccessoriesId", "AccessoriesName", 0);
            accessoriestype = accessoriesBuisness.GetAccessoriesType(0,0).ToList();
            ViewBag.AccessoriesType = new SelectList(accessoriestype, "AccessoriesTypeId", "AccessoriesTypeName", 0);
            return View(stockMaster);
        }

        [HttpPost]
        public JsonResult GetAvailableStocks(int AccessoriesId)
        {
            long userid = 11;
            List<StockMaster> stockMasters = null;
            StockBuisness stockBuisness = null;
            try
            {
                stockMasters = new List<StockMaster>();
                stockBuisness = new StockBuisness();
                stockMasters = stockBuisness.GetAvailableStocks(AccessoriesId).ToList();
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Accessories list", ex, "Accessories");
            }

            return Json(stockMasters, JsonRequestBehavior.AllowGet);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using FMS.BL.Validation;

namespace FMS.BL
{
    public class DDLList
    {
        public string tableName { get; set; }

        public string Text { get; set; }
        public string Value { get; set; }
        public string param { get; set; }
        public string ColumnName { get; set; }
        public decimal? PId { get; set; }
        public string ColumnName1 { get; set; }
        public decimal? PId1 { get; set; }
        public List<DDLList> ddlList { get; set; }
        public DDLList()
        {
            ddlList = new List<DDLList>();
        }
    }


    public class Status
    {
        public string SuccessFlag { get; set; }

        public string Msg { get; set; }

        public string ErrMsg { get; set; }

        public string RefNo { get; set; }

    }
    public class ProductCategory
    {
        public long CategoryId { get; set; }

        [Display(Name = "Category Name")]
        [Required(ErrorMessage = "Select a Category Name")]
        public string CategoryName { get; set; }

        [Display(Name = "Category Description")]
        [Required(ErrorMessage = "Select a Category Description")]
        public string Description { get; set; }
        public int? IsActive { get; set; }
        public int? IsDeleted { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public long? CompanyId { get; set; }

    }
    public class Unit
    {
        public long UnitId { get; set; }
        [Required(ErrorMessage = "Enter unit name")]
        [StringLength(50)]
        [Display(Name = "Unit Name")]
        public string UnitName { get; set; }
      
        [StringLength(500)]
        [Display(Name = "Unit Description ")]
        public string UnitDescription { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public long? CompanyId { get; set; }
    }

    public class EmployeeType
    {
        public long EmployeeTypeId { get; set; }

        [Display(Name = "Employee Type Name")]
        [Required(ErrorMessage = "Select a Employee Type Name")]
        public string EmployeeTypeName { get; set; }

        [Display(Name = "Employee Type Description")]
        [Required(ErrorMessage = "Select a Employee Type Description")]
        public string EmployeeTypeDescription { get; set; }
        public int? IsActive { get; set; }
        public int? IsDeleted { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public long? CompanyId { get; set; }
    }


    public class Employee
    {
        public long? EmployeeId { get; set; }
        [Required(ErrorMessage = "Enter first name")]
        [StringLength(50)]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        [StringLength(50)]
        [Required(ErrorMessage = "Enter last name")]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        public string Fullname => string.Format("{0} {1}", FirstName, LastName);
        
        [Display(Name = "Branch")]
       
        public long? BranchId { get; set; }
        public string BranchName { get; set; }
        [Display(Name = "Mobile")]
        [Required(ErrorMessage = "Enter mobile number")]
        [StringLength(10)]
        [RegularExpression(@"^(\d{10})$", ErrorMessage = "Enter mobile no")]
        [Phone()]
        public string Mobile { get; set; }

        [Display(Name = "Email")]
        [Required(ErrorMessage = "Enter Email ID")]
        [EmailAddress]
        public string Email { get; set; }
        
        public string PAddress { get; set; }
        public bool? IsCurrentSameWithPermanent { get; set; }

        [Display(Name = "Contact Address")]
       
        [Required(ErrorMessage = "Enter contact address")]
        public string CAddress { get; set; }
        [Display(Name = "Employee Type")]
        [Required(ErrorMessage = "Select employee type")]
        public long? EmployeeTypeId { get; set; }
        public string EmployeeTypeName { get; set; }

        [Display(Name = "PAKA SALARY")]
       
        public decimal? PAKA_SALARY { get; set; }
        [Display(Name = "Kacha SALARY")]
        public decimal? KACHA_SALARY { get; set; }
        [Display(Name = "PF Amount")]
        public decimal? PF { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public long? CompanyId { get; set; }

        public decimal? LoanAmount { get; set; }
        public decimal? DueAmount { get; set; }
        public bool IsPaid { get; set; }
        public bool IsChecked { get; set; }
        
        public decimal? DeductionAmount { get; set; }
        public decimal? TotalSalary => (PAKA_SALARY+PF)-DeductionAmount;
    }

    public class SalayDisburse : Employee
    {
        public string Year { get; set; }
        public int Month { get; set; }

    }

    public class Branch
    {
        public long BranchId { get; set; }

        [Required(ErrorMessage = "Enter Branch name")]
        [StringLength(50)]
        [Display(Name = "Branch Name")]
        public string BranchName { get; set; }

        [Required(ErrorMessage = "Enter Address")]
        [Display(Name = "Adress")]
        public string Address { get; set; }

        [Display(Name = "Mobile")]
        [StringLength(10)]
        [Required(ErrorMessage = "Enter mobile number")]
        [RegularExpression(@"^(\d{10})$", ErrorMessage = "Enter mobile no")]
        [Phone()]
        public string Phone { get; set; }

        [Display(Name = "E-mail")]
        [Required(ErrorMessage = "Enter Email ID")]
        [EmailAddress]
        public string Email { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public long? CompanyId { get; set; }
    }

    public class Item
    {
        public long ItemId { get; set; }

        [Display(Name = "Category")]
        [Required(ErrorMessage = "Select a category")]
        public long CategoryId { get; set; }

        [Display(Name = "Item Name")]
        [Required(ErrorMessage = "Enter Item name")]
        public string ItemName { get; set; }

        [Display(Name = "Category Name")]
      
        public string CategoryName { get; set; }

        [Display(Name = "Unit ID")]
        [Required(ErrorMessage = "Enter Unit ID")]
        public long UnitId { get; set; }

        [Display(Name = "Unit Name")]
      
        public string UnitName { get; set; }

        [Display(Name = "GST")]
        public long? GSTId { get; set; }

        public string HSN { get; set; }

        [Display(Name = "Item description")]
        public string ItemDescription { get; set; }

        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public long? CompanyId { get; set; }

        public decimal Percentage { get; set; }
    }
    public class GST
    {
        public long GSTId { get; set; }

        [Display(Name = "HSN")]
        [Required(ErrorMessage = "Input a HSN")]
        public string HSN { get; set; }

        [Display(Name = "CGST")]
        [Required(ErrorMessage = "Select a CGST")]
        public decimal? CGST { get; set; }

        [Display(Name = "SGST")]
        [Required(ErrorMessage = "Select a SGST")]
        public decimal? IGST { get; set; }

        [Display(Name = "Percentage")]
        [Required(ErrorMessage = "Select a Percentage")]
        public decimal Percentage { get; set; }

        [Display(Name = "GST Description")]
        [Required(ErrorMessage = "Select a GST Description")]
        public string GSTDescription { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public long? CompanyId { get; set; }

        public string ddlPer { get; set; }
    }
    //
    public class Accessories
    {
        public long AccessoriesId { get; set; }
        [Display(Name = "Accessories Type")]
        public long AccessoriesTypeId { get; set; }
        [Display(Name = "Accessories Name")]
        [Required(ErrorMessage = "Enter a Accessories Name")]
        public string AccessoriesName { get; set; }
        [Display(Name = "Accessories Description")]
        public string AccessoriesDescription { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public long? CompanyId { get; set; }
        public string AccessoriesTypeName { get; set; }
    }

    public class Ledger
    {
        public long LedgerId { get; set; }

        [Display(Name = "Ledger Name")]
        [Required(ErrorMessage = "Enter Ledger Name")]
        public string LedgerName { get; set; }

        [Display(Name = "Ledger Type")]
        [Required(ErrorMessage = "Select a Ledger Type")]
        public long LedgerTypeId { get; set; }

        [Display(Name = "Ledger Description")]
        [Required(ErrorMessage = "Enter a Ledger Description")]
        public string LedgerDescription { get; set; }

        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public long? CompanyId { get; set; }

        public string LedgerTypeName { get; set; }

        
    }

    public class LedgerType
    {
        public long LedgerTypeId { get; set; }

        [Display(Name = "Ledger Type Name")]
        [Required(ErrorMessage = "Enter Ledger Type Name")]
        public string LedgerTypeName { get; set; }

        [Display(Name = "Ledger Type Description")]
        public string LedgerTypeDescription { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public long? CompanyId { get; set; }
    }

    public class Customer
    {
        public long CustomerId { get; set; }



        [Display(Name = "Point of person Name")]
        [Required(ErrorMessage = "Enter a Point of person Name")]
        public string PointOfPersonName { get; set; }

        [Display(Name = "Phone")]
        [Required(ErrorMessage = "Enter Phone Number")]
        [Phone()]
        public string Phone { get; set; }

        [Display(Name = "E-mail")]
        [Required(ErrorMessage = "Enter a E-mail Address")]
        [EmailAddress]
        public string Email { get; set; }

        [Display(Name = "Address")]
        [Required(ErrorMessage = "Enter Address")]
        public string Address { get; set; }

        [Display(Name = "Block")]
        [Required(ErrorMessage = "Enter a Block")]
        public string Block { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public long? CompanyId { get; set; }


    }

    public class Supplier
    {
        public long SupplierId { get; set; }

        [Display(Name = "Supplier Type ")]
        [Required(ErrorMessage = "Enter a Supplier Type ")]
        public long SupplierTypeId { get; set; }

        [Display(Name = "Point of person Name")]
        [Required(ErrorMessage = "Enter a Point of person Name")]
        public string PointOfPersonName { get; set; }

        [Display(Name = "Phone")]
        [Required(ErrorMessage = "Enter Phone Number")]
        [Phone()]
        public string Phone { get; set; }

        [Display(Name = "E-mail")]
        [Required(ErrorMessage = "Enter a E-mail Address")]
        [EmailAddress]
        public string Email { get; set; }

        [Display(Name = "Address")]
        [Required(ErrorMessage = "Enter Address")]
        public string Address { get; set; }

        [Display(Name = "Block")]
        [Required(ErrorMessage = "Enter a Block")]
        public string Block { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public long? CompanyId { get; set; }

        public string SupplierTypeName { get; set; }
    }

    public class SupplierType
    {
        public long SupplierTypeId { get; set; }

        [Display(Name = "Supplier Type Name")]
        [Required(ErrorMessage = "Enter a Supplier Type Name")]
        public string SupplierTypeName { get; set; }

      
        public string SupplierTypeDescription { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public long? CompanyId { get; set; }
    }

    public class Role
    {
        public long RoleId { get; set; }

        [Display(Name = "Role Name")]
        [Required(ErrorMessage = "Enter Role Name")]
        public string Name { get; set; }

      
        public string RoleDescription { get; set; }

        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public long? CompanyId { get; set; }
    }
    public class User
    {
        public long Id { get; set; }
        [Display(Name = "Full Name")]
        [Required(ErrorMessage = "Enter full Name")]
        public string FullName { get; set; }
        [Display(Name = "User Id")]
        [Required(ErrorMessage = "Enter User Id")]
        public string UserId { get; set; }
        [Display(Name = "Phone number")]
        [Required(ErrorMessage = "Enter phone number")]
        [RegularExpression(@"^(\d{10})$", ErrorMessage = "Enter mobile no")]
        [Phone()]
        public string Phone { get; set; }
        [Display(Name = "Email")]
        [Required(ErrorMessage = "Enter email")]
        [EmailAddress]
        public string Email { get; set; }
        [Display(Name = "Full address")]
        [Required(ErrorMessage = "Enter full address")]
      
        public string Address { get; set; }
        [Display(Name = "Password")]
        [Required(ErrorMessage = "Enter password")]
        public string Password { get; set; }
        [Display(Name = "Role")]
        [Required(ErrorMessage = "Enter a role")]
        public long? Role { get; set; }

        
        public string RoleName { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public long? CompanyId { get; set; }
    }

    public class LabourWorkType
    {
        public long LabourWorkTypeId { get; set; }

        [Display(Name = "Labour Work Type Name")]
        [Required(ErrorMessage = "Enter Labour Work Type")]
        public string LabourWorkTypeName { get; set; }

        [Display(Name = "Per Bag Rate")]
        [Required(ErrorMessage = "Enter Per Bag Rate")]
        public decimal PerBagrRate { get; set; }

        [Display(Name = "Labour Work Type Description")]
        [Required(ErrorMessage = "Enter Labour Work Type Description")]
        public string LabourWorkTypeDescription { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public long? CompanyId { get; set; }
    }

    public class Machine
    {
        public long MachineId { get; set; }

        [Display(Name = "Machine Name")]
        [Required(ErrorMessage = "Enter Machine Name")]
        public string MachineName { get; set; }

        [Display(Name = "Quantity")]
        [Required(ErrorMessage = "Enter Quantity")]
        public int Quantity { get; set; }

        [Display(Name = "Unit")]
        [Required(ErrorMessage = "Select a unit ")]
        public long UnitId { get; set; }

        public string UnitName { get; set; }

        [Display(Name = "Cost Of Machineries")]
        [Required(ErrorMessage = "Enter Cost Of Machineries")]
        public decimal CostOfMachineries { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public long? CompanyId { get; set; }
    }


    public class Bag
    {
        public long BagId { get; set; }

        [Display(Name = "Bag Name")]
        [Required(ErrorMessage = "Enter a Bag Name")]
        public string BagName { get; set; }

        [Display(Name = "Bag Description")]
        [Required(ErrorMessage = "Enter a Bag Description")]
        public string BagDescription { get; set; }

        [Display(Name = "Net weight")]
        [Required(ErrorMessage = "Enter a Net weight")]
        public decimal NetWeight { get; set; } // net weight

        [Display(Name = "Default Weight")]
        [Required(ErrorMessage = "Enter a Default Weight")]
        public decimal PPWeight { get; set; } // default weight
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public long? CompanyId { get; set; }
    }
    public class Alert
    {
        public long AlertId { get; set; }

        [Display(Name = "Alert Name")]
        [Required(ErrorMessage = "Enter a Alert Name")]
        public string AlertName { get; set; }

        [Display(Name = "Alert Description")]
        [Required(ErrorMessage = "Enter a Alert Description")]
        public string AlertDescription { get; set; }

        [Display(Name = "Issue Date")]
        [Required(ErrorMessage = "Enter a Issue Date")]
        public DateTime? IssueDate { get; set; }

        public string StrIssueDate { get; set; }



        [Display(Name = "Next Renewal Date")]
        [Required(ErrorMessage = "Enter a Next Renewal Date")]
        public DateTime? NextRenewalDate { get; set; }

        public string StrNextRenewalDate { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public long? CompanyId { get; set; }
    }

    public class Company
    {
        public long Id { get; set; }

        [Display(Name = "Company Name")]
        public string CompanyName { get; set; }

        [Display(Name = "Address")]
        public string Address { get; set; }

        [Display(Name = "Land Mark")]
        public string LandMark { get; set; }

        [Display(Name = "About Us")]
        public string AboutUs { get; set; }

        [Display(Name = "Contact Us")]
        public string ContactUs { get; set; }

        [Display(Name = "Mobile Number")]
        [RegularExpression(@"^(\d{10})$", ErrorMessage = "Enter mobile no")]
        public string Mobile { get; set; }

        [Display(Name = "Email Address")]
        public string Email { get; set; }

        [Display(Name = "GST Number")]
        public string GST { get; set; }

        [Display(Name = "Registration Details")]
        public string RegistrationDetails { get; set; }

        [Display(Name = "VAT Number")]
        public string VATNo { get; set; }

        [Display(Name = "CST Number")]
        public string CSTNo { get; set; }

        [Display(Name = "PAN Number")]
        public string PAN { get; set; }
        
    }

    public class WeightDetails
    {

        public long TransactionId { get; set; }

        [Display(Name = "Customer Name")]
        [Required(ErrorMessage = "Enter  Customer Name")]
        public string CustomerName { get; set; }

        [Display(Name = "Vehicle Number")]
        [Required(ErrorMessage = "Enter  Vehicle Number")]
        public string VehicleNo { get; set; }

        

        [Display(Name = "Product")]
        [Required(ErrorMessage = "Enter  Product")]
        public string Product { get; set; }


        [Display(Name = "Misc 1st Weight")]
        [Required(ErrorMessage = "Enter Misc 1st Weight")]
        public string Misc_1st_weight { get; set; }

        [Display(Name = "Misc 2nd Weight")]
        [Required(ErrorMessage = "Enter Misc 2nd Weight")]
        public string Misc_2nd_weight { get; set; }

        [Display(Name = "First Weight Date")]
        [Required(ErrorMessage = "Enter First Weight Date")]
        public DateTime? Date_First_Weight { get; set; }

        [Display(Name = "First Weight Time")]
        [Required(ErrorMessage = "Enter First Weight Time")]
        public string First_Weight_Time { get; set; }


        [Display(Name = "Second Weight Date")]
        [Required(ErrorMessage = "Enter Second Weight Date")]
        public DateTime? Date_Second_Weight { get; set; }

        [Display(Name = "Second Weight Time")]
        [Required(ErrorMessage = "Enter Second Weight Time")]
        public string Second_Weight_Time { get; set; }


        [Display(Name = "First Weight")]
        [Required(ErrorMessage = "Enter First Weight")]
        public decimal? First_weight { get; set; }

        [Display(Name = "Second Weight")]
        [Required(ErrorMessage = "Enter Second Weight")]
        public decimal? Second_weight { get; set; }
        public long? UnitId { get; set; }

        [Display(Name = "Net Weight")]
        [Required(ErrorMessage = "Enter  Net Weight")]
        public decimal? NetWeight { get; set; }

        [Display(Name = "Weight Charges")]
        [Required(ErrorMessage = "Enter Weight Charges")]
        public decimal? WeightCharges { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public long? CompanyId { get; set; }
        public string UnitName { get; set; }
        public string str_Date_First_Weight { get; set; }
        public string str_Date_Second_Weight { get; set; }

    }

    public class Documents
    {
        public long DocumentId { get; set; }
        public string DocumentName { get; set; }
        public string DocumentPath { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public long? CompanyId { get; set; }
    }
    public class ApplicationLog
    {
        public Guid LOG_ID { get; set; }
        public DateTime LOG_TIME { get; set; }
        public string LOG_TYPE { get; set; }
        public string USER_NAME { get; set; }
        public string LOG_CLASS { get; set; }
        public string METHOD_NAME { get; set; }
        public string LOG_MESSAGE { get; set; }
        public string ERROR_MESSAGE { get; set; }
        public string STACK_TRACE { get; set; }
        public string Str_LOG_TIME { get; set; }
    }

    public class IncomeExpense
    {
        public int? IncomeExpenseId { get; set; }
        public int? LedgerOperationId { get; set; }
        public decimal? Amount { get; set; }
        public int? TransactionId { get; set; }
        public DateTime? Date { get; set; }
    }

    public class LedgerOperation
    {
        public int? LedgerOperationId { get; set; }
        public int? LedgerId { get; set; }
        public string OperationName { get; set; }
        public string OperationDescription { get; set; }
    }
    public class OperationMaster
    {
        public int OperationID { get; set; }
        public string OperationName { get; set; }
        public string OperationDesc { get; set; }
        [Display(Name = "Ledger")]
        public int? LedgerID { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
    }
    public class OperationValue
    {
        public int OperationId { get; set; }
        public decimal OperationAmount { get; set; }
        public int LedgerId { get; set; }
    }
    public class TransactionDetails
    {
        public long TransactionDetailsID { get; set; }
        public long TransactionID { get; set; }
        public long? OperationID { get; set; }
        public long? LedgerID { get; set; }
        public decimal? Ammount { get; set; }
        public DateTime TransactionDate { get; set; }
        public string Partyname { get; set; }
        public string strTransactionDate => TransactionDate.ToString("MM/dd/yyyy");
        public string LedgerName { get; set; }
        public string ChequeNo { get; set; }
    }

    public class TransactionMaster
    {
        public long TransactionID { get; set; }
        public string IFSC { get; set; }
        public string ChequeNo { get; set; }
        public string DraftNo { get; set; }
        public string BankName { get; set; }
        public string Partyname { get; set; }
        public decimal? Amount { get; set; }
        public DateTime? TransactionDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
    }


    public class OperationByLedger
    {
    public List<OperationMaster> operationMasters { get; set; }
    public List<TransactionDetails> transactionDetails { get; set; }
    }

    public class Assets
    {
        public int AssetsId { get; set; }
        public string SerialNumber { get; set; }
        public string DeedNumber { get; set; }
        public DateTime? Date { get; set; }
        public string PlotNumber { get; set; }
        public string Moja { get; set; }
        public string AreaOfLand { get; set; }
        public string ValueOfLand { get; set; }
        public string Remarks { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public long? CompanyId { get; set; }
    }

    public class StockMaster
    {
        [Display(Name = "Accessories Type")]
        [Required(ErrorMessage = "Select Accessories Type")]
        public int AccessoriesTypeId { get; set; }
        [Display(Name = "Accessories")]
        [Required(ErrorMessage = "Select Accessories")]
        public int AccessoriesId { get; set; }
        [Display(Name = "Quantity")]
        [Required(ErrorMessage = "Enter Quantity")]
        public decimal? AvailableQuantity { get; set; }
        public string Remarks { get; set; }
        public string AccessoriesName { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public string Type { get; set; }
    }

    
        public class LoanManagement
        {
            public int Id { get; set; }
            public int? BranchId { get; set; }
            public int? EmployeeId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Fullname => string.Format("{0} {1}", FirstName, LastName);
        public DateTime Date { get; set; }
        public string strDate => Date.ToString("MM/dd/yyyy");
            public decimal Amount { get; set; }
        public decimal PaidAmount { get; set; }
        public decimal TotalLoan { get; set; }
        public decimal DueLoan => (TotalLoan - PaidAmount);

    }

    public class EmployeeSalary
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Fullname => string.Format("{0} {1}", FirstName, LastName);
        public int Id { get; set; }
        [Required(ErrorMessage = "Select year")]
        public string Year { get; set; }
        [Required(ErrorMessage = "Select month")]
        public int Month { get; set; }
        [Display(Name = "Employee")]
        [Required(ErrorMessage = "Select Employee")]
        public int EmployeeId { get; set; }
        public bool? IsPaid { get; set; }
        [Display(Name = "Paka Salary")]
        public decimal? Paka_Salary { get; set; }
        [Display(Name = "Kacha Salary")]
        public decimal? Kacha_Salary { get; set; }
        [Display(Name = "Due Amount")]
        public decimal? DueAmount { get; set; }
        
        [Display(Name = "Loan Amount")]
        public decimal? LoanAmount { get; set; }
        [Display(Name = "Loan Deduction")]
        public decimal? LoanDeduction { get; set; }
        public decimal? PF { get; set; }
        public decimal? Bonus { get; set; }
        [Display(Name = "Bonus Remarks")]
        public string BonusRemarks { get; set; }

        public decimal? Deduction { get; set; }
        [Display(Name = "Deduction Remarks")]
        public string DeductionRemarks { get; set; }
        [Display(Name = "Gross Salary")]
        public string Gross { get; set; }
        public DateTime Date { get; set; }
        public string MonthName { get; set; }
    }

    public class ItemCustomerSalesStockTransaction
    {
        public int Id { get; set; }
        public int? CustomerId { get; set; }
        public int PacketId { get; set; }
        public decimal BostaQuantity { get; set; }
        public decimal Weight { get; set; }
        public decimal Price { get; set; }
        public int? TransactionId { get; set; }
        public DateTime? Date { get; set; }
        public string Description { get; set; }
        public string Unit { get; set; }
    }

    public class ItemProductionStock
    {
        public int ProductionStockId { get; set; }
        public int PacketId { get; set; }
        public decimal TotalWeight { get; set; }
        public int BostaQuantity { get; set; }
        public DateTime UpdatedOn { get; set; }
        public string Description { get; set; }
        public string Unit { get; set; }
    }

    public class ItemProductionStockTransaction
    {
        public int Id { get; set; }
        public int PacketId { get; set; }
        public decimal BostaQuantity { get; set; }
        public decimal Weight { get; set; }
        public DateTime? Date { get; set; }
        public string Description { get; set; }
        public string Unit { get; set; }
    }

    public class ItemPurchaseStockTransaction : ItemPurchaseStock
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        [Display(Name ="Packet")]
        public new int PacketId { get; set; }
       // public new decimal BostaQuantity { get; set; }
        public decimal Price { get; set; }
        public int? TransactionId { get; set; }
        public DateTime? Date { get; set; }
        //public new string Unit { get; set; }
        //public new string Description { get; set; }
    }

    public class ItemSalesStock
    {
        public int SalesStockId { get; set; }
        public int PacketId { get; set; }
        public decimal TotalWeight { get; set; }
        public int BostaQuantity { get; set; }
        public DateTime UpdatedOn { get; set; }
        public string Description { get; set; }
        public string Unit { get; set; }
    }

    public class ItemSalesStockTransaction
    {
        public int Id { get; set; }
        public int PacketId { get; set; }
        public decimal BostaQuantity { get; set; }
        public decimal Weight { get; set; }
        public DateTime? Date { get; set; }
        public string Description { get; set; }
        public string Unit { get; set; }
    }
    public class ItemPurchaseStock : Packet
    {
        public int PurchaseStockId { get; set; }
        public int PacketId { get; set; }
        public decimal TotalWeight { get; set; }
        public int BostaQuantity { get; set; }
        public DateTime UpdatedOn { get; set; }
    }
    public class Packet
    {
        public int     PacketId { get; set; }
        public string  Description { get; set; }
        public decimal BagQuantity { get; set; }
        public decimal Weight { get; set; }
        public string  Unit { get; set; }
    }

    public class LoanStatus
    {
        public decimal LaonAmount { get; set; }
        public decimal DueAmount { get; set; }

    }

    public class LedgerTransactionMaster : Ledger
    {
        public long TransactionID { get; set; }
        public string PaymentMode { get; set; }
        public string IFSC { get; set; }
        public string ChequeNo { get; set; }
        public string DraftNo { get; set; }
        public string BankName { get; set; }
        public decimal? Amount { get; set; }
        public DateTime? TransactionDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
    }

    public class RoadChalan
    {
        public int Id { get; set; }
        [Display(Name = "Name Of Distributor")]
        [Required(ErrorMessage = "Enter name of Distributor")]
        public string NameOfDistributor { get; set; }
        [Display(Name = "Address Of Distributor")]
        [Required(ErrorMessage = "Enter address of Distributor")]
        public string AddressOfDistributor { get; set; }
        [Required(ErrorMessage = "Enter destination")]
        public string Destination { get; set; }
        [Required(ErrorMessage = "Enter quantity")]
        [Display(Name = "Quantity of PDS Atta")]
        public string Quantity { get; set; }
        [Display(Name = "Truck number")]
        [Required(ErrorMessage = "Enter truck number")]
        public string TruckNo { get; set; }
        public DateTime? Date { get; set; }
        public bool? IsDeleted { get; set; }

        [Display(Name = "Vehicle Number")]
        //[Required(ErrorMessage = "Enter Vehicle number")]
        public string VehicleNumber { get; set; }

        [Display(Name = "Product")]
        [Required(ErrorMessage = "Enter Product")]
        public string Product { get; set; }

        [Display(Name = "Total Weight")]
        [Required(ErrorMessage = "Enter weight")]
        public string TotalWeight { get; set; }

        [Display(Name = "Driver Name")]
        //[Required(ErrorMessage = "Enter truck number")]
        public string DriverName { get; set; }

        [Display(Name = "Driver Mobile")]
        //[Required(ErrorMessage = "Enter truck number")]
        public string DriverMobile { get; set; }

       
    }

    public class AccessoriesType
    {
        public long AccessoriesTypeId { get; set; }
        [Display(Name = "Accessories Type Name")]
        [Required(ErrorMessage = "Enter Accessories Type Name")]
        public string AccessoriesTypeName { get; set; }
        public string Description { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? Date { get; set; }
    }


    //public class PurchaseEntry
    //{
    //    public long Id { get; set; }
    //    public long ItemTypeId { get; set; }
    //    public long NoOfBosta { get; set; }
    //    public decimal Weight { get; set; }
    //    public decimal Price { get; set; }
    //    public DateTime PurchaseDate { get; set; }
    //}

   

    public class PurchaseEntryMaster
    {
        public long Id { get; set; }
        public long ItemTypeId { get; set; }
        public long NoOfBosta { get; set; }
        public decimal Weight { get; set; }
        public DateTime PurchaseDate { get; set; }
    }

    public class PurchaseStock
    {
        public long Id { get; set; }
        public long ItemTypeId { get; set; }
        public decimal Weight { get; set; }
        public DateTime StockDate { get; set; }
        public bool? IsProductionEntry { get; set; }
    }

    public class PurchaseStockWastage
    {
        public long Id { get; set; }
        public long ItemTypeId { get; set; }
        public decimal Weight { get; set; }
        public DateTime StockDate { get; set; }
    }


    #region sale and purchase
    public class CategoryMaster
    {
        public long Id { get; set; }
        public string CategoryType { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
    }

    public class ItemMaster
    {
        public long Id { get; set; }
        [Display(Name = "Category")]
        [Required(ErrorMessage = "Select a category")]
        public long CategoryId { get; set; }
        [Display(Name = "Item Name")]
        [Required(ErrorMessage = "Enter item name")]
        public string ItemName { get; set; }
        [Required(ErrorMessage = "Enter unit")]
        public string Unit { get; set; }
        public string GST { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public string CategoryName { get; set; }
        //public string DropdownValue { get; set; }
        public string DropdownValue => string.Format("{0} ({1})", ItemName, Unit);
    }

    public class PurchaseEntry : PurchaseTransactionDetails
    {
        public string PurchaseId { get; set; }
        public DateTime PurchaseDate { get; set; }
        [RequiredIf(nameof(SupplierTypeId))]
        public string DONumber { get; set; }
        [RequiredIf(nameof(SupplierTypeId))]
        public string ChallanNumber { get; set; }
        [Display(Name = "Supplier ")]
        [Required(ErrorMessage = "Select supplier ")]
        public long? SupplierId { get; set; }
        [Required(ErrorMessage = "Select supplier type ")]
        public long SupplierTypeId { get; set; }
        public string TruckNumber { get; set; }
        [Display(Name = "Item ")]
        [Required(ErrorMessage = "Select item ")]
        public long ItemType { get; set; }
        [Required(ErrorMessage = "Number of Bags ")]
        public long? NumberOfBag { get; set; }
        [Required(ErrorMessage = "Weight")]
        public decimal? Weight { get; set; }
        public string Unit { get; set; }
        [RequiredIf(nameof(SupplierTypeId))]
       //[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:#.#}")]
        [Display(Name = "Total")]
        public decimal? Total { get; set; }
        [RequiredIf(nameof(SupplierTypeId))]
        public string PaymentMode { get; set; }
        [RequiredIf(nameof(PaymentMode))]
        public string ChequeNumber { get; set; }
        public string ItemName { get; set; }
        public string Date { get; set; }
    }

    public class PurchaseTransactionDetails
    {
        public string TransactionId { get; set; }
        //public string PurchaseId { get; set; }
        public DateTime TransactionDate { get; set; }
       
        public string BankName { get; set; }
        //public string ChequeNumber { get; set; }
        public string DraftNumber { get; set; }
       
    }

    public class ProdEntry
    {
        public string ProductionID { get; set; }
        [Display(Name = "Item ")]
        [Required(ErrorMessage = "Select item ")]
        public long? ItemId { get; set; }
        [Required(ErrorMessage = "Number of Bags ")]
        public long? NumberOfBags { get; set; }
        [Required(ErrorMessage = "Weight")]
        public decimal? Weight { get; set; }
        public string Units { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string Date { get; set; }
        public string ItemName { get; set; }
    }

    public class FlourBranRefStock
    {
        public long ItemTypeId { get; set; }
        public decimal? Weight { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public string ItemName { get; set; }
    }

    public class ProdEntryMaster
    {
        public long Id { get; set; }
        public long ItemTypeId { get; set; }
        public long NoOfBosta { get; set; }
        public decimal Weight { get; set; }
        public DateTime EntryDate { get; set; }
    }

    public class ProductionStockEntry
    {
        public string ProductionStockId { get; set; }
        public DateTime EntryDate { get; set; }
        [Display(Name = "Item")]
        public long? ItemId { get; set; }
        [Display(Name = "Used quantity for production")]
        public decimal? Weight { get; set; }
        [Display(Name = "Flour(ATTA) in Quintal")]
        [Required(ErrorMessage = "Select item ")]
        public decimal? Flour { get; set; }
        [Display(Name = "Bran in Quintal")]
        [Required(ErrorMessage = "Select item ")]
        public decimal? Bran { get; set; }
        [Display(Name = "Ref in Quintal")]
        [Required(ErrorMessage = "Select item ")]
        public decimal? Ref { get; set; }
        [Display(Name = "Vitamin in KG")]
        [Required(ErrorMessage = "Select item ")]
        public decimal? Vitamin { get; set; }
        public string Unit { get; set; }
        public string ItemName { get; set; }
        public string Date { get; set; }
    }

    public class Sales : SaleTransactionDetails
    {
        public string SalesID { get; set; }
        [Display(Name = "Invoice Number")]
        [Required(ErrorMessage = "Enter invoice number")]
        public string InvoiceNumber { get; set; }
        [Display(Name = "Memo Number")]
        [Required(ErrorMessage = "Enter memo number")]
        public string MemoNumber { get; set; }
        [Display(Name = "Dispath Doc Number")]
        [Required(ErrorMessage = "Enter dispath doc number")]
        public string DispathDocNumber { get; set; }
        [Display(Name = "Customer")]
        [Required(ErrorMessage = "Select a customer")]
        public long? CustomerId { get; set; }
        [Display(Name = "Truck Number")]
        [Required(ErrorMessage = "Enter truck number")]
        public string TruckNumber { get; set; }
        [Display(Name = "Destination")]
        [Required(ErrorMessage = "Enter Destination")]
        public string Destination { get; set; }
        [Display(Name = "Batch Number")]
        [Required(ErrorMessage = "Enter batch number")]
        public string BatchNumber { get; set; }
        [Display(Name = "Sale Item")]
        [Required(ErrorMessage = "Select item ")]
        public long? ItemId { get; set; }
        [Display(Name = "Packet")]
        [Required(ErrorMessage = "Select packet ")]
        public long? PacketId { get; set; }
        [Display(Name = "No of packet")]
        [Required(ErrorMessage = "Enter no of packet")]
        public decimal? NoOfPacket { get; set; }
        [Display(Name = "No of Bag")]
        [Required(ErrorMessage = "Enter no of bag")]
        public decimal? NoOfBag { get; set; }
        [Display(Name = "Total Weight")]
        [Required(ErrorMessage = "Enter total weight")]
        public decimal? TotalWeight { get; set; }
        public string Units { get; set; }
        [Display(Name = "Date")]
        [Required(ErrorMessage = "Enter Date")]
        public DateTime? CreatedDate { get; set; }
        public string ItemName { get; set; }
        public string Date { get; set; }
        public string Description { get; set; }
    }


    public class SaleTransactionDetails
    {
        public string TransactionId { get; set; }
        public decimal? Total { get; set; }
        public DateTime? TransactionDate { get; set; }
        public string PaymentMode { get; set; }
        public string BankName { get; set; }
        public string ChequeNumber { get; set; }
        public string DraftNumber { get; set; }
        public long? CategoryId { get; set; }

    }

    public class Wastage
    {
        public long Id { get; set; }
        public string WastageId { get; set; }
        [Display(Name = "Item")]
        [Required(ErrorMessage = "Select intem")]
        public long ItemTypeId { get; set; }
        [Display(Name = "Weight")]
        [Required(ErrorMessage = "Enter weight")]
        public decimal Weight { get; set; }
        public DateTime StockDate { get; set; }
        public string ItemName { get; set; }
        public string Date { get; set; }
    }






    #endregion





}





﻿CREATE PROCEDURE  [dbo].[sp_DeleteSelectOperation]
 @LedgerId [bigint] NULL
,@LedgerOperationId [bigint] NULL
,@OperationId [bigint]  --1=delete,2=Select
,@UserId [bigint] NULL
AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
IF(@OperationId  = 1 AND @LedgerId >0)
BEGIN
    UPDATE tbl_OperationMaster
    SET
    IsDeleted = 1
   ,UpdatedBy = @UserId
   ,UpdatedOn = GETDATE()
    WHERE LedgerId = @LedgerId AND OperationID = @LedgerOperationId
SELECT 'Operation deleted successfully.' as Msg,1 as SuccessFlag
END
ELSE
BEGIN
SELECT OP.*,TLT.LedgerName
FROM  tbl_OperationMaster OP WITH(NOLOCK)
INNER JOIN tbl_Ledger TLT ON TLT.LedgerId = OP.LedgerID
--WHERE OP.OperationID = ISNULL(@OperationID,OperationID)
WHERE OP.LedgerID = @LedgerId
END

END TRY
BEGIN CATCH
SELECT 'Sorry something went wrong, unable to process,' as Msg,0 as SuccessFlag
RETURN
END CATCH
END
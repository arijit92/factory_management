﻿CREATE TABLE [dbo].[tblPurchaseStock] (
    [Id]                BIGINT          IDENTITY (1, 1) NOT NULL,
    [ItemTypeId]        BIGINT          NOT NULL,
    [Weight]            DECIMAL (18, 2) NOT NULL,
    [StockDate]         DATETIME        NOT NULL,
    [IsProductionEntry] BIT             NULL,
    CONSTRAINT [PK_tblPurchaseStock] PRIMARY KEY CLUSTERED ([Id] ASC)
);


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FMS.BL;
using FMS.BL.Buisness;
using FMS.BL.Utilities;
namespace FMS.Controllers
{
    public class LedgerController : Controller
    {
        // GET: Role  
        public ActionResult List()
        {
            ViewBag.ParentMenu = "LedgerMaster";
            ViewBag.ChildMenu = "ledger";
            return View();
        }

        public ActionResult LedgerOperation()
        {
            ViewBag.ParentMenu = "LedgerMaster";
            ViewBag.ChildMenu = "LedgerOperation";
            List<Ledger> ledgers = new List<Ledger>();
            LedgerBuisness ledgerBuisness = new LedgerBuisness();
            try
            {
                ledgers = ledgerBuisness.GetLedger(0, 1);
                ViewBag.Ledger = new SelectList(ledgers, "LedgerId", "LedgerName", 0);
            }
            catch(Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to add LedgerOperation", ex, "LedgerOperation/Get");
            }

            return View();
        }
        [HttpPost]
        public ActionResult LedgerOperation(OperationMaster operationMaster)
        {
            ViewBag.ParentMenu = "LedgerMaster";
            ViewBag.ChildMenu = "LedgerOperation";
            List<Ledger> ledgers = new List<Ledger>();
            LedgerBuisness ledgerBuisness = new LedgerBuisness();
            Status status = new Status();
            long userId = 1;
            try
            {
                ledgers = ledgerBuisness.GetLedger(0, 1);
                ViewBag.Ledger = new SelectList(ledgers, "LedgerId", "LedgerName", 0);
                status = ledgerBuisness.AddUpdateOperationMaster(operationMaster, userId);
                TempData["SuccessFlag"] = status.SuccessFlag;
                TempData["Msg"] = status.Msg;
                return View(operationMaster);
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to add LedgerOperation", ex, "LedgerOperation/Post");
            }
            return View(operationMaster);
        }

        [HttpGet]
        public ActionResult Add(int? ledgerid)
        {
            ViewBag.ParentMenu = "LedgerMaster";
            ViewBag.ChildMenu = "ledger";
            List<LedgerType> ledgersType = null;
            Ledger ledger = null;
            LedgerBuisness ledgerBuisness = null;
            LedgerTypeBuisness ledgerTypeBuisness = null;
            long userid = 11;
            try
            {
                ledgersType = new List<LedgerType>();
                ledger = new Ledger();
                ledgerBuisness = new LedgerBuisness();
                ledgerTypeBuisness = new LedgerTypeBuisness();
                if (ledgerid > 0)
                {
                    ledger = ledgerBuisness.GetLedger(ledgerid, userid).SingleOrDefault();
                }
                ledgersType = ledgerTypeBuisness.GetLedgerType(0, 11).Where(i => i.IsActive == true && i.IsDeleted == false).ToList();
                ViewBag.LedgerType = new SelectList(ledgersType, "LedgerTypeId", "LedgerTypeName", 0);
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Ledger list", ex, "Ledger");
            }
            return View(ledger);
        }
        [HttpPost]
        public ActionResult Add(Ledger ledger)
        {
            ViewBag.ParentMenu = "LedgerMaster";
            ViewBag.ChildMenu = "ledger";
            List<LedgerType> ledgerType = null;
            LedgerTypeBuisness ledgerTypeBuisness = null;
            ledgerType = new List<LedgerType>();
            ledgerTypeBuisness = new LedgerTypeBuisness();
            ledgerType = ledgerTypeBuisness.GetLedgerType(0, 11).Where(i => i.IsActive == true && i.IsDeleted == false).ToList();
            ViewBag.LedgerType = new SelectList(ledgerType, "LedgerTypeId", "LedgerTypeName", 0);
            if (ModelState.IsValid)
            {
                LedgerBuisness buisness = new LedgerBuisness();
                Status status = new Status();
                status = buisness.AddUpdateLedger(ledger, 1);
                TempData["SuccessFlag"] = status.SuccessFlag;
                TempData["Msg"] = status.Msg;
                if (status.SuccessFlag == "1")
                {
                    return RedirectToAction("List");
                }
                else
                {
                    return View(ledger);
                }
            }
            return View(ledger);
        }


        [HttpPost]
        public JsonResult LoadLedger(int LedgerId)
        {
            ViewBag.ParentMenu = "LedgerMaster";
            ViewBag.ChildMenu = "ledger";
            long userid = 11;
            List<Ledger> ledger= null;
            LedgerBuisness ledgerBuisness = null;
            try
            {
                ledger = new List<Ledger>();
                ledgerBuisness = new LedgerBuisness();
                ledger = ledgerBuisness.GetLedger(LedgerId, userid).Where(i => i.IsDeleted == false).ToList();
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Ledger list", ex, "Ledger");
            }

            return Json(ledger, JsonRequestBehavior.AllowGet);
        }



        
        [HttpPost]
        public JsonResult LoadOperation(int LedgerId)
        {
            ViewBag.ParentMenu = "LedgerMaster";
            ViewBag.ChildMenu = "ledger";
            long userid = 11;
            List<OperationMaster> operationMasters = null;
            LedgerBuisness ledgerBuisness = null;
            try
            {
                operationMasters = new List<OperationMaster>();
                ledgerBuisness = new LedgerBuisness();
                operationMasters = ledgerBuisness.GetOperation(LedgerId, userid).Where(i => i.IsDeleted == false).ToList();
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Ledger LoadOperation", ex, "LoadOperation");
            }

            return Json(operationMasters, JsonRequestBehavior.AllowGet);
        }

       

        public ActionResult Delete(int ledgerid)
        {
            ViewBag.ParentMenu = "LedgerMaster";
            ViewBag.ChildMenu = "ledger";
            LedgerBuisness ledgerBuisness = null;
            Status status = null;
            long userid = 11;
            try
            {
                ledgerBuisness = new LedgerBuisness();
                status = new Status();
                status = ledgerBuisness.DeleteLedger(ledgerid, userid);
                TempData["SuccessFlag"] = status.SuccessFlag;
                TempData["Msg"] = status.Msg;
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to delete Ledger ", ex, "Ledger");
            }
            return RedirectToAction("List");
        }

        


    }
}
﻿CREATE PROCEDURE  [dbo].[sp_InsertUpdateRole]
-- Add the parameters for the stored procedure here
 @RoleId [bigint]
,@RoleName [NVARCHAR](MAX)
,@RoleDescription [NVARCHAR](MAX)
,@IsActive [int] NULL
,@CreatedBy [bigint] NULL
,@CompanyId [bigint] NULL
AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
--update or insert checking
IF(@RoleId>0)
BEGIN
--Duplicate value check
IF EXISTS(SELECT * FROM tbl_Role WITH(NOLOCK) WHERE Name = @RoleName AND RoleId != @RoleId AND IsDeleted = 0)
BEGIN
SELECT 'Already exists.' as Msg,0 as SuccessFlag
END
ELSE
BEGIN
--update
UPDATE tbl_Role
SET
Name =UPPER(@RoleName)
,RoleDescription = @RoleDescription
,IsActive =@IsActive
,UpdatedBy =@CreatedBy
,UpdatedOn      =GETDATE()
,CompanyId =@CompanyId
WHERE RoleId =@RoleId
SELECT 'Role updated successfully.' as Msg,1 as SuccessFlag
END
END
ELSE
BEGIN
--Duplicate value check
IF EXISTS (SELECT * FROM tbl_Role WITH(NOLOCK) WHERE Name = @RoleName AND IsDeleted = 0)
BEGIN
SELECT 'Already exists.' as Msg,0 as SuccessFlag
END
ELSE
BEGIN
--insert
INSERT INTO tbl_Role
(
 Name
,RoleDescription
,IsActive
,IsDeleted
,CreatedBy
,CreatedOn
,CompanyId
)
VALUES
(
UPPER(@RoleName)
,@RoleDescription
,@IsActive
,0
,@CreatedBy
,GETDATE()
,@CompanyId
)
SELECT 'Role created successfully.' as Msg,1 as SuccessFlag
END
END
END TRY
BEGIN CATCH
SELECT 'Sorry something went wrong, unable to process,' as Msg,0 as SuccessFlag
RETURN
END CATCH
END
﻿CREATE PROCEDURE  [dbo].[sp_DeleteSelectEmployee]
-- Add the parameters for the stored procedure here
 @EmployeeId [bigint] NULL
,@OperationId [bigint]  --1=delete,2=Select
,@UserId [bigint] NULL

AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
IF(@OperationId  = 1 AND @EmployeeId >0)
BEGIN
UPDATE tbl_Employee
SET
    IsDeleted = 1
   ,UpdatedBy = @UserId
   ,UpdatedOn = GETDATE()
    WHERE EmployeeId = @EmployeeId
SELECT 'Employee deleted successfully.' as Msg,1 as SuccessFlag
END
ELSE
BEGIN
SELECT 
TE.*
,TB.BranchName 
,TET.EmployeeTypeName
FROM tbl_Employee TE WITH(NOLOCK) LEFT JOIN tbl_Branch TB WITH(NOLOCK) on TB.BranchId = TE.BranchId 
RIGHT JOIN tbl_EmployeeType TET WITH(NOLOCK) ON TET.EmployeeTypeId = TE.EmployeeTypeId
WHERE TE.EmployeeId = ISNULL(@EmployeeId,TE.EmployeeId)
END

END TRY
BEGIN CATCH
SELECT ERROR_MESSAGE() as Msg,0 as SuccessFlag
RETURN
END CATCH
END
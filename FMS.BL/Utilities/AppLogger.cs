﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;
using System.Runtime.CompilerServices;
using System.Reflection;

namespace FMS.BL.Utilities
{
    public sealed class AppLogger
    {
        public static readonly AppLogger Instance = new AppLogger();

        NLog.Logger _LoggerDB;
        LogEventInfo logEvent;
        const string LoggerName = "databaseLogger";
        bool DBLogging = false;

        public AppLogger()
        {
            _LoggerDB = LogManager.GetLogger(LoggerName);
        }

        public void Debug(Type Source, string message, [CallerMemberName] string methodName = "")
        {

            //  string userName = sessionHandler.GetSessionString("UserName");
            string userName = "11";
                logEvent = LogEventInfo.Create(LogLevel.Debug, LoggerName, message);
                logEvent.Properties["classname"] = Source.ToString();
                logEvent.Properties["methodname"] = methodName;
                logEvent.Properties["username"] = userName;
                _LoggerDB.Log(logEvent);
            
        }

        public void Info(Type Source, string message, [CallerMemberName] string methodName = "")
        {

            //  string userName = sessionHandler.GetSessionString("UserName");
            string userName = "11";

            logEvent = LogEventInfo.Create(LogLevel.Info, LoggerName, message);
                logEvent.Properties["classname"] = Source.ToString();
                logEvent.Properties["methodname"] = methodName;
                logEvent.Properties["username"] = userName;
                _LoggerDB.Log(logEvent);
            
        }

        public void Warn(Type Source, string message, [CallerMemberName] string methodName = "")
        {
            //  string userName = sessionHandler.GetSessionString("UserName");
            string userName = "11";

            logEvent = LogEventInfo.Create(LogLevel.Warn, LoggerName, message);
                logEvent.Properties["classname"] = Source.ToString();
                logEvent.Properties["methodname"] = methodName;
                logEvent.Properties["username"] = userName;
                _LoggerDB.Log(logEvent);
            
        }

        public void Info(Type type, string v, MethodBase methodBase)
        {
            throw new NotImplementedException();
        }

        public void Error(Type Source, string message, Exception ex, [CallerMemberName] string methodName = "")
        {
            try
            {
                //  string userName = sessionHandler.GetSessionString("UserName");
                string userName = "11";
                logEvent = LogEventInfo.Create(LogLevel.Error, LoggerName, message);
                    logEvent.Properties["classname"] = Source.ToString();
                    logEvent.Properties["methodname"] = methodName;
                    logEvent.Properties["username"] = userName;
                    logEvent.Properties["stacktrace"] = ex.StackTrace.ToString();
                    logEvent.Exception = ex;
                    _LoggerDB.Log(logEvent);
                
            }
            catch (Exception b)
            {
                string st = b.ToString();
            }
        }

        public void Fatal(Type Source, string message, [CallerMemberName] string methodName = "")
        {
            //  string userName = sessionHandler.GetSessionString("UserName");
            string userName = "11";
            logEvent = LogEventInfo.Create(LogLevel.Fatal, LoggerName, message);
                logEvent.Properties["classname"] = Source.ToString();
                logEvent.Properties["methodname"] = methodName;
                logEvent.Properties["username"] = userName;
                _LoggerDB.Log(logEvent);
            
        }
    }
}

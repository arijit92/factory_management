﻿CREATE PROCEDURE  [dbo].[sp_GetAvailableStock]
-- Add the parameters for the stored procedure here
@AccessoriesId [bigint]
AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY



BEGIN
SELECT tbl_Accessories.AccessoriesId,tbl_Accessories.AccessoriesName,tbl_StockMaster.AvailableQuantity FROM  tbl_StockMaster  INNER JOIN tbl_Accessories on tbl_StockMaster.AccessoriesId = tbl_Accessories.AccessoriesId
WHERE tbl_StockMaster.AccessoriesId = ISNULL(@AccessoriesId,tbl_StockMaster.AccessoriesId )
--WHERE tbl_StockMaster.AvailableQuantity != 0
END

END TRY
BEGIN CATCH
SELECT 'Sorry something went wrong, unable to process,' as Msg,0 as SuccessFlag
RETURN
END CATCH
END
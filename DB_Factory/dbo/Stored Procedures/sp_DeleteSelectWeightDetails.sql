﻿CREATE PROCEDURE  [dbo].[sp_DeleteSelectWeightDetails]
-- Add the parameters for the stored procedure here
 @TransactionId [bigint] NULL
--,@OperationId [bigint]  --1=delete,2=Select
,@UserId [bigint] NULL

AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY

SELECT * FROM tbl_WeightDetails WITH(NOLOCK)
WHERE TransactionId = ISNULL(@TransactionId,TransactionId)


END TRY
BEGIN CATCH
SELECT 'Sorry something went wrong, unable to process,' as Msg,0 as SuccessFlag
RETURN
END CATCH
END
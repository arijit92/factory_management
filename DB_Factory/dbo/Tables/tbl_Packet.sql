﻿CREATE TABLE [dbo].[tbl_Packet] (
    [PacketId]    INT           IDENTITY (1, 1) NOT NULL,
    [Description] NVARCHAR (50) NOT NULL,
    [Weight]      DECIMAL (18)  NOT NULL,
    [Unit]        NVARCHAR (50) NULL
);


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FMS.BL;
using FMS.BL.Buisness;
using FMS.BL.Utilities;
namespace FMS.Controllers
{
    public class SupplierTypeController : Controller
    {
        // GET: Role  
        public ActionResult List()
        {
            ViewBag.ParentMenu = "superadmin";
            ViewBag.ChildMenu = "supplierType";
            return View();
        }

        [HttpGet]
        public ActionResult Add(int? supplierTypeid)
        {
            ViewBag.ParentMenu = "superadmin";
            ViewBag.ChildMenu = "supplierType";
            SupplierType supplierType = null;
            SupplierTypeBuisness supplierTypeBuisness = null;
            long userid = 11;
            try
            {
                supplierType = new SupplierType();
                supplierTypeBuisness = new SupplierTypeBuisness();
                if (supplierTypeid > 0)
                {
                    supplierType = supplierTypeBuisness.GetSupplierType(supplierTypeid, userid).SingleOrDefault();
                }


            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch SupplierType list", ex, "SupplierType");
            }
            return View(supplierType);
        }
        [HttpPost]
        public ActionResult Add(SupplierType supplierType)
        {
            ViewBag.ParentMenu = "production";
            ViewBag.ChildMenu = "ProductionEntry";
            if (ModelState.IsValid)
            {
                SupplierTypeBuisness buisness = new SupplierTypeBuisness();
                Status status = new Status();
                status = buisness.AddUpdateSupplierType(supplierType, 1);
                TempData["SuccessFlag"] = status.SuccessFlag;
                TempData["Msg"] = status.Msg;
                if (status.SuccessFlag == "1")
                {
                    return RedirectToAction("List");
                }
                else
                {
                    return View(supplierType);
                }
            }
            return View(supplierType);
        }


        [HttpPost]
        public JsonResult LoadSupplierType(int SupplierTypeId)
        {
            ViewBag.ParentMenu = "superadmin";
            ViewBag.ChildMenu = "supplierType";
            long userid = 11;
            List<SupplierType> supplierType = null;
            SupplierTypeBuisness supplierTypeBuisness = null;
            try
            {
                supplierType = new List<SupplierType>();
                supplierTypeBuisness = new SupplierTypeBuisness();
                supplierType = supplierTypeBuisness.GetSupplierType(SupplierTypeId, userid).Where(i => i.IsDeleted == false).ToList();
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch SupplierType list", ex, "SupplierType");
            }

            return Json(supplierType, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(int supplierTypeid)
        {
            SupplierTypeBuisness supplierTypeBuisness = null;
            Status status = null;
            long userid = 11;
            try
            {
                supplierTypeBuisness = new SupplierTypeBuisness();
                status = new Status();
                status = supplierTypeBuisness.DeleteSupplierType(supplierTypeid, userid);
                TempData["SuccessFlag"] = status.SuccessFlag;
                TempData["Msg"] = status.Msg;
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to delete SupplierType ", ex, "SupplierType");
            }
            return RedirectToAction("List");
        }

    }
}
﻿CREATE PROCEDURE  [dbo].[sp_DeleteSelectAssets]
-- Add the parameters for the stored procedure here
 @AssetsId [bigint] NULL
,@OperationId [bigint]  --1=delete,2=Select
,@UserId [bigint] NULL

AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
IF(@OperationId  = 1 AND @AssetsId >0)
BEGIN
UPDATE tbl_AssetsMaster
SET
IsDeleted = 1
   ,UpdatedBy = @UserId
   ,UpdatedOn = GETDATE()
    WHERE AssetsId = @AssetsId
SELECT 'Asset deleted successfully.' as Msg,1 as SuccessFlag
END
ELSE
BEGIN
SELECT * FROM tbl_AssetsMaster WITH(NOLOCK)
WHERE AssetsId = ISNULL(@AssetsId,AssetsId)
END

END TRY
BEGIN CATCH
SELECT 'Sorry something went wrong, unable to process,' as Msg,0 as SuccessFlag
RETURN
END CATCH
END
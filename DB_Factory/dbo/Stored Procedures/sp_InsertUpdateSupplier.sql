﻿CREATE PROCEDURE  [dbo].[sp_InsertUpdateSupplier]
-- Add the parameters for the stored procedure here
 @SupplierId [bigint]
 ,@SupplierTypeId [bigint]
,@PointOfPersonName [NVARCHAR](100)
,@Address [NVARCHAR] (MAX)
,@Phone [NVARCHAR] (50)
,@Email [NVARCHAR] (100)
,@Block [NVARCHAR] (100)
,@IsActive [int] NULL
,@CreatedBy [bigint] NULL
,@CompanyId [bigint] NULL
AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
--update or insert checking
IF(@SupplierId>0)
BEGIN
--Duplicate value check
IF EXISTS(SELECT * FROM tbl_Supplier WITH(NOLOCK) WHERE (Phone = @Phone OR Email = @Email)  AND SupplierId != @SupplierId)
BEGIN
SELECT 'Already exists.' as Msg,0 as SuccessFlag
END
ELSE
BEGIN
--update
UPDATE tbl_Supplier
SET
SupplierTypeId = @SupplierTypeId
,PointOfPersonName =@PointOfPersonName
,Address =@Address
,Phone =@Phone
,Email =@Email
,Block = @Block
,IsActive =@IsActive
,UpdatedBy =@CreatedBy
,UpdatedOn  =GETDATE()
,CompanyId =@CompanyId
WHERE SupplierId =@SupplierId
SELECT 'Supplier updated successfully.' as Msg,1 as SuccessFlag
END
END
ELSE
BEGIN
--Duplicate value check
IF EXISTS (SELECT * FROM tbl_Supplier WITH(NOLOCK) WHERE (Phone = @Phone OR Email = @Email))
BEGIN
SELECT 'Already exists.' as Msg,0 as SuccessFlag
END
ELSE
BEGIN
--insert
INSERT INTO tbl_Supplier
(
SupplierTypeId
,PointOfPersonName
,Address
,Phone 
,Email
,Block
,IsActive
,IsDeleted
,CreatedBy
,CreatedOn
,CompanyId
)
VALUES
(
@SupplierTypeId
,@PointOfPersonName
,@Address
,@Phone
,@Email
,@Block
,@IsActive
,0
,@CreatedBy
,GETDATE()
,@CompanyId
)
SELECT 'Supplier created successfully.' as Msg,1 as SuccessFlag
END
END
END TRY
BEGIN CATCH
SELECT 'Sorry something went wrong, unable to process,' as Msg,0 as SuccessFlag
RETURN
END CATCH
END
﻿CREATE PROCEDURE  [dbo].[sp_DeleteSelectSupplier]
-- Add the parameters for the stored procedure here
 @SupplierId [bigint] NULL
,@OperationId [bigint]  --1=delete,2=Select
,@UserId [bigint] NULL

AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
IF(@OperationId  = 1 AND @SupplierId >0)
BEGIN
UPDATE tbl_Supplier
SET
IsDeleted = 1
   ,UpdatedBy = @UserId
   ,UpdatedOn = GETDATE()
    WHERE SupplierId = @SupplierId
SELECT 'Supplier deleted successfully.' as Msg,1 as SuccessFlag
END
ELSE
BEGIN
SELECT TS.*,TST.SupplierTypeName FROM tbl_Supplier TS WITH(NOLOCK) INNER JOIN tbl_SupplierType TST ON TST.SupplierTypeId = TS.SupplierTypeId
WHERE SupplierId = ISNULL(@SupplierId,SupplierId)
END

END TRY
BEGIN CATCH
SELECT 'Sorry something went wrong, unable to process,' as Msg,0 as SuccessFlag
RETURN
END CATCH
END
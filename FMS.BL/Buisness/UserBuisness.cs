﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FMS.BL.Utilities;

namespace FMS.BL.Buisness
{
  public  class UserBuisness : Common
    {
        

        #region add upadte User
        public Status AddUpdateuser(User user, long UserID)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    para.Add("@Id", user.Id);
                    para.Add("@FullName", user.FullName);
                    para.Add("@UserId", user.UserId);
                    para.Add("@Address", user.Address);
                    para.Add("@Password", user.Password);
                    para.Add("@Phone", user.Phone);
                    para.Add("@Email", user.Email);
                    para.Add("@Role", user.Role);
                    para.Add("@IsActive", user.IsActive);
                    para.Add("@CreatedBy", UserID);
                    para.Add("@CompanyId", user.CompanyId);
                    const string procName = "[dbo].[sp_InsertUpdateUser]";
                    connection.Open();
                    return connection.Query<Status>(procName, para, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to insert or update user", ex, "AddUpdateuser");
                return null;
            }

        }


        #endregion

        #region  ---get all user
        public List<User> GetAlluser(long? id, long userID)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    if (id == 0)
                    {
                        id = null;
                    }
                    para.Add("@Id", id);
                    para.Add("@OperationId", 2);
                    para.Add("@UserId", userID);
                    const string procName = "[dbo].[sp_DeleteSelectUser]";
                    connection.Open();
                    return connection.Query<User>(procName, para, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch user", ex, "GetAlluser");
                return null;
            }

        }
        #endregion

        #region  ---get loginUser
        public List<User> GetLoginUser(string userId)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                   
                    para.Add("@UserId", userId);
                    const string procName = "[dbo].[sp_GetLoginUser]";
                    connection.Open();
                    return connection.Query<User>(procName, para, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to get login user", ex, "GetLoginuser");
                return null;
            }

        }
        #endregion

        #region delete user
        public Status Deleteuser(long? useridTodelete, long UserID)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    para.Add("@Id", useridTodelete);
                    para.Add("@OperationId", 1);
                    para.Add("@UserId", UserID);
                    const string procName = "[dbo].[sp_DeleteSelectuser]";
                    connection.Open();
                    return connection.Query<Status>(procName, para, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to delete user", ex, "Deleteuser");
                return null;
            }

        }
        #endregion
    }
}

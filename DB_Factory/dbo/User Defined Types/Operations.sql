﻿CREATE TYPE [dbo].[Operations] AS TABLE (
    [OperationID] BIGINT          NULL,
    [LedgerID]    BIGINT          NULL,
    [Ammount]     DECIMAL (18, 2) NULL);


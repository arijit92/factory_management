﻿CREATE PROCEDURE  [dbo].[sp_InsertUpdateBranch]
-- Add the parameters for the stored procedure here
 @BranchId [bigint]
,@BranchName [NVARCHAR](MAX)
,@Address [NVARCHAR] (MAX)
,@Phone [NVARCHAR] (50)
,@Email [NVARCHAR] (100)
,@IsActive [int] NULL
,@CreatedBy [bigint] NULL
,@CompanyId [bigint] NULL
AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
--update or insert checking
IF(@BranchId>0)
BEGIN
--Duplicate value check
IF EXISTS(SELECT * FROM tbl_Branch WITH(NOLOCK) WHERE BranchName = @BranchName AND BranchId != @BranchId)
BEGIN
SELECT 'Already exists.' as Msg,0 as SuccessFlag
END
ELSE
BEGIN
--update
UPDATE tbl_Branch
SET
BranchName =UPPER(@BranchName)
,Address =@Address
,Phone =@Phone
,Email =@Email
,IsActive =@IsActive
,UpdatedBy =@CreatedBy
,UpdatedOn      =GETDATE()
,CompanyId =@CompanyId
WHERE BranchId =@BranchId
SELECT 'Branch updated successfully.' as Msg,1 as SuccessFlag
END
END
ELSE
BEGIN
--Duplicate value check
IF EXISTS (SELECT * FROM tbl_Branch WITH(NOLOCK) WHERE BranchName = @BranchName)
BEGIN
SELECT 'Already exists.' as Msg,0 as SuccessFlag
END
ELSE
BEGIN
--insert
INSERT INTO tbl_Branch
(
 BranchName
,Address
,Phone 
,Email 
,IsActive
,IsDeleted
,CreatedBy
,CreatedOn
,CompanyId
)
VALUES
(
UPPER(@BranchName)
,@Address
,@Phone
,@Email
,@IsActive
,0
,@CreatedBy
,GETDATE()
,@CompanyId
)
SELECT 'Branch created successfully.' as Msg,1 as SuccessFlag
END
END
END TRY
BEGIN CATCH
SELECT 'Sorry something went wrong, unable to process,' as Msg,0 as SuccessFlag
RETURN
END CATCH
END
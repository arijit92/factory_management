﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FMS.BL.Utilities;


namespace FMS.BL.Buisness
{
    public class WeightDetailsBuisness : Common
    {
        #region WeightDetails region

        public Status AddUpdateWeightDetails(WeightDetails pmodel, long UserID)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    para.Add("@TransactionId", pmodel.TransactionId);
                    para.Add("@CustomerName", pmodel.CustomerName);
                    para.Add("@VehicleNo", pmodel.VehicleNo);
                    para.Add("@Product", pmodel.Product);
                    para.Add("@Misc_1st_weight", pmodel.Misc_1st_weight);
                    para.Add("@Misc_2nd_weight", pmodel.Misc_2nd_weight);
                    para.Add("@Date_First_Weight", pmodel.Date_First_Weight);
                    para.Add("@Time_First_Weight", pmodel.First_Weight_Time);
                    para.Add("@Date_Second_Weight", pmodel.Second_Weight_Time);
                    para.Add("@Time_Second_Weight", pmodel.Second_Weight_Time);
                    para.Add("@First_weight", pmodel.First_weight);
                    para.Add("@Second_weight", pmodel.Second_weight);
                    para.Add("@UnitId", pmodel.UnitId);
                    para.Add("@NetWeight", pmodel.NetWeight);
                    para.Add("@WeightCharges", pmodel.WeightCharges);
                    para.Add("@IsActive", 1);
                    para.Add("@CreatedBy", UserID);
                    para.Add("@CompanyId", pmodel.CompanyId);
                    const string procName = "[dbo].[sp_InsertUpdateWeightDetails]";
                    connection.Open();
                    return connection.Query<Status>(procName, para, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to insert or update Weight Details", ex, "AddUpdateWeightDetails");
                return null;
            }

        }
        public List<WeightDetails> GetWeightDetails(long? TransactionId, long UserID)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    if (TransactionId == 0)
                    {
                        TransactionId = null;
                    }
                    para.Add("@TransactionId", TransactionId);
                   
                    const string procName = "[dbo].[sp_DeleteSelectWeightDetails]";
                    connection.Open();
                    return connection.Query<WeightDetails>(procName, para, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Weight Details", ex, "GetWeightDetails");
                return null;
            }

        }
        public Status DeleteWeightDetails(long? TransactionId, long UserID)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    para.Add("@TransactionId", TransactionId);
                    para.Add("@OperationId", 1);
                    para.Add("@UserId", UserID);
                    const string procName = "[dbo].[sp_DeleteSelectWeightDetails]";
                    connection.Open();
                    return connection.Query<Status>(procName, para, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to delete Weight Details", ex, "DeleteWeightDetails");
                return null;
            }

        }
        #endregion
    }
}

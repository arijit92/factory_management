﻿using FMS.BL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FMS.BL.Buisness;
using FMS.BL.Utilities;


namespace FMS.Controllers
{
    public class UserController : Controller
    {
        // GET: User
        public ActionResult List()
        {
            return View();
        }
        // ADD: User
        public ActionResult Add(int? id)
        {
        
            UserBuisness userBuisness = null;
            
            User user = null;
            RoleBuisness roleBuisness = null;
            List<Role> roles = null;
            long userid = 11;

            try
            {
                userBuisness = new UserBuisness();
                user = new User();
                user.IsActive = true;
                roleBuisness = new RoleBuisness();
                roles = new List<Role>();
                roles = roleBuisness.GetRole(0, 11).Where(i => i.IsActive == true && i.IsDeleted == false).ToList();
                ViewBag.ForRole = new SelectList(roles, "RoleId", "Name", 0);
                if (id > 0)
                {
                    user = userBuisness.GetAlluser(id, userid).SingleOrDefault();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to render Employee Add page", ex, "Add");
            }
            return View(user);
        }

        // ADD: Employee : POST
        [HttpPost]
        public ActionResult Add(User user)
        {
            List<User> users = null;
            UserBuisness userBuisness = null;
            Status status = null;
            RoleBuisness roleBuisness = null;
            List<Role> roles = null;
            try
            {
                userBuisness = new UserBuisness();
                users = new List<User>();
                status = new Status();
                roleBuisness = new RoleBuisness();
                roles = new List<Role>();
                roles = roleBuisness.GetRole(0, 11).Where(i => i.IsActive == true && i.IsDeleted == false).ToList();
                ViewBag.ForRole = new SelectList(roles, "RoleId", "Name", 0);

                user.CompanyId = 12;
                //// string uid = Session["UserBE"].UserId;
                long userid = 11;
                var errors = ModelState.Values.SelectMany(v => v.Errors);
                if (ModelState.IsValid)
                {
                    user.Password = BL.Utilities.Common.Encrypt(user.Password);
                    status = userBuisness.AddUpdateuser(user, userid);
                    // ViewBag.Status = status;
                    TempData["SuccessFlag"] = status.SuccessFlag;
                    TempData["Msg"] = status.Msg;
                    if (status.SuccessFlag == "1")
                    {
                        return RedirectToAction("List");
                    }
                    else
                    {
                        return View(user);

                    }

                }
                else
                {
                    return View(user);
                }

            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to post user Add page", ex, "Add");
                return View(user);
            }

        }

        [HttpPost]
        public JsonResult LoadUser(int id)
        {
            //// string uid = Session["UserBE"].UserId;
            long userid = 11;
            List<User> users = null;
            UserBuisness userBuisness = null;

            try
            {
                users = new List<User>();
                userBuisness = new UserBuisness();
                // AppLogger.Instance.Info(this.GetType(), "Fetch the list of all category", "LoadCategory");aa
                users = userBuisness.GetAlluser(id, userid).Where(i => i.IsDeleted == false).ToList();
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch user list", ex, "LoadUser");
            }

            return Json(users, JsonRequestBehavior.AllowGet);
        }
       
        public ActionResult Delete(int userid)
        {
            UserBuisness userBuisness = null;
            Status status = null;
            long Userid = 11;
            try
            {
                userBuisness = new UserBuisness();
                status = new Status();
                status = userBuisness.Deleteuser(userid, Userid);
                TempData["SuccessFlag"] = status.SuccessFlag;
                TempData["Msg"] = status.Msg;

            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to delete user ", ex, "Delete");
            }
            return RedirectToAction("List");
        }

    }
}
﻿CREATE PROCEDURE  [dbo].[sp_DeleteSelectRole]
-- Add the parameters for the stored procedure here
 @RoleId [bigint] NULL
,@OperationId [bigint]  --1=delete,2=Select
,@UserId [bigint] NULL

AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
IF(@OperationId  = 1 AND @RoleId >0)
BEGIN
UPDATE tbl_Role
SET
IsDeleted = 1
   ,UpdatedBy = @UserId
   ,UpdatedOn = GETDATE()
    WHERE RoleId = @RoleId
SELECT 'Role deleted successfully.' as Msg,1 as SuccessFlag
END
ELSE
BEGIN
SELECT * FROM tbl_Role WITH(NOLOCK)
WHERE RoleId = ISNULL(@RoleId,RoleId)
END

END TRY
BEGIN CATCH
SELECT 'Sorry something went wrong, unable to process,' as Msg,0 as SuccessFlag
RETURN
END CATCH
END
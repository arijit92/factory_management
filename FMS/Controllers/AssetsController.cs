﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FMS.BL;
using FMS.BL.Buisness;
using FMS.BL.Utilities;

namespace FMS.Controllers
{
    public class AssetsController : Controller
    {
        // GET: Assets
        public ActionResult List()
        {
            ViewBag.ParentMenu = "master";
            ViewBag.ChildMenu = "assets";
            return View();
        }

        [HttpGet]
        public ActionResult Add(int? assetsid)
        {
            ViewBag.ParentMenu = "master";
            ViewBag.ChildMenu = "assets";
            Assets assets = null;
            AssetBuisness assetBuisness = null;
            long userid = 11;
            try
            {
                assets = new Assets();
                assetBuisness = new AssetBuisness();
                if (assetsid > 0)
                {
                    assets = assetBuisness.GetAssets(assetsid, userid).SingleOrDefault();
                   // Asset = AssetBuisness.GetAsset(assetsid, userid).SingleOrDefault();
                }


            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Asset list", ex, "Asset");
            }
            return View(assets);
        }

        [HttpPost]
        public ActionResult Add(Assets Assets)
        {
            ViewBag.ParentMenu = "master";
            ViewBag.ChildMenu = "Assets";
            if (ModelState.IsValid)
            {
                AssetBuisness buisness = new AssetBuisness();
                Status status = new Status();
                status = buisness.AddUpdateAssets(Assets, 1);
                TempData["SuccessFlag"] = status.SuccessFlag;
                TempData["Msg"] = status.Msg;
                if (status.SuccessFlag == "1")
                {
                    return RedirectToAction("List");
                }
                else
                {
                    return View(Assets);
                }
            }
            return View(Assets);
        }

        [HttpPost]
        public JsonResult LoadAssets(int AssetsId)
        {
            ViewBag.ParentMenu = "master";
            ViewBag.ChildMenu = "Assets";
            long userid = 11;
            List<Assets> assets = null;
            AssetBuisness AssetBuisness = null;
            try
            {
                assets = new List<Assets>();
                AssetBuisness = new AssetBuisness();
                assets = AssetBuisness.GetAssets(AssetsId, userid).Where(i => i.IsDeleted == false).ToList();
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Assets list", ex, "Assets");
            }

            return Json(assets, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(int Assetsid)
        {
            ViewBag.ParentMenu = "master";
            ViewBag.ChildMenu = "Assets";
            AssetBuisness AssetBuisness = null;
            Status status = null;
            long userid = 11;
            try
            {
                AssetBuisness = new AssetBuisness();
                status = new Status();
                status = AssetBuisness.DeleteAssets(Assetsid, userid);
                TempData["SuccessFlag"] = status.SuccessFlag;
                TempData["Msg"] = status.Msg;
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to delete Assets ", ex, "Delete");
            }
            return RedirectToAction("List");
        }
    }
}
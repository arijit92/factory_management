﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FMS.BL;
using FMS.BL.Buisness;
using FMS.BL.Utilities;
namespace FMS.Controllers
{
    public class BranchController : Controller
    {
        // GET: Branch  
        public ActionResult List()
        {
            ViewBag.ParentMenu = "master";
            ViewBag.ChildMenu = "branch";
            return View();
        }

        [HttpGet]
        public ActionResult Add(int? branchid)
        {
            ViewBag.ParentMenu = "master";
            ViewBag.ChildMenu = "branch";
            Branch branch = null;
            BranchBuisness branchBuisness = null;
            long userid = 11;
            try
            {
                branch = new Branch();
                branchBuisness = new BranchBuisness();
                if (branchid > 0)
                {
                    branch = branchBuisness.GetBranch(branchid, userid).SingleOrDefault();
                }


            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Branch list", ex, "Branch");
            }
            return View(branch);
        }
        [HttpPost]
        public ActionResult Add(Branch branch)
        {
            ViewBag.ParentMenu = "master";
            ViewBag.ChildMenu = "branch";
            if (ModelState.IsValid)
            {
                BranchBuisness buisness = new BranchBuisness();
                Status status = new Status();
                status = buisness.AddUpdateBranch(branch, 1);
                TempData["SuccessFlag"] = status.SuccessFlag;
                TempData["Msg"] = status.Msg;
                if (status.SuccessFlag == "1")
                {
                    return RedirectToAction("List");
                }
                else
                {
                    return View(branch);
                }
            }
            return View(branch);
        }


        [HttpPost]
        public JsonResult LoadBranch(int BranchId)
        {
            ViewBag.ParentMenu = "master";
            ViewBag.ChildMenu = "branch";
            long userid = 11;
            List<Branch> branch = null;
            BranchBuisness branchBuisness = null;
            try
            {
                branch = new List<Branch>();
                branchBuisness = new BranchBuisness();
                branch = branchBuisness.GetBranch(BranchId, userid).Where(i => i.IsDeleted == false).ToList();
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Branch list", ex, "Branch");
            }

            return Json(branch, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(int branchid)
        {
            ViewBag.ParentMenu = "master";
            ViewBag.ChildMenu = "branch";
            BranchBuisness branchBuisness = null;
            Status status = null;
            long userid = 11;
            try
            {
                branchBuisness = new BranchBuisness();
                status = new Status();
                status = branchBuisness.DeleteBranch(branchid, userid);
                TempData["SuccessFlag"] = status.SuccessFlag;
                TempData["Msg"] = status.Msg;
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to delete Branch ", ex, "Delete");
            }
            return RedirectToAction("List");
        }

    }
}
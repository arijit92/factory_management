﻿CREATE PROCEDURE  [dbo].[sp_DeleteSelectBag]
-- Add the parameters for the stored procedure here
 @BagId [bigint] NULL
,@OperationId [bigint]  --1=delete,2=Select
,@UserId [bigint] NULL

AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
IF(@OperationId  = 1 AND @BagId >0)
BEGIN
UPDATE tbl_Bag
SET
IsDeleted = 1
   ,UpdatedBy = @UserId
   ,UpdatedOn = GETDATE()
    WHERE BagId = @BagId
SELECT 'Bag deleted successfully.' as Msg,1 as SuccessFlag
END
ELSE
BEGIN
SELECT * FROM tbl_Bag WITH(NOLOCK)
WHERE BagId = ISNULL(@BagId,BagId)
END

END TRY
BEGIN CATCH
SELECT 'Sorry something went wrong, unable to process,' as Msg,0 as SuccessFlag
RETURN
END CATCH
END
﻿CREATE PROCEDURE  [dbo].[sp_GetLedgerValue]
-- Add the parameters for the stored procedure here
@LedgerId int,
@Date Date null

AS
BEGIN
--DECLARE @Where nvarchar(MAX)
--IF(@LedgerId>0)
--SET @Where = 'WHERE LedgerID='+@LedgerId+''
SELECT TD.[TransactionDetailsID]
      ,TD.[TransactionID]
      ,TD.[OperationID]
      ,TD.[LedgerID]
      ,TD.[Ammount]
	  ,TM.TransactionDate
	  ,ISNULL(TM.Partyname,'') Partyname
	  ,ISNULL(TM.ChequeNo,'') ChequeNo
	 ,(Select LedgerName from tbl_Ledger where LedgerId = TD.LedgerID) LedgerName
	 -- ,(select top 1 TransactionDate from tbl_TransactionMaster where TransactionID = TD.TransactionID) as TransactionDate
	 -- ,ISNULL((select top 1 Partyname from tbl_TransactionMaster where TransactionID = TD.TransactionID),'') as Partyname
  FROM [dbo].[tbl_TransactionDetails] TD right join tbl_TransactionMaster TM on TM.TransactionID = TD.TransactionID
  where TD.LedgerID = COALESCE(@LedgerId,TD.LedgerID) AND
        CAST(TM.TransactionDate as Date) = CAST(COALESCE(@Date,TM.TransactionDate) as Date)
 -- COALESCE(@Cus_Name,Cus_Name)
END
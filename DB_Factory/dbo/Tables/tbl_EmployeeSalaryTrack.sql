﻿CREATE TABLE [dbo].[tbl_EmployeeSalaryTrack] (
    [Id]          INT          IDENTITY (1, 1) NOT NULL,
    [Year]        NVARCHAR (4) NOT NULL,
    [Month]       INT          NOT NULL,
    [IsLocked]    BIT          NOT NULL,
    [IsDisbursed] BIT          NOT NULL
);


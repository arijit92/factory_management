﻿CREATE PROCEDURE  [dbo].[sp_ItemCustomerSalesStockDetails]
@Date datetime = null
AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
SELECT P.Description,P.Unit,IPST.BostaQuantity,IPST.Price,IPST.PacketId,IPST.Weight FROM 
tbl_ItemCustomerSalesStockTransaction IPST INNER JOIN tbl_Packet P 
ON P.PacketId = IPST.PacketId where Date = ISNULL(@Date,Date)
--SELECT 'Alert deleted successfully.' as Msg,1 as SuccessFlag
END TRY
BEGIN CATCH
SELECT 'Sorry something went wrong, unable to process,' as Msg,0 as SuccessFlag
RETURN
END CATCH
END
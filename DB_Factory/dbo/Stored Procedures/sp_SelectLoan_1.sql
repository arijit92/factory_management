﻿CREATE PROCEDURE  [dbo].[sp_SelectLoan]
-- Add the parameters for the stored procedure here
AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
--Declare @TotalRepayAmount int
--SET @TotalRepayAmount = (Select SUM(Amount) from tbl_LoanRepayManagement where EmployeeId)
--SELECT *
--,E.FirstName
--,E.LastName 
--,(Select SUM(Amount) from tbl_LoanRepayManagement where EmployeeId = E.EmployeeId) as PaidAmount
--,(Select SUM(Amount) from tbl_LoanManagement  group by EmployeeId)
--FROM tbl_LoanManagement LM inner join tbl_Employee E on LM.EmployeeId = E.EmployeeId
select 
 E.FirstName
,E.LastName
,LM.EmployeeId
,(Select SUM(Amount) from tbl_LoanRepayManagement where EmployeeId = LM.EmployeeId) as PaidAmount
,SUM(LM.Amount) TotalLoan from tbl_LoanManagement LM 
inner join tbl_Employee E on E.EmployeeId = LM.EmployeeId
group by LM.EmployeeId,E.FirstName,E.LastName
order by LM.EmployeeId
END TRY
BEGIN CATCH
SELECT 'Sorry something went wrong, unable to process,' as Msg,0 as SuccessFlag
RETURN
END CATCH
END
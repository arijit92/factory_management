﻿CREATE TABLE [dbo].[tbl_Branch] (
    [BranchId]   BIGINT         IDENTITY (1, 1) NOT NULL,
    [BranchName] NVARCHAR (MAX) NOT NULL,
    [Address]    NVARCHAR (MAX) NULL,
    [Phone]      NVARCHAR (50)  NOT NULL,
    [Email]      NVARCHAR (100) NULL,
    [IsActive]   INT            NULL,
    [IsDeleted]  INT            NULL,
    [CreatedBy]  BIGINT         NULL,
    [CreatedOn]  DATETIME       NULL,
    [UpdatedBy]  BIGINT         NULL,
    [UpdatedOn]  DATETIME       NULL,
    [CompanyId]  BIGINT         NULL
);


﻿CREATE TABLE [dbo].[tbl_Supplier] (
    [SupplierId]        BIGINT         IDENTITY (1, 1) NOT NULL,
    [SupplierTypeId]    BIGINT         NOT NULL,
    [PointOfPersonName] NVARCHAR (100) NULL,
    [Phone]             NVARCHAR (50)  NOT NULL,
    [Email]             NVARCHAR (100) NULL,
    [Address]           NVARCHAR (MAX) NULL,
    [Block]             NVARCHAR (100) NULL,
    [IsActive]          INT            NULL,
    [IsDeleted]         INT            NULL,
    [CreatedBy]         BIGINT         NULL,
    [CreatedOn]         DATETIME       NULL,
    [UpdatedBy]         BIGINT         NULL,
    [UpdatedOn]         DATETIME       NULL,
    [CompanyId]         BIGINT         NULL
);


﻿CREATE PROCEDURE  [dbo].[sp_DeleteSelectSupplierType]
-- Add the parameters for the stored procedure here
 @SupplierTypeId [bigint] NULL
,@OperationId [bigint]  --1=delete,2=Select
,@UserId [bigint] NULL

AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
IF(@OperationId  = 1 AND @SupplierTypeId >0)
BEGIN
UPDATE tbl_SupplierType
SET
IsDeleted = 1
   ,UpdatedBy = @UserId
   ,UpdatedOn = GETDATE()
    WHERE SupplierTypeId = @SupplierTypeId
SELECT 'SupplierType deleted successfully.' as Msg,1 as SuccessFlag
END
ELSE
BEGIN
SELECT * FROM tbl_SupplierType WITH(NOLOCK)
WHERE SupplierTypeId = ISNULL(@SupplierTypeId,SupplierTypeId)
END

END TRY
BEGIN CATCH
SELECT 'Sorry something went wrong, unable to process,' as Msg,0 as SuccessFlag
RETURN
END CATCH
END
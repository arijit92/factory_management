﻿CREATE PROCEDURE  [dbo].[sp_DeleteSelectLabourWorkType]
-- Add the parameters for the stored procedure here
 @LabourWorkTypeId [bigint] NULL
,@OperationId [bigint]  --1=delete,2=Select
,@UserId [bigint] NULL

AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
IF(@OperationId  = 1 AND @LabourWorkTypeId >0)
BEGIN
UPDATE tbl_LabourWorkType
SET
IsDeleted = 1
   ,UpdatedBy = @UserId
   ,UpdatedOn = GETDATE()
    WHERE LabourWorkTypeId = @LabourWorkTypeId
SELECT 'Labour Work Type deleted successfully.' as Msg,1 as SuccessFlag
END
ELSE
BEGIN
SELECT * FROM tbl_LabourWorkType WITH(NOLOCK)
WHERE LabourWorkTypeId = ISNULL(@LabourWorkTypeId,LabourWorkTypeId)
END

END TRY
BEGIN CATCH
SELECT 'Sorry something went wrong, unable to process,' as Msg,0 as SuccessFlag
RETURN
END CATCH
END
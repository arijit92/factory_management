﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FMS.BL;
using FMS.BL.Buisness;
using FMS.BL.Utilities;

namespace FMS.Controllers
{
    public class CategoryController : Controller
    {
        // GET: Category
        CategoryBuisness categoryBuisness = new CategoryBuisness();
        Status returnValue = null;
        AppLogger appLogger = new AppLogger();
        
        public ActionResult CategoryAdd()
        {
            ViewBag.ParentMenu = "superadmin";
            ViewBag.ChildMenu = "category";
            return View();
        }
        #region Function to Insert Or Update Category
        [HttpPost]
        public JsonResult InsertUpdateCategory(ProductCategory requestData)
        {

            //// string uid = Session["UserBE"].UserId;
            long usrid = 11;
            requestData.CompanyId = 12;
            try
            {
               // AppLogger.Instance.Info(this.GetType(), "Category insert or update", "InsertUpdateCategory");
                returnValue = categoryBuisness.AddUpdateProductCategory(requestData, usrid);
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to insert or update category", ex, "InsertUpdateCategory");
                return null;
            }
            return Json(returnValue, JsonRequestBehavior.AllowGet);
        }
        #endregion

        public ActionResult CategoryList()
        {
            ViewBag.ParentMenu = "superadmin";
            ViewBag.ChildMenu = "category";

            return View();
        }

        [HttpPost]
        public JsonResult LoadCategory(int categoryId)
        {
            ViewBag.ParentMenu = "superadmin";
            ViewBag.ChildMenu = "category";
            //// string uid = Session["UserBE"].UserId;
            long usrid = 11;
            List<ProductCategory> productCategories = null;

            try
            {
                productCategories = new List<ProductCategory>();
                categoryBuisness = new CategoryBuisness();
               // AppLogger.Instance.Info(this.GetType(), "Fetch the list of all category", "LoadCategory");aa
                productCategories = categoryBuisness.GetProductCategory(categoryId, usrid);               
            }
            catch (Exception ex)
            {
                  AppLogger.Instance.Error(this.GetType(), "Unable to fetch category list", ex, "LoadCategory");
            }

            return Json(productCategories, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteCategory(int categoryId)
        {
            ViewBag.ParentMenu = "superadmin";
            ViewBag.ChildMenu = "category";
            //// string uid = Session["UserBE"].UserId;
            long usrid = 11;
            try
            {
                
                categoryBuisness = new CategoryBuisness();
                //AppLogger.Instance.Info(this.GetType(), "Category deleted", "DeleteCategory");
                returnValue = categoryBuisness.DeleteProductCategory(categoryId, usrid);
            }
            catch (Exception ex)
            {
                  AppLogger.Instance.Error(this.GetType(), "Unable to delete category", ex, "DeleteCategory");
            }

            return Json(returnValue, JsonRequestBehavior.AllowGet);
        }

        #region redirect to application page
        public ActionResult Redirect(int categoryid)
        {
            ViewBag.ParentMenu = "superadmin";
            ViewBag.ChildMenu = "category";
            try
            {
                if(categoryid>0)
                {
                    TempData["Category_Id"] = categoryid;
                }
                return RedirectToAction("CategoryAdd", "Category");

            }
            catch (Exception ex)
            {
              //  AppLogger.Instance.Error(ex.GetType(), "redirection failed", ex, "Edit");
                return null;
            }
        }
        #endregion
    }
}
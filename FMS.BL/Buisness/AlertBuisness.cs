﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FMS.BL.Utilities;


namespace FMS.BL.Buisness
{
    public class AlertBuisness : Common
    {
        #region Alert region

        public Status AddUpdateAlert(Alert pmodel, long UserID)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    para.Add("@AlertId", pmodel.AlertId);
                    para.Add("@AlertName", pmodel.AlertName);
                    para.Add("@AlertDescription", pmodel.AlertDescription);
                    para.Add("@IssueDate", pmodel.IssueDate);
                    para.Add("@NextRenewalDate", pmodel.NextRenewalDate);
                    para.Add("@IsActive", Convert.ToInt32(pmodel.IsActive == false ? 0 : 1));
                    para.Add("@CreatedBy", UserID);
                    para.Add("@CompanyId", pmodel.CompanyId);
                    const string procName = "[dbo].[sp_InsertUpdateAlert]";
                    connection.Open();
                    return connection.Query<Status>(procName, para, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to insert or update Alert", ex, "AddUpdateAlert");
                return null;
            }

        }
        public List<Alert> GetAlert(long? AlertId, long UserID)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    if (AlertId == 0)
                    {
                        AlertId = null;
                    }
                    para.Add("@AlertId", AlertId);
                    para.Add("@OperationId", 2);
                    para.Add("@UserId", UserID);
                    const string procName = "[dbo].[sp_DeleteSelectAlert]";
                    connection.Open();
                    return connection.Query<Alert>(procName, para, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Alert", ex, "GetAlert");
                return null;
            }

        }
        public Status DeleteAlert(long? AlertId, long UserID)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    para.Add("@AlertId", AlertId);
                    para.Add("@OperationId", 1);
                    para.Add("@UserId", UserID);
                    const string procName = "[dbo].[sp_DeleteSelectAlert]";
                    connection.Open();
                    return connection.Query<Status>(procName, para, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to delete Alert", ex, "DeleteAlert");
                return null;
            }

        }

        public List<Alert> GetAlertNotification()
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    const string procName = "[dbo].[sp_AlertNotification]";
                    connection.Open();
                    return connection.Query<Alert>(procName, para, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Alert", ex, "GetAlertNotification");
                return null;
            }

        }
        #endregion
    }
}

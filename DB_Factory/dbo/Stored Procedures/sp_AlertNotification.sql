﻿CREATE PROCEDURE  [dbo].[sp_AlertNotification]
-- Add the parameters for the stored procedure here
 

AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
--select * from tbl_Alert

 SELECT *,CONVERT(VARCHAR(100),IssueDate,103 ) AS StrIssueDate,CONVERT(VARCHAR(100),NextRenewalDate,103 ) AS StrNextRenewalDate FROM tbl_Alert WHERE NextRenewalDate >= DATEADD(day, -5, GetDate())

END TRY
BEGIN CATCH
SELECT 'Sorry something went wrong, unable to process,' as Msg,0 as SuccessFlag
RETURN
END CATCH
END
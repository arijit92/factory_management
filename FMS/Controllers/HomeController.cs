﻿using FMS.BL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FMS.BL.Buisness;
using FMS.BL.Utilities;

namespace FMS.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Invoice()
        {
            return View();
        }
        public ActionResult Logout()
        {
            Session.Clear();
            Session.Abandon();
            //return View("Login");
            return RedirectToAction("Login", "Home");
        }


        // GET: Home
        public ActionResult CompanyProfile()
        {
            HomeBuisness homeBuisness = new HomeBuisness();
            Company company = new Company();
            try { 
            company = homeBuisness.CompanyProfile().SingleOrDefault();
            }
            catch(Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to get company details", ex, "CompanyProfile");
            }
            return View(company);
        }

        // GET: Login
        public ActionResult Login()
        {
            LoginModel model = new LoginModel();
            return View(model);
        }
        [HttpPost]
        public ActionResult Login(LoginModel model)
        {
            UserBuisness userBuisness = null;
            User user = null;
            try
            {
                if (ModelState.IsValid)
                {
                    userBuisness = new UserBuisness();
                    user = new User();
                    user = userBuisness.GetLoginUser(model.Username).SingleOrDefault();
                    if (user != null)
                    {
                        if (model.Password == BL.Utilities.Common.Decrypt(user.Password))
                        {
                           Session["UserName"] = user.FullName;
                            Session["UserId"] = user.Id;
                            Session["RoleName"] = user.RoleName;

                            return RedirectToAction("Index");
                        }
                        else
                        {
                            //wrong password
                            ViewBag.Alert = "Enter correct username and password";
                            return View(model);

                        }

                    }
                    else
                    {
                        //enter correct user name and password
                        ViewBag.Alert = "Enter correct username and password";
                        return View(model);
                    }
                }
                else
                {//user name or password blank submit
                    ViewBag.Alert = "Enter correct username and password";
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                return View(model);
            }
        }
    }
}
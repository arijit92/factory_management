﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FMS.BL.Utilities;


namespace FMS.BL.Buisness
{
    public class ApplicationLogBuisness : Common
    {
        #region ApplicationLog region
        public List<ApplicationLog> GetApplicationLog()
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                   
                    const string procName = "[dbo].[sp_SelectApplicationLog]";
                    connection.Open();
                    return connection.Query<ApplicationLog>(procName, null, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch AppliactionLog", ex, "GetApplicationLog");
                return null;
            }

        }
        #endregion
    }
}

﻿CREATE TABLE [dbo].[tbl_Machine] (
    [MachineId]         BIGINT          IDENTITY (1, 1) NOT NULL,
    [MachineName]       NVARCHAR (200)  NOT NULL,
    [Quantity]          DECIMAL (18, 2) NOT NULL,
    [UnitId]            BIGINT          NOT NULL,
    [CostOfMachineries] DECIMAL (18, 2) NOT NULL,
    [IsActive]          INT             NULL,
    [IsDeleted]         INT             NULL,
    [CreatedBy]         BIGINT          NULL,
    [CreatedOn]         DATETIME        NULL,
    [UpdatedBy]         BIGINT          NULL,
    [UpdatedOn]         DATETIME        NULL,
    [CompanyId]         BIGINT          NULL
);




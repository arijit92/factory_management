using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Linq;
using FMS.BL.DataHandler;

namespace FMS.BL.Functions
{
    public class ControlFunctions
    {

        //Return same value exist in datatable or not
        public static int SearchDataTableWithSameValue(DataTable dt, string Column, string ColumnVal)
        {
            int Flag = 0;
            foreach (DataRow dr in dt.Rows)
            {
                foreach (DataColumn column in dt.Columns)
                {
                    CommonDataHandler bp = new CommonDataHandler();
                    if (bp.GetStringValue(dr[column].ToString()) == ColumnVal)
                    {
                        Flag = 1;
                        break;
                    }
                }
            }
            return Flag;
        }

    }
}

﻿CREATE TABLE [dbo].[tblPurchaseEntryMaster] (
    [Id]           BIGINT          NOT NULL,
    [ItemTypeId]   BIGINT          NOT NULL,
    [NoOfBosta]    BIGINT          NOT NULL,
    [Weight]       DECIMAL (18, 2) NOT NULL,
    [PurchaseDate] DATETIME        NOT NULL
);


﻿CREATE PROCEDURE  [dbo].[sp_InsertUpdateBag]
-- Add the parameters for the stored procedure here
 @BagId [bigint]
,@BagName [NVARCHAR](MAX)
,@BagDescription [NVARCHAR](MAX) NULL
,@NetWeight NUMERIC(18,2)
,@PPWeight NUMERIC(18,2)
,@IsActive [int] NULL
,@CreatedBy [bigint] NULL
,@CompanyId [bigint] NULL
AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
--update or insert checking
IF(@BagId>0)
BEGIN
--Duplicate value check
IF EXISTS(SELECT * FROM tbl_Bag WITH(NOLOCK) WHERE BagName = @BagName AND BagId != @BagId AND IsDeleted = 0)
BEGIN
SELECT 'Already exists.' as Msg,0 as SuccessFlag
END
ELSE
BEGIN
--update
UPDATE tbl_Bag
SET
BagName =UPPER(@BagName)
,BagDescription=@BagDescription
,NetWeight =@NetWeight
,PPWeight =@PPWeight
,IsActive =@IsActive
,UpdatedBy =@CreatedBy
,UpdatedOn      =GETDATE()
,CompanyId =@CompanyId
WHERE BagId =@BagId
SELECT 'Bag updated successfully.' as Msg,1 as SuccessFlag
END
END
ELSE
BEGIN
--Duplicate value check
IF EXISTS (SELECT * FROM tbl_Bag WITH(NOLOCK) WHERE BagName = @BagName AND IsDeleted=0)
BEGIN
SELECT 'Already exists.' as Msg,0 as SuccessFlag
END
ELSE
BEGIN
--insert
INSERT INTO tbl_Bag
(
 BagName
,BagDescription
,NetWeight 
,PPWeight 
,IsActive
,IsDeleted
,CreatedBy
,CreatedOn
,CompanyId
)
VALUES
(
UPPER(@BagName)
,@BagDescription
,@NetWeight
,@PPWeight
,@IsActive
,0
,@CreatedBy
,GETDATE()
,@CompanyId
)
SELECT 'Bag created successfully.' as Msg,1 as SuccessFlag
END
END
END TRY
BEGIN CATCH
SELECT 'Sorry something went wrong, unable to process,' as Msg,0 as SuccessFlag
RETURN
END CATCH
END
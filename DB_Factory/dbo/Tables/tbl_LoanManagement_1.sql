﻿CREATE TABLE [dbo].[tbl_LoanManagement] (
    [Id]         INT             IDENTITY (1, 1) NOT NULL,
    [BranchId]   INT             NULL,
    [EmployeeId] INT             NULL,
    [Date]       DATE            NOT NULL,
    [Amount]     DECIMAL (18, 2) NOT NULL
);


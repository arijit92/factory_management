﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FMS.BL.Utilities;


namespace FMS.BL.Buisness
{
    public class SupplierBuisness : Common
    {
        #region Supplier region

        public Status AddUpdateSupplier(Supplier pmodel, long UserID)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    para.Add("@SupplierId", pmodel.SupplierId);
                    para.Add("@@SupplierTypeId", pmodel.SupplierTypeId);
                    para.Add("@PointOfPersonName", pmodel.PointOfPersonName);
                    para.Add("@Address", pmodel.Address);
                    para.Add("@Phone", pmodel.Phone);
                    para.Add("@Email", pmodel.Email);
                    para.Add("@Block", pmodel.Block);
                    para.Add("@IsActive", Convert.ToInt32(pmodel.IsActive == false ? 0 : 1));
                    para.Add("@CreatedBy", UserID);
                    para.Add("@CompanyId", pmodel.CompanyId);
                    const string procName = "[dbo].[sp_InsertUpdateSupplier]";
                    connection.Open();
                    return connection.Query<Status>(procName, para, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to insert or update Supplier", ex, "AddUpdateSupplier");
                return null;
            }

        }
        public List<Supplier> GetSupplier(long? SupplierId, long UserID)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    if (SupplierId == 0)
                    {
                        SupplierId = null;
                    }
                    para.Add("@SupplierId", SupplierId);
                    para.Add("@OperationId", 2);
                    para.Add("@UserId", UserID);
                    const string procName = "[dbo].[sp_DeleteSelectSupplier]";
                    connection.Open();
                    return connection.Query<Supplier>(procName, para, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch Supplier", ex, "GetSupplier");
                return null;
            }

        }
        public Status DeleteSupplier(long? SupplierId, long UserID)
        {
            try
            {
                using (IDbConnection connection = base.GetConnection())
                {
                    var para = new DynamicParameters();
                    para.Add("@SupplierId", SupplierId);
                    para.Add("@OperationId", 1);
                    para.Add("@UserId", UserID);
                    const string procName = "[dbo].[sp_DeleteSelectSupplier]";
                    connection.Open();
                    return connection.Query<Status>(procName, para, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to delete Supplier", ex, "DeleteSupplier");
                return null;
            }

        }
        #endregion
    }
}

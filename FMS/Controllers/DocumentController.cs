﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FMS.BL;
using FMS.BL.Buisness;
using FMS.BL.Utilities;

namespace FMS.Controllers
{
    public class DocumentController : Controller
    {
        // GET: Document
        public ActionResult Documents(long? DocumentsId)
        {
            ViewBag.ParentMenu = "administrator";
            ViewBag.ChildMenu = "document";
            Documents documents = new Documents();
            DocumentBuisness documentBuisness = null;
            try
            {
                documentBuisness = new DocumentBuisness();
                if(DocumentsId>0)
                {
                    documents = documentBuisness.GetDocuments(DocumentsId, 11).SingleOrDefault();
                }
              

            }
            catch(Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to  document page", ex, "Documents");

            }
            return View(documents);
        }
        [HttpPost]
        public ActionResult Documents(HttpPostedFileBase postedFile, Documents documents)
        {
            ViewBag.ParentMenu = "administrator";
            ViewBag.ChildMenu = "document";
            DocumentBuisness documentBuisness = null;
            Status status = null;
            try
            {
                if (ModelState.IsValid)
                {
                    documentBuisness = new DocumentBuisness();
                    status = new Status();
                    long userid = 11;
                    string path = Server.MapPath("~/Uploads/");
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }

                    postedFile.SaveAs(path + Path.GetFileName(postedFile.FileName));
                    documents.DocumentPath = "/Uploads/" + postedFile.FileName;
                    status = documentBuisness.AddUpdateDocument(documents, userid);
                    TempData["SuccessFlag"] = status.SuccessFlag;
                    TempData["Msg"] = status.Msg;
                    if (status.SuccessFlag == "1")
                    {
                        return RedirectToAction("List");
                    }
                    else
                    {
                        return View(documents);
                    }
                }
                
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to add document", ex, "Documents");
            }


            return View(documents);
        }

        public ActionResult List()
        {
            ViewBag.ParentMenu = "administrator";
            ViewBag.ChildMenu = "document";
            Documents dc = new Documents();
            return View(dc);
        }

        [HttpPost]
        public JsonResult LoadDocuments(int DocumentsId)
        {
            ViewBag.ParentMenu = "administrator";
            ViewBag.ChildMenu = "document";
            long userid = 11;
            List<Documents> documents = null;
            DocumentBuisness documentBuisness = null;
            try
            {
                documents = new List<Documents>();
                documentBuisness = new DocumentBuisness();
                documents = documentBuisness.GetDocuments(DocumentsId, userid).Where(i => i.IsDeleted == false).ToList();
            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to fetch documents list", ex, "LoadDocuments");
            }

            return Json(documents, JsonRequestBehavior.AllowGet);
        }


        public ActionResult Delete(int DocumentsId)
        {
            ViewBag.ParentMenu = "administrator";
            ViewBag.ChildMenu = "document";
            DocumentBuisness documentBuisness = null;
            Documents documents = null;
            Status status = null;
            long userid = 11;
            try
            {
                documentBuisness = new DocumentBuisness();
                documents = new Documents();
                status = new Status();
                documents = documentBuisness.GetDocuments(DocumentsId, userid).SingleOrDefault();
                 var fullPath = Server.MapPath(documents.DocumentPath);
                if (System.IO.File.Exists(fullPath))
                {
                    System.IO.File.Delete(fullPath);
                }
                status = documentBuisness.DeleteDocument(DocumentsId, userid);
                TempData["SuccessFlag"] = status.SuccessFlag;
                TempData["Msg"] = status.Msg;

            }
            catch (Exception ex)
            {
                AppLogger.Instance.Error(this.GetType(), "Unable to delete Item ", ex, "Delete");
            }
            return RedirectToAction("List");
        }


    }
}